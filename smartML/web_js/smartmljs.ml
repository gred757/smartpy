(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Js_of_ocaml

(* Callback Context *)
let browserContext = "smartpyContext"

(* Initiate global object (exports) *)
let () =
  Js.Unsafe.eval_string
    (Printf.sprintf "globalThis.exports = globalThis.exports || {}")

let exportToJs s f =
  Js.Unsafe.set (Js.Unsafe.variable "exports") s (Js.wrap_callback f)

let exportToJsString s f = exportToJs s (fun x -> Js.string (f x))

let setOutput output =
  Js.Unsafe.meth_call
    (Js.Unsafe.variable browserContext)
    "setOutput"
    [|Js.Unsafe.inject (Js.string output)|]

let addOutput output =
  Js.Unsafe.meth_call
    (Js.Unsafe.variable browserContext)
    "addOutput"
    [|Js.Unsafe.inject (Js.string output)|]

let setOutputToMethod method_name output =
  Js.Unsafe.meth_call
    (Js.Unsafe.variable browserContext)
    method_name
    [|Js.Unsafe.inject (Js.string output)|]

let getText id =
  Js.to_string
    (Js.Unsafe.eval_string
       (Printf.sprintf "document.getElementById('%s').value" id))

let setText id value =
  ignore
    (Js.Unsafe.eval_string
       (Printf.sprintf
          "document.getElementById('%s').innerHTML = \"%s\""
          id
          (String.escaped value)))

let setValue id value =
  ignore
    (Js.Unsafe.eval_string
       (Printf.sprintf
          "document.getElementById('%s').value = \"%s\""
          id
          (String.escaped value)))

let isChecked id =
  Js.to_bool
    (Js.Unsafe.eval_string
       (Printf.sprintf "document.getElementById('%s').checked" id))

let parseDate date =
  Js.to_string
    (Js.Unsafe.eval_string
       (Printf.sprintf "((new Date('%s')).getTime() / 1000).toString()" date))

let () =
  let ctx =
    { SmartJsInterface.exportToJs
    ; exportToJsString
    ; js_to_string = Js.to_string
    ; string_to_js = Js.string
    ; getText
    ; setText
    ; setValue
    ; setOutput
    ; addOutput
    ; setOutputToMethod
    ; isChecked
    ; parseDate }
  in
  SmartJsInterface.interface ctx
