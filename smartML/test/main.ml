(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open Utils

let config = Smartml.Config.default

let pp = print_endline

module Hashtbl_ = Hashtbl
open Base
open Smartml

let dbg f : unit =
  Fmt.(
    epr
      "%a@.@?"
      (fun ppf () ->
        box
          (fun ppf () ->
            string ppf "|DBG-test>";
            sp ppf ();
            f ppf)
          ppf
          ())
      ())

(*
   A first experiment of OCaml EDSL.

   - Very simple wrapping.
   - No types/GADTs.
   - Some very incomplete type inference.

   Cf, https://gitlab.com/SmartPy/smartpy-private/issues/19
*)
module EDSL_exp0 = struct
  open Basics
  open Typed

  module Ty = struct
    let bool = Type.bool

    let string = Type.string

    let unit = Type.unit

    let int = Type.int ()

    let record = Type.record

    let address = Type.address

    let token = Type.token
  end

  let get_type_or_try_to_infer infer_fun t v =
    match t with
    | Some s -> s
    | None -> infer_fun v

  let infer_from_expression storage_type = function
    | EMPrim1 (Not, _) -> Ty.bool
    | EPrim0 (ELocal "__storage__") -> storage_type
    | EPrim0 (ELocal "__operations__") -> Type.list Type.operation
    | ETransfer _ -> Type.operation
    | EPrim2 (ECons, _, l) -> l.et
    | other ->
        Fmt.kstrf
          failwith
          "expresssion: cannot infer type from expr: %a"
          (pp_expr_f pp_texpr pp_tcommand Type.pp (fun _ -> elim_typed))
          other

  let expression ?t storage_type e =
    { e
    ; et = get_type_or_try_to_infer (infer_from_expression storage_type) t e
    ; line_no = [] }

  let infer_from_value = function
    | Literal Unit -> Ty.unit
    | Literal (String _) -> Ty.string
    | Literal (Int _) -> Ty.int
    | Literal (Address _) -> Ty.address
    | Literal (Mutez _) -> Ty.token
    | other ->
        Fmt.kstrf
          failwith
          "value: cannot infer type from %a"
          (pp_value_f pp_value)
          other

  let value v = build_value v

  let big_int = Bigint.of_int

  module Val = struct
    let unit = Value.unit

    let string s = Value.string s

    let record ~t l =
      match Type.getRepr t with
      | TRecord {layout = _; row} ->
          let layout =
            Type.default_layout_of_row Config.Comb (List.map ~f:fst row)
          in
          value (Record (layout, l))
      | _ -> assert false

    let address s = Value.address s

    let int i = Value.int (big_int i)

    let token t = Value.mutez (big_int t)
  end

  module Var = struct
    let string v = (v, Ty.string)
  end

  let message channel params = {channel; params}

  let channel channel ?tparameter_ep body =
    { channel
    ; tparameter_ep
    ; originate = true
    ; lazify = None
    ; lazy_no_code = None
    ; line_no = []
    ; body }

  let command c ?(line = []) () = {line_no = line; c; ct = Type.unit}

  module Cmd = struct
    let make = command

    let seq ?(line = []) l = Command.seq ~line_no:line l

    let set_var var e = Command.set ~line_no:[] (Expr.local ~line_no:[] var) e

    let transfer ~destination ~amount:amount_ () =
      let op f = f ~line_no:[] in
      let open Expr in
      let ops = op operations in
      op
        Command.set
        ops
        (op
           cons
           (op transfer ~arg:(op cst Literal.unit) ~amount:amount_ ~destination)
           ops)
  end

  let contract
      ?(unknown_parts = None)
      ?(balance = 0)
      ?(storage = Expr.unit)
      ?(flags = [])
      entry_points =
    { contract =
        { template_id = None
        ; balance =
            Some (Expr.cst ~line_no:[] (Literal.mutez (big_int balance)))
        ; storage = Some storage
        ; baker = None
        ; tstorage = Type.full_unknown ()
        ; entry_points
        ; entry_points_layout = None
        ; unknown_parts
        ; flags
        ; global_variables = []
        ; metadata = []
        ; views = []
        ; derived = U } }

  module Example = struct
    module Send_channel = struct
      let params_type =
        Ty.(
          record
            (Unknown.unknown ())
            [("address", address); ("amount", Type.token)])

      let channel =
        let params_field name =
          Expr.attr ~line_no:[] (Expr.params ~line_no:[]) name
        in
        let destination =
          params_field "address"
          |> Expr.contract ~line_no:[] None (Type.full_unknown ())
          |> fun x -> Expr.openVariant ~line_no:[] "Some" x None
        in
        channel
          "send"
          ~tparameter_ep:params_type
          (Cmd.seq
             [Cmd.transfer ~destination ~amount:(params_field "amount") ()])
    end
  end
end

(** Additional constructs for {!Alcotest}. *)
module Testing_helpers = struct
  (** Alcotest.testable for {!Basics.Execution.error}. *)
  let testable_exec_error =
    Alcotest.of_pp
      (Fmt.option ~none:Fmt.(const string "No-error") Basics.Execution.pp_error)

  (** Alcotest.testable for {!Basics.toperation}. *)
  let testable_toperation = Alcotest.of_pp Basics.pp_operation

  (** [does_raise "interesting thing to say"
        ~p:predicate_on_exn ~f:function_supposed_to_raise]. *)
  let does_raise : string -> p:(exn -> bool) -> f:(unit -> unit) -> unit =
   fun msg ~p ~f ->
    try
      f ();
      Alcotest.failf "%s: did not raise an exception" msg
    with
    | e when p e -> ()
    | other ->
        Alcotest.failf
          "%s: did not raise the right exception: %a"
          msg
          Exn.pp
          other

  (** Match any exception in the [~p] argument of {!does_raise}. *)
  let any_failure = function
    | Failure _ | Basics.SmartExcept _ -> true
    | _ -> false

  (** Do not use this directly. *)
  exception Found

  (** [check_find m f] calls f with a function [found : unit -> 'a]
      that has to be called for the test to be considered successful. *)
  let check_find msg f =
    Alcotest.check_raises msg Found (fun () -> f (fun () -> raise Found))
end

(** Tests for {!Smartml.Contract.eval}. *)
module Contract_eval_test = struct
  let do_test_of_contract_eval
      ?context ~scenario_state ~env (contract : Smartml.Basics.contract) message
      =
    let primitives = (module Smartml_ml.Primitives : Primitives.Primitives) in
    let ctx =
      Option.value
        context
        ~default:
          (Interpreter.context
             ~sender:"tz1sender"
             ~time:(Bigint.of_int 42)
             ~amount:(Bigint.of_int 10)
             ~level:Bigint.zero_big_int
             ~voting_powers:[("tz1sender", Bigint.of_int 10)]
             ~line_no:[("", 10)]
             ()
             ~debug:false)
    in
    let contract = Checker.check_contract Config.default contract in
    let contract =
      Interpreter.interpret_contract
        ~config
        ~primitives
        ~typing_env:env
        ~scenario_state
        contract
    in
    let contract_cmd_option, operations, error_opt, _ =
      Interpreter.interpret_message
        ~config
        ~primitives
        ~scenario_state
        ~env
        ctx
        contract
        message
    in
    let module Printer = (val Printer.get config : Printer.Printer) in
    ( match error_opt with
    | None ->
        pp (Printer.type_to_string (Basics.type_of_value message.params));
        ( try Solver.apply ~config env with
        | Basics.SmartExcept l -> failwith (Printer.pp_smart_except false l) )
    | Some _ -> () );
    dbg
      Fmt.(
        fun ppf ->
          vbox
            ~indent:2
            (fun ppf () ->
              string ppf "Testing Contract.eval:";
              sp ppf ();
              pf
                ppf
                "Contract-commands:@, %a"
                (box (option Basics.pp_instance))
                contract_cmd_option;
              cut ppf ();
              pf
                ppf
                "Operations: %d:@, %a."
                (List.length operations)
                (list Basics.pp_operation)
                operations;
              cut ppf ();
              pf
                ppf
                "Error: %a."
                (option ~none:(const string "none") Basics.Execution.pp_error)
                error_opt)
            ppf
            ());
    (contract_cmd_option, operations, error_opt)

  let do_test_of_contract_eval_ignore
      ?context ~scenario_state ~env contract message =
    let _, _, _ =
      do_test_of_contract_eval ?context ~scenario_state ~env contract message
    in
    ()

  let run () =
    let open EDSL_exp0 in
    let open Testing_helpers in
    let () =
      let scenario_state = Smartml.Basics.scenario_state in
      let env = Typing.init_env () in
      (* Completely empty contract called on an inexistent entry-point *)
      let _, _, error_opt =
        do_test_of_contract_eval
          ~scenario_state
          ~env
          (contract [])
          (message "hello_no_entrypoint" Val.unit)
      in
      Alcotest.check
        testable_exec_error
        "entry-point not found"
        (Some
           (Basics.Execution.Exec_failure
              (Value.string "Channel not found: hello_no_entrypoint", [])))
        error_opt
    in
    let hello_channel_sets_storage =
      channel
        "hello"
        ~tparameter_ep:Ty.string
        (Cmd.seq [Cmd.set_var "__storage__" (Expr.params ~line_no:[])])
    in
    Alcotest.check_raises
      "Duplicate channels"
      (Failure "Too many channels")
      (fun () ->
        let scenario_state = Smartml.Basics.scenario_state in
        let env = Typing.init_env () in
        let contract =
          contract
            ~balance:10
            ~storage:(Expr.cst ~line_no:[] (Literal.string "world"))
            [hello_channel_sets_storage; hello_channel_sets_storage]
        in
        let message = message "hello" (Val.string "foo") in
        do_test_of_contract_eval_ignore ~scenario_state ~env contract message);
    does_raise "wrong parameter type" ~p:any_failure ~f:(fun () ->
        let scenario_state = Smartml.Basics.scenario_state in
        let env = Typing.init_env () in
        do_test_of_contract_eval_ignore
          ~scenario_state
          ~env
          (contract
             ~balance:10
             ~storage:(Expr.cst ~line_no:[] (Literal.string "world"))
             [hello_channel_sets_storage])
          (message "hello" Val.unit));
    let () =
      let scenario_state = Smartml.Basics.scenario_state in
      let env = Typing.init_env () in
      let new_value = Val.(string "foo") in
      let new_contract, operations, error_opt =
        do_test_of_contract_eval
          ~scenario_state
          ~env
          (contract
             ~balance:10
             ~storage:(Expr.cst ~line_no:[] (Literal.string "world"))
             [hello_channel_sets_storage])
          (message "hello" new_value)
      in
      Alcotest.check testable_exec_error "no error" None error_opt;
      Alcotest.(check (list testable_toperation)) "no operation" [] operations;
      check_find "storage got changed" (fun found ->
          match new_contract with
          | None -> failwith "not here"
          | Some {template = _; state = {storage = Some storage; _}} ->
              if Poly.equal storage new_value then found () else ()
          | Some {template = _; state = {storage = None; _}} ->
              failwith "missing storage in contract")
    in
    let () =
      let scenario_state = Smartml.Basics.scenario_state in
      let env = Typing.init_env () in
      let new_value =
        Val.(
          record
            ~t:Example.Send_channel.params_type
            [("address", address "KT1blabla"); ("amount", token 42)])
      in
      let _new_contract, operations, error_opt =
        try
          Solver.apply ~config env;
          let c =
            contract
              ~balance:10
              ~storage:(Expr.cst ~line_no:[] (Literal.string "world"))
              [Example.Send_channel.channel; hello_channel_sets_storage]
          in
          Solver.apply ~config env;
          let m = message "send" new_value in
          Solver.apply ~config env;
          do_test_of_contract_eval ~scenario_state ~env c m
        with
        | exn ->
            let module Printer = (val Printer.get config : Printer.Printer) in
            failwith (Printer.exception_to_string false exn)
      in
      Alcotest.check testable_exec_error "no error" None error_opt;
      check_find "one operation" (fun found ->
          List.iter operations ~f:(function
              | Transfer {params = _; destination = _; amount; _}
                when Big_int.eq_big_int amount (big_int 42) ->
                  (* TODO: also check left side *)
                  found ()
              | _ -> ()))
    in
    (* Alcotest.(check bool) "JUST MAKING TEST FAIL" true false ; *)
    ()
end

(** Tests for {!Michelson_compiler.michelson_contract}. *)
module Michelson_compiler_test = struct
  let run () =
    let open EDSL_exp0 in
    let open Testing_helpers in
    let run_compiler msg contract =
      let res =
        Result.try_with (fun () ->
            Compiler.compile_instance ~config:Config.default contract)
      in
      dbg
        Fmt.(
          fun ppf ->
            vbox
              (fun ppf () ->
                pf ppf "Test: %s" msg;
                cut ppf ();
                box
                  ~indent:2
                  (fun ppf () ->
                    pf ppf "# Compiler on:";
                    sp ppf ();
                    Basics.pp_instance ppf contract)
                  ppf
                  ();
                cut ppf ();
                box
                  ~indent:2
                  (fun ppf () ->
                    pf ppf "# Result:";
                    sp ppf ();
                    match res with
                    | Ok o -> pf ppf "OK:@ %a" Michelson.pp_tcontract o
                    | Error e -> pf ppf "ERROR:@ %a" Exn.pp e)
                  ppf
                  ())
              ppf
              ());
      res
    in
    let res =
      let c =
        contract
          ~storage:(Expr.cst ~line_no:[] (Literal.string "Sme-string"))
          []
      in
      let c = Smartml.Checker.check_contract Config.default c in
      let primitives = (module Smartml_ml.Primitives : Primitives.Primitives) in
      let scenario_state = Smartml.Basics.scenario_state in
      let c =
        Interpreter.interpret_contract
          ~config
          ~primitives
          ~typing_env:(Typing.init_env ())
          ~scenario_state
          c
      in
      run_compiler "first test" c
    in
    check_find "empty contract" (fun found ->
        match res with
        | Ok
            { tstorage = {mt = MT0 T_string; _}
            ; tparameter = {mt = MT0 T_unit; _}, None
            ; code = _
            ; lazy_entry_points = _
            ; storage = _ } ->
            found ()
        | _ -> ())
end

module Command_dot_fix = struct
  let run () =
    let open Testing_helpers in
    let open EDSL_exp0 in
    let env = Typing.init_env () in
    let homogenize_command c =
      Smartml.Basics.Untyped.{(c : Basics.Untyped.command) with line_no = []}
    in
    let try_fix cmd =
      try
        Solver.apply ~config env;
        let cmd = Checker.check_command config [] cmd in
        Solver.apply ~config env;
        Smartml.Closer.close_command ~config cmd
        |> Smartml.Basics.erase_types_command
        |> homogenize_command
      with
      | exn ->
          let module Printer = (val Printer.get config : Printer.Printer) in
          failwith (Printer.exception_to_string false exn)
    in
    does_raise "Duplicate local variables" ~p:any_failure ~f:(fun () ->
        let cmd =
          try_fix
            (Cmd.seq
               [ Command.defineLocal ~line_no:[] "aa" Expr.now
               ; Command.defineLocal ~line_no:[] "aa" Expr.now ])
        in
        dbg
          Fmt.(
            fun ppf ->
              pf ppf "command result %a" Smartml.Basics.Untyped.pp_command cmd));
    let testable_command =
      Alcotest.testable Basics.Untyped.pp_command Basics.Untyped.equal_command
    in
    let while_loop ~ok ~in_seq =
      let while_thing =
        if ok
        then
          Command.whileLoop
            ~line_no:[("", 1)]
            (Expr.cst ~line_no:[] (Literal.bool false))
            (Command.seq ~line_no:[] [])
        else
          Command.whileLoop
            ~line_no:[("", 1)]
            (Expr.cst ~line_no:[] (Literal.small_nat 2))
            (Command.seq ~line_no:[] [])
      in
      if in_seq then Cmd.(seq [while_thing]) else while_thing
    in
    Alcotest.check
      testable_command
      "OK while loop"
      (try_fix (while_loop ~ok:true ~in_seq:true))
      (while_loop ~ok:true ~in_seq:false |> homogenize_command);
    does_raise "Wrong while" ~p:any_failure ~f:(fun () ->
        try_fix (while_loop ~ok:false ~in_seq:true) |> ignore);
    ()

  let alcotest_test =
    ("command-dot-fix", [Alcotest.test_case "Command.fix" `Quick run])
end

module Interpreter_on_expressions = struct
  let test_interpret_expr_early () =
    let do_check name expr expected =
      let testable_value = Alcotest.testable Value.pp Value.equal in
      let typing_env = Typing.init_env () in
      let expr = Smartml.Checker.check_expr config [] expr in
      Smartml.Solver.apply ~config typing_env;
      Alcotest.(check testable_value)
        (Fmt.str "interpret_expr_early:%s" name)
        (Interpreter.interpret_expr_external
           ~config
           ~primitives:(module Smartml_ml.Primitives : Primitives.Primitives)
           ~typing_env
           ~no_env:[`Text "Computing expression"; `Expr expr; `Line expr.line_no]
           ~scenario_state:Smartml.Basics.scenario_state
           expr)
        expected
    in
    do_check
      "zero"
      Expr.(cst ~line_no:[] (Literal.bool false))
      Value.(bool false);
    let of_which which =
      match which with
      | `SB_string -> ("String", Literal.string, Value.string, Type.string)
      | `SB_bytes -> ("Bytes", Literal.bytes, Value.bytes, Type.bytes)
    in
    let check_slice ~which ~ofs ~len s =
      let ks, lit, value, t = of_which which in
      let res =
        match String.sub s ~pos:ofs ~len with
        | s -> Value.some (value s)
        | exception _ -> Value.none t
      in
      do_check
        (Fmt.str "slice%s:%S:%d--%d" ks s ofs len)
        Expr.(
          slice
            ~line_no:[]
            ~offset:(cst ~line_no:[] @@ Literal.small_nat ofs)
            ~length:(cst ~line_no:[] @@ Literal.small_nat len)
            ~buffer:(cst ~line_no:[] @@ lit s))
        res
    in
    let both f =
      f ~which:`SB_string;
      f ~which:`SB_bytes
    in
    both @@ check_slice ~ofs:0 ~len:0 "";
    both @@ check_slice "" ~ofs:0 ~len:0;
    both @@ check_slice "a" ~ofs:0 ~len:1;
    both @@ check_slice "a" ~ofs:1 ~len:0;
    both @@ check_slice "abcdef" ~ofs:1 ~len:3;
    both @@ check_slice "abcdef" ~ofs:1 ~len:5;
    both @@ check_slice "abcdef" ~ofs:0 ~len:50;
    let check_concat ~which l =
      let ks, lit, value, _t = of_which which in
      let res = String.concat ~sep:"" l in
      let t =
        match which with
        | `SB_string -> Type.string
        | `SB_bytes -> Type.bytes
      in
      let e =
        Expr.(
          concat_list
            ~line_no:[]
            (type_annotation
               ~line_no:[]
               (build_list
                  ~line_no:[]
                  ~elems:(List.map l ~f:(fun s -> cst ~line_no:[] @@ lit s)))
               (Type.list t)))
      in
      let env = Typing.init_env () in
      let _ = Smartml.Checker.check_expr config [] e in
      Solver.apply ~config env;
      try do_check (Fmt.str "concat%s:%S" ks res) e (value res) with
      | exn ->
          let module Printer = (val Printer.get config : Printer.Printer) in
          failwith (Printer.exception_to_string false exn)
    in
    both @@ check_concat [];
    both @@ check_concat ["a"];
    both @@ check_concat ["a"; "b"];
    both @@ check_concat (List.init 42 ~f:(Fmt.str "%d"));
    let check_size ~which s =
      let ks, lit, _, _ = of_which which in
      let res = String.length s in
      do_check
        (Fmt.str "size%s:%S" ks s)
        Expr.(size ~line_no:[] (cst ~line_no:[] @@ lit s))
        (Value.nat (Bigint.of_int res))
    in
    both @@ check_size "";
    both @@ check_size "a";
    both @@ check_size "aaaaaaaaaa";

    (* Alcotest.(check bool) "JUST MAKING TEST FAIL" true false ; *)
    ()

  let alcotest_test =
    ( "interpreter-on-expressions"
    , [ Alcotest.test_case
          "interpret_expr_early"
          `Quick
          test_interpret_expr_early ] )
end

module Data_encoding_tests = struct
  let micheline_example =
    "{\"prim\":\"Pair\",\"args\":[{\"int\":\"42\"},{\"int\":\"42\"}]}"

  let micheline_encoded = "0707002a002a"

  let bytes_to_michelson () =
    let result =
      match Micheline_encoding.parse_hex_bytes micheline_encoded with
      | Ok (json, _) -> Ezjsonm.value_to_string ~minify:true json
      | _ -> "FAILED"
    in
    Alcotest.(check string) "Verify micheline output" micheline_example result;
    ()

  let michelson_to_bytes () =
    let (`Hex result) =
      micheline_example
      |> Ezjsonm.value_from_string
      |> Micheline_encoding.micheline_of_ezjsonm
      |> Micheline_encoding.pack_node_expression
      |> Hex.of_string
    in
    Alcotest.(check string) "Verify bytes output" micheline_encoded result;
    ()

  let michelson_to_bytes_f name s1 s2 =
    Alcotest.test_case name `Quick (fun () ->
        let (`Hex result) =
          s1
          |> Ezjsonm.value_from_string
          |> Micheline_encoding.micheline_of_ezjsonm
          |> Micheline_encoding.pack_node_expression
          |> Hex.of_string
        in
        Alcotest.(check string) "Verify bytes output" s2 result;
        ())

  let instruction_to_michelson_json_f name s1 s2 =
    Alcotest.test_case name `Quick (fun () ->
        let result = Basics.show_micheline (Micheline_encoding.parse_node s1) in
        Alcotest.(check string) "Verify bytes output" s2 result;
        ())

  let alcotest_test =
    ( "Data_encoding"
    , [ Alcotest.test_case "bytes_to_michelson" `Quick bytes_to_michelson
      ; Alcotest.test_case "michelson_to_bytes" `Quick michelson_to_bytes
      ; instruction_to_michelson_json_f
          "instruction_to_michelson_json (ADD)"
          {|
          parameter int;
          storage   int;
          code
              {
              UNPAIR;
              ADD;
              NIL operation;
              PAIR;
              };
          |}
          {|(Sequence
   [Primitive {name = "parameter"; annotations = [];
      arguments =
      [Primitive {name = "int"; annotations = []; arguments = []}]};
     Primitive {name = "storage"; annotations = [];
       arguments =
       [Primitive {name = "int"; annotations = []; arguments = []}]};
     Primitive {name = "code"; annotations = [];
       arguments =
       [(Sequence
           [Primitive {name = "UNPAIR"; annotations = []; arguments = []};
             Primitive {name = "ADD"; annotations = []; arguments = []};
             Primitive {name = "NIL"; annotations = [];
               arguments =
               [Primitive {name = "operation"; annotations = [];
                  arguments = []}
                 ]};
             Primitive {name = "PAIR"; annotations = []; arguments = []}])
         ]}
     ])|}
      ; michelson_to_bytes_f "0xabcd" "{\"bytes\":\"abcd\"}" "0a00000002abcd"
      ; michelson_to_bytes_f "abcd" "{\"string\":\"abcd\"}" "010000000461626364"
      ; michelson_to_bytes_f "123456" "{\"int\":\"123456\"}" "0080890f" ] )
end

module Crypto = struct
  let sha256 s = Digestif.SHA256.(to_raw_string (digest_string s))
end

module Base58_tests = struct
  (*
   *  left ) public key hash
   *  right) Base58 encoding
   *)
  let tz1 =
    ( "0002298c03ed7d454a101eb7022bc95f7e5f41ac78"
    , "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx" )

  let tz2 =
    ( "014b7c404fd4fbcf931cde0a8971caf76f53c8e5c0"
    , "tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93" )

  let tz3 =
    ( "0248480adaec07ecafbe1de0b2bb68688238740184"
    , "tz3SvEa4tSowHC5iQ8Aw6DVKAAGqBPdyK1MH" )

  let kt1 =
    ( "0138560805b4c8d7b7fbbafad5c59dbfa3878ca70500"
    , "KT1DieU51jzXLerQx5AqMCiLC1SsCeM8yRat" )

  let kt1_with_entrypoint =
    ( "0138560805b4c8d7b7fbbafad5c59dbfa3878ca70500666f6f"
    , "KT1DieU51jzXLerQx5AqMCiLC1SsCeM8yRat%foo" )

  let fake_kt1 =
    ( "01d1371b91fdbd07c8855659c8461e12ebe2d81f2e00"
    , "KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU"
    , 0
    , None )

  let fake_kt1_with_entrypoint =
    ( "01d1371b91fdbd07c8855659c84652230be0eaecd500666f6f5f31"
    , "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%foo_1"
    , 0
    , Some "foo_1" )

  let fake_kt1_by_index =
    [ "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"
    ; "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"
    ; "KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H"
    ; "KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC"
    ; "KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3"
    ; "KT1Tezooo5zzSmartPyzzSTATiCzzzz48Z4p"
    ; "KT1Tezooo6zzSmartPyzzSTATiCzzztY1196"
    ; "KT1Tezooo7zzSmartPyzzSTATiCzzzvTbG1z"
    ; "KT1Tezooo8zzSmartPyzzSTATiCzzzzp29d1"
    ; "KT1Tezooo9zzSmartPyzzSTATiCzzztdBMLX" ]

  let edpk =
    ( "008742b0cf6be543a183bdd1069f9491bd144a9e94a418accd349312b6e5da133b"
    , "edpkufnz668CdgnVC4X5KRvJoXCMgicr29Tjmr5vxGzvMdipwkTZmM" )

  let generic_signature =
    ( "8df7020dc9082744fb5e04691cfd3bbf1928e782e7dde7c46ea01fe400b23bac6ff1da09bbaf750fb04409220e135b8605daad8c1e73d28765a6ba0b873efa00"
    , "siggZXnjqYnFMjMxfE1avK2PZdRmRekp5fr56F5uJcuQkfHPL23HNDdtz2iG1QeYtU8DGEniWXjqDh1RxGx6scVgMaK74CrF"
    )

  let ed25519_signature =
    ( "4d6738931b59605ca0449b7b76a01d210ace9220051821ecf43382a7024ed99063c5bedb85c81b919d33a213c6d8fb47f2c9b4deaf6f68f56dee038ea739740f"
    , "edsigthw6sSCfcZbjKCqGE9CZ9PoTsW1Kh5cgGu5SSU1AzcKyJ37oubeKVnDfY291minBiui7khzr8pzhoFVtF9ULs3hnUVKXGx"
    )

  let secp256k1_signature =
    ( "865b0ceaa2decd64fea1b7e0e97d6ae8035125d821ebe99d7d5c8a559f36fdd43388a570e475011f6d2bbef184ece9bafbf0ac12157f7257988e9575ae2cbca9"
    , "spsig1PPUFZucuAQybs5wsqsNQ68QNgFaBnVKMFaoZZfi1BtNnuCAWnmL9wVy5HfHkR6AeodjVGxpBVVSYcJKyMURn6K1yknYLm"
    )

  let p256_signature =
    ( "e7b25662eb9bb1e92ef7cb20c2fcaca120eccbe2d6af7511c2e510eb586ca8612d9dfe26bc7882a33b29b8c9706a04124f98dd281f1622d8da50db7a5cd4ab43"
    , "p2sigsceCzcDw2AeYDzUonj4JT341WC9Px4wdhHBxbZcG1FhfqFVuG7f2fGCzrEHSAZgrsrQWpxduDPk9qZRgrpzwJnSHC3gZJ"
    )

  let sg1 =
    ( "dd577f8a5cd09a4a1e62241676ffaf4bc0c5b87f"
    , "SG1jfZeHRzeWAM1T4zrwunEyUpwWc82D4tbv" )

  let decode ~f tuple =
    let decoded, encoded = tuple in
    let result = f encoded in
    Alcotest.(check string) "Check decoding result" decoded result;
    ()

  let encode ~f tuple =
    let decoded, encoded = tuple in
    let result = f decoded in
    Alcotest.(check string) "Check encoding result" encoded result;
    ()

  let fake_address_of_contract_id ~static tuple =
    let decoded, encoded, contract_id, entry_point = tuple in
    let fake_address =
      Utils.Bs58.address_of_contract_id ~static contract_id entry_point
    in
    Alcotest.(check string) "Check encoded" encoded fake_address;
    (decoded, fake_address)

  let test_fake_addresses_encoding_by_index addresses =
    for i = 0 to List.length addresses - 1 do
      let fake_address =
        Utils.Bs58.address_of_contract_id ~static:true i None
      in
      Alcotest.(check string)
        "Check encoded"
        (List.nth_exn addresses i)
        fake_address
    done

  let decoding_tests =
    ( "Base58_decoding"
    , [ Alcotest.test_case "decode tz1" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_key_hash tz1)
      ; Alcotest.test_case "decode tz2" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_key_hash tz2)
      ; Alcotest.test_case "decode tz3" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_key_hash tz3)
      ; Alcotest.test_case "decode KT1" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_address kt1)
      ; Alcotest.test_case "decode KT1 with entrypoint" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_address kt1_with_entrypoint)
      ; Alcotest.test_case "decode fake KT1" `Quick (fun () ->
            decode
              ~f:Utils.Bs58.decode_address
              (fake_address_of_contract_id ~static:false fake_kt1))
      ; Alcotest.test_case "decode fake KT1 with entrypoint" `Quick (fun () ->
            decode
              ~f:Utils.Bs58.decode_address
              (fake_address_of_contract_id
                 ~static:true
                 fake_kt1_with_entrypoint))
      ; Alcotest.test_case "decode ed25519 key" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_key edpk)
      ; Alcotest.test_case "decode generic signature" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_signature generic_signature)
      ; Alcotest.test_case "decode ed25519 signature" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_signature ed25519_signature)
      ; Alcotest.test_case "decode secp256k1 signature" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_signature secp256k1_signature)
      ; Alcotest.test_case "decode p256 signature" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_signature p256_signature)
      ; Alcotest.test_case "decode sg1" `Quick (fun () ->
            decode ~f:Utils.Bs58.decode_baker_hash sg1) ] )

  let encoding_tests =
    ( "Base58_encoding"
    , [ Alcotest.test_case "encode tz1" `Quick (fun () ->
            encode ~f:Utils.Bs58.encode_key_hash tz1)
      ; Alcotest.test_case "encode tz2" `Quick (fun () ->
            encode ~f:Utils.Bs58.encode_key_hash tz2)
      ; Alcotest.test_case "encode tz3" `Quick (fun () ->
            encode ~f:Utils.Bs58.encode_key_hash tz3)
      ; Alcotest.test_case "encode KT1" `Quick (fun () ->
            encode ~f:Utils.Bs58.encode_address kt1)
      ; Alcotest.test_case "encode KT1 with entrypoint" `Quick (fun () ->
            encode ~f:Utils.Bs58.encode_address kt1_with_entrypoint)
      ; Alcotest.test_case "encode ed25519 key" `Quick (fun () ->
            encode ~f:Utils.Bs58.encode_key edpk)
      ; Alcotest.test_case "encode generic signature" `Quick (fun () ->
            encode ~f:Utils.Bs58.encode_signature generic_signature)
      ; Alcotest.test_case
          "test fake addresses encoding byindex"
          `Quick
          (fun () -> test_fake_addresses_encoding_by_index fake_kt1_by_index)
      ; Alcotest.test_case "encode sg1" `Quick (fun () ->
            encode ~f:Utils.Bs58.encode_baker_hash sg1) ] )
end

let () =
  Misc.Dbg.on := true;
  Alcotest.run
    ~argv:Sys.(get_argv ())
    "smartml-library"
    [ ( "contract-eval"
      , [Alcotest.test_case "Contract.eval" `Quick Contract_eval_test.run] )
    ; ( "michompilation"
      , [ Alcotest.test_case
            "Michelson_compiler.michelson_contract"
            `Quick
            Michelson_compiler_test.run ] )
    ; Command_dot_fix.alcotest_test
    ; Interpreter_on_expressions.alcotest_test
    ; Data_encoding_tests.alcotest_test
    ; Base58_tests.decoding_tests
    ; Base58_tests.encoding_tests
      (* ; ( "fake-primitive-implementations"
       *   , [ Alcotest.test_case
       *         "fake-primitive-implementations-self-test"
       *         `Quick
       *         (fun () ->
       *           List.iter
       *             ~f:(function
       *               | (Ok s | Error s) as res ->
       *                   Alcotest.(check bool)
       *                     (Fmt.str "fake-prim: %s" s)
       *                     ( match res with
       *                     | Ok _ -> true
       *                     | _ -> false )
       *                     true)
       *             (Primitives.test_primitives
       *                (module Smartml_ml.Primitives : Primitives.Primitives))) ] ) *)
    ]
