(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open SmartML
open Utils

module Main (C : Cmd.S) (P : Primitives.Primitives) : sig
  val main : unit
end
