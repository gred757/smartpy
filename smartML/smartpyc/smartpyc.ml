(* Copyright 2019-2021 Smart Chain Arena LLC. *)

open SmartML
open Tools
open Utils

module Main (C : Cmd.S) (P : Primitives.Primitives) = struct
  module Tezos = Smartml_unix.Run_tezos_scenario.Make (P)

  type config =
    { script : string option
    ; kind : Basics.scenario_kind
    ; accept_empty : bool
    ; output : string option
    ; config : Config.t
    ; sandbox : int option
    ; mockup : string option
    ; purge : bool
    ; html : bool
    ; install : string
    ; misc : string list option
    ; script_args : string list }

  let config0 =
    { script = None
    ; kind = {kind = ""}
    ; accept_empty = false
    ; output = None
    ; config = Config.default
    ; sandbox = None
    ; mockup = None
    ; purge = false
    ; html = false
    ; install = ""
    ; misc = None
    ; script_args = [] }

  let usage () =
    print_endline
      {|
Usage: smartpyc <options> <source file>

The <source file> is a python file containing a class that inherits
from sp.Contract.

OPTIONS

 -o | --output
    Specify a different output file (default: contract.smlse).

 -s | --sandbox <port>
    Run scenarios running on a sandbox on localhost at <port>.

 -h | --help
    Print help.

EXAMPLE

smartpyc --kind test calculator.py --output calculator.smlse
|}

  let script_extensions = [".py"; ".json"; ".ts"; ".ml"]

  let rec parse c = function
    | [] -> c
    | p :: rest
      when List.mem (Filename.extension p) script_extensions && c.script = None
      ->
        parse {c with script = Some p} rest
    | "--kind" :: kind :: rest -> parse {c with kind = {kind}} rest
    | "--accept_empty" :: rest -> parse {c with accept_empty = true} rest
    | ("-o" | "--output") :: o :: rest -> parse {c with output = Some o} rest
    | ("-s" | "--sandbox") :: p :: rest ->
        parse {c with sandbox = Some (int_of_string p)} rest
    | ("-m" | "--mockup") :: m :: rest -> parse {c with mockup = Some m} rest
    | ("-p" | "--purge") :: rest -> parse {c with purge = true} rest
    | "--html" :: rest -> parse {c with html = true} rest
    | "--install" :: f :: rest -> parse {c with install = f} rest
    | ("-h" | "--help") :: _ ->
        usage ();
        exit 0
    | "--" :: rest -> {c with script_args = rest}
    | "--misc" :: rest -> {c with misc = Some rest}
    | x :: xs ->
        let err () =
          failwith
            (Printf.sprintf "Don't know what to do with %s. %S" x c.kind.kind)
        in
        if String.sub x 0 2 = "--"
        then
          let x = String.(sub x 2 (length x - 2)) in
          match Config.parse_flag [x] with
          | Some flag ->
              parse {c with config = Config.apply_flag c.config flag} xs
          | None ->
            ( match xs with
            | [] -> err ()
            | arg :: rest ->
              ( match Config.parse_flag [x; arg] with
              | None -> err ()
              | Some flag ->
                  parse {c with config = Config.apply_flag c.config flag} rest
              ) )
        else err ()

  let mkdir_p x =
    let r = C.run "mkdir" ["-p"; x] in
    if r <> 0 then failwith (Printf.sprintf "mkdir failed: %d" r)

  let rm_rf x =
    let r = C.run "rm" ["-rf"; x] in
    if r <> 0 then failwith (Printf.sprintf "rm failed: %d" r)

  let run_smartpyc_py args =
    let path = Option.default "" (Sys.getenv_opt "PATH") in
    let dir = Filename.dirname Sys.argv.(0) in
    let pythonpath =
      match Sys.getenv_opt "PYTHONPATH" with
      | None -> dir
      | Some pythonpath -> String.concat ":" [dir; pythonpath]
    in
    let env = [("PATH", path); ("PYTHONPATH", pythonpath)] in
    let r = C.run ~env "python3" (Filename.concat dir "smartpyc.py" :: args) in
    match r with
    | 0 -> ()
    | 1 -> exit 1
    | r -> failwith (Printf.sprintf "unexpected exit status from python: %d" r)

  let write_targets_py out_scenario_sc in_py in_pure_py script_args =
    run_smartpyc_py
      (["write_tests"; out_scenario_sc; in_py; in_pure_py] @ script_args)

  let run_smarttscli args =
    let path = Option.default "" (Sys.getenv_opt "PATH") in
    let dir = Filename.dirname Sys.argv.(0) in
    let pythonpath =
      match Sys.getenv_opt "PYTHONPATH" with
      | None -> dir
      | Some pythonpath -> String.concat ":" [dir; pythonpath]
    in
    let env = [("PATH", path); ("PYTHONPATH", pythonpath)] in
    let r = C.run ~env "node" (Filename.concat dir "smart-ts-cli.js" :: args) in
    match r with
    | 0 -> ()
    | 1 -> exit 1
    | r -> failwith (Printf.sprintf "unexpected exit status from python: %d" r)

  let write_targets_ts out_scenario_sc in_ts out_dir script_args =
    run_smarttscli
      ( ["scenario"; out_scenario_sc; "--file"; in_ts; "--outDir"; out_dir]
      @ script_args )

  let run_smartmlcli args =
    let path = Option.default "" (Sys.getenv_opt "PATH") in
    let dir = Filename.dirname Sys.argv.(0) in
    let env = [("PATH", path)] in
    let r = C.run ~env (Filename.concat dir "smarttop") args in
    match r with
    | 0 -> ()
    | 1 -> exit 1
    | r ->
        failwith (Printf.sprintf "unexpected exit status from smarttop: %d" r)

  let write_targets_ml in_ml script_args = run_smartmlcli ([in_ml] @ script_args)

  let run_scenarios
      ~kind ~accept_empty ~config ~html ~install fn_scenario out_dir =
    let scenarios =
      Yojson.Basic.Util.to_list (Yojson.Basic.from_file fn_scenario)
    in
    let read_scenario scenario =
      let primitives = (module P : Primitives.Primitives) in
      let scenario = Scenario.load_from_string ~primitives config scenario in
      if kind <> scenario.scenario.kind
      then None
      else
        let out_dir = Filename.concat out_dir scenario.scenario.shortname in
        Some (primitives, scenario, out_dir)
    in
    let scenarios = List.map_some read_scenario scenarios in
    let scenarios =
      match scenarios with
      | [] when not accept_empty ->
          raise
            (Basics.SmartExcept
               [ `Text (Printf.sprintf "Target %S not found" kind.kind)
               ; `Text "in"
               ; `Text fn_scenario ])
      | scenarios -> scenarios
    in
    let run_scenario (primitives, scenario, out_dir) =
      mkdir_p out_dir;
      Smartml_scenario.run ~primitives ~html ~install ~scenario (Some out_dir)
    in
    List.concat (List.map run_scenario scenarios)

  let run_targets
      ~kind
      ~accept_empty
      ~config
      ~sandbox
      ~mockup
      ~html
      ~install
      out_dir
      script
      script_args =
    mkdir_p out_dir;
    let fn_scenario = Filename.concat out_dir "scenario.json" in
    let errors =
      try
        begin
          match Filename.extension script with
          | ".py" ->
              let fn_pure_py = Filename.concat out_dir "script_pure.py" in
              Io.write_file
                (Filename.concat out_dir "script_init.py")
                (Io.read_file script);
              write_targets_py fn_scenario script fn_pure_py script_args
          | ".json" ->
              Io.write_file
                (Filename.concat out_dir "scenario.json")
                (Io.read_file script)
          | ".ts" ->
              Io.write_file
                (Filename.concat out_dir "script_init.ts")
                (Io.read_file script);
              write_targets_ts fn_scenario script out_dir script_args
          | ".ml" ->
              Io.write_file
                (Filename.concat out_dir "script_init.ml")
                (Io.read_file script);
              if true
              then (
                let top = Filename.concat out_dir "script_init_top.ml" in
                Io.write_file
                  top
                  (Printf.sprintf
                     "#directory \"+compiler-libs\";;\n\
                      let r = Toploop.run_script Format.std_formatter %S [||] in\n\
                      if r then\n\
                     \  SmartML.Target.dump %S\n\
                      else\n\
                     \  exit 1;;"
                     script
                     fn_scenario);
                write_targets_ml top script_args )
              else
                let extended =
                  Filename.concat out_dir "script_init_extended.ml"
                in
                Io.write_file
                  extended
                  (Printf.sprintf
                     "%s\nlet () = SmartML.Target.dump %S"
                     (Io.read_file script)
                     fn_scenario);
                write_targets_ml extended script_args
          | _ -> assert false
        end;
        run_scenarios
          ~kind
          ~accept_empty
          ~config
          ~html
          ~install
          fn_scenario
          out_dir
      with
      | Basics.SmartExcept l -> [(`Error, l)]
    in
    let is_error = function
      | `Error, _ -> true
      | _ -> false
    in
    let module Printer = (val Printer.get config : Printer.Printer) in
    ( match List.partition is_error errors with
    | [], [] -> ()
    | (_ :: _ as errors), _ ->
        let f (_, x) =
          print_endline ("[error] " ^ Printer.pp_smart_except false x)
        in
        List.iter f errors;
        exit 1
    | [], (_ :: _ as warnings) ->
        let f (_, x) =
          print_endline ("[warning] " ^ Printer.pp_smart_except false x)
        in
        List.iter f warnings );
    let client_command = "ext/tezos-binaries/tezos-client" in
    ( match sandbox with
    | None -> ()
    | Some p ->
        let connection =
          Tezos.This_client.Connection.(
            Node {address = None; port = p; tls = false})
        in
        let client =
          Tezos.This_client.state
            ~client_command
            ~confirmation:`Bake
            ~funder:
              "unencrypted:edsk3RFgDiCt7tWB2oe96w1eRw72iYiiqZPLu9nnEY23MYRp2d8Kkx"
            ~connection
            (out_dir ^ "/sandbox")
        in
        let scenarios =
          let open Tezos.Smartml_scenario in
          let scenarios = of_json_file fn_scenario config in
          List.filter (fun x -> kind = x.sc.scenario.kind) scenarios
        in
        let r = Tezos.run ~scenarios ~client () in
        Fmt.pr
          "\n%a\n%!"
          Fmt.(
            vbox
              ~indent:2
              (pair
                 ~sep:cut
                 string
                 (list ~sep:cut (fun ppf -> pf ppf "* `%s`"))))
          ( Fmt.str
              "Targets %s, results:"
              (if r#success then "SUCCEEDED" else "FAILED")
          , [r#results_dot_txt; r#results_dot_json; r#results_full_dot_txt] ) );
    match mockup with
    | None -> ()
    | Some m ->
        let connection = Tezos.This_client.Connection.(Mockup {path = m}) in
        let client =
          Tezos.This_client.state
            ~client_command
            ~confirmation:`Auto
            ~funder:"bootstrap1"
            ~connection
            (out_dir ^ "/mockup")
        in
        let scenarios =
          let open Tezos.Smartml_scenario in
          let scenarios = of_json_file fn_scenario config in
          List.filter (fun x -> kind = x.sc.scenario.kind) scenarios
        in
        let r = Tezos.run ~scenarios ~client () in
        Fmt.pr
          "\n%a\n%!"
          Fmt.(
            vbox
              ~indent:2
              (pair
                 ~sep:cut
                 string
                 (list ~sep:cut (fun ppf -> pf ppf "* `%s`"))))
          ( Fmt.str
              "Targets %s, results:"
              (if r#success then "SUCCEEDED" else "FAILED")
          , [r#results_dot_txt; r#results_dot_json; r#results_full_dot_txt] )

  let build_kt1_address prefix =
    let kt1, hex = Bs58.build_kt1_address prefix in
    Printf.ksprintf print_endline "%s %s" kt1 hex

  let misc_commands _env =
    let unary name f =
      ( name
      , function
        | [x] -> f x
        | _ -> failwith "Bad parameters for unary command" )
    in
    [ unary "base58.list_kt1_addresses" (fun n ->
          Bs58.build_kt1_addresses (int_of_string n))
    ; unary "base58.kt1_address" build_kt1_address
    ; unary "base58.encode" (fun hex -> print_endline (Bs58.encode_hex hex))
    ; unary "base58.decode" (fun x -> print_endline (Bs58.decode_to_hex x)) ]

  let run_misc env = function
    | [] -> List.iter (fun (name, _) -> print_endline name) (misc_commands env)
    | f :: l ->
      ( match List.assoc_opt f (misc_commands env) with
      | Some command -> command l
      | None -> failwith "Unknown misc command" )

  let run () =
    let ( { script
          ; kind
          ; accept_empty
          ; output
          ; config
          ; mockup
          ; sandbox
          ; purge
          ; html
          ; install
          ; misc
          ; script_args } as env ) =
      parse config0 (List.tl (Array.to_list Sys.argv))
    in
    match misc with
    | Some command -> run_misc env command
    | None ->
        let script = Option.of_some_exn ~msg:"source file required" script in
        let basefilename = Filename.chop_extension script in
        let do_purge output_dir = if purge then rm_rf output_dir in
        let out_dir = Option.default basefilename output in
        do_purge out_dir;
        run_targets
          ~kind
          ~accept_empty
          ~config
          ~sandbox
          ~mockup
          ~html
          ~install
          out_dir
          script
          script_args

  let main =
    ( try run () with
    | exn ->
        let module Printer = ( val Printer.get_by_language Config.SmartPy
                                 : Printer.Printer )
        in
        print_endline (Printer.exception_to_string false exn);
        exit 1 );
    exit 0
end
