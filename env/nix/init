#!/usr/bin/env nix-shell
#!nix-shell -i bash --show-trace shell.nix
set -euo pipefail

ENV=nix
COMPILER=ocaml-base-compiler.4.10.2
SWITCH=env/current/switches/$COMPILER

export PATH=$PWD/env/nix/bin:$PATH

echo Pointing to $ENV...
rm -f env/current
ln -s $ENV/ env/current

echo Ensure opam is initialized...
[ -a ~/.opam ] || OPAMNO=1 opam init --bare

echo Ensure opam switch is initialized...
[ -a $SWITCH/_opam ] || opam switch create $SWITCH $COMPILER
rm -rf _opam
ln -s $SWITCH/_opam

echo Ensure opam switch has necessary packages...
opam switch import --yes env/switch.export
opam uninstall --yes --auto-remove
opam switch export env/switch.export
git --no-pager diff env/switch.export

echo Ensure node modules are installed...
cp -f env/current/{package,package-lock}.json .
npm install

echo Done!
