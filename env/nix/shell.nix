let
  # https://github.com/NixOS/nixpkgs/tags
  rev = "21.05";
  nixpkgs = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    sha256 = "1ckzhh24mgz6jd1xhfgx0i9mijk6xjqxwsshnvq789xsavrmsc36";
  };
in
with (import nixpkgs {});

let
  util-linux   = callPackage ./pkgs/util-linux.nix {};
  asciidoctor  = callPackage ./pkgs/asciidoctor/default.nix {};
in

mkShell rec {
  name = "smartpy";

  hardeningDisable = ["strictoverflow"];

  buildInputs =
    (if builtins.currentSystem == "x86_64-darwin" then
      [ util-linux  # Provides setsid.
      ] else []) ++
    [ asciidoctor
      binutils
      coreutils
      curl
      docker
      git
      gmp
      gnumake
      hidapi
      libev
      libffi
      lsof
      m4
      nix
      nixpkgs
      nodejs
      ocaml
      ocamlformat
      opam
      openssh
      perl
      pkgconfig
      python3
      rsync
      tree
      which
      xdg_utils
      yarn
    ];

  OCAMLPARAM="_,ccopt=-Wno-unused-command-line-argument";

  shellHook = ''
    # Source opam environment, if any:
    [ -d _opam ] && eval $(opam env)
  '';
}
