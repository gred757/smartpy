#!/usr/bin/env nix-shell
#!nix-shell -i bash --keep NIX_PATH --keep NIX_SSL_CERT_FILE --show-trace shell.nix

set -euo pipefail

command=$1
dir=$2
cache=$3
shift 3

get() {
    &> /dev/null ls $cache || { echo "Cache miss."; return 1; }
    rm -rf $dir
    mkdir -p $dir
    cp -R $cache/. $dir
    echo "Loaded from cache at $cache."
}

put() {
    [ -e $dir ] || { echo "$dir does not exist."; return 1; }
    [ -e $cache ] && { echo "Already cached."; return 0; }
    rm -rf $cache
    cp -R $dir/. $cache
    echo "Stored in cache at $cache."
}

case "$command" in
    get)
        get
        ;;

    put)
        put
        ;;

    with)
        get || :
        "$@"
        put
        ;;

    *)
        echo "Unknown command."
        exit 1
        ;;
esac
