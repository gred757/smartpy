package crawler

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/pkg/errors"
	LOG "github.com/romarq/tezplorer/internal/logger"
	"github.com/romarq/tezplorer/pkg/utils"

	"github.com/tidwall/gjson"

	"blockwatch.cc/tzgo/micheline"
	"blockwatch.cc/tzgo/tezos"
)

type michelsonMapValue struct {
	Key   interface{}
	Value interface{}
}

type michelsonMapType struct {
	Key   json.RawMessage
	Value json.RawMessage
}

// Core data types (https://tezos.gitlab.io/alpha/michelson.html#core-data-types-and-notations)
const (
	primTypeInt       = "int"       // "prim":"int"
	primTypeNat       = "nat"       // "prim":"nat"
	primTypeTimestamp = "timestamp" // "prim":"timestamp"
	primTypeAddress   = "address"   // "prim":"address"
	primTypeString    = "string"    // "prim":"string"
	primTypeBytes     = "bytes"     // "prim":"bytes"
	primTypeBool      = "bool"      // "prim":"bool"
	primTypePair      = "pair"      // "prim":"pair"
	primTypeMap       = "map"       // "prim":"map"
	primTypeBigMap    = "big_map"   // "prim":"big_map"
	primTypeOption    = "option"    // "prim":"option"
	primTypeKey       = "key"       // "prim":"key"
	primTypeLambda    = "lambda"    // "prim":"lambda"
	primTypeOr        = "or"        // "prim":"or"
	primTypeList      = "list"      // "prim":"list"
	primTypeSet       = "set"       // "prim":"set"
	primTypeUnit      = "unit"      // "prim":"unit"
)

// https://tezos.gitlab.io/alpha/michelson.html#full-grammar
const (
	primValuePair  = "Pair"  // "prim":"Pair"
	primValueElt   = "Elt"   // "prim":"Elt"
	primValueLeft  = "Left"  // "prim":"Left"
	primValueRight = "Right" // "prim":"Right"
	primValueTrue  = "True"  // "prim":"True"
	primValueFalse = "False" // "prim":"False"
	primValueNone  = "None"  // "prim":"None"
	primValueUnit  = "Unit"  // "prim":"Unit"
)

type michelsonExpression struct {
	Bytes  string `json:"bytes,omitempty"`
	Int    string `json:"int,omitempty"`
	String string `json:"string,omitempty"`
	// Generic prim (can have args and annots)
	Prim string `json:"prim,omitempty"`
	// Args cannot be of type []michelsonExpression,
	// since it can contain arrays and objects as items
	Args   []json.RawMessage `json:"args,omitempty"`
	Annots []string          `json:"annots,omitempty"`
}

// Get contract storage type and value
func getContractStorage(endpoint string, contractAddress string) (michelsonExpression, json.RawMessage, error) {
	var typeExpr michelsonExpression
	var valueRaw json.RawMessage

	// Get contract script in "Optimized" form
	postBody, _ := json.Marshal(map[string]string{
		"unparsing_mode": "Optimized",
	})
	resp, err := http.Post(
		fmt.Sprintf("%s/chains/main/blocks/head/context/contracts/%s/script/normalized", endpoint, contractAddress),
		"application/json",
		bytes.NewBuffer(postBody),
	)
	if err != nil {
		return typeExpr, valueRaw, errors.Wrapf(err, "Could not fetch contract script")
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return typeExpr, valueRaw, errors.Wrapf(err, "Could not read contract script")
	}

	// Get the storage type
	// { "code": [{...}, {"prim": "storage", args [{}]}, {...}], "storage": {} }
	// The line below gets this item --------------^
	result := gjson.GetBytes(body, `code.#(prim=="storage").args.0`)
	var raw []byte
	if result.Index > 0 {
		raw = body[result.Index : result.Index+len(result.Raw)]
	} else {
		raw = []byte(result.Raw)
	}
	typeExpr, err = convertRawToMichelsonExpression(raw)
	if err != nil {
		return typeExpr, valueRaw, errors.Wrapf(err, "Could not unmarshall script JSON to a micheline expression")
	}

	// Get the storage value
	// { "code": [], "storage": {} }
	// -------------------------^
	result = gjson.GetBytes(body, "storage")
	valueRaw = json.RawMessage(result.Raw)

	return typeExpr, valueRaw, nil
}

func (c *Crawler) extractBigMapDiff(id int64, annotation string, contract string, diff micheline.BigmapDiffElem) (michelsonMapValue, error) {
	result := michelsonMapValue{}

	// Get the request type from the oracle storage type, it contains
	// the label(called annotation) and the type of each value
	typeExpr, _, err := getContractStorage(c.Config.RPC.URL, contract)
	if err != nil {
		return result, err
	}

	// Get the big_map type expression
	bigMapTypeExpr, err := getType(typeExpr, annotation)
	if err != nil {
		return result, err
	}

	// Extract the new request from the big_map_diff
	keyRaw, err := diff.Key.MarshalJSON()
	if err != nil {
		return result, err
	}
	result.Key, err = extractValuesFromExpression(bigMapTypeExpr.Key, keyRaw)
	if err != nil {
		LOG.Debug("Could not extract difference of big map (%s). %v", annotation, err)
		return result, err
	}

	// Extract the new request from the big_map_diff
	valueRaw, err := diff.Value.MarshalJSON()
	if err != nil {
		return result, err
	}
	result.Value, err = extractValuesFromExpression(bigMapTypeExpr.Value, valueRaw)
	if err != nil {
		LOG.Debug("Could not extract difference of big map (%s). %v", annotation, err)
		return result, err
	}

	return result, nil
}

// Extract the request values map(key => value) from the expression,
// where (key) is the annotation obtained from the storage type
func extractValuesFromExpression(typeRaw json.RawMessage, valueRaw json.RawMessage) (interface{}, error) {
	req := make(map[string]interface{})

	typeExpr, err := convertRawToMichelsonExpression(typeRaw)
	if err != nil {
		LOG.Error("Could not convert raw type to michelson expression")
		// Raw was not a valid michelson expression
		return req, errors.WithStack(err)
	}
	valueExpr, err := convertRawToMichelsonExpression(valueRaw)
	if err != nil {
		LOG.Error("Could not convert raw value to michelson expression")
		// Raw was not a valid michelson expression
		return req, errors.WithStack(err)
	}

	switch typeExpr.Prim {
	case primTypeInt, primTypeNat, primTypeTimestamp, primTypeBigMap,
		primTypeString, primTypeBytes, primTypeAddress, primTypeBool, primTypeKey, primTypeUnit:
		value, err := extractVariantValue(typeExpr, valueExpr)
		if err != nil {
			return req, err
		}

		// Extract field name from the annotation
		field, err := getAnnotation(typeExpr.Annots)
		if err != nil {
			return value, nil
		}
		req[field] = value
	case primTypeOr:
		switch valueExpr.Prim {
		case primValueLeft:
			return extractValuesFromExpression(typeExpr.Args[0], valueExpr.Args[0])
		case primValueRight:
			return extractValuesFromExpression(typeExpr.Args[1], valueExpr.Args[0])
		}
	case primTypeLambda:
		// ignored on purpose
		return req, err
	case primTypeOption:
		var value interface{}
		// Optional values can be None (we return nil in those cases)
		if valueExpr.Prim != primValueNone {
			// Some value is present
			value, err = extractValuesFromExpression(typeExpr.Args[0], valueExpr.Args[0])
			if err != nil {
				return nil, err
			}
		}
		// Extract field name from the annotation
		field, err := getAnnotation(typeExpr.Annots)
		if err != nil {
			return value, nil
		}
		req[field] = value
	case primTypeList, primTypeSet:
		// Extract values from the list
		var values []interface{}
		for _, v := range valueExpr.Args {
			value, err := extractValuesFromExpression(typeExpr.Args[0], v)
			if err != nil {
				return nil, err
			}
			values = append(values, value)
		}

		// Extract field name from the annotation
		field, err := getAnnotation(typeExpr.Annots)
		if err != nil {
			return values, nil
		}
		req[field] = values
	case primTypeMap:
		// Extract key and values from the map
		keyValues, err := extractMapKeyValues(typeExpr, valueExpr.Args)
		if err != nil {
			LOG.Error("Could not extract keys and values from map")
			return nil, errors.WithStack(err)
		}

		// Extract field name from the annotation
		field, err := getAnnotation(typeExpr.Annots)
		if err != nil {
			return keyValues, nil
		}
		req[field] = keyValues
	case primTypePair:
		if len(typeExpr.Args) != len(valueExpr.Args) {
			typeExpr, valueExpr, err = normalizeTypeAndValue(typeExpr, valueExpr)
			if err != nil {
				LOG.Error("Could not normalize type and value: %v", errors.WithStack(err))
				return req, errors.WithStack(err)
			}
		}

		pairValues := make(map[string]interface{})
		for i, reqType := range typeExpr.Args {
			value, err := extractValuesFromExpression(reqType, valueExpr.Args[i])
			if err != nil {
				return nil, err
			}
			kv, ok := (value.(map[string]interface{}))
			if ok {
				for k, v := range kv {
					pairValues[k] = v
				}
			} else {
				pairValues[strconv.Itoa(i)] = value
			}
		}
		// Extract field name from the annotation
		field, err := getAnnotation(typeExpr.Annots)
		if err != nil {
			return pairValues, nil
		}
		req[field] = pairValues
	default:
		return req, fmt.Errorf("unexpected expression type %s", typeExpr.Prim)
	}

	return req, nil
}

// Normalize the type the value (the number or args should be equal on type and value)
func normalizeTypeAndValue(typeExpr michelsonExpression, valueExpr michelsonExpression) (michelsonExpression, michelsonExpression, error) {
	typeExpr, err := optimizeExpression(typeExpr)
	if err != nil {
		return typeExpr, valueExpr, err
	}
	valueExpr, err = optimizeExpression(valueExpr)
	if err != nil {
		return typeExpr, valueExpr, err
	}

	return typeExpr, valueExpr, nil
}

// Optimize expression (spread inner pairs to the parent pair)
func optimizeExpression(expr michelsonExpression) (michelsonExpression, error) {
	// [Optimized]			: the shortest representation is used. It depends on the number of arguments:
	//							n < 4 	=> { Pair(a1, a2), Pair(a3, Pair(a4, a5)) }
	//							n >= 4	=> Pair(a1, a2, ... an)
	// [Optimized_legacy]	: nested pairs are always used: { Pair(a1, a2), Pair(a3, Pair(a4, a5)) }
	aux := expr
	aux.Args = []json.RawMessage{}
	for _, arg := range expr.Args {
		innerExpr, err := convertRawToMichelsonExpression(arg)
		if err != nil {
			LOG.Debug("Could not optimize expression:\n%s", utils.PrettifyJSON(expr))
			return expr, err
		}

		if innerExpr.Prim == primTypePair || innerExpr.Prim == primValuePair {
			aux.Args = append(aux.Args, innerExpr.Args...)
		} else {
			aux.Args = append(aux.Args, arg)
		}
	}

	return aux, nil
}

// Extract key values from map
func extractMapKeyValues(tExpr michelsonExpression, vExpr []json.RawMessage) ([]michelsonMapValue, error) {
	// The following types are enforced:
	// * [key] => string
	// * [value] => (string|int64)
	var values []michelsonMapValue

	// For each map entry, get the associated key and value
	for _, expr := range vExpr {
		expression, err := convertRawToMichelsonExpression(expr)
		if err != nil {
			return nil, err
		}

		// "prim":"Elt" means "element" and every map entry is identified by an element
		if expression.Prim != primValueElt && len(expression.Args) == 2 {
			return values, fmt.Errorf("expected element (Elt) with 2 args, but instead got %v", expression.Prim)
		}

		// Get key type expression
		keyType, err := convertRawToMichelsonExpression(tExpr.Args[0])
		if err != nil {
			return nil, err
		}

		// Get key expression
		keyExpr, err := convertRawToMichelsonExpression(expression.Args[0])
		if err != nil {
			return nil, err
		}

		// Get value type expression
		valueType, err := convertRawToMichelsonExpression(tExpr.Args[1])
		if err != nil {
			return nil, err
		}

		// Get value expression
		valueExpr, err := convertRawToMichelsonExpression(expression.Args[1])
		if err != nil {
			return nil, err
		}

		// Extract the normalized type from michelson expression
		var key interface{}
		switch keyType.Prim {
		case primTypePair, primTypeOr, primTypeSet, primTypeList:
			key, err = extractValuesFromExpression(tExpr.Args[0], expression.Args[0])
		case primTypeBigMap, primTypeMap:
			key, err = extractMapKeyValues(keyType, keyExpr.Args)
		default:
			key, err = extractVariantValue(keyType, keyExpr)
		}
		if err != nil {
			LOG.Debug("Could not extract key (type: %v, value %v). %v", utils.PrettifyJSON(keyType), utils.PrettifyJSON(keyExpr), err)
			return nil, err
		}

		// Extract the normalized value from michelson expression
		var value interface{}
		switch valueType.Prim {
		case primTypePair, primTypeOr, primTypeSet, primTypeList:
			value, err = extractValuesFromExpression(tExpr.Args[1], expression.Args[1])
		case primTypeBigMap, primTypeMap:
			value, err = extractMapKeyValues(valueType, valueExpr.Args)
		default:
			value, err = extractVariantValue(valueType, valueExpr)
		}
		if err != nil {
			LOG.Debug("Could not extract value (type: %v, value %v). %v", utils.PrettifyJSON(valueType), utils.PrettifyJSON(valueExpr), err)
			return nil, err
		}

		values = append(values, michelsonMapValue{
			Key:   key,
			Value: value,
		})
	}

	return values, nil
}

// Lookup a given annotation and return the respective expression
func extractExpressionByAnnotation(expr michelsonExpression, annot string) (michelsonExpression, error) {
	// INPUTS:
	// -- expr: { prim: "pair", annots: ["%a"], args: [{ prim: "int", annots: ["%b"] }, {}] }
	// -- annot: "%b"
	//
	// OUTPUT: { prim: "int", annots: ["%b"] }

	if len(expr.Annots) > 0 && expr.Annots[0] == annot {
		return expr, nil
	}

	for _, arg := range expr.Args {
		exp, err := convertRawToMichelsonExpression(arg)
		if err != nil {
			return exp, err
		}

		if exp, err = extractExpressionByAnnotation(exp, annot); err == nil {
			return exp, err
		}
	}

	return expr, fmt.Errorf("expression doesn't contain the annotation %s", annot)
}

// Get value type of big map "requests", the type is used to map values to their respective field names
func getType(typeExpr michelsonExpression, annotation string) (michelsonMapType, error) {
	// Extract requests big_map type from contract storage type
	typeExpr, err := extractExpressionByAnnotation(typeExpr, annotation)
	if err != nil {
		return michelsonMapType{}, err
	}

	// Only the "%requests" value type is needed
	return michelsonMapType{
		Key:   typeExpr.Args[0],
		Value: typeExpr.Args[1],
	}, err
}

// Get field from type annotation (["%field"] => "field")
func getAnnotation(annot []string) (string, error) {
	l := len(annot)
	if l != 1 {
		return "", fmt.Errorf("expected one annotation, but received %d", l)
	}
	if !strings.HasPrefix(annot[0], "%") {
		return "", fmt.Errorf(`invalid annotation format (%s), expected prefix (%c)`, annot[0], '%')
	}
	return strings.TrimPrefix(annot[0], "%"), nil
}

// Extract value from michelson expression
func extractVariantValue(t michelsonExpression, v michelsonExpression) (interface{}, error) {
	if t.Prim == primTypeString || v.String != "" {
		if v.String == "" {
			return "", nil
		}
		return v.String, nil
	} else if v.Int != "" {
		// Convert string to int64
		return strconv.ParseInt(v.Int, 10, 64)
	} else if v.Bytes != "" {
		bytes, err := hex.DecodeString(v.Bytes)
		if err != nil {
			return nil, errors.Wrap(err, "Could not decode bytes")
		}
		if t.Prim == primTypeKey {
			if key, err := tezos.DecodeKey(bytes); err == nil {
				return key.String(), nil
			} else {
				return nil, err
			}
		}
		prim := micheline.NewBytes(bytes)
		if prim.IsPacked() {
			prim, err = prim.Unpack()
			if err != nil {
				return nil, errors.Wrap(err, "Could not unpack bytes")
			}
			if prim.String != "" {
				return prim.String, nil
			}
		}
		return v.Bytes, nil
	} else if v.Prim == primValueTrue {
		return true, nil
	} else if v.Prim == primValueFalse {
		return false, nil
	} else if v.Prim == primValueNone {
		return nil, nil
	} else if v.Prim == primValueUnit {
		return primValueUnit, nil
	} else if v.Prim == primValueRight || v.Prim == primValueLeft {
		orArg, err := convertRawToMichelsonExpression(v.Args[0])
		if err != nil {
			return nil, err
		}
		return extractVariantValue(t, orArg)
	}

	return nil, errors.Errorf("could not get value from prim (%s) of type (%s)", v.Prim, t.Prim)
}

// Convert json.RawMessage to michelsonExpression
// valueRaw can be an object or an array
func convertRawToMichelsonExpression(valueRaw json.RawMessage) (michelsonExpression, error) {
	var vExpression michelsonExpression
	err := json.Unmarshal(valueRaw, &vExpression)
	// If we cannot unmarshal to an object, check if we can unmarshal to an array of objects
	// This can happen because of the "unparsing_mode" used in "internal transactions",
	// which currently is (Optimized mode)
	//
	// [Readable]			: comb pairs are always written Pair(a1, a2, ... an)
	// [Optimized]			: the shortest representation is used. It depends on the number of arguments:
	//							n < 4 	=> { Pair(a1, a2), Pair(a3, Pair(a4, a5)) }
	//							n >= 4	=> Pair(a1, a2, ... an)
	// [Optimized_legacy]	: nested pairs are always used: { Pair(a1, a2), Pair(a3, Pair(a4, a5)) }
	if err != nil {
		vExpressions, err := convertToJSONArray(valueRaw)
		if err != nil {
			LOG.Debug("Could not get michelson expression")
			// This was neither an object or an array of objects, return error
			return vExpression, err
		}

		// vExpr is an array of expressions ([ expression, ... ])
		// It must either be a sequence or an optimized pair
		vExpression = michelsonExpression{
			Args: vExpressions,
		}
	}

	return vExpression, nil
}

// Convert json.RawMessage to []json.RawMessage
func convertToJSONArray(expr json.RawMessage) ([]json.RawMessage, error) {
	var expressions []json.RawMessage
	err := json.Unmarshal(expr, &expressions)
	return expressions, err
}
