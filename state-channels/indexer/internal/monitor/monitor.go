package monitor

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/romarq/tezplorer/internal/logger"
	RPC "github.com/romarq/tezplorer/internal/rpc"
)

const (
	retryInterval = 5 * time.Second
)

// Monitor struct
type Monitor struct {
	endpoint  string
	completed bool
}

// New Create a monitor
func New(endpoint string) Monitor {
	return Monitor{
		endpoint:  endpoint,
		completed: false,
	}
}

// Stop Monitor
func (m Monitor) Stop() {
	m.completed = true
}

// WatchNewBlocks - Listens to new blocks
func (m Monitor) WatchNewBlocks(channel chan<- RPC.BlockHeader) {
	logger.Debug("Started monitoring new blocks...")

	for {
		m.MonitorHeads(channel)

		if m.completed {
			break
		}

		time.Sleep(retryInterval)
	}

	logger.Debug("Stopped monitoring new blocks...")
}

func (m Monitor) MonitorHeads(channel chan<- RPC.BlockHeader) {
	defer close(channel)

	resp, err := m.getStreamedResponse(m.endpoint)
	if err != nil {
		logger.Error("Failed to open RPC streamed response at %s. %v", m.endpoint, err)
		return
	}
	defer resp.Body.Close()

	reader := bufio.NewReader(resp.Body)
	lines := make(chan []byte)
	go m.readLines(lines, reader)

	for {
		logger.Debug("Waiting for new blocks...")
		line, ok := <-lines
		if !ok {
			logger.Error("Stream channel closed unexpectly.")
			// Restart http stream
			return
		}

		var blockHeader RPC.BlockHeader
		err = json.Unmarshal(line, &blockHeader)
		if err != nil {
			logger.Error("Failed to parse block header. %v", err)
			// Restart http stream
			return
		}

		logger.Debug("Found new block at level %d: %s", blockHeader.Level, blockHeader.Hash)

		channel <- blockHeader
	}
}

func (m Monitor) readLines(lines chan []byte, reader *bufio.Reader) {
	defer close(lines)
	for {
		line, err := reader.ReadBytes('\n')
		if m.completed {
			return
		}

		if err == io.EOF {
			lines <- line
			logger.Warn("Connection stream closed, retrying in %d...", retryInterval)
			return
		}

		if err != nil {
			logger.Error("Failed to read bytes from stream. %v", err)
			return
		}

		lines <- line
	}
}

// Gets streamed HTTP response
func (m Monitor) getStreamedResponse(path string) (*http.Response, error) {
	resp, err := http.Get(m.endpoint)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		resp.Body.Close()
		return nil, fmt.Errorf("Unexpected status code %v from URL %s", resp.StatusCode, m.endpoint)
	}

	return resp, nil
}
