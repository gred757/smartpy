package utils

import "encoding/json"

func PrettifyJSON(o interface{}) string {
	prettyJSON, _ := json.MarshalIndent(o, "", "  ")
	return string(prettyJSON)
}

func Contains(s []string, el string) bool {
	for _, a := range s {
		if a == el {
			return true
		}
	}
	return false
}
