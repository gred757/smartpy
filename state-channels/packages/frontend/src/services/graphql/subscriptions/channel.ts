import { gql } from '@apollo/client';

export const CHANNELS = gql`
    subscription Channels($address: String!, $platform: String!) {
        channels: channel(
            where: {
                _and: [
                    { participants: { public_key_hash: { _eq: $address } } }
                    { platform: { id: { _eq: $platform } } }
                ]
            }
            order_by: { created_at: desc }
            limit: 10
        ) {
            id
            closed
            nonce
            withdrawDelay: withdraw_delay
            createdAt: created_at
            updatedAt: updated_at
            participants {
                publicKeyHash: public_key_hash
                publicKey: public_key
                withdrawID: withdraw_id
                withdraw
                bonds {
                    amount
                    id: bond_id
                }
            }
            games(order_by: { created_at: desc }, limit: 30) {
                id
                bonds
                nonce: game_nonce
                onChain: on_chain
                settlements
                state
                settled
                playDelay: play_delay
                players
                model {
                    metadata
                    outcomes
                }
                signatures {
                    publicKey: public_key
                    signature
                }
                createdAt: created_at
                updatedAt: updated_at
            }
        }
    }
`;
