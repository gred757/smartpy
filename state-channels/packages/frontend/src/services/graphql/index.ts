import { ApolloClient, InMemoryCache } from '@apollo/client';
import { split, HttpLink } from '@apollo/client';
import { getMainDefinition } from '@apollo/client/utilities';
import { WebSocketLink } from '@apollo/client/link/ws';
import { Constants } from 'state-channels-common';

export * as Subscription from './subscriptions';
export * as Query from './queries';

const wsLink = new WebSocketLink({
    uri: Constants.graphQlWs,
    options: {
        reconnect: true,
    },
});

const httpLink = new HttpLink({
    uri: Constants.graphQlHttp,
});

// Split between http / websocket depending on the operation
const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query);
        return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
    },
    wsLink,
    httpLink,
);

const client = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache(),
});

export default client;
