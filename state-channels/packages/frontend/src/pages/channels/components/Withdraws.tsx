import React from 'react';
import {
    Box,
    Grid,
    Typography,
    TableCell,
    TableBody,
    TableHead,
    Dialog,
    Divider,
    MenuItem,
    Select,
    Stack,
    TextField,
    SelectChangeEvent,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    Tooltip,
    Collapse,
    Chip,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { DeleteForever as DeleteIcon } from '@mui/icons-material';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import PlayIcon from '@mui/icons-material/PlayArrow';
import { convertUnitWithSymbol, EntryPoints, SC_Channel, SC_Platform, SC_Token, TezUnit } from 'state-channels-common';
import BondIcon from 'src/components/base/BondIcon';
import Table from 'src/components/base/Table';
import TableRow from 'src/components/base/TableRow';
import Button from 'src/components/base/Button';
import Fab from 'src/components/base/Fab';
import Wallet from 'src/services/wallet';
import CountDownTime from 'src/components/base/CountDownTime';
import Avatar from 'src/components/base/Avatar';
import useWalletContext from 'src/hooks/useWalletContext';
import Logger from 'src/services/logger';
import { copyToClipboard } from 'src/utils/clipboard';
import CopyButton from 'src/components/base/CopyButton';
import RouterFab from 'src/components/base/RouterFab';

interface WithdrawRowProps {
    channel: SC_Channel;
    localPlayer?: string;
    tokens: Record<number, SC_Token>;
    withdraw: {
        id: string;
        address: string;
        challenge?: string[];
        challengeTokens: Record<string, number>;
        timeout: number;
        tokens: Record<string, number>;
    };
    finalizeWithdraw: () => void;
    cancelWithdraw: () => void;
    withdrawChallenge: (withdrawer: { address: string; challenge?: string[] }) => void;
}

const WithdrawRow: React.FC<WithdrawRowProps> = ({
    channel,
    localPlayer,
    tokens,
    withdraw,
    finalizeWithdraw,
    cancelWithdraw,
    withdrawChallenge,
}) => {
    const [open, setOpen] = React.useState(false);

    return (
        <>
            <TableRow key={withdraw.address}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell>
                    <Grid container alignItems="center" spacing={2}>
                        <Grid item>
                            <Avatar value={withdraw.address} />
                        </Grid>
                        <Grid item>{withdraw.address === localPlayer ? 'You' : 'Opponent'}</Grid>
                    </Grid>
                </TableCell>
                <TableCell align="right">
                    <CountDownTime targetTime={withdraw.timeout} />
                </TableCell>
                <TableCell align="right">
                    <Button color="error" disabled={withdraw.address !== localPlayer} onClick={cancelWithdraw}>
                        Cancel
                    </Button>
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box margin={3}>
                            <Table
                                header={
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Token</TableCell>
                                            <TableCell align="right">Amount</TableCell>
                                        </TableRow>
                                    </TableHead>
                                }
                                body={
                                    <TableBody>
                                        {Object.entries(withdraw.tokens).map(([tokenID, amount]) => (
                                            <TableRow key={tokenID}>
                                                <TableCell>
                                                    <BondIcon token={tokens?.[Number(tokenID)]} />
                                                </TableCell>
                                                <TableCell align="right">
                                                    {Number(tokenID) === 0
                                                        ? convertUnitWithSymbol(amount, TezUnit.uTez, TezUnit.tez)
                                                        : amount}
                                                </TableCell>
                                            </TableRow>
                                        ))}
                                        <TableRow>
                                            <TableCell colSpan={2}>
                                                <Divider />
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell colSpan={2}>
                                                <Typography variant="overline">Opponent challenges</Typography>{' '}
                                                <Typography variant="caption" color="error">
                                                    (Unsettled games most to be settled before finalizing the withdraw)
                                                </Typography>
                                                {withdraw.address === localPlayer && withdraw.challenge ? (
                                                    <Table
                                                        header={
                                                            <TableHead>
                                                                <TableRow>
                                                                    <TableCell>Game ID</TableCell>
                                                                    <TableCell>Settled</TableCell>
                                                                    <TableCell></TableCell>
                                                                </TableRow>
                                                            </TableHead>
                                                        }
                                                        body={
                                                            <TableBody>
                                                                {withdraw.challenge?.map((gameID) => {
                                                                    const game = channel.games.find(
                                                                        (g) => g.id === gameID,
                                                                    );
                                                                    if (game) {
                                                                        return (
                                                                            <TableRow key={gameID}>
                                                                                <TableCell>
                                                                                    <CopyButton
                                                                                        onClick={() =>
                                                                                            copyToClipboard(gameID)
                                                                                        }
                                                                                        label={gameID}
                                                                                        sx={{ maxWidth: 220 }}
                                                                                    />
                                                                                </TableCell>
                                                                                <TableCell>
                                                                                    <Chip
                                                                                        size="small"
                                                                                        color={
                                                                                            game.settled
                                                                                                ? 'success'
                                                                                                : 'error'
                                                                                        }
                                                                                        label={
                                                                                            <Typography variant="overline">
                                                                                                {String(game.settled)}
                                                                                            </Typography>
                                                                                        }
                                                                                    />
                                                                                </TableCell>
                                                                                <TableCell align="right">
                                                                                    <RouterFab
                                                                                        variant="extended"
                                                                                        to={`/channels/${channel.id}/games/${gameID}`}
                                                                                        color="primary"
                                                                                        size="small"
                                                                                    >
                                                                                        {game.outcome ? (
                                                                                            'Open'
                                                                                        ) : (
                                                                                            <>
                                                                                                <PlayIcon
                                                                                                    sx={{ mr: 1 }}
                                                                                                />
                                                                                                Play
                                                                                            </>
                                                                                        )}
                                                                                    </RouterFab>
                                                                                </TableCell>
                                                                            </TableRow>
                                                                        );
                                                                    }
                                                                })}
                                                                <TableRow>
                                                                    <TableCell align="right" colSpan={3}>
                                                                        <Button
                                                                            fullWidth
                                                                            onClick={() => withdrawChallenge(withdraw)}
                                                                        >
                                                                            Resolve Opponent Protest
                                                                        </Button>
                                                                    </TableCell>
                                                                </TableRow>
                                                            </TableBody>
                                                        }
                                                    />
                                                ) : null}
                                            </TableCell>
                                        </TableRow>
                                        <TableRow>
                                            <TableCell align="right" colSpan={2}>
                                                {withdraw.address === localPlayer ? (
                                                    <Button
                                                        disabled={withdraw.address !== localPlayer}
                                                        onClick={finalizeWithdraw}
                                                    >
                                                        Finalize
                                                    </Button>
                                                ) : (
                                                    <Button color="error" onClick={() => withdrawChallenge(withdraw)}>
                                                        Protest
                                                    </Button>
                                                )}
                                            </TableCell>
                                        </TableRow>
                                    </TableBody>
                                }
                            />
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </>
    );
};

interface OwnProps {
    channel: SC_Channel;
    onError: (err: string) => void;
    platform: SC_Platform;
    tokens: Record<number, SC_Token>;
}

const Withdraws: React.FC<OwnProps> = ({ platform, channel, tokens, onError }) => {
    const { pkh } = useWalletContext();
    const [withdrawOpen, setWithdrawOpen] = React.useState(false);
    const [withdraws, setWithdraws] = React.useState<Record<number, number>>({});
    const [tokenID, setTokenID] = React.useState(0);
    const [amount, setAmount] = React.useState<number>();

    const channelWithdraws = React.useMemo(
        () =>
            channel.participants
                .filter(({ withdraw }) => withdraw)
                .map((player) => ({
                    ...player.withdraw,
                    id: player.withdrawID,
                    address: player.publicKeyHash,
                })),
        [channel],
    );

    const handleTokenChange = (e: SelectChangeEvent<number>) => {
        setTokenID(Number(e.target.value));
    };

    const handleAmountChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
        setAmount(e.target.value ? Number(e.target.value) : undefined);
    };

    const addWithdraw = React.useCallback(() => {
        if (amount) {
            setWithdraws((w) => ({
                ...w,
                [tokenID]: amount,
            }));
            setAmount(undefined);
            setTokenID(0);
        }
    }, [tokenID, amount]);

    const removeWithdraw = React.useCallback((tID: number) => {
        setWithdraws((w) => {
            delete w[tID];
            return { ...w };
        });
    }, []);

    const sendWithdrawRequest = React.useCallback(async () => {
        const parameters = EntryPoints.Channel.withdrawRequest(
            channel.id,
            Object.entries(withdraws).map(([tId, tAmount]) => ({
                tokenId: Number(tId),
                amount: Number(tId) === 0 ? tAmount * 1000000 : tAmount,
            })),
        );
        try {
            await Wallet.Beacon.transfer(platform.id, parameters);
            setWithdraws({});
            setWithdrawOpen(false);
        } catch (e) {
            Logger.debug(e);
            onError(e?.message || e);
        }
    }, [withdraws, platform]);

    const withdrawChallenge = React.useCallback(
        async (withdraw: { address: string; challenge?: string[] }) => {
            const parameters = EntryPoints.Channel.withdrawChallenge(
                channel.id,
                withdraw.address === pkh
                    ? withdraw.challenge || []
                    : channel.games.filter((g) => !g.settled && g.onChain).map(({ id }) => id),
                withdraw.address,
            );
            try {
                await Wallet.Beacon.transfer(platform.id, parameters);
            } catch (e) {
                Logger.debug(e);
                onError(e?.message || e);
            }
        },
        [channel, platform],
    );

    const finalizeWithdraw = React.useCallback(async () => {
        const parameters = EntryPoints.Channel.withdrawFinalise(channel.id);

        try {
            await Wallet.Beacon.transfer(platform.id, parameters);
        } catch (e) {
            Logger.debug(e);
            onError(e?.message || e);
        }
    }, [channel, platform]);

    const cancelWithdraw = React.useCallback(async () => {
        const parameters = EntryPoints.Channel.withdrawCancel(channel.id);

        try {
            await Wallet.Beacon.transfer(platform.id, parameters);
        } catch (e) {
            Logger.debug(e);
            onError(e?.message || e);
        }
    }, [channel, platform]);

    return (
        <>
            <Box margin={2}>
                <Grid container spacing={2} alignItems="center" sx={{ marginBottom: 2 }}>
                    <Grid item>
                        <Typography variant="h6" gutterBottom>
                            Withdraws
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Fab size="small" color="primary" onClick={() => setWithdrawOpen(true)}>
                            <AddIcon />
                            Add Withdraws
                        </Fab>
                    </Grid>
                </Grid>
                <Table
                    header={
                        <TableHead>
                            <TableRow>
                                <TableCell></TableCell>
                                <TableCell>Player</TableCell>
                                <TableCell align="right">Timeout</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                    }
                    body={
                        <TableBody>
                            {channelWithdraws.length ? (
                                channelWithdraws.map((withdraw) => {
                                    return (
                                        <WithdrawRow
                                            channel={channel}
                                            withdrawChallenge={withdrawChallenge}
                                            finalizeWithdraw={finalizeWithdraw}
                                            cancelWithdraw={cancelWithdraw}
                                            tokens={tokens}
                                            localPlayer={pkh}
                                            withdraw={withdraw}
                                        />
                                    );
                                })
                            ) : (
                                <TableRow>
                                    <TableCell colSpan={6}>
                                        <Box
                                            sx={{
                                                display: 'flex',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }}
                                        >
                                            <Typography variant="overline" textAlign="center">
                                                No withdraws yet
                                            </Typography>
                                        </Box>
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    }
                />
            </Box>
            <Dialog open={withdrawOpen} onClose={() => setWithdrawOpen(false)}>
                <DialogTitle>Add Withdraws</DialogTitle>
                <DialogContent dividers>
                    <Table
                        header={
                            <TableHead>
                                <TableRow>
                                    <TableCell>Token</TableCell>
                                    <TableCell align="right">Amount</TableCell>
                                    <TableCell align="right"></TableCell>
                                </TableRow>
                            </TableHead>
                        }
                        body={
                            <TableBody>
                                {Object.keys(withdraws).length ? (
                                    Object.entries(withdraws).map(([tId, tAmount]) => {
                                        const tID = Number(tId);
                                        return (
                                            <TableRow key={tID}>
                                                <TableCell>
                                                    <BondIcon token={tokens?.[tID]} />
                                                </TableCell>
                                                <TableCell align="right">
                                                    {tID === 0
                                                        ? convertUnitWithSymbol(tAmount, TezUnit.tez, TezUnit.tez)
                                                        : tAmount}
                                                </TableCell>
                                                <TableCell align="right">
                                                    <Tooltip title="Remove Settlement">
                                                        <IconButton onClick={() => removeWithdraw(tID)}>
                                                            <DeleteIcon color="error" />
                                                        </IconButton>
                                                    </Tooltip>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })
                                ) : (
                                    <TableRow>
                                        <TableCell colSpan={6}>
                                            <Box
                                                sx={{
                                                    display: 'flex',
                                                    justifyContent: 'center',
                                                    alignItems: 'center',
                                                }}
                                            >
                                                <Typography variant="overline" textAlign="center">
                                                    No withdraws yet
                                                </Typography>
                                            </Box>
                                        </TableCell>
                                    </TableRow>
                                )}
                                <TableRow>
                                    <TableCell colSpan={6} align="right">
                                        <Stack
                                            direction="row"
                                            spacing={2}
                                            alignItems="stretch"
                                            justifyContent="center"
                                            divider={<Divider orientation="vertical" flexItem />}
                                        >
                                            <Select value={tokenID} onChange={handleTokenChange}>
                                                {Object.values(tokens || []).map((token) => (
                                                    <MenuItem value={token.id} key={token.id}>
                                                        <BondIcon token={token} />
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                            <TextField
                                                onChange={handleAmountChange}
                                                value={amount || 0}
                                                id="amount"
                                                required
                                                type="number"
                                                inputProps={{
                                                    min: 0,
                                                }}
                                                label="Amount"
                                            />
                                            <Button onClick={addWithdraw}>Add</Button>
                                        </Stack>
                                    </TableCell>
                                </TableRow>
                            </TableBody>
                        }
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => sendWithdrawRequest()}>Send withdraw request</Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default Withdraws;
