import React from 'react';
import { SigningType } from '@airgap/beacon-sdk';

import { Alert, Chip, Divider, Grid, Paper, styled, Switch, TextField, Theme, Typography } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';

import {
    Api,
    EntryPoints,
    GameConstants,
    Hashers,
    OffchainViews,
    Packers,
    SC_Game,
    SC_GameMoveAction,
    SC_Platform,
} from 'state-channels-common';

import Wallet from 'src/services/wallet';
import Logger from 'src/services/logger';
import Button from 'src/components/base/Button';
import useWalletContext from 'src/hooks/useWalletContext';
import HeadTailUtils, { HeadTailState } from './HeadTail.utils';
import { getBase } from 'src/utils/misc';
import { randomNonce } from 'src/utils/random';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        board: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            margin: 10,
            padding: 15,
        },
        cell: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: 64,
            height: 64,
            fontSize: '32pt',
        },
        divider: {
            margin: theme.spacing(1),
        },
    }),
);

const HeadTailSwitch = styled(Switch)(({ theme }) => ({
    width: 120,
    height: 66,
    padding: 7,
    '& .MuiSwitch-switchBase': {
        margin: 1,
        padding: 0,
        transform: 'translateX(6px)',
        backgroundImage: `url("icons/head.svg")`,
        '&.Mui-checked': {
            color: '#fff',
            transform: 'translateX(50px)',
            '& .MuiSwitch-thumb:before': {
                backgroundImage: `url("${getBase()}/icons/tail.svg")`,
            },
            '& + .MuiSwitch-track': {
                opacity: 1,
                backgroundColor: theme.palette.mode === 'dark' ? '#8796A5' : '#aab4be',
            },
        },
    },
    '& .MuiSwitch-thumb': {
        backgroundColor: theme.palette.mode === 'dark' ? '#003892' : '#001e3c',
        width: 64,
        height: 64,
        '&:before': {
            content: "''",
            position: 'absolute',
            width: '100%',
            height: '100%',
            left: 0,
            top: 0,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundImage: `url("${getBase()}/icons/head.svg")`,
        },
    },
    '& .MuiSwitch-track': {
        opacity: 1,
        backgroundColor: theme.palette.mode === 'dark' ? '#8796A5' : '#aab4be',
        borderRadius: 50,
    },
}));

interface GameHeaderProps {
    outcome?: Record<string, string>;
    currentPlayerNumber: number;
    localPlayerNumber: number;
    latestMove?: SC_GameMoveAction;
    pendingMove?: SC_GameMoveAction;
    onAcceptMovement?: () => void;
}

const GameHeader: React.FC<GameHeaderProps> = ({
    outcome,
    localPlayerNumber,
    currentPlayerNumber,
    pendingMove,
    latestMove,
    onAcceptMovement,
}) => {
    if (outcome) {
        for (const o in outcome) {
            switch (o) {
                case 'game_finished':
                    return (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            Game finished (
                            {localPlayerNumber === Number(outcome[o].split('_')[1]) ? 'You Won!' : 'You Lost!'})
                        </Alert>
                    );
                case 'player_inactive':
                    return (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            Player inactive ({localPlayerNumber !== Number(outcome[o]) ? 'You Won!' : 'You Lost!'})
                        </Alert>
                    );
                case 'player_double_played':
                    return (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            Player double played ({localPlayerNumber !== Number(outcome[o]) ? 'You Won!' : 'You Lost!'})
                        </Alert>
                    );
            }
        }
    }
    switch (latestMove?.outcome) {
        case 'player_1_won':
        case 'player_2_won':
            return (
                <Alert variant="outlined" severity="warning" icon={false}>
                    {localPlayerNumber === Number(latestMove.outcome.split('_')[1]) ? 'You Won!' : 'You Lost!'}
                </Alert>
            );
    }
    if (pendingMove && onAcceptMovement) {
        if (localPlayerNumber === pendingMove?.playerNumber) {
            return (
                <Paper>
                    {pendingMove.outcome ? (
                        <Alert variant="outlined" severity="warning" icon={false}>
                            This move contains the following outcome: <Chip label={pendingMove.outcome} />
                        </Alert>
                    ) : (
                        <Alert variant="outlined" severity="info" icon={false}>
                            You must accept the opponent move before procceeding
                        </Alert>
                    )}
                    <Button fullWidth onClick={onAcceptMovement}>
                        Accept Oppenent Move
                    </Button>
                </Paper>
            );
        } else {
            return (
                <Alert variant="outlined" severity="info" icon={false}>
                    Waiting for the opponent to accept your move
                </Alert>
            );
        }
    }

    if (localPlayerNumber !== currentPlayerNumber) {
        return (
            <Alert variant="outlined" severity="info" icon={false}>
                It is not your turn yet
            </Alert>
        );
    }

    return (
        <Alert variant="outlined" severity="info" icon={false}>
            It is your turn
        </Alert>
    );
};

interface OwnProps {
    platform: SC_Platform;
    game: SC_Game;
    onError: (msg: string) => void;
    onSuccess: (msg: string) => void;
}

const HeadTail: React.FC<OwnProps> = ({ platform, game, onError, onSuccess }) => {
    const classes = useStyles();
    const { pkh, publicKey } = useWalletContext();
    const [onChainMove, setOnChainMove] = React.useState<HeadTailState>({
        hash1: randomNonce(),
    });
    const [offChainMove, setOffChainMove] = React.useState<HeadTailState>(onChainMove);

    const pendingMove: SC_GameMoveAction | undefined = React.useMemo(
        () => (game?.pendingMove?.length ? game.pendingMove[0] : undefined),
        [game],
    );
    const latestMove: SC_GameMoveAction | undefined = React.useMemo(
        () => (game?.latestMove?.length ? game.latestMove[0] : undefined),
        [game],
    );

    const onchainState = React.useMemo(() => {
        if (game?.state) {
            const state = HeadTailUtils.unpackState(game.state);
            state.revealed1 = (state.revealed1 as any)?.toString(10);
            state.revealed2 = (state.revealed2 as any)?.toNumber() === 1;
            return state;
        }

        return {};
    }, [game]);
    const offChainState = React.useMemo(() => {
        let state;
        if (pendingMove) {
            state = HeadTailUtils.unpackState(pendingMove.state);
        } else if (latestMove) {
            state = HeadTailUtils.unpackState(latestMove.state);
        }
        if (state) {
            state.revealed1 = (state.revealed1 as any)?.toString(10);
            state.revealed2 = (state.revealed2 as any)?.toNumber() === 1;
            return state;
        }

        return onchainState;
    }, [onchainState, latestMove]);

    const playerNumber = React.useMemo(() => game.players[pkh!], [game, pkh]);

    const handleFieldChange = React.useCallback((e: React.ChangeEvent<HTMLInputElement>, checked?: boolean) => {
        if (e.target.id.startsWith('onchain')) {
            setOnChainMove((mv) => ({
                ...mv,
                [e.target.id.replace('onchain_', '')]: e.target.id === 'onchain_revealed2' ? checked : e.target.value,
            }));
        } else if (e.target.id.startsWith('offchain')) {
            setOffChainMove((mv) => ({
                ...mv,
                [e.target.id.replace('offchain_', '')]: e.target.id === 'offchain_revealed2' ? checked : e.target.value,
            }));
        }
    }, []);

    const acceptOffchainMove = React.useCallback(async () => {
        try {
            if (game && pkh && publicKey && pendingMove) {
                const players = Object.entries(game.players).reduce<Record<number, string>>(
                    (acc, [address, number]) => ({ ...acc, [number]: address }),
                    {},
                );
                const moveData = Packers.packData(
                    pendingMove.moveNumber === 1
                        ? {
                              prim: 'Left',
                              args: [
                                  {
                                      bytes: pendingMove.move.hash1,
                                  },
                              ],
                          }
                        : {
                              prim: 'Right',
                              args: [
                                  {
                                      int:
                                          pendingMove.moveNumber === 3
                                              ? pendingMove.move.revealed1
                                              : pendingMove.move.revealed2
                                              ? '1'
                                              : '0',
                                  },
                              ],
                          },

                    {
                        prim: 'or',
                        args: [
                            { prim: 'bytes', annots: ['%hash'] },
                            { prim: 'nat', annots: ['%revealed'] },
                        ],
                        annots: ['%move_data'],
                    },
                );
                const moveResult = await OffchainViews.offchain_compute_game_play(platform, {
                    player: players[latestMove?.playerNumber || game.currentPlayer],
                    moveBytes: moveData,
                    game: {
                        modelID: game.modelID,
                        initParams: game.initParams,
                        settled: game.settled,
                        current: {
                            player: latestMove?.playerNumber || game.currentPlayer,
                            moveNumber: latestMove?.moveNumber || game.moveNumber,
                            outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                        },
                        players,
                        state: latestMove?.state ? latestMove.state : HeadTailUtils.packState({}),
                    },
                });

                if (!moveResult.state) {
                    return onError('Move is invalid');
                }

                const bytes = Packers.Game.packPlayAction(game.id, moveResult.current, moveResult.state, moveData);
                const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

                await Api.Game.acceptGameMove(game.id, moveResult.current.moveNumber, {
                    publicKey,
                    signature: signature.prefixSig,
                });
            }
        } catch (e) {
            Logger.debug(e);
            if (e?.isAxiosError && Array.isArray(e.response.data)) {
                onError(
                    e.response.data.reduce(
                        (err: string, cur: any) =>
                            cur.with ? (err ? `${err}, ${cur.with.string}` : cur.with.string) : err,
                        '',
                    ),
                );
            } else {
                onError('Something went wrong!');
            }
        }
    }, [game, pkh, publicKey]);

    const submitOffchainMove = React.useCallback(async () => {
        try {
            if (game && pkh && publicKey && offChainMove) {
                const players = Object.entries(game.players).reduce<Record<number, string>>(
                    (acc, [address, number]) => ({ ...acc, [number]: address }),
                    {},
                );
                const moveBytes = Packers.packData(
                    !offChainState.hash1 && offChainMove.hash1
                        ? {
                              prim: 'Left',
                              args: [
                                  {
                                      bytes: Hashers.hashData(
                                          Packers.packData({ int: offChainMove.hash1 }, { prim: 'nat' }),
                                      ),
                                  },
                              ],
                          }
                        : {
                              prim: 'Right',
                              args: [
                                  {
                                      int:
                                          latestMove?.moveNumber === 1
                                              ? offChainMove.revealed2
                                                  ? '1'
                                                  : '0'
                                              : offChainMove.revealed1!,
                                  },
                              ],
                          },
                    {
                        prim: 'or',
                        args: [
                            { prim: 'bytes', annots: ['%hash'] },
                            { prim: 'nat', annots: ['%revealed'] },
                        ],
                        annots: ['%move_data'],
                    },
                );
                const moveResult = await OffchainViews.offchain_compute_game_play(platform, {
                    player: pkh,
                    moveBytes,
                    game: {
                        modelID: game.modelID,
                        initParams: game.initParams,
                        settled: game.settled,
                        current: {
                            player: latestMove?.playerNumber || game.currentPlayer,
                            moveNumber: latestMove?.moveNumber || game.moveNumber,
                            outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                        },
                        players,
                        state: HeadTailUtils.packState(offChainState),
                    },
                });

                if (!moveResult.state) {
                    return onError('Move is invalid');
                }

                const bytes = Packers.Game.packPlayAction(game.id, moveResult.current, moveResult.state, moveBytes);
                const signature = await Wallet.Beacon.signer.sign(bytes, undefined, SigningType.MICHELINE);

                await Api.Game.newGameMove(game.id, {
                    current: moveResult.current,
                    state: moveResult.state,
                    moveBytes,
                    move: {
                        ...offChainMove,
                        ...(offChainMove.hash1
                            ? {
                                  hash1: Hashers.hashData(
                                      Packers.packData({ int: offChainMove.hash1 }, { prim: 'nat' }),
                                  ),
                              }
                            : {}),
                    },
                    signature: {
                        publicKey,
                        signature: signature.prefixSig,
                    },
                });
            }
        } catch (e) {
            Logger.debug(e);
            if (e?.isAxiosError && Array.isArray(e.response.data)) {
                onError(
                    e.response.data.reduce(
                        (err: string, cur: any) =>
                            cur.with ? (err ? `${err}, ${cur.with.string}` : cur.with.string) : err,
                        '',
                    ),
                );
            } else {
                onError('Something went wrong!');
            }
        } finally {
            setOffChainMove({});
        }
    }, [game, pkh, publicKey, offChainMove]);

    const playOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game && onChainMove) {
            try {
                const moveData = Packers.packData(
                    onchainState.hash1
                        ? {
                              prim: 'Right',
                              args: [
                                  {
                                      int:
                                          typeof onchainState.revealed2 !== 'undefined'
                                              ? onChainMove.revealed1!
                                              : onChainMove.revealed2
                                              ? '1'
                                              : '0',
                                  },
                              ],
                          }
                        : {
                              prim: 'Left',
                              args: [
                                  {
                                      bytes: Hashers.hashData(
                                          Packers.packData({ int: onChainMove.hash1! }, { prim: 'nat' }),
                                      ),
                                  },
                              ],
                          },
                    {
                        prim: 'or',
                        args: [
                            { prim: 'bytes', annots: ['%hash'] },
                            { prim: 'nat', annots: ['%revealed'] },
                        ],
                        annots: ['%move_data'],
                    },
                );
                // Build "play" call
                //await Wallet.Beacon.transfer(platform.id, EntryPoints.Game.play(game.id, moveData, game.moveNumber));
            } catch (e) {
                Logger.debug(e);
                onError(e.message);
            } finally {
                setOnChainMove({});
            }
        }
    }, [game, pkh, publicKey, onChainMove]);

    const updateGameStateOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game && latestMove) {
            try {
                // Call "update_state" entrypoint
                await Wallet.Beacon.transfer(
                    platform.id,
                    EntryPoints.Game.updateState(
                        game.id,
                        {
                            moveNumber: latestMove.moveNumber,
                            player: latestMove.playerNumber,
                            outcome: latestMove?.outcome ? { game_finished: latestMove.outcome } : game.outcome,
                        },
                        latestMove.state,
                        latestMove.signatures,
                    ),
                );
            } catch (e) {
                Logger.debug(e);
                onError(e.message);
            }
        }
    }, [game, latestMove]);

    const submitGameOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game) {
            try {
                // Call "new_game" entrypoint
                const constants: GameConstants = {
                    channelID: game.channelID,
                    gameNonce: game?.nonce,
                    modelID: game?.modelID,
                    playDelay: game?.playDelay,
                    settlements: game?.settlements,
                    bonds: game?.bonds,
                    players: game?.players,
                };
                await Wallet.Beacon.transfer(
                    platform.id,
                    EntryPoints.Game.newGame(game.initParams, constants, game.signatures),
                );
            } catch (e) {
                Logger.debug(e);
                onError(e.message);
            }
        }
    }, [game, latestMove]);

    const settleOnChain = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game) {
            try {
                // Call "settle" entrypoint
                await Wallet.Beacon.transfer(platform.id, EntryPoints.Game.settle(game.id));
            } catch (e) {
                Logger.debug(e);
                onError(e.message);
            }
        }
    }, [game]);

    const denounceDoublePlay = React.useCallback(async () => {
        onError('');
        onSuccess('');
        if (game) {
            try {
                const opponent = Object.keys(game.players).find((addr) => addr !== pkh);
                if (!opponent) {
                    return onError('Could not get opponent number');
                }

                let sig = '';
                let state = '';
                if (pendingMove) {
                    sig = pendingMove.signatures.find((s) => !!s.publicKey)?.signature || '';
                    state = pendingMove.state;
                } else if (latestMove) {
                    sig = latestMove.signatures.find((s) => s.publicKey !== publicKey)?.signature || '';
                    state = latestMove.state;
                }

                // Call "dispute_double_signed" entrypoint
                await Wallet.Beacon.transfer(
                    platform.id,
                    EntryPoints.Game.disputeDoubleSigned(game.id, game.players[opponent], {
                        sig,
                        state,
                    }),
                );
            } catch (e) {
                Logger.debug(e);
                onError(e.message);
            }
        }
    }, []);

    return (
        <Grid container alignItems="stretch" spacing={1}>
            <Grid xs={6} item display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                <Typography variant="overline">On-Chain State (Move Number: {game.moveNumber})</Typography>
                <Grid container justifyContent="center" flexGrow={1} sx={{ padding: 1 }} component={Paper}>
                    <Grid item xs={12}>
                        <GameHeader
                            outcome={game?.outcome}
                            localPlayerNumber={game.players[pkh!]}
                            currentPlayerNumber={game.currentPlayer}
                        />
                    </Grid>
                    <div className={classes.divider} />
                    <Grid item xs={12}>
                        <TextField
                            margin="normal"
                            fullWidth
                            id={'onchain_hash1'}
                            value={onchainState.hash1 || onChainMove.hash1}
                            label={'Commitment'}
                            variant="outlined"
                            focused
                            disabled={
                                !!game.outcome ||
                                !!onchainState.hash1 ||
                                game.currentPlayer !== 1 ||
                                game.currentPlayer !== playerNumber
                            }
                            onChange={handleFieldChange}
                        />
                    </Grid>
                    <Grid item xs={12} display="flex" justifyContent="center">
                        <HeadTailSwitch
                            id={'onchain_revealed2'}
                            checked={
                                typeof onChainMove.revealed2 !== 'undefined'
                                    ? onChainMove.revealed2
                                    : offChainState.revealed2
                            }
                            onChange={handleFieldChange}
                            disabled={
                                !!game.outcome ||
                                !!onchainState.revealed2 ||
                                game.currentPlayer !== 2 ||
                                game.currentPlayer !== playerNumber
                            }
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            margin="normal"
                            fullWidth
                            id={'onchain_revealed1'}
                            value={onchainState.revealed1 || onChainMove.revealed1}
                            label={'Reveal Commitment'}
                            variant="outlined"
                            type="number"
                            focused
                            inputProps={{
                                min: 0,
                            }}
                            disabled={
                                !!game.outcome ||
                                !onchainState.hash1 ||
                                !!onchainState.revealed1 ||
                                game.currentPlayer !== 1 ||
                                game.currentPlayer !== playerNumber
                            }
                            onChange={handleFieldChange}
                        />
                    </Grid>

                    <div className={classes.divider} />
                    <Grid item xs={12} display="flex" direction="column" justifyContent="center">
                        <Button
                            fullWidth
                            onClick={playOnChain}
                            disabled={!!game?.outcome || JSON.stringify(onchainState) === JSON.stringify(onChainMove)}
                        >
                            Submit move (On-Chain)
                        </Button>
                        <Divider flexItem orientation="horizontal" sx={{ margin: 0.3 }} />
                        <Button onClick={submitGameOnChain} fullWidth disabled={game.onChain}>
                            Submit game (On-Chain)
                        </Button>
                        <Divider flexItem orientation="horizontal" sx={{ margin: 0.3 }} />
                        <Button onClick={updateGameStateOnChain} fullWidth disabled={!game.onChain || !latestMove}>
                            Update game state (On-Chain)
                        </Button>
                        <Divider flexItem orientation="horizontal" sx={{ margin: 0.3 }} />
                        <Button onClick={settleOnChain} fullWidth disabled={game.settled || !game?.outcome}>
                            Settle game (On-Chain)
                        </Button>
                        <Divider flexItem orientation="horizontal" sx={{ margin: 0.3 }} />
                        {/* <Button onClick={denounceDoublePlay} fullWidth disabled={!!game?.outcome}>
                            Denounce double play
                        </Button> */}
                    </Grid>
                </Grid>
            </Grid>
            <Grid xs={6} item display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                <Typography variant="overline">
                    Off-Chain State (Move Number: {latestMove?.moveNumber || 'N/A'})
                </Typography>
                <Grid container justifyContent="center" flexGrow={1} sx={{ padding: 1 }} component={Paper}>
                    <Grid item xs={12}>
                        <GameHeader
                            outcome={game?.outcome}
                            pendingMove={pendingMove}
                            latestMove={latestMove}
                            localPlayerNumber={game.players[pkh!]}
                            currentPlayerNumber={latestMove?.playerNumber || game.currentPlayer}
                            onAcceptMovement={acceptOffchainMove}
                        />
                    </Grid>
                    <div className={classes.divider} />
                    <Grid item xs={12}>
                        <TextField
                            margin="normal"
                            fullWidth
                            id={'offchain_hash1'}
                            value={offChainState.hash1 || offChainMove.hash1}
                            label={'Commitment'}
                            variant="outlined"
                            focused
                            disabled={!!game.outcome || !!offChainState.hash1}
                            onChange={handleFieldChange}
                        />
                    </Grid>
                    <Grid item xs={12} display="flex" justifyContent="center">
                        <HeadTailSwitch
                            id={'offchain_revealed2'}
                            checked={
                                typeof offChainMove.revealed2 !== 'undefined'
                                    ? offChainMove.revealed2
                                    : offChainState.revealed2
                            }
                            onChange={handleFieldChange}
                            disabled={
                                !!game.outcome ||
                                !offChainState.hash1 ||
                                !!offChainState.revealed2 ||
                                latestMove?.playerNumber !== 2 ||
                                latestMove?.playerNumber !== playerNumber
                            }
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            margin="normal"
                            fullWidth
                            id={'offchain_revealed1'}
                            value={offChainState.revealed1 || offChainMove.revealed1}
                            label={'Reveal Commitment'}
                            variant="outlined"
                            type="number"
                            focused
                            inputProps={{
                                min: 0,
                            }}
                            disabled={
                                !!game.outcome ||
                                !offChainState.revealed2 ||
                                !!offChainState.revealed1 ||
                                latestMove?.playerNumber !== 1 ||
                                latestMove?.playerNumber !== playerNumber
                            }
                            onChange={handleFieldChange}
                        />
                    </Grid>
                    <div className={classes.divider} />
                    <Grid item xs={12}>
                        <Button
                            fullWidth
                            onClick={submitOffchainMove}
                            disabled={JSON.stringify(offChainState) === JSON.stringify(offChainMove)}
                        >
                            Submit Move (Off-Chain)
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default HeadTail;
