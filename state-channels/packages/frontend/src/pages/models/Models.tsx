import React from 'react';

import { Container, Grid, Theme } from '@mui/material';
import { makeStyles, createStyles } from '@mui/styles';
import AddIcon from '@mui/icons-material/Add';

import RouterFab from '../../components/base/RouterFab';
import Table from './components/Table';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        divider: {
            margin: theme.spacing(3),
        },
    }),
);

const Models: React.FC = () => {
    const classes = useStyles();
    return (
        <>
            <Grid item justifyContent="center" alignItems="center">
                <RouterFab variant="extended" to="/new-model" color="primary">
                    <AddIcon sx={{ mr: 1 }} />
                    New model
                </RouterFab>
            </Grid>
            <div className={classes.divider} />
            <Container maxWidth="md">
                <Table />
            </Container>
        </>
    );
};

export default Models;
