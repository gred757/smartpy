import React from 'react';
import { useSubscription } from '@apollo/client';
import {
    styled,
    TableCell,
    TableHead as MuiTableHead,
    TableBody as MuiTableBody,
    TableRow as MuiTableRow,
    Box,
    Typography,
} from '@mui/material';
import { SC_Model, Time } from 'state-channels-common';

import Table from 'src/components/base/Table';
import CopyButton from 'src/components/base/CopyButton';
import { copyToClipboard } from 'src/utils/clipboard';
import { Subscription } from 'src/services/graphql';
import useAppContext from 'src/hooks/useAppContext';

const StyledTableRow = styled<any>(MuiTableRow)(({ theme }) => ({
    textDecoration: 'none',
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td': {
        border: 0,
    },
}));

const ModelRow = ({ model }: { model: SC_Model }) => {
    return (
        <StyledTableRow>
            <TableCell component="th" scope="row">
                {model.metadata.name}
            </TableCell>
            <TableCell component="th" scope="row">
                <CopyButton
                    onClick={() => copyToClipboard(model.id)}
                    label={model.id}
                    sx={{ maxWidth: 200 }}
                ></CopyButton>
            </TableCell>
            <TableCell component="th" scope="row" align="right">
                {model.outcomes.length}
            </TableCell>
            <TableCell component="th" scope="row" align="right">
                {Time.prettifyTimestamp(model.updatedAt)}
            </TableCell>
            <TableCell component="th" scope="row" align="right">
                {Time.prettifyTimestamp(model.createdAt)}
            </TableCell>
        </StyledTableRow>
    );
};

const TableTemplate = (Component: React.FC) => () =>
    (
        <Table
            header={
                <MuiTableHead>
                    <MuiTableRow>
                        <TableCell>Name</TableCell>
                        <TableCell>Identifier</TableCell>
                        <TableCell align="right">Outcomes</TableCell>
                        <TableCell align="right">Last Update</TableCell>
                        <TableCell align="right">Created Update</TableCell>
                    </MuiTableRow>
                </MuiTableHead>
            }
            body={<MuiTableBody>{<Component />}</MuiTableBody>}
        />
    );

const ModelsTable: React.FC = () => {
    const { platform } = useAppContext();
    const { loading, error, data } = useSubscription<{ models: SC_Model[] }, { platform: string }>(
        Subscription.Model.MODELS,
        {
            variables: { platform: platform.id },
        },
    );
    if (loading) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            Loading...
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }
    if (error) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            {error?.message || 'Something went wrong'}
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }
    if (!data?.models?.length) {
        return (
            <MuiTableRow>
                <TableCell colSpan={6}>
                    <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                        <Typography variant="overline" textAlign="center">
                            No models yet
                        </Typography>
                    </Box>
                </TableCell>
            </MuiTableRow>
        );
    }

    return (
        <>
            {data.models
                .filter(({ metadata }) => !!metadata.name)
                .map((model) => (
                    <ModelRow key={model.id} model={model} />
                ))}
        </>
    );
};

export default TableTemplate(ModelsTable);
