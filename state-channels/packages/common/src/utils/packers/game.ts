import { MichelsonType, packDataBytes } from '@taquito/michel-codec';
import { MichelsonBuilders } from '..';

import { GameID, GameConstants } from '../../@types/game';
import { buildID, buildNewGameAction, buildOutcome } from '../michelsonBuilders/game';

/**
 * @description Pack a game state action
 * @param {string} initParams Game initialization parameters
 * @param {GameConstants} constants Game constantants
 * @returns {string} Packed new game action
 */
export function packNewGameAction(initParams: string, constants: GameConstants): string {
    const type: MichelsonType = {
        prim: 'pair',
        args: [
            { prim: 'string' },
            {
                prim: 'pair',
                args: [
                    {
                        prim: 'map',
                        args: [{ prim: 'int' }, { prim: 'map', args: [{ prim: 'nat' }, { prim: 'nat' }] }],
                        annots: ['%bonds'],
                    },
                    { prim: 'bytes', annots: ['%channel_id'] },
                    { prim: 'string', annots: ['%game_nonce'] },
                    { prim: 'bytes', annots: ['%model_id'] },
                    { prim: 'int', annots: ['%play_delay'] },
                    { prim: 'map', args: [{ prim: 'int' }, { prim: 'address' }], annots: ['%players_addr'] },
                    {
                        prim: 'map',
                        args: [
                            {
                                prim: 'or',
                                args: [
                                    { prim: 'string', annots: ['%game_finished'] },
                                    {
                                        prim: 'or',
                                        args: [
                                            { prim: 'int', annots: ['%player_double_played'] },
                                            { prim: 'int', annots: ['%player_inactive'] },
                                        ],
                                    },
                                ],
                            },
                            {
                                prim: 'list',
                                args: [
                                    {
                                        prim: 'pair',
                                        args: [
                                            {
                                                prim: 'map',
                                                args: [{ prim: 'nat' }, { prim: 'nat' }],
                                                annots: ['%bonds'],
                                            },
                                            { prim: 'int', annots: ['%receiver'] },
                                            { prim: 'int', annots: ['%sender'] },
                                        ],
                                    },
                                ],
                            },
                        ],
                        annots: ['%settlements'],
                    },
                ],
                annots: ['%constants'],
            },
            { prim: 'bytes', annots: ['%params'] },
        ],
    };
    return packDataBytes(buildNewGameAction(initParams, constants), type).bytes;
}

export function packID(data: GameID): string {
    const type: MichelsonType = {
        prim: 'pair',
        args: [
            { prim: 'bytes' },
            {
                prim: 'pair',
                args: [{ prim: 'bytes' }, { prim: 'string' }],
            },
        ],
    };
    return packDataBytes(buildID(data), type).bytes;
}

/**
 * @description Pack a game state action
 * @param {string} gameID The game identifier (in bytes)
 * @param current
 * @param {string} state packed game
 * @returns {string} packed new state action
 */
export function packPlayAction(
    gameID: string,
    current: { moveNumber: number; player: number; outcome?: Record<string, string> },
    state: string,
    moveBytes: string,
): string {
    return packDataBytes(
        {
            prim: 'Pair',
            args: [
                { string: 'Play' },
                { bytes: gameID },
                {
                    prim: 'Pair',
                    args: [
                        { int: String(current.moveNumber), annots: ['%move_nb'] },
                        buildOutcome(current.outcome),
                        { int: String(current.player), annots: ['%player'] },
                    ],
                },
                { bytes: state },
                { bytes: moveBytes },
            ],
        },
        {
            prim: 'pair',
            args: [
                { prim: 'string' },
                { prim: 'bytes' },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat', annots: ['%move_nb'] },
                        {
                            prim: 'option',
                            args: [
                                {
                                    prim: 'or',
                                    args: [
                                        { prim: 'string', annots: ['%game_finished'] },
                                        {
                                            prim: 'or',
                                            args: [
                                                { prim: 'int', annots: ['%player_double_played'] },
                                                { prim: 'int', annots: ['%player_inactive'] },
                                            ],
                                        },
                                    ],
                                },
                            ],
                            annots: ['%outcome'],
                        },
                        { prim: 'int', annots: ['%player'] },
                    ],
                },
                { prim: 'bytes' },
                { prim: 'bytes' },
            ],
        },
    ).bytes;
}

/**
 * @description Pack game initial parameters
 * @param type Michelson type
 * @param args Game initial parameters, they are mapped to each annotation
 * @returns
 */
export function packInitParams(type: MichelsonType, args: Record<string, unknown> = {}): string {
    return packDataBytes(MichelsonBuilders.buildDataFromType(type as any, args), type).bytes;
}
