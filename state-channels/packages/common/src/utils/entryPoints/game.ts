import { MichelsonData } from '@taquito/michel-codec';
import { Comparison, MichelsonBuilders } from '..';
import { GameConstants, GameCurrent, GameSignature } from '../../@types/game';
import { buildOutcome } from '../michelsonBuilders/game';

/**
 * @description Build parameters for "new_game" entry point call
 * @param initParams Game initialization parameters
 * @param constants Game constants
 * @param signatures Game initialization signatures
 * @returns
 */
export function newGame(
    initParams: string,
    constants: GameConstants,
    signatures: GameSignature[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    const constantsMichelson: MichelsonData = MichelsonBuilders.Game.buildConstants(constants);
    return {
        entrypoint: 'new_game',
        value: {
            prim: 'Pair',
            args: [
                constantsMichelson,
                {
                    prim: 'Pair',
                    args: [
                        { bytes: initParams },
                        signatures
                            .sort(({ publicKey: pk1 }, { publicKey: pk2 }) => Comparison.compare(pk1, pk2))
                            .map(({ publicKey, signature }) => ({
                                prim: 'Elt',
                                args: [
                                    { string: publicKey },
                                    {
                                        string: signature,
                                    },
                                ],
                            })),
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "update_state" entry point call
 * @param gameID
 * @param current
 * @param state
 * @param signatures
 * @returns
 */
export function updateState(
    gameID: string,
    current: GameCurrent,
    state: string,
    signatures: GameSignature[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'game_set_state',
        value: {
            prim: 'Pair',
            args: [
                {
                    prim: 'Pair',
                    args: [
                        {
                            bytes: gameID,
                        },
                        {
                            prim: 'Pair',
                            args: [
                                { int: String(current.moveNumber) },
                                {
                                    prim: 'Pair',
                                    args: [buildOutcome(current.outcome), { int: String(current.player) }],
                                },
                            ],
                        },
                    ],
                },
                {
                    prim: 'Pair',
                    args: [
                        { bytes: state },
                        signatures
                            .sort(({ publicKey: pk1 }, { publicKey: pk2 }) => Comparison.compare(pk1, pk2))
                            .map(({ publicKey, signature }) => ({
                                prim: 'Elt',
                                args: [
                                    { string: publicKey },
                                    {
                                        string: signature,
                                    },
                                ],
                            })),
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "settle" entry point call
 * @param gameID
 * @returns
 */
export function settle(
    gameID: string,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'game_settle',
        value: {
            bytes: gameID,
        },
    };
}

/**
 * @description Build parameters for "starving" entry point call
 * @param gameID
 * @param flag
 * @returns
 */
export function starving(
    gameID: string,
    flag: boolean,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'dispute_starving',
        value: {
            prim: 'Pair',
            args: [{ prim: flag ? 'True' : 'False' }, { bytes: gameID }],
        },
    };
}

/**
 * @description Build parameters for "starved" entry point call
 * @param gameID
 * @param playerID
 * @returns
 */
export function starved(
    gameID: string,
    playerID: number,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'dispute_starved',
        value: {
            prim: 'Pair',
            args: [{ bytes: gameID }, { int: String(playerID) }],
        },
    };
}

/**
 * @description Build parameters for "play" entry point call
 * @param {string} gameID
 * @param {string} moveData
 * @param {number} moveNumber
 * @returns
 */
export function play(
    gameID: string,
    moveData: string,
    new_state: string,
    current: GameCurrent,
    signatures: GameSignature[],
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'game_play',
        value: {
            prim: 'Pair',
            args: [
                { bytes: gameID },
                {
                    prim: 'Pair',
                    args: [
                        { bytes: moveData },
                        {
                            prim: 'Pair',
                            args: [
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: String(current.moveNumber) },
                                        {
                                            prim: 'Pair',
                                            args: [buildOutcome(current.outcome), { int: String(current.player) }],
                                        },
                                    ],
                                },
                                {
                                    prim: 'Pair',
                                    args: [
                                        { bytes: new_state },
                                        signatures
                                            .sort(({ publicKey: pk1 }, { publicKey: pk2 }) =>
                                                Comparison.compare(pk1, pk2),
                                            )
                                            .map(({ publicKey, signature }) => ({
                                                prim: 'Elt',
                                                args: [
                                                    { string: publicKey },
                                                    {
                                                        string: signature,
                                                    },
                                                ],
                                            })),
                                    ],
                                },
                            ],
                        },
                    ],
                },
            ],
        },
    };
}

/**
 * @description Build parameters for "dispute_double_signed" entry point call
 * @param {string} gameID
 * @param {number} playerID
 * @param settlement1
 * @returns entrypoint call parameters
 */
export function disputeDoubleSigned(
    gameID: string,
    playerID: number,
    settlement1: { sig: string; state: string },
): {
    entrypoint: string;
    value: MichelsonData;
} {
    /**
     * @TODO This entrypoint was also changed (many parameters are necessary now and I don't like it)
     */
    return {
        entrypoint: 'dispute_double_signed',
        value: {
            prim: 'Pair',
            args: [
                {
                    prim: 'Pair',
                    args: [
                        {
                            bytes: gameID,
                        },
                        {
                            int: String(playerID),
                        },
                    ],
                },
                {
                    prim: 'Pair',
                    args: [
                        {
                            prim: 'Pair',
                            args: [
                                {
                                    string: settlement1.sig,
                                },
                                {
                                    bytes: settlement1.state,
                                },
                            ],
                        },
                        {
                            prim: 'None',
                        },
                    ],
                },
            ],
        },
    };
}
