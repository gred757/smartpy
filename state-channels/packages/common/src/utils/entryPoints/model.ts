import type { MichelsonData } from '@taquito/michel-codec';
import { Comparison } from '..';
import type { SC_NewModelRequest } from '../../@types/model';

/**
 * @description Build parameters for "admin_new_model" entry point call
 * @param model
 * @returns
 */
export function newModel(
    model: SC_NewModelRequest,
): {
    entrypoint: string;
    value: MichelsonData;
} {
    return {
        entrypoint: 'admin_new_model',
        value: {
            prim: 'Pair',
            args: [
                Object.entries(model.metadata)
                    .sort(([n1], [n2]) => Comparison.compare(n1, n2))
                    .map(([key, value]) => ({
                        prim: 'Elt',
                        args: [{ string: key }, { bytes: value }],
                    })),
                {
                    prim: 'Pair',
                    args: [
                        {
                            bytes: model.modelBytes,
                        },
                        {
                            prim: 'Pair',
                            args: [[], []],
                        },
                    ],
                },
            ],
        },
    };
}
