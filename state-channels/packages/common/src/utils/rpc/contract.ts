import { Constants } from '../../constants';
import Http from '../../services/http';

/**
 * @description Get public key of a given contract/account address
 * @param {string} address contract/account address (tz{1,2,3}|KT1)
 * @returns {string} public key
 */
export const getManagerKey = async (address: string): Promise<string> =>
    (await Http.get(`${Constants.rpc}/chains/main/blocks/head/context/contracts/${address}/manager_key`)).data;

/**
 * @description Get contract storage normalized
 * @param {string} address contract address from where the storage will be extracted
 * @returns {Promise<Record<string, unknown>>} contract storage
 */
export async function getNormalizedStorage(address: string): Promise<Record<string, unknown>> {
    return (
        await Http.post(
            `${Constants.rpc}/chains/main/blocks/head/context/contracts/${address}/storage/normalized`,
            {
                unparsing_mode: 'Optimized_legacy',
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        )
    ).data;
}

/**
 * @description Get contract script
 * @param {string} address contract address from where the script will be extracted
 * @returns {Promise<Record<string, unknown>>} contract script
 */
export async function getScript(
    address: string,
): Promise<{
    code: [{ prim: 'storage'; args: any[] }, { prim: 'parameter'; args: any[] }, { prim: 'code'; args: any[] }];
}> {
    return (
        await Http.get(`${Constants.rpc}/chains/main/blocks/head/context/contracts/${address}/script`, {
            headers: {
                'Content-Type': 'application/json',
            },
        })
    ).data;
}
