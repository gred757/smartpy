import { blake2bHex } from 'blakejs';

export * as Channel from './channel';
export * as Model from './model';
export * as Game from './game';

export function hashData(data: string): string {
    return blake2bHex(Buffer.from(data, 'hex'), undefined, 32);
}
