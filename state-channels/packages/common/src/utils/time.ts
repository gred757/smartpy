import { Temporal as Time } from '@js-temporal/polyfill';

const TIME_ZONE = Time.TimeZone.from('UTC');

export const SECOND = 1000;
export const MINUTE = 60 * SECOND;
export const HOUR = 60 * MINUTE;
export const DAY = 24 * HOUR;

export const prettifyMilliseconds = (milliseconds: number): string => {
    let elapsedTimeString = '';
    try {
        // DAYS
        const days = Math.floor(milliseconds / DAY);
        if (days) {
            elapsedTimeString += `${days}d`;
        }
        // HOURS
        const hours = Math.floor((milliseconds % DAY) / HOUR);
        if (hours) {
            elapsedTimeString += `${elapsedTimeString.length > 0 ? ' ' : ''}`;
            elapsedTimeString += `${hours}h`;
        }
        // MINUTES
        const minutes = Math.floor(((milliseconds % DAY) % HOUR) / MINUTE);
        if (minutes) {
            elapsedTimeString += `${elapsedTimeString.length > 0 ? ' ' : ''}`;
            elapsedTimeString += `${minutes}m`;
        }
        // SECONDS
        const seconds = Math.floor((((milliseconds % DAY) % HOUR) % MINUTE) / SECOND);
        if (seconds) {
            elapsedTimeString += `${elapsedTimeString.length > 0 ? ' ' : ''}`;
            elapsedTimeString += `${seconds}s`;
        }
    } catch (e) {
        // INVALID DATE
    }

    return elapsedTimeString;
};

export const getDuration = (milliseconds: number): string => {
    let timeString = '';
    try {
        if (!milliseconds || milliseconds < 0) {
            return '0s';
        }
        // DAYS
        const days = Math.floor(milliseconds / DAY);
        if (days) {
            timeString += `${days}d`;
        }
        // HOURS
        const hours = Math.floor((milliseconds % DAY) / HOUR);
        if (hours) {
            timeString += `${timeString.length > 0 ? ' ' : ''}`;
            timeString += `${hours}h`;
        }
        // MINUTES
        const minutes = Math.floor(((milliseconds % DAY) % HOUR) / MINUTE);
        if (minutes) {
            timeString += `${timeString.length > 0 ? ' ' : ''}`;
            timeString += `${minutes}m`;
        }
        // SECONDS
        const seconds = Math.floor((((milliseconds % DAY) % HOUR) % MINUTE) / SECOND);
        if (seconds) {
            timeString += `${timeString.length > 0 ? ' ' : ''}`;
            timeString += `${seconds}s`;
        }
    } catch (e) {
        // INVALID DATE
        console.error(e);
    }

    return timeString;
};

export const getCurrentInstant = (): Time.Instant => TIME_ZONE.getInstantFor(Time.Now.instant().toString());

export const instantFrom = (dateTime: number | string | Time.PlainDateTime | Time.DateTimeLike): Time.Instant => {
    if (typeof dateTime === 'number') {
        return Time.Instant.fromEpochSeconds(dateTime).toZonedDateTimeISO('UTC').toInstant();
    }
    return TIME_ZONE.getInstantFor(dateTime);
};

export const prettifyTimestamp = (dateTime: string | Time.PlainDateTime | Time.DateTimeLike): string => {
    const target = TIME_ZONE.getPlainDateTimeFor(instantFrom(dateTime));
    const now = TIME_ZONE.getPlainDateTimeFor(Time.Now.instant());

    if (target.year !== now.year || target.dayOfYear !== now.dayOfYear) {
        return target.toPlainDate().toLocaleString();
    }
    return target.toPlainTime().toLocaleString();
};
