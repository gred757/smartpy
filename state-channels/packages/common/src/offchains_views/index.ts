import { MichelsonMap, Schema } from '@taquito/michelson-encoder';
import type BigNumber from 'bignumber.js';
import Http from '../services/http';
import type { GameConstants, GameSignature, SC_GameOfModel, SC_Platform } from '../@types';
import { Comparison, MichelsonBuilders, RPC } from '../utils';

export async function offchain_new_game(
    platform: SC_Platform,
    constants: GameConstants,
    initParams: string,
    signatures: GameSignature[],
): Promise<SC_GameOfModel> {
    const input = {
        prim: 'Pair',
        args: [
            MichelsonBuilders.Game.buildConstants(constants),
            {
                prim: 'Pair',
                args: [
                    { bytes: initParams },
                    signatures
                        .sort(({ publicKey: pk1 }, { publicKey: pk2 }) => Comparison.compare(pk1, pk2))
                        .map((signature) => ({
                            prim: 'Elt',
                            args: [
                                { string: signature.publicKey },
                                {
                                    string: signature.signature,
                                },
                            ],
                        })),
                ],
            },
        ],
    };

    const viewJSON = (
        await Http.get<{ views: { name: string; implementations: { michelsonStorageView: any }[] }[] }>(
            platform.metadata,
        )
    )?.data.views.find(({ name }) => name === 'offchain_new_game')?.implementations?.[0]?.michelsonStorageView;

    const chainID = await RPC.Chain.getChainID();
    const storage = await RPC.Contract.getNormalizedStorage(platform.id);
    const storageType = (await RPC.Contract.getScript(platform.id)).code[0].args[0];
    const result = await RPC.Helpers.runCode(
        storage,
        [
            { prim: 'CAR' },
            viewJSON.code,
            { prim: 'SOME' },
            { prim: 'NIL', args: [{ prim: 'operation' }] },
            { prim: 'PAIR' },
        ],
        {
            prim: 'pair',
            args: [viewJSON.parameter, storageType],
        },
        {
            prim: 'option',
            args: [viewJSON.returnType],
        },
        input,
        chainID,
    );

    const resultSchema = new Schema({
        prim: 'option',
        args: [viewJSON.returnType],
    });
    const viewResult = resultSchema.Execute(result.storage);
    const getPlayers = (playersMap: MichelsonMap<string, BigNumber>): Record<number, string> => {
        const players: Record<number, string> = {};
        for (const [key, value] of playersMap.entries()) {
            players[value.toNumber()] = key;
        }
        return players;
    };

    return {
        settled: viewResult.settled,
        initParams: viewResult.init_input,
        modelID: viewResult.constants.model_id,
        current: {
            moveNumber: viewResult.current.move_nb.toNumber(),
            player: viewResult.current.player.toNumber(),
            outcome: viewResult.current.outcome,
        },
        players: getPlayers(viewResult.addr_players),
    };
}

export async function offchain_compute_game_play(
    platform: SC_Platform,
    params: {
        player: string;
        moveBytes: string;
        game: SC_GameOfModel;
    },
): Promise<SC_GameOfModel> {
    const input = {
        prim: 'Pair',
        args: [
            {
                prim: 'Pair',
                args: [
                    {
                        prim: 'Pair',
                        args: [
                            {
                                bytes: params.game.modelID,
                            },
                            Object.entries(params.game.players).map(([index, address]) => ({
                                prim: 'Elt',
                                args: [
                                    {
                                        int: String(index),
                                    },
                                    {
                                        string: address,
                                    },
                                ],
                            })),
                        ],
                    },
                    {
                        prim: 'Pair',
                        args: [
                            {
                                prim: 'Pair',
                                args: [
                                    {
                                        int: String(params.game.current.moveNumber),
                                    },
                                    {
                                        prim: 'Pair',
                                        args: [
                                            MichelsonBuilders.Game.buildOutcome(params.game.current.outcome),
                                            {
                                                int: String(params.game.current.player),
                                            },
                                        ],
                                    },
                                ],
                            },
                            {
                                prim: 'Pair',
                                args: [
                                    {
                                        bytes: params.game.initParams,
                                    },
                                    params.game.state
                                        ? {
                                              prim: 'Some',
                                              args: [
                                                  {
                                                      bytes: params.game.state,
                                                  },
                                              ],
                                          }
                                        : { prim: 'None' },
                                ],
                            },
                        ],
                    },
                ],
            },
            {
                prim: 'Pair',
                args: [
                    { bytes: params.moveBytes },
                    {
                        string: params.game.players[params.game.current.player],
                    },
                ],
            },
        ],
    };

    const viewJSON = (
        await Http.get<{ views: { name: string; implementations: { michelsonStorageView: any }[] }[] }>(
            platform.metadata,
        )
    )?.data.views.find(({ name }) => name === 'offchain_compute_game_play')?.implementations?.[0]?.michelsonStorageView;

    const chainID = await RPC.Chain.getChainID();
    const storage = await RPC.Contract.getNormalizedStorage(platform.id);
    const storageType = (await RPC.Contract.getScript(platform.id)).code[0].args[0];
    const result = await RPC.Helpers.runCode(
        storage,
        [
            { prim: 'CAR' },
            viewJSON.code,
            { prim: 'SOME' },
            { prim: 'NIL', args: [{ prim: 'operation' }] },
            { prim: 'PAIR' },
        ],
        {
            prim: 'pair',
            args: [viewJSON.parameter, storageType],
        },
        {
            prim: 'option',
            args: [viewJSON.returnType],
        },
        input,
        chainID,
    );

    const getPlayers = (playersMap: MichelsonMap<number, string>): Record<number, string> => {
        const players: Record<number, string> = {};
        for (const [key, value] of playersMap.entries()) {
            players[key] = value;
        }
        return players;
    };
    const resultSchema = new Schema({
        prim: 'option',
        args: [viewJSON.returnType],
    });
    const { init_input, settled, model_id, state, current, constants } = resultSchema.Execute(result.storage);
    return {
        settled,
        initParams: init_input,
        modelID: model_id,
        current: {
            moveNumber: current.move_nb.toNumber(),
            player: current.player.toNumber(),
            outcome: current.outcome,
        },
        players: getPlayers(constants.players_addr),
        state,
    };
}
