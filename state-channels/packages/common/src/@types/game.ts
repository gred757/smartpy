import { SC_Model } from './model';

export enum SC_SettlementKind {
    GameFinished = 'game_finished',
    GameAborted = 'game_aborted',
    PlayerInactive = 'player_inactive',
    PlayerDoublePlayed = 'player_double_played',
}

export const SC_SettlementOrder = {
    [SC_SettlementKind.GameFinished]: 1,
    [SC_SettlementKind.GameAborted]: 2,
    [SC_SettlementKind.PlayerDoublePlayed]: 3,
    [SC_SettlementKind.PlayerInactive]: 4,
};

export interface SC_Settlement {
    kind: SC_SettlementKind;
    value?: number | string;
    bonds: {
        [tokenID: number]: number /* amount */;
    };
    sender?: number;
    recipient?: number;
}

export interface GameID {
    channelID: string;
    modelID: string;
    gameNonce: string;
}

export interface GameConstants {
    channelID: string;
    modelID: string;
    gameNonce: string;
    playDelay: number;
    bonds: Record<number, { [bound: number]: number }>;
    players: {
        [address: string]: number;
    };
    settlements: SC_Settlement[];
}

export interface NewGameRequest {
    signature: GameSignature;
    initParams: string;
    constants: GameConstants;
}

export interface NewGameStateRequest {
    signature: GameSignature;
    move: any;
    moveBytes: string;
    current: GameCurrent;
    state: string;
}

export interface AcceptGameStateRequest {
    signature: GameSignature;
    moveNumber: number;
}

export interface GameSignature {
    publicKey: string;
    signature: string;
}

export interface GameCurrent {
    player: number;
    moveNumber: number;
    outcome?: Record<string, string>;
}

export interface SC_GameOfModel {
    settled: boolean;
    initParams: string;
    modelID: string;
    current: GameCurrent;
    players: Record<number, string>;
    state?: string;
}

export interface SC_GameMoveAction {
    id: number;
    moveNumber: number;
    playerNumber: number;
    outcome: string;
    state: string;
    move: any;
    signatures: GameSignature[];
}
export interface SC_Game {
    id: string;
    onChain: boolean;
    settled: boolean;
    state?: string;
    currentPlayer: number;
    metadata?: any;
    moveNumber: number;
    outcome?: Record<string, string>;
    timeouts: {
        createdAt: string;
        timeout: string;
        playerID: number;
        updatedAt: string;
    }[];
    signatures: GameSignature[];
    // constants
    channelID: string;
    modelID: string;
    nonce: string;
    initParams: string;
    players: Record<string, number>;
    settlements: SC_Settlement[];
    bonds: Record<number, { [bound: number]: number }>;
    playDelay: number;
    // audit fields
    updatedAt: string;
    createdAt: string;
    model: SC_Model;
    latestMove: any[];
    pendingMove: any[];
}
