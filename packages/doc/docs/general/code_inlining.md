# Code Inlining

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

Inlining allows for replacing a function call at a given location with the function's body being called. This approach enables the user to reduce code duplication.

For more extensive functions, [sp.TLambda](/types/lambdas) should be used instead because inline expansion will produce bigger contracts.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Inlining allows for replacing a function call at a given location with the function's body being called. This approach enables the user to reduce code duplication.

For more extensive functions, [TLambda](/types/lambdas) should be used instead because inline expansion will produce bigger contracts.

</Snippet>

## Example

<Snippet syntax={SYNTAX.PY}>

In SmartPy all non annotated functions get inlined when called inside `entry_point` or `lambda` methods, as shown below.

```python
import smartpy as sp

admin1 = sp.address('tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm')
admin2 = sp.address('tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU')

class InlineExample(sp.Contract):
    def __init__(self):
        self.init(admins = sp.set([admin1]))

    def isAdmin(self, address):
        return self.data.admins.contains(address)

    def onlyAdmin(self):
        sp.verify(self.isAdmin(sp.sender), 'onlyAdmin')

    @sp.entry_point
    def changeAdmins(self, remove, add):
        self.onlyAdmin()
        sp.if remove.is_some():
            sp.for admin in remove.open_some().elements():
                self.data.admins.remove(admin)
        sp.if add.is_some():
            sp.for admin in add.open_some().elements():
                self.data.admins.add(admin)

@sp.add_test(name = "InlineExample")
def test():
    scenario = sp.test_scenario()
    c1 = InlineExample()
    scenario += c1
    scenario.verify(c1.data.admins.contains(admin1))

    scenario.h2('Remove and add admins')
    c1.changeAdmins(
        remove = sp.some(sp.set([admin1])),
        add = sp.some(sp.set([admin2]))
    ).run( sender = admin1 )
    scenario.verify_equal(c1.data.admins, sp.set([admin2]))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Inlining in SmartTS is achieved by annotating class methods with `@Inline` as shown below.

```typescript
interface TStorage {
    admins: TSet<TAddress>;
}

const admin1: TAddress = 'tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm';
const admin2: TAddress = 'tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU';

@Contract
export class InlineExample {
    storage: TStorage = {
        admins: [admin1],
    };

    @Inline
    isAdmin = (address: TAddress): TBool => this.storage.admins.contains(address);

    @Inline
    onlyAdmin(): void {
        Sp.verify(this.isAdmin(Sp.sender), 'onlyAdmin');
    }

    @EntryPoint
    changeAdmins(remove: TOption<TSet<TAddress>>, add: TOption<TSet<TAddress>>): void {
        this.onlyAdmin();
        if (remove.isSome()) {
            for (const admin of remove.openSome().elements()) {
                this.storage.admins.remove(admin);
            }
        }
        if (add.isSome()) {
            for (const admin of add.openSome().elements()) {
                this.storage.admins.add(admin);
            }
        }
    }
}

Dev.test({ name: 'Double Snap hook Admin Contract' }, () => {
    Scenario.h2('Originating contract');
    const c1 = Scenario.originate(new InlineExample());
    Scenario.verify(c1.storage.admins.contains(admin1));

    Scenario.h2('Remove and add admins');
    Scenario.transfer(c1.changeAdmins(Sp.some([admin1]), Sp.some([admin2])), { sender: admin1 });
    Scenario.verifyEqual(c1.storage.admins, [admin2]);
});
```

</Snippet>
