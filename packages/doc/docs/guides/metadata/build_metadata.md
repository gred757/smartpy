# Build a Metadata Object

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

See the specification is available [here](https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-16/tzip-16.md)

## Build a Metadata Object

<Snippet syntax={SYNTAX.PY}>

In the code snippet below, we have a contract example that builds a metadata object. It compiles the functions `get_x` and `get_storage` functions into Michelson to be used as off-chain views.

<Tabs
  defaultValue="code"
  values={[
    {label: 'Code', value: 'code'},
    {label: 'Metadata JSON', value: 'metadata'}
  ]}>
  <TabItem value="code">

```python
import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)

        # A python dictionary that contains metadata entries
        metadata = {
            "name": "Contract Name",
            "description": "A description about the contract",
            "version": 1,
            "views" : [self.get_x, self.get_storage],
        }

        # Helper method that builds the metadata and produces the JSON representation as an artifact.
        self.init_metadata("example1", metadata)

    @sp.offchain_view(pure = True)
    def get_x(self, params):
        """blah blah ' some documentation """
        sp.result(sp.record(a = self.data.x, b = 12 + params))

    @sp.offchain_view(doc = "The storage")
    def get_storage(self):
        sp.result(self.data.x)

@sp.add_test("test")
def test():
    s = sp.test_scenario()
    s += MyContract(x = 1)
```

  </TabItem>
  <TabItem value="metadata">

```json
{
  "name": "Contract Name",
  "description": "A description about the contract",
  "version": "1",
  "views": [
    {
      "name": "get_x",
      "pure": true,
      "description": "blah blah ' some documentation ",
      "implementations": [
        {
          "michelsonStorageView": {
            "parameter": { "prim": "int" },
            "returnType": { "prim": "pair", "args": [ { "prim": "int" }, { "prim": "int" } ] },
            "code": [ { "prim": "UNPAIR" }, { "prim": "PUSH", "args": [ { "prim": "int" }, { "int": "12" } ] }, { "prim": "ADD" }, { "prim": "SWAP" }, { "prim": "PAIR" } ]
          }
        }
      ]
    },
    { "name": "get_storage", "pure": false, "description": "The storage", "implementations": [ { "michelsonStorageView": { "returnType": { "prim": "int" }, "code": [] } } ] }
  ]
}
```

  </TabItem>
</Tabs>

The image below shows how to obtain the metadata artifact from the online IDE.

![Build metadata](/img/guides/metadata/build_metadata.gif)

</Snippet>


<Snippet syntax={SYNTAX.TS}>

Work in progress ...

</Snippet>
