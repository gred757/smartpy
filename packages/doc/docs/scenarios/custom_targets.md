# Custom Targets

import SyntaxSelector from '@theme/Syntax/Selector';
import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

A custom target is similar to a test or a compilation target but associated with another custom `kind` (a free form string).

**`@sp.add_target(name, kind)`**

#### Example

```python
@sp.add_target(name = "orig", kind = "origination")
def origin():
    scenario = sp.test_scenario()
    c1 = MyContract(x=12)
    scenario += c1
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Custom targets are not available in SmartTS.

</Snippet>
