# Returning and Binding data

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';

<Snippet syntax={SYNTAX.PY}>

This is an advanced topic and need not be looked at for regular SmartPy developers.

In a block, we can write `sp.result(expr)` to compute `expr` and prepare the local result. This data can be bound by using `sp.bind_block()`.

See reference [Bind](https://smartpy.io/ide?template=test_bind.py) template.

In this example, we define a block b, computes some value and return it.

```python
def f(self, params)
    ​b = sp.bind_block()
    ​with b:
        ​sp.if params > 12:
            ​sp.result(self.data.x + params)
        ​else:
            ​sp.result(self.data.x + 2)
    ​return b.value
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress...
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

Work in progress...

</Snippet>
