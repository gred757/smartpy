# Lists

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

The type of lists over type `t` is [sp.TList](/general/types#list)(`t`).<br/>
All elements need to be of the same type `t`.<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="list" url="https://tezos.gitlab.io/michelson-reference/#type-list"/>.

See reference [Lists](https://smartpy.io/ide?template=testLists.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

The type of lists over type `t` is [sp.TList](/general/types#list)(`t`).<br/>
All elements need to be of the same type `t`.<br/>
The corresponding type in Michelson is <MichelsonDocLink placeholder="list" url="https://tezos.gitlab.io/michelson-reference/#type-list"/>.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.list(l = ..., t = ...)`** <br />
Define a list of (optional) elements in `l` whose optional type is `t`.

Standard Python lists are also accepted. `[3, 2, 1]`, `["aa", "bb", "cc"]`.

#### Example

```python
aNatList    = sp.list([3, 2, 1], t = sp.TNat)
aStringList = sp.list(["aa", "bb", "cc"], t = sp.TString)
# Uses type inference to determine the type
anotherList = [3, 2, 1];
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`[...] as TList<t>`** <br />
Define a list of (optional) elements in `[...]` whose type is `t`.

#### Example

```typescript
const aNatList: TList<TNat> = [3, 2, 1];
const aStringList: TList<TString> = ["aa", "bb", "cc"];
```

</Snippet>

<MichelsonDocLink placeholder="NIL" url="https://tezos.gitlab.io/michelson-reference/#instr-NIL"/>

## Operations

### Push an element on top of a list

<Snippet syntax={SYNTAX.PY}>

**`<list>.push(<element>)`** <br />
Push an `element` on top of `list`.

**`sp.cons(x, xs)`** <br />
Create a new list with `x` on top of list `xs` without affecting `xs`.

#### Example

```python
self.data.my_list = [3, 2, 1]

self.data.my_list.push(4)
# self.data.my_list is now [4, 3, 2, 1]

self.data.my_list2 = sp.cons(12, self.data.my_list)
# self.data.my_list2 is now [12, 4, 3, 2, 1]
# self.data.my_list is still [4, 3, 2, 1]

```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<list>.push(<element>)`** <br />
Push an `element` on top of `list`.

**`Sp.cons(x, xs)`** <br />
Create a new list with `x` on top of list `xs` without affecting `xs`.

#### Example

```typescript
const myList: TList<TNat> = [3, 2, 1];
myList.push(4) // [4, 3, 2, 1]
```

</Snippet>


<MichelsonDocLink placeholder="CONS" url="https://tezos.gitlab.io/michelson-reference/#instr-CONS"/>

### Obtaining Size

<Snippet syntax={SYNTAX.PY}>

**`sp.len(<list>)`** <br />
Return the length of `<list>`.

#### Example

```python
size = sp.len([3, 2, 1])
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

`<list>.size()`

Return the length of `<list>`.

#### Example

```typescript
const size: TNat = ([3, 2, 1] as TList<TNat>).size()
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="SIZE" url="https://tezos.gitlab.io/michelson-reference/#instr-SIZE"/>

### Concatenation

<Snippet syntax={SYNTAX.PY}>

**`sp.concat(myList)`** <br />
Concatenate a list `myList` of [sp.TString](/general/types#string) or [sp.TBytes](/general/types#bytes).

#### Example

```python
sp.concat(["Hello", " ", "World"]) # Hello World
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.concat(myList)`** <br />
Concatenate a list `myList` of [TString](/general/types#string) or [TBytes](/general/types#bytes).

#### Example

```typescript
const list: TList<TString> = ["Hello", " ", "World"];
const result: TString = Sp.concat(list); // Hello World
```

</Snippet>

<Snippet syntax={SYNTAX.ML}>

```ocaml
Work in progress
```

</Snippet>

<MichelsonDocLink placeholder="CONCAT" url="https://tezos.gitlab.io/michelson-reference/#instr-CONCAT"/>

### Define a range

<Snippet syntax={SYNTAX.PY}>

**`sp.range(x, y, step = ...)`** <br />
A list from `x` (inclusive) to `y` (exclusive) and `step` as incrementor. Useful in conjunction with `sp.for` loops.

#### Example

```python
sp.range(1, 5, step = 1) # [1, 2, 3, 4]
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>

### Reverse a list

<Snippet syntax={SYNTAX.PY}>

**`myList.rev()`** <br />
Reverse a list.

#### Example

```python
myList       # [3, 2, 1]
myList.rev() # [1, 2, 3]
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`myList.reverse()`** <br />
Reverse a list.

```typescript
const list: TList<TNat> = [3, 2, 1].reverse(); // [1, 2, 3]
```

</Snippet>

### Match a list and expose its head and tail

<Snippet syntax={SYNTAX.PY}>

**`sp.match_cons(myList)`** <br />
Match a list and expose its head and tail if any.

See reference [Lists](https://smartpy.io/ide?template=testLists.py) template.

```python
with sp.match_cons(myList) as x1:
    self.data.head = x1.head
    self.data.tail = x1.tail
sp.else:
    self.data.head = "abc"
```

Please note that there is no way to perform random access on a list.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

```typescript
Work in progress
```

</Snippet>


## Iterate over a list

<Snippet syntax={SYNTAX.PY}>

**`sp.for x in <list>:`** <br />
To iterate on [sp.TMap](/general/types#map)(`key`, `value`) or [sp.TSet](/general/types#set)(`elem`), we first convert to an [sp.TList](/general/types#list)(..) with `<expr>.items()`, `<expr.keys()`, `expr.values()` or `expr.elements()`.

#### Example

```python
@sp.entry_point
def sum(self, params):
    self.data.result = 0
    sp.for x in params:
        self.data.result += x
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`for (const x of <list>) {}`** <br />
To iterate on [TMap](/general/types#map)(`key`, `value`) or [TSet](/general/types#set)(`elem`), we first convert to an [TList](/general/types#list)(..) with `<expr>.items()`, `<expr.keys()`, `expr.values()` or `expr.elements()`.

#### Example

```typescript
@EntryPoint
sum(param: TList<TNat>) {
    const result: TNat = 0;
    for (const x of param) {
        result += x;
    }
}
```

</Snippet>
