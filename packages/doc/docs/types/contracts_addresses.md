# Contracts and Addresses

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Following Michelson, there are two ways to point to other contracts in SmartPy:

  - typed [sp.TContract](/general/types#contract)(`t`) for contracts with an entry point of type `t`;
  - and untyped [sp.TAddress](/general/types#address).

The corresponding types in Michelson are <MichelsonDocLink placeholder="contract" url="https://tezos.gitlab.io/michelson-reference/#type-contract"/> and <MichelsonDocLink placeholder="address" url="https://tezos.gitlab.io/michelson-reference/#type-address"/>.

See [Inter-Contract Calls](https://smartpy.io/ide?template=inter_contract_calls.py) (simple), [On Chain Contract Calls - Collatz](https://smartpy.io/ide?template=collatz.py) (advanced) and [FA1.2](https://smartpy.io/ide?template=FA1.2.py) (applied, in the `balance_of` entry point) templates.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Following Michelson, there are two ways to point to other contracts in SmartPy:

  - Typed [TContract](/general/types#contract)<`t`> for contracts with an entry point of type `t`;
  - And untyped [TAddress](/general/types#address).

The corresponding types in Michelson are <MichelsonDocLink placeholder="contract" url="https://tezos.gitlab.io/michelson-reference/#type-contract"/> and <MichelsonDocLink placeholder="address" url="https://tezos.gitlab.io/michelson-reference/#type-address"/>.


See reference [On Chain Contract Calls - Collatz](https://smartpy.io/ts-ide?template=Collatz.ts) and [FA1.2](https://smartpy.io/ide?template=FA1.2.py) templates.

</Snippet>

## Literal

<Snippet syntax={SYNTAX.PY}>

**`sp.address(<address>)`** <br />
Create a literal of type [sp.TAddress](/general/types#address).

#### Example

```python
address = sp.address("tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm");
address2 = sp.address("KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3");
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`"<address>" as TAddress`** <br />
Create a literal of type [TAddress](/general/types#address).

#### Example

```typescript
const address: TAddress = "tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm";
const address2: TAddress = "KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3";
```

</Snippet>

## Global properties

### The current contract

<Snippet syntax={SYNTAX.PY}>

**`sp.self`** <br />
Get the current contract of type [sp.TContract](/general/types#contract)(`t`) for some type `t`.

#### Example

```python
contract = sp.self
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.self`** <br />
Get the current contract of type [TContract](/general/types#contract)(`t`) for some type `t`.

#### Example

```typescript
contract = Sp.self
```

</Snippet>

<MichelsonDocLink placeholder="SELF" url="https://tezos.gitlab.io/michelson-reference/#instr-SELF"/>

### The address of the current contract

<Snippet syntax={SYNTAX.PY}>

**`sp.self_address`** <br />
This is the proper way to get a contract's own address.

#### Example

```python
address = sp.self_address
```

In tests, a contract's address is accessible through the `<contract>.address` field.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.selfAddress`** <br />
This is the proper way to get a contract's own address.

#### Example

```typescript
address = Sp.selfAddress
```

In tests, a contract's address is accessible through the `<contract>.address` field.

</Snippet>

<MichelsonDocLink placeholder="SELF_ADDRESS" url="https://tezos.gitlab.io/michelson-reference/#instr-SELF_ADDRESS"/>

### The sender address

<Snippet syntax={SYNTAX.PY}>

**`sp.sender`** <br />
The address that called the current entry point.

#### Example

```python
address = sp.sender
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.sender`** <br />
The address that called the current entry point.

#### Example

```typescript
address = Sp.sender
```

</Snippet>

More information available [here](/general/block_properties).

<MichelsonDocLink placeholder="SENDER" url="https://tezos.gitlab.io/michelson-reference/#instr-SENDER"/>


### The source address

<Snippet syntax={SYNTAX.PY}>

**`sp.source`** <br />
The address that initiated the current transaction.<br/>
It may or may not be equal to `sp.sender`.

#### Example

```python
address = sp.source
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.source`** <br />
The address that initiated the current transaction.<br/>
It may or may not be equal to `Sp.sender`.

#### Example

```typescript
address = Sp.source
```

</Snippet>

More information available [here](/general/block_properties).

<MichelsonDocLink placeholder="SOURCE" url="https://tezos.gitlab.io/michelson-reference/#instr-SOURCE"/>


## Operations

### Get entry point from current contract

<Snippet syntax={SYNTAX.PY}>

**`sp.self_entry_point(entry_point = '<entry_point_name>')`** <br />
Get an entry point from the current contract, where the optional `entry_point` argument contains the entry point's name. The returned contract is of type [sp.TContract](/general/types#contract)(`t`) where `t` is the type of the entry point's parameters.

#### Example

```python
contract = sp.self_entry_point(entry_point = '<entry_point_name>')
```

If `entry_point` is empty, then the current entry point is used.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.selfEntryPoint('<entry_point_name>')`** <br />
Get an entry point from the current contract, where the optional `entry_point` argument contains the entry point's name. The returned contract is of type [TContract](/general/types#contract)(`t`) where `t` is the type of the entry point's parameters.

#### Example

```typescript
contract = Sp.selfEntryPoint(entry_point = '<entry_point_name>')
```

If `entry_point` is empty, the the current entry point is used.

</Snippet>

### Convert contract to address

<Snippet syntax={SYNTAX.PY}>

**`sp.to_address(<contract>)`** <br />
Compute the address of type [sp.TAddress](/general/types#address), from a contract of type [sp.TContract](/general/types#contract)(`t`) for some type `t`.

#### Example

```python
address = sp.to_address(contract)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.toAddress(<contract>)`** <br />
Compute the address of type [TAddress](/general/types#address), from a contract of type [TContract](/general/types#contract)(`t`) for some type `t`.

#### Example

```typescript
const address: TAddress = Sp.toAddress(contract)
```

</Snippet>

<MichelsonDocLink placeholder="ADDRESS" url="https://tezos.gitlab.io/michelson-reference/#instr-ADDRESS"/>

### Get entry point address from current contract

<Snippet syntax={SYNTAX.PY}>

**`sp.self_entry_point_address(entry_point = '<entry_point_name>')`** <br />
It is an alias for `sp.to_address(sp.self_entry_point(entry_point))`.

This is the proper way to get a contract's own address of an entry point.

#### Example

```python
address = sp.self_entry_point_address(entry_point = '<entry_point_name>')
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.selfEntryPointAddress('<entry_point_name>')`** <br />
It is an alias for `Sp.toAddress(Sp.selfEntryPoint(entry_point))`.

This is the proper way to get a contract's own address of an entry point.

#### Example

```typescript
address = Sp.selfEntryPointAddress(entry_point = '<entry_point_name>')
```

</Snippet>

### Cast an address to a typed contract

<Snippet syntax={SYNTAX.PY}>

**`sp.contract(t, address, entry_point = '<entry_point_name>')`** <br />
Cast an address of type [sp.TAddress](/general/types#address) to an optional typed contract of type `t`.
It returns an expression of type [sp.TOption](/general/types#option)([sp.TContract](/general/types#contract)(`t`)).

#### Example

```python
contract = sp.contract(sp.TNat, sp.address("KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"), "an_entrypoint").open_some("INTERFACE_MISMATCH")
```

  - When optional parameter `entry_point` is empty or unspecified, it returns `sp.some(c)`, where `c` is a contract handle of type [TContract](/general/types#contract)(`t`), if `address`, of type [TAddress](/general/types#address), points to a contract that expects a parameter of type `t`. Otherwise it returns `sp.none`.

  - When `entry_point` is not empty, it returns the specific entry point specified by the string `entry_point` of the contract. `t` must match the entry point's expected parameter type. Otherwise, it returns `sp.none`.

Due to restrictions of Michelson, it only works properly for contracts with multiple entry points.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.contract<t>(address, '<entry_point_name>').openSome(<error_message>)`** <br />
Cast an address of type [TAddress](/general/types#address) to an optional typed contract of type [TContract](/general/types#contract)(`t`).

#### Example

```typescript
contract = Sp.contract<TNat>("KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF", "an_entrypoint").openSome("INTERFACE_MISMATCH")
```

  - When optional parameter `entry_point` is empty or unspecified, it returns `Sp.some(c)`, where `c` is a contract handle of type [TContract](/general/types#contract)(`t`), if `address`, of type [TAddress](/general/types#address), points to a contract that expects a parameter of type `t`. Otherwise it returns `Sp.none`.

  - When `entry_point` is not empty, it returns the specific entry point specified by the string `entry_point` of the contract. `t` must match the entry point's expected parameter type. Otherwise, it returns `Sp.none`.

Due to restrictions of Michelson, it only works properly for contracts with multiple entry points.

</Snippet>

<MichelsonDocLink placeholder="CONTRACT" url="https://tezos.gitlab.io/michelson-reference/#instr-CONTRACT"/>

### Cast key_hash to a typed contract

<Snippet syntax={SYNTAX.PY}>

**`sp.implicit_account(<key_hash>)`** <br />
Implicit accounts are contracts which always have type [sp.TUnit](/general/types#unit).

The instruction above converts a value of type [sp.TKeyHash](/general/types#key_hash) into [sp.TContract](/general/types#contract)([sp.TUnit](/general/types#unit)).

#### Example

```python
contract = sp.implicit_account(key_hash)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>


See [Key Hash](keys#key_hash) for description.

<MichelsonDocLink placeholder="IMPLICIT_ACCOUNT" url="https://tezos.gitlab.io/michelson-reference/#instr-IMPLICIT_ACCOUNT"/>

### Transfer

<Snippet syntax={SYNTAX.PY}>

**`sp.transfer(arg, amount, destination)`** <br />
Call the `destination` contract with argument `arg` while sending the specified `amount` to it. Note that `destination` must be of type [sp.TContract](/general/types#contract)(`t`). The type of `arg` must be `t`, i.e. the argument sent to the destination must be consistent with what it expects.

#### Example

```python
sp.transfer(arg, amount, destination)
```

<br/>


**`sp.send(destination, amount, message = None)`** <br />
Send the specified `amount` to the `destination` contract. Will fail with optional error `message` if `destination` (of type [sp.TAddress](/general/types#address)) does not resolve to a contract that expects a [sp.TUnit](/general/types#unit) argument (e.g. an account that does not result in any actions).<br/>

#### Example

```python
sp.send(destination, amount, message = None)
```

**Abbreviation for:**

```python
sp.transfer(sp.unit, amount, sp.contract(sp.TUnit, destination).open_some(message = message))
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.transfer(arg, amount, destination)`** <br />
Call the `destination` contract with argument `arg` while sending the specified `amount` to it. Note that `destination` must be of type [TContract](/general/types#contract)(`t`). The type of `arg` must be `t`, i.e. the argument sent to the destination must be consistent with what it expects.

#### Example

```typescript
const contract: TContract<TNat> = Sp.contract<TNat>("KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF", "entrypoint1").openSome("Invalid Interface");
Sp.transfer(10, 0, contract);
```

</Snippet>

<MichelsonDocLink placeholder="TRANSFER_TOKENS" url="https://tezos.gitlab.io/michelson-reference/#instr-TRANSFER_TOKENS"/>

### Create a new contract

<Snippet syntax={SYNTAX.PY}>

**`sp.create_contract(contract, storage = None, amount = sp.tez(0), baker = None)`** <br />
Create a new contract from stored SmartPy contract with optional storage, amount and baker.

#### Example

```python
sp.create_contract(contract, storage = "abc", amount = sp.tez(2))
```

See reference [Create Contract](https://smartpy.io/ide?template=create_contract.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.createContract(<contract_class>, [storage], [amount], [baker])`** <br />
Create a new contract from stored SmartTS contract with optional storage, amount and baker.

#### Example

```typescript
Sp.createContract(AContract, { value: 10 }, 0, "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx")
```

See reference [Create Contract](https://smartpy.io/ts-ide?template=CreateContract.ts) template.

</Snippet>

<MichelsonDocLink placeholder="CREATE_CONTRACT" url="https://tezos.gitlab.io/michelson-reference/#instr-CREATE_CONTRACT"/>
