# Variants

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

Variants in SmartPy are of type [sp.TVariant](/general/types#variant)(`**kargs`) where `kargs` is a Python `dict` of SmartPy types indexed by strings. They generalize the Michelson type <MichelsonDocLink placeholder="or" url="https://tezos.gitlab.io/michelson-reference/#type-or"/>.

Variants are used to define sum-types, similar to enums in other languages with the extra feature that these enums contain values.

See reference [Variant](https://smartpy.io/ide?template=testVariant.py) template.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Variants in SmartTS are of type [TVariant](/general/types#variant)<`expr1 | expr2 | ...expr N`> where `expr N` represent sum-types.<br/>
They generalize the Michelson type <MichelsonDocLink placeholder="or" url="https://tezos.gitlab.io/michelson-reference/#type-or"/>.

Variants are used to define sum-types, similar to enums in other languages with the extra feature that these enums contain values.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`sp.variant(<name>, <value>)`** <br />
Introduce a variant.

#### Example

```python
sp.variant('a_field', 10)
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`Sp.variant(<name>, <value>)`** <br />
Introduce a variant.

#### Example

```typescript
Sp.variant('a_field', 10)
```
</Snippet>

<MichelsonDocLink placeholder="LEFT" url="https://tezos.gitlab.io/michelson-reference/#instr-LEFT"/> and <MichelsonDocLink placeholder="RIGHT" url="https://tezos.gitlab.io/michelson-reference/#instr-RIGHT"/>


## Operations

### Add left variant

<Snippet syntax={SYNTAX.PY}>

**`sp.left(value)`** <br />
Introduce a left/right variant.

#### Example

```python
sp.left("field1")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>


<MichelsonDocLink placeholder="LEFT" url="https://tezos.gitlab.io/michelson-reference/#instr-LEFT"/>

### Add right variant

<Snippet syntax={SYNTAX.PY}>

**`sp.right(value)`** <br />
Introduce a left/right variant.

#### Example

```python
sp.right("field2")
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>

<MichelsonDocLink placeholder="RIGHT" url="https://tezos.gitlab.io/michelson-reference/#instr-RIGHT"/>

### Check if is variant

<Snippet syntax={SYNTAX.PY}>

**`expr.is_variant(v)`** <br />
For a variant, checks whether `expr` is `sp.variant(v, ...)`.

#### Example

```python
variant_type = sp.TVariant(
    action1 = sp.TNat,
    action2 = sp.TString,
)
variant = sp.variant("action1", 10)

sp.if variant.is_variant("action1"):
    # This is true, above we built an action1 variant

sp.if variant.is_variant("action2"):
    # This is false
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<expr>.isVariant(v)`** <br />
For a variant, checks whether `expr` is `Sp.variant(v, ...)`.

#### Example

```typescript
type Variant = TVariant<
    | { kind: "action1", value: TNat }
    | { kind: "action2", value: TString }
>
const variant: Variant = Sp.variant("action1", 10);

if (variant.isVariant("action1")) {
    // This is true
}

if (variant.isVariant("action2")) {
    // This is false
}
```

</Snippet>


<MichelsonDocLink placeholder="IF_LEFT" url="https://tezos.gitlab.io/michelson-reference/#instr-IF_LEFT"/>

### Check if is left variant

<Snippet syntax={SYNTAX.PY}>

**`expr.is_left()`** <br />
For a left/right variant, checks whether it is `sp.left(...)`.

#### Example

```python
expr.is_left()
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>


<MichelsonDocLink placeholder="IF_LEFT" url="https://tezos.gitlab.io/michelson-reference/#instr-IF_LEFT"/>

### Check if is right variant

<Snippet syntax={SYNTAX.PY}>

**`expr.is_right()`** <br />
For a left/right variant, checks whether it is `sp.right(...)`.

#### Example

```python
expr.is_right()
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

Work in progress...

</Snippet>


<MichelsonDocLink placeholder="IF_LEFT" url="https://tezos.gitlab.io/michelson-reference/#instr-IF_LEFT"/>

### Open a variant

<Snippet syntax={SYNTAX.PY}>

**`expr.open_variant(v)`** <br />
If `expr` is equal to `sp.variant(v, x)`, return `x`. Otherwise fail.

#### Example

```python
variant_type = sp.TVariant(
    action1 = sp.TNat,
    action2 = sp.TString,
)
variant1 = sp.variant("action1", 10)
variant2 = sp.variant("action2", "Hello World")

variant1.open_variant("action1") # 10
variant1.open_variant("action2") # "Hello World"
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>


**`<expr>.openVariant(v)`** <br />
If `expr` is equal to `sp.variant(v, x)`, return `x`. Otherwise fail.

#### Example

```typescript
type Variant = TVariant<
    | { kind: "action1", value: TNat }
    | { kind: "action2", value: TString }
>

const variant1: Variant = Sp.variant("action1", 10);
const variant2: Variant = Sp.variant("action2", "Hello World");

variant1.openVariant("action1") // 10
variant2.openVariant("action2") // "Hello World
```


</Snippet>

<MichelsonDocLink placeholder="IF_LEFT" url="https://tezos.gitlab.io/michelson-reference/#instr-IF_LEFT"/>

### Match cases

<Snippet syntax={SYNTAX.PY}>

**`<expr>.match_cases(v)`** <br />
Similar to a switch case statement, where `expr` is a variant.

Only one branch is executed. The notion of `fallthrough` doesn't exist.

#### Example

```python
variant_type = sp.TVariant(
    action1 = sp.TNat,
    action2 = sp.TString,
)
variant = sp.variant("action1", 10)

with variant.match_cases() as arg:
    with arg.match("action1") as a1:
        # Will be executed
        sp.verify(a1 == 10, 'Expected value to be (10).')
    with arg.match("action2") as a2:
        # Will not be executed
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`switch (<expr>.kind)`** <br />
Partially equivalent to a switch case statement, where `expr` is a variant.

Only one branch is executed. The notion of `fallthrough` doesn't exist, `breaks` are not allowed.

#### Example

```typescript
type Variant = TVariant<
    | { kind: "action1", value: TNat }
    | { kind: "action2", value: TString }
>

const variant: Variant = Sp.variant("action1", 10);

switch (variant.kind) {
    case 'action1': {
        // Will be executed
        Sp.verify(variant.value === 10, 'Expected value to be (10).');
    }
    case 'action2': {
        // Will not be executed
    }
}
```


</Snippet>
