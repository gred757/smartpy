# Integers

import Snippet, {SYNTAX} from '@theme/Syntax/Snippet';
import MichelsonDocLink from '@theme/MichelsonDocLink';

<Snippet syntax={SYNTAX.PY}>

There are two main types of integers in SmartPy:

- signed integers [sp.TInt](/general/types#int);
- unsigned integers [sp.TNat](/general/types#nat).

The corresponding types in Michelson are <MichelsonDocLink placeholder="int" url="https://tezos.gitlab.io/michelson-reference/#type-int"/> and <MichelsonDocLink placeholder="nat" url="https://tezos.gitlab.io/michelson-reference/#type-nat"/>.

SmartPy also uses a third definition [sp.TIntOrNat](/general/types#int_or_nat) which stands for integers that are not yet determined as [sp.TInt](/general/types#int) or [sp.TNat](/general/types#nat).

</Snippet>

<Snippet syntax={SYNTAX.TS}>

There are two main types of integers in SmartTS:

- signed integers [TInt](/general/types#int);
- unsigned integers [TNat](/general/types#nat).

The corresponding types in Michelson are <MichelsonDocLink placeholder="int" url="https://tezos.gitlab.io/michelson-reference/#type-int"/> and <MichelsonDocLink placeholder="nat" url="https://tezos.gitlab.io/michelson-reference/#type-nat"/>.

</Snippet>

## Literals

<Snippet syntax={SYNTAX.PY}>

**`1, 2, 0, -5`** <br />
Literal of type [sp.TIntOrNat](/general/types#int_or_nat) when non negative and [sp.TInt](/general/types#int) otherwise.<br/>

**`sp.int(i)`** <br />
A literal of type [sp.TInt](/general/types#int) when `i` is a Python integer literal.

**`sp.nat(n)`** <br />
A literal of type [sp.TNat](/general/types#nat) when `n` is a non negative Python integer literal.

#### Example

```python
value  = 1          # sp.TIntOrNat
value1 = -1         # sp.TInt
value2 = sp.int(10) # sp.TInt
value3 = sp.nat(20) # sp.TNat
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`i as TNat`** <br />
A literal of type [sp.TInt](/general/types#int).

**`n as TNat`** <br />
A literal of type [sp.TNat](/general/types#nat).

#### Example

```typescript
const value: TInt  = 1; // sp.TInt
const value2: TNat = 1; // sp.TNat
```

</Snippet>


## Arithmetic Operations

The usual arithmetic operators `+`, `-`, `*`, `%`, `//`, `<<`, `>>` behave just like in Python.

<Snippet syntax={SYNTAX.PY}>

In SmartPy, type inference of arithmetic operators imposes that both sides have the same type. This constraint can be relaxed by explicitly using `sp.to_int`.

</Snippet>

### Addition

<Snippet syntax={SYNTAX.PY}>

**`expr1 + expr2`** <MichelsonDocLink placeholder="ADD"
url="https://tezos.gitlab.io/michelson-reference/#instr-ADD"/> <br />
Add two numerical values, `expr1` and `expr2`.

#### Example

```python
sp.int(1) + sp.int(2) # 3 of type sp.TInt
sp.nat(1) + sp.nat(2) # 3 of type sp.TNat
1 + sp.int(2)         # 3 of type sp.TInt
1 + sp.nat(2)         # 3 of type sp.TNat
1 + 2                 # 3 of type sp.TIntOrNat
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 + expr2`** <MichelsonDocLink placeholder="ADD"
url="https://tezos.gitlab.io/michelson-reference/#instr-ADD"/> <br />
Add two numerical values, `expr1` and `expr2`.

#### Example

```typescript
const sum1: TInt = 1 + 2;
const sum2: TNat = 1 + 2;
```

</Snippet>

### Subtraction

<Snippet syntax={SYNTAX.PY}>

**`expr1 - expr2`** <MichelsonDocLink placeholder="SUB"
url="https://tezos.gitlab.io/michelson-reference/#instr-SUB"/> <br />
Subtract two numerical values, `expr1` and `expr2`.

#### Example

```python
sp.int(1) - sp.int(2) # -1 of type sp.TInt
sp.nat(1) - sp.nat(2) # -1 of type sp.TInt
sp.int(2) - sp.int(1) #  1 of type sp.TInt
sp.nat(2) - sp.nat(1) #  1 of type sp.TInt
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 - expr2`** <MichelsonDocLink placeholder="SUB"
url="https://tezos.gitlab.io/michelson-reference/#instr-SUB"/> <br />
Subtract two numerical values, `expr1` and `expr2`.

#### Example

```typescript
const value: TInt = 1 - 2;
```

</Snippet>

### Negation

<Snippet syntax={SYNTAX.PY}>

**`- expr`** <MichelsonDocLink placeholder="NEG"
url="https://tezos.gitlab.io/michelson-reference/#instr-NEG"/> <br />
Negate a numerical value, `expr`.

#### Example

```python
value = - sp.nat(2) # -2 of type sp.TInt
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`- expr`** <MichelsonDocLink placeholder="NEG" url="https://tezos.gitlab.io/michelson-reference/#instr-NEG"/> <br />
Negate a numerical value, `expr`.

#### Example

```typescript
const value: TInt = - 2;
```

</Snippet>

### Multiplication

<Snippet syntax={SYNTAX.PY}>


**`expr1 * expr2`**,
**`sp.mul(<expr1>, <expr2>)`** (Overloaded version)  <MichelsonDocLink
placeholder="MUL"
url="https://tezos.gitlab.io/michelson-reference/#instr-MUL"/> <br />
Multiply two numerical values, `expr1` and `expr2`.

#### Example

```python
value1 = sp.int(2) * sp.int(2) # 4 of type sp.TInt
value2 = sp.nat(2) * sp.nat(2) # 4 of type sp.TNat
value3 = sp.mul(sp.nat(2), sp.int(2)) # 4 of type sp.TInt
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`expr1 * expr2`**,
**`<expr1>.multiply(<expr2>)`** (Overloaded version) <MichelsonDocLink placeholder="MUL" url="https://tezos.gitlab.io/michelson-reference/#instr-MUL"/> <br />
Multiply two numerical values, `expr1` and `expr2`.

#### Example

```typescript
const value1: TInt = 2 * -2;
const value2: TNat = 2 * 3;
const value3: TInt = (2 as TNat).multiply(3)
```

</Snippet>


### Division

<Snippet syntax={SYNTAX.PY}>

**`expr1 % expr2`** <MichelsonDocLink placeholder="EDIV"
url="https://tezos.gitlab.io/michelson-reference/#instr-EDIV"/> <br />
Perform euclidean division and get the `remainder`, where `expr1` is the dividend, and `expr2` is the divisor.

#### Example

```python
remainder = 11 % 2 # 1
```

**Which is an alias of:**

```python
remainder = sp.snd(sp.ediv(11, 2).open_some()) # 1
```

**`expr1 // expr2`** <MichelsonDocLink placeholder="EDIV"
url="https://tezos.gitlab.io/michelson-reference/#instr-EDIV"/> <br />
Perform euclidean division and get the `quotient`, where `expr1` is the dividend, and `expr2` is the divisor.

#### Example

```python
quotient = 11 // 2 # 5
```

**Which is an alias of:**

```python
quotient = sp.fst(sp.ediv(11, 2).open_some()) # 5
```

**`sp.ediv(expr1, expr2)`** <MichelsonDocLink placeholder="EDIV"
url="https://tezos.gitlab.io/michelson-reference/#instr-EDIV"/> <br />
Perform euclidean division, where `expr1` is the dividend, and `expr2` is the divisor.

When `expr1` and `expr2` are both of type [sp.TNat](/general/types#nat), the returned value is of type [sp.TOption](/general/types#option)([sp.TPair](/general/types#pair)([sp.TNat](/general/types#nat), [sp.TNat](/general/types#nat))).

In the remaining cases, the returned value is of type [sp.TOption](/general/types#option)([sp.TPair](/general/types#pair)([sp.TInt](/general/types#nat), [sp.TNat](/general/types#nat))).

The first element of [sp.TPair](/general/types#pair)([sp.TInt](/general/types#nat), [sp.TNat](/general/types#nat)) is the `quotient`, whereas the second element is the `remainder`.

#### Example

```python
(quotient, remainder) = sp.ediv(11, 2).open_some()
```

</Snippet>

## Logical Operations

### Shift

**`expr1 << expr2`** <MichelsonDocLink placeholder="LSL"
url="https://tezos.gitlab.io/michelson-reference/#instr-LSL"/> <br />
Logically left shift a natural number.

The operation expects `expr1` and `expr2` to be of type [sp.TNat](/general/types#nat) and to be less than or equal to **`256`**. It produces the first number logically left-shifted by second number, which is of type [sp.TNat](/general/types#nat).

**`expr1 >> expr2`** <MichelsonDocLink placeholder="LSR"
url="https://tezos.gitlab.io/michelson-reference/#instr-LSR"/> <br />
Logically right shift a natural number.

The operation expects `expr1` and `expr2` to be of type [sp.TNat](/general/types#nat) and to be less than or equal to **`256`**. It produces the first number logically right-shifted by second number, which is of type [sp.TNat](/general/types#nat).

### Or

**`expr1 | expr2`** <MichelsonDocLink placeholder="OR"
url="https://tezos.gitlab.io/michelson-reference/#instr-OR"/> <br />
Compute bitwise `expr1` or `expr2` for `expr1` and `expr2` of type [sp.TNat](/general/types#nat). <br/>
Result is also of type [sp.TNat](/general/types#nat).

### And

**`expr1 & expr2`** <MichelsonDocLink placeholder="AND"
url="https://tezos.gitlab.io/michelson-reference/#instr-AND"/> <br />
Compute bitwise `expr1` and `expr2` for `expr1` and `expr2` of type [sp.TNat](/general/types#nat). <br/>
Result is also of type [sp.TNat](/general/types#nat).

### Exclusive or

**`expr1 ^ expr2`** <MichelsonDocLink placeholder="XOR"
url="https://tezos.gitlab.io/michelson-reference/#instr-XOR"/> <br />
Compute bitwise `expr1` xor `expr2` for `expr1` and `expr2` of type [sp.TNat](/general/types#nat). <br/>
Result is also of type [sp.TNat](/general/types#nat).


## Int vs Nat conversion

### Cast int to nat

<Snippet syntax={SYNTAX.PY}>

**`abs(i)`** <br />
Return the absolute value of `i`.<br/>
`abs` converts an [sp.TInt](/general/types#int) into a [sp.TNat](/general/types#nat).

#### Example

```python
self.data.x = -10
natValue = abs(self.data.x) # equivalent to 10 of type sp.TNat
```

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<expr>.abs()`** <br />
Return the absolute value of `expr`.<br/>
`abs` converts an [TInt](/general/types#int) into a [TNat](/general/types#nat).

#### Example

```typescript
const intValue: TInt = -1;
const natValue = intValue.abs() // 1
```

</Snippet>

<MichelsonDocLink placeholder="ABS" url="https://tezos.gitlab.io/michelson-reference/#instr-ABS"/>

### Cast nat to int

<Snippet syntax={SYNTAX.PY}>

**`sp.to_int(n)`** <br />
Convert a [sp.TNat](/general/types#nat) into an [sp.TInt](/general/types#int).

#### Example

```python
intValue = sp.to_int(sp.nat(10)) # 10 of type sp.TInt
```

</Snippet>


<Snippet syntax={SYNTAX.TS}>

**`<expr>.toInt(n)`** <br />
Convert a [TNat](/general/types#nat) into an [TInt](/general/types#int).

#### Example

```typescript
const natValue: TNat = 1;
const intValue: TInt = natValue.toInt();
```

</Snippet>

<MichelsonDocLink placeholder="INT" url="https://tezos.gitlab.io/michelson-reference/#instr-INT"/>

### Extract nat from int

<Snippet syntax={SYNTAX.PY}>

**`sp.is_nat(i)`** <br />
Convert a [sp.TInt](/general/types#int) into an [sp.TOption](/general/types#option)([sp.TNat](/general/types#nat)).

`sp.is_nat(i) == sp.some(n)` when `i` is a non negative [sp.TInt](/general/types#int) and `sp.none` otherwise.

#### Example

```python
intValue = sp.is_nat(i)
```

**`sp.as_nat(i)`** <br />
Convert an [sp.TInt](/general/types#int) into a [sp.TNat](/general/types#nat) and fails if not possible with the optional `message`, i.e., when `i` is negative.

```python
natValue = sp.as_nat(i, message = None)
```

It is an alias of `sp.is_nat(i).open_some(message = message)`.

</Snippet>

<Snippet syntax={SYNTAX.TS}>

**`<expr>.toNat(i)`** <br />
Convert an [TInt](/general/types#int) into a [TNat](/general/types#nat) and fails if not possible, i.e., when `i` is negative.

```typescript
const intValue: TInt = 1;
const natValue: TNat = intValue.toNat();
```

</Snippet>


<MichelsonDocLink placeholder="ISNAT" url="https://tezos.gitlab.io/michelson-reference/#instr-ISNAT"/>
