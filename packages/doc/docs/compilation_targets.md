# Compilation Targets

Compilation targets allow users to produce multiple compiled variations for `contracts` and `expressions`.

It is usually used with the [CLI](/cli) to produce the **contract code** and its **initial storage**.

## Adding a Compilation Target

### Contract compilation

It compiles a `contract` with `name` with optional `storage`.

```python
sp.add_compilation_target(name, contract, storage = None)
```

### Expression compilation

It compiles an `expression` called `name`.

```python
sp.add_expression_compilation_target(name, expression)
```


### Examples

```python
# A contract with an empty (unit) storage
sp.add_compilation_target("min_comp", MyContract())

# A contract with a simple int storage
sp.add_compilation_target("min_comp_int", MyContract(x = 1))

# An expression
sp.add_expression_compilation_target("x", 42)

# Another expression
sp.add_expression_compilation_target("y", ("a", [1, 2, 3]))
```
