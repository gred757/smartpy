import React from 'react';
import styles from './HomepageFeatures.module.css';

export default function HomepageFeatures() {
    return (
        <section className={styles.features}>
            <div className="container">
                <h1 className="hero__title">SmartPy Overview</h1>
                <div className="text--center">
                    <img src="/img/overview.svg" alt="SmartPy Overview" />
                </div>
            </div>
        </section>
    );
}
