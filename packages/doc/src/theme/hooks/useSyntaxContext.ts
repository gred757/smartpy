import { useContext } from 'react';

import SyntaxContext from '../Syntax/SyntaxContext';

export enum SYNTAX {
    PY = 'Python',
    TS = 'Typescript',
    ML = 'Ocaml',
}

interface OwnProps {
    syntax: SYNTAX;
    setSyntax: (syntax: SYNTAX) => void;
}

function useSyntaxContext(): OwnProps {
    const context = useContext<OwnProps | undefined>(SyntaxContext);
    if (context == null) {
        throw new Error('`useSyntaxContext` not available.');
    }
    return context;
}

export default useSyntaxContext;
