import React from 'react';

function Entrypoint(props: any) {
    return (
        <section className="michelson">
            <section className="entrypoint">
                <h3>
                    Entrypoint <span className="entrypointName">{props.name}</span>
                </h3>
                {props.children}
            </section>
        </section>
    );
}

export default Entrypoint;
