import { execSync } from 'child_process';
import { readdirSync } from 'fs';

beforeAll(() => {
    execSync(`rm -rf ./test/tmp`);
});

afterAll(() => {
    execSync(`rm -rf ./test/tmp`);
});

test('Test typescript boilerplate', async () => {
    execSync('node ./bin/index.js ./test/tmp --typescript');

    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const pkg = require('./tmp/package.json');
    expect(pkg.name).toBe('tmp');

    const files = readdirSync('./test/tmp');
    expect(files).toEqual([
        '.eslintrc.js',
        '.prettierrc.js',
        'README.md',
        'node_modules',
        'package-lock.json',
        'package.json',
        'scripts',
        'src',
        'templates',
        'tsconfig.json',
    ]);
});
