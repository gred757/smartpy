const tar = require('tar');
const fs = require('fs');
const chalk = require('chalk');

const TYPESCRIPT_BUNDLE = './pkg/smartpy-boilerplate-typescript.tar.gz';

// Create folder if it doesn't exist
fs.mkdirSync('./pkg', { recursive: true });

tar.c(
    {
        strict: true,
        cwd: './boilerplates/typescript',
        gzip: true,
        follow: true, // Follow symbolic links
        file: './pkg/smartpy-boilerplate-typescript.tar.gz',
    },
    ['./'],
).then((_) => {
    console.log();
    console.log(`Generated Typescript boilerplate package, available at ${chalk.green(TYPESCRIPT_BUNDLE)}.`);
    console.log();
});
