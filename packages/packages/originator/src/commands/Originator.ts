import { CommanderStatic } from 'commander';
import * as fs from 'fs';

import Logger from '../services/Logger';
import Common from '@smartpy/common';

const FAUCET_URL = 'https://alt.smartpy.io/faucet';

const getFromFaucet = (): Promise<string> =>
    new Promise(async (resolve, reject) => {
        const http = await import('https');
        http.get(FAUCET_URL, (res) => {
            let data = '';

            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                resolve(data);
            });
        }).on('error', (error) => {
            reject(error);
        });
    });

export const originatorCommands = (Commander: CommanderStatic): void => {
    Commander.command('originate-contract')
        .requiredOption('--code <path>')
        .requiredOption('--storage <path>')
        .requiredOption('--rpc <url>')
        .option('--private-key [string]')
        .option('--amount [amount]')
        .action(async (args) => {
            let code = fs.readFileSync(args.code, { encoding: 'utf8', flag: 'r' });
            if (args.code.endsWith('.json')) {
                // We accept both formats (Michelson and Micheline)
                // Michelson needs to be parsed to an object
                code = JSON.parse(code);
            }
            let initialStorage = fs.readFileSync(args.storage, { encoding: 'utf8', flag: 'r' });
            if (args.storage.endsWith('.json')) {
                // We accept both formats (Michelson and Micheline)
                // Michelson needs to be parsed to an object
                initialStorage = JSON.parse(initialStorage);
            }

            const rpc = args.rpc;
            let privateKey = args.privateKey;

            if (!privateKey) {
                privateKey = await getFromFaucet();
            }

            Logger.info(`Using RPC ${rpc}...`);

            try {
                const config = { RPC: rpc };
                const address = await Common.originateContract(code, initialStorage, privateKey, config, args.amount);
                Logger.info(`Contract ${address} originated!!!`);
            } catch (e) {
                console.error(JSON.stringify(e, null, 4));
            }
        });
};
