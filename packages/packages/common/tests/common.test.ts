import Common from '../src/Common'
import * as fs from 'fs';

// Origination can take a few seconds
jest.setTimeout(92000);

const SECRET_KEY =
'edskRsk6poVViybe3kujNJafTxUef7HDcKE6wPGw7PTZZhYBULJXt8TX2aNRsujiw9Zf5tEqdxnirReK4o3qF4KABuUDxKvZXb';
const TEZOS_RPC = 'https://granadanet.smartpy.io';

test('Originate Contract', async () => {
    const CODE = fs.readFileSync('tests/code.tz', { encoding: 'utf8', flag: 'r' });
    const STORAGE = fs.readFileSync('tests/storage.tz', { encoding: 'utf8', flag: 'r' });
    const CONFIG = { 'RPC': TEZOS_RPC };
    const AMOUNT = 0;
    const address = await Common.originateContract(CODE, STORAGE, SECRET_KEY, CONFIG, AMOUNT)
    console.log(address)
});

test('Originate and Call Contract', async () => {
    const CODE = fs.readFileSync('tests/code.tz', { encoding: 'utf8', flag: 'r' });
    const STORAGE = fs.readFileSync('tests/storage.tz', { encoding: 'utf8', flag: 'r' });
    const CONFIG = { 'RPC': TEZOS_RPC };
    const AMOUNT = 0;
    const address = await Common.originateContract(CODE, STORAGE, SECRET_KEY, CONFIG, AMOUNT)
    const PARAMETER = { prim: 'Unit' };
    const ENTRYPOINT = 'entry_point_1';

    const op = await Common.callContract(address, SECRET_KEY, CONFIG, PARAMETER, ENTRYPOINT, AMOUNT)
    console.log(op)
});
