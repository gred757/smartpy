/*  This contract is a demo example

    This contract is a "two carabiners locking Admin system".

    To update the admin, the current admin must add another admin
    Then the new admin must remove the old one.

    This security pattern prevent the admin to specify a non-working account
    as the new administrator.

*/
interface TStorage {
    admins: TSet<TAddress>;
}

const admin1: TAddress = 'tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm';
const admin2: TAddress = 'tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU';

@Contract
export class AdminContract {
    storage: TStorage = {
        admins: [admin1],
    };

    @Inline
    isAdmin = (sender: TAddress): TBool => this.storage.admins.contains(sender);

    @Inline
    onlyAdmin(): void {
        Sp.verify(this.isAdmin(Sp.sender), 'onlyAdmin');
    }

    @EntryPoint
    changeAdmins(remove: TOption<TSet<TAddress>>, add: TOption<TSet<TAddress>>): void {
        this.onlyAdmin();
        if (remove.isSome()) {
            for (const admin of remove.openSome().elements()) {
                Sp.verify(admin !== Sp.sender, 'SelfRemoving');
                this.storage.admins.remove(admin);
            }
        }
        if (add.isSome()) {
            for (const admin of add.openSome().elements()) {
                this.storage.admins.add(admin);
            }
        }
    }
}

Dev.test({ name: 'Double Snap hook Admin Contract' }, () => {
    Scenario.h1('Double Snap hook Admin contract');
    Scenario.h2('Originating contract');
    const c1 = Scenario.originate(new AdminContract());
    Scenario.verify(c1.storage.admins.contains(admin1));

    Scenario.h2('Add new admin');
    Scenario.transfer(c1.changeAdmins(Sp.none, Sp.some([admin2])), { sender: admin1 });
    Scenario.verifyEqual(c1.storage.admins, [admin1, admin2] as TSet<TAddress>);

    Scenario.h2('Remove old admin');
    Scenario.transfer(c1.changeAdmins(Sp.some([admin1]), Sp.none), { sender: admin2 });
    Scenario.verifyEqual(c1.storage.admins, [admin2] as TSet<TAddress>);
});

Dev.test({ name: 'Double Snap hook Admin Contract Expected failures' }, () => {
    Scenario.h1('Double Snap hook Admin contract');
    Scenario.h2('Originating contract');
    const c1 = Scenario.originate(new AdminContract());
    Scenario.verify(c1.storage.admins.contains(admin1));

    Scenario.h2('Add new admin without rights');
    Scenario.transfer(c1.changeAdmins(Sp.none, Sp.some([admin2])), {
        sender: admin2,
        valid: false,
        exception: 'onlyAdmin',
    });

    Scenario.h2('Add new admin');
    Scenario.transfer(c1.changeAdmins(Sp.none, Sp.some([admin2])), { sender: admin1 });
    Scenario.verifyEqual(c1.storage.admins, [admin1, admin2] as TSet<TAddress>);

    Scenario.h2('Try to remove itself');
    Scenario.transfer(c1.changeAdmins(Sp.some([admin2]), Sp.none), {
        sender: admin2,
        valid: false,
        exception: 'SelfRemoving',
    });
});

Dev.compileContract('compile_contract', new AdminContract());
