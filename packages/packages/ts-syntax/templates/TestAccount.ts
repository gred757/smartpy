interface TStorage {
    value: TNat;
}

@Contract
export class TestAccount {
    storage: TStorage = {
        value: 1,
    };

    @EntryPoint
    ep(value: TNat): void {
        this.storage.value = value;
    }
}

Dev.test({ name: 'TestAccount' }, () => {
    const bob = Scenario.testAccount('Bob');

    Scenario.h4('All properties');
    Scenario.show(bob);

    Scenario.p('Account address');
    Scenario.show(bob.address);

    Scenario.p('Account public key');
    Scenario.show(bob.publicKey);

    Scenario.p('Account public key hash');
    Scenario.show(bob.publicKeyHash);

    Scenario.p('Account secret key');
    Scenario.show(bob.secretKey);
});

Dev.compileContract('minimal', new TestAccount());
