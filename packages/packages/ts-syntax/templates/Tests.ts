interface TStorage {
    storedValue: TNat;
}

const initial_storage: TStorage = {
    storedValue: 1,
};

@Contract
export class StoreValue {
    constructor(public storage: TStorage = initial_storage) {}

    @EntryPoint
    replace(value: TNat): void {
        this.storage.storedValue = value;
    }

    @EntryPoint
    add(value: TNat, value2: TNat): void {
        this.storage.storedValue = value + value2;
    }

    @EntryPoint
    fail(): void {
        Sp.failWith('FAILED');
    }
}

// Tests

const name_test_1 = 'A test';
const run_scenario_1 = true;

Dev.test({ name: name_test_1, enabled: run_scenario_1 }, () => {
    const c1 = Scenario.originate(new StoreValue(), { show: false });
    Scenario.h1('Calling replace Entrypoint');
    Scenario.transfer(c1.replace(1), { amount: 1 });
    Scenario.h1('Calling add Entrypoint');
    Scenario.transfer(c1.add(2, 2), { amount: 10000000000000 });
});

Dev.test(
    {
        name: 'test 2',
        enabled: true,
        flags: [['protocol', 'florence']],
    },
    () => {
        const c1 = Scenario.originate(new StoreValue());
        const alice = Scenario.testAccount('Alice');
        Scenario.transfer(c1.fail(), { valid: false });
        Scenario.h1('Calling add Entrypoint');
        Scenario.transfer(c1.add(2, 2), { sender: alice });
        Scenario.transfer(c1.add(2, 2), { sender: alice.address });

        const expectedStorageValue = 4;
        Scenario.verify(c1.storage.storedValue === expectedStorageValue);

        const expectedBalance: TMutez = 0;
        Scenario.verify(c1.balance === expectedBalance);
        Scenario.verifyEqual(c1.balance, expectedBalance);

        Scenario.show(1 + 2, { html: false, compile: true, stripStrings: true });
        Scenario.show(c1.typed, { html: false, compile: true, stripStrings: true });
        Scenario.show(c1.typed.add, { html: false, compile: true, stripStrings: true });
    },
);

// Compilation

Dev.compileContract(
    'compile_contract',
    new StoreValue({
        storedValue: 10,
    }),
);
