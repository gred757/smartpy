export const staticId = (id: number, line: string): string => `(static_id ${id} ${line})`;

export const capitalizeBoolean = (bool: boolean): string => (bool ? 'True' : 'False');
