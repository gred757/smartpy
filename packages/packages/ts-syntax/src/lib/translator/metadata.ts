import { ST_LiteralKind } from '../../enums/literal';
import { FrontendType } from '../../enums/type';
import ErrorUtils from '../utils/Error';
import LineUtils from '../utils/Line';
import { ST_ExpressionKind } from '../../enums/expression';
import LiteralTranslator from './literal';
import type { ST_Expression } from '../../@types/expression';

const elem = (name: string, value: string) => `(elem ${name} ${value})`;
const meta_map = (elements: string[], line: string | number) => `(meta_map ${line} ${elements.join(' ')})`;
const meta_list = (elements: string[], line: string | number): string => `(meta_list ${line} ${elements.join(' ')})`;
const meta_view = (name: string, line: string | number) => `(meta_view "${name}" ${line})`;
const meta_expr = (expr: string) => `(meta_expr ${expr})`;

const translateMetadataObject = (properties: Record<string, ST_Expression>, line: string | number): string => {
    const elements = Object.entries(properties).map(([name, element]) => translateMetadataElement(name, element));
    return meta_map(elements, line);
};

const translateMetadataElement = (name: string, expr: ST_Expression): string => {
    const line = LineUtils.getLineNumber(expr.line);
    const nameString = LiteralTranslator.translateLiteral(
        {
            kind: ST_LiteralKind.String,
            value: name,
            line: expr.line,
        },
        { type: FrontendType.TString },
    );
    switch (expr.kind) {
        case ST_ExpressionKind.ObjectLiteralExpression: {
            const valueString = translateMetadataObject(expr.properties, line);
            return elem(nameString, valueString);
        }
        case ST_ExpressionKind.LiteralExpr: {
            const valueString = translateMetadataValue(expr);
            return elem(nameString, valueString);
        }
        case ST_ExpressionKind.ArrayLiteralExpression:
            // Views is a reserved field and is parsed differently
            // Template : Each view => (meta_view "<name>" <line>)
            if (name === 'views') {
                const views = expr.elements.map((v) => {
                    if (v.kind === ST_ExpressionKind.LiteralExpr && v.literal.kind === ST_LiteralKind.String) {
                        return meta_view(v.literal.value, LineUtils.getLineNumber(v.literal.line));
                    }
                    return ErrorUtils.failWithInfo({
                        msg: `Unexpected view expression (${v.kind}).`,
                        line: v.line,
                    });
                });
                const valueString = meta_list(views, line);
                return elem(nameString, valueString);
            } else {
                const valueString = meta_list(expr.elements.map(translateMetadataValue), line);

                return elem(nameString, valueString);
            }
    }

    return ErrorUtils.failWithInfo({
        msg: `Unexpected expression (${expr.kind}).`,
        line: expr.line,
    });
};

const translateMetadataValue = (expr: ST_Expression): string => {
    switch (expr.kind) {
        case ST_ExpressionKind.LiteralExpr:
            return meta_expr(LiteralTranslator.translateLiteral(expr.literal, expr.type));
    }

    return ErrorUtils.failWithInfo({
        msg: `Unexpected metadata value expression (${expr.kind}).`,
        line: expr.line,
    });
};

const MetadataTranslator = {
    translateMetadataObject,
};

export default MetadataTranslator;
