import type { ST_Literal } from '../../../@types/literal';
import { ST_LiteralKind } from '../../../enums/literal';

export const isString = (l: ST_Literal): l is Extract<ST_Literal, { kind: ST_LiteralKind.String }> =>
    l.kind === ST_LiteralKind.String;

export const isBoolean = (l: ST_Literal): l is Extract<ST_Literal, { kind: ST_LiteralKind.Boolean }> =>
    l.kind === ST_LiteralKind.Boolean;

export const isNumeric = (l: ST_Literal): l is Extract<ST_Literal, { kind: ST_LiteralKind.Numeric }> =>
    l.kind === ST_LiteralKind.Numeric;

const literal = {
    isString,
    isBoolean,
    isNumeric,
};

export default literal;
