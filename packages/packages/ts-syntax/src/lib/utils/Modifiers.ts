import {
    ClassDeclaration,
    EnumDeclaration,
    FunctionDeclaration,
    InterfaceDeclaration,
    MethodDeclaration,
    Node,
    PropertyDeclaration,
    SyntaxKind,
    TypeAliasDeclaration,
    VariableDeclaration,
    VariableStatement,
    ParameterDeclaration,
} from 'typescript';
import { ST_Modifier } from '../../enums/Modifiers';
import ErrorUtils from './Error';

/**
 * @description Extract declaration modifiers (Export, public, private, etc...).
 * @param {Node} node
 * @returns {ST_Modifier[]} node name
 */
const extractModifiers = (
    node:
        | TypeAliasDeclaration
        | InterfaceDeclaration
        | ClassDeclaration
        | FunctionDeclaration
        | MethodDeclaration
        | PropertyDeclaration
        | VariableStatement
        | VariableDeclaration
        | EnumDeclaration
        | ParameterDeclaration,
): ST_Modifier[] => {
    return (node.modifiers || []).map(extractModifier);
};

const extractModifier = (node: Node): ST_Modifier => {
    switch (node.kind) {
        case SyntaxKind.ExportKeyword:
            return ST_Modifier.Export;
        case SyntaxKind.StaticKeyword:
            return ST_Modifier.Static;
        case SyntaxKind.PublicKeyword:
            return ST_Modifier.Public;
        case SyntaxKind.PrivateKeyword:
            return ST_Modifier.Private;
    }
    return ErrorUtils.failWith(`Unexpected modifier (${SyntaxKind[node.kind]}).`);
};

export const ModifiersUtils = {
    extractModifiers,
};

export default ModifiersUtils;
