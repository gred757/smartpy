import {
    Node,
    TypeAliasDeclaration,
    TypeReferenceNode,
    isPropertySignature,
    isPropertyAssignment,
    isPropertyDeclaration,
    isTypeLiteralNode,
    TypeNode,
    isTypeReferenceNode,
    isVariableDeclaration,
    isArrayLiteralExpression,
    isObjectLiteralExpression,
    isIdentifier,
    isParameter,
    isFunctionDeclaration,
    isMethodDeclaration,
    isArrowFunction,
    isAsExpression,
    isPropertyAccessExpression,
    isCallExpression,
    NodeArray,
    isTupleTypeNode,
    isUnionTypeNode,
    TypeLiteralNode,
    InterfaceDeclaration,
    isLiteralTypeNode,
    isStringLiteral,
    NamedTupleMember,
    isBinaryExpression,
    isParenthesizedExpression,
    isShorthandPropertyAssignment,
    isEnumDeclaration,
    isEnumMember,
    CallExpression,
    isPrefixUnaryExpression,
} from 'typescript';

import { SyntaxKind } from 'typescript';
import type { Nullable } from '../../@types/common';
import type { ST_Expression } from '../../@types/expression';
import type { ST_TypeDef, ST_TypeDefs, ST_Layout } from '../../@types/type';
import { ST_BinaryToken, ST_ExpressionKind } from '../../enums/expression';
import * as Native from '../../enums/native';

import { FrontendType, Layout } from '../../enums/type';
import TypeBuilder from '../utils/builders/type';
import CheckerUtils from '../utils/Checker';
import guards from '../utils/guard';
import LineUtils from '../utils/Line';
import ModifiersUtils from '../utils/Modifiers';
import printer from '../utils/printer';

import { InterpreterBase } from './Base';

export default class TypeInterpreter extends InterpreterBase {
    public visitInterfaceDeclaration = (node: InterfaceDeclaration): void => {
        const typeName = this.transpiler.extractName(node);
        const typeDefs = this.getTypesFromArray(node.members);
        const modifiers = ModifiersUtils.extractModifiers(node);

        this.output.emitTypeDef(
            typeName,
            {
                type: FrontendType.TRecord,
                properties: typeDefs,
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            },
            modifiers,
        );
    };

    public visitTypeAliasDeclaration = (node: TypeAliasDeclaration): void => {
        const typeName = this.transpiler.extractName(node);
        const typeDef = this.extractTypeDef(node.type);
        const modifiers = ModifiersUtils.extractModifiers(node);

        this.output.emitTypeDef(typeName, typeDef, modifiers);
    };

    /**
     * @description Extract type definition from type declaration.
     * @param {TypeNode} node
     * @returns {ST_TypeDef} Type definition
     */
    public extractTypeDef = (node: TypeNode): ST_TypeDef => {
        if (isTypeReferenceNode(node)) {
            const namespace = this.transpiler.extractNamespace(node.typeName);

            // If type is a native SmartTS type, use it
            if (FrontendType[namespace[0] as FrontendType]) {
                return this.composeFrontendType(FrontendType[namespace[0] as FrontendType], node);
            }

            return this.dereferenceType(namespace, node);
        }
        if (isTupleTypeNode(node)) {
            return {
                type: FrontendType.TTuple,
                innerTypes: node.elements.map(this.extractTypeDef),
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            };
        }
        if (isTypeLiteralNode(node)) {
            return {
                type: FrontendType.TRecord,
                properties: this.getTypesFromArray(node.members),
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            };
        }
        if (isUnionTypeNode(node)) {
            if (node.types.every(isTypeLiteralNode)) {
                const types = <NodeArray<TypeLiteralNode>>node.types;
                return {
                    type: FrontendType.TVariant,
                    properties: types.reduce((p, { members }) => {
                        let variantName: string | undefined;
                        const variant = members.reduce((p, c) => {
                            if (isPropertySignature(c)) {
                                const propName = this.transpiler.extractName(c);
                                if (c.type) {
                                    if (
                                        propName === 'kind' &&
                                        isLiteralTypeNode(c.type) &&
                                        isStringLiteral(c.type.literal)
                                    ) {
                                        variantName = c.type.literal.text;
                                    }

                                    p[propName] = this.extractTypeDef(c.type);
                                } else {
                                    this.output.emitError(c, 'Property signature must specify a type.');
                                }
                            }
                            return p;
                        }, {} as ST_TypeDefs);

                        p[variantName!] = {
                            type: FrontendType.TRecord,
                            properties: variant,
                            line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                        };

                        return p;
                    }, {} as ST_TypeDefs),
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
            this.output.emitError(node, `Union types must be type literals, (e.g. type v = { A: TNat } | { B: TInt })`);
        }
        if (isLiteralTypeNode(node)) {
            return {
                type: FrontendType.TString,
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            };
        }
        switch (node.kind) {
            case SyntaxKind.NumericLiteral:
            case SyntaxKind.NumberKeyword:
                return {
                    type: FrontendType.TIntOrNat,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            case SyntaxKind.TrueKeyword:
            case SyntaxKind.FalseKeyword:
            case SyntaxKind.BooleanKeyword:
                return {
                    type: FrontendType.TBool,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            case SyntaxKind.StringLiteral:
            case SyntaxKind.StringKeyword:
                return {
                    type: FrontendType.TString,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            case SyntaxKind.NeverKeyword:
                return {
                    type: FrontendType.TNever,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            case SyntaxKind.VoidKeyword:
                return {
                    type: FrontendType.TUnit,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
        }

        return this.output.emitError(node, `Unknown type node (${SyntaxKind[node.kind]}).`);
    };

    /**
     * @description Check property type against the initializer.
     * @param {Node} node
     * @returns {ST_TypeDef} Type definition
     */
    public checkType = (node: Node): ST_TypeDef => {
        const line = LineUtils.getLineAndCharacter(this.sourceFile, node);
        let explicitType: ST_TypeDef = TypeBuilder.unknown(line);
        let implicitType: ST_TypeDef = TypeBuilder.unknown(line);
        if (isPrefixUnaryExpression(node) && node.operator === SyntaxKind.ExclamationToken) {
            implicitType = TypeBuilder.bool();
        } else if (isIdentifier(node) || isPropertyAccessExpression(node) || isShorthandPropertyAssignment(node)) {
            const namespace = this.transpiler.extractNamespace(node);
            implicitType = this.dereferencePropertyType(namespace, node);
        } else if (
            isPropertySignature(node) ||
            isPropertyDeclaration(node) ||
            isVariableDeclaration(node) ||
            isParameter(node)
        ) {
            // Get variable explicit type if provided (let a: TNat = ...).
            if (node.type) {
                explicitType = this.extractTypeDef(node.type);
            }
            // Get implicit type from the initialized to check against the explicit one
            if (node.initializer) {
                implicitType = this.checkType(node.initializer);
            } else {
                // If the initialized is empty, it means that no check is required.
                return explicitType;
            }
        } else if (isArrayLiteralExpression(node)) {
            // Array nodes can be lists, sets, maps or big_maps
            // Array nodes with children array nodes can be maps or big_maps
            if (node.elements.length > 0 && isArrayLiteralExpression(node.elements[0])) {
                let keyType: ST_TypeDef = { type: FrontendType.TUnknown };
                let valueType: ST_TypeDef = { type: FrontendType.TUnknown };
                for (const el of node.elements) {
                    // Every entry in a map must be an array and have 2 items. [key, value]
                    if (!isArrayLiteralExpression(el) || el.elements.length !== 2) {
                        return this.output.emitError(el, `Expected an array, but received (${SyntaxKind[el.kind]}).`);
                    }
                    const elKeyType = this.checkType(el.elements[0]);
                    const elValueType = this.checkType(el.elements[1]);

                    // Check key type against the previous entries
                    if (keyType.type !== elKeyType.type && keyType.type !== FrontendType.TUnknown) {
                        return this.output.emitError(
                            el,
                            `Expected a map key of type (${keyType.type}), but received (${elKeyType.type}).`,
                        );
                    }

                    // Check value type against the previous entries
                    if (valueType.type !== elValueType.type && valueType.type !== FrontendType.TUnknown) {
                        return this.output.emitError(
                            el,
                            `Expected a map value of type (${valueType.type}), but received (${elValueType.type}).`,
                        );
                    }

                    keyType = elKeyType;
                    valueType = elValueType;
                }
                implicitType = {
                    type: FrontendType.TMap,
                    keyType,

                    valueType,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            } else {
                implicitType = {
                    type: FrontendType.TList,
                    innerTypes:
                        node.elements.length > 0
                            ? node.elements.map(this.checkType)
                            : [
                                  {
                                      type: FrontendType.TUnknown,
                                  },
                              ],
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
        } else if (isEnumDeclaration(node)) {
            implicitType = {
                type: FrontendType.TRecord,
                properties: node.members.reduce<ST_TypeDefs>((state, member) => {
                    const typeName = this.transpiler.extractName(member);
                    state[typeName] = this.checkType(member);
                    return state;
                }, {}),
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            };
        } else if (isObjectLiteralExpression(node)) {
            implicitType = {
                type: FrontendType.TRecord,
                properties: node.properties.reduce<ST_TypeDefs>((state, member) => {
                    const typeName = this.transpiler.extractName(member);
                    state[typeName] = this.checkType(member);
                    return state;
                }, {}),
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            };
        } else if (isPropertyAssignment(node)) {
            implicitType = this.checkType(node.initializer);
        } else if (isFunctionDeclaration(node) || isMethodDeclaration(node) || isArrowFunction(node)) {
            const isArrowFunc = isArrowFunction(node);
            if (!isArrowFunc && !isMethodDeclaration(node)) {
                // Not being an array functions, means that the function can be inlined and must have void as return type;
                if (!node.type || node.type.kind !== SyntaxKind.VoidKeyword) {
                    return this.output.emitError(node, 'Non lambda functions must explicitly set void as return type.');
                }
            }
            // Get type information
            implicitType = {
                type: isArrowFunc ? FrontendType.TLambda : FrontendType.TFunction,
                inputTypes: node.parameters.reduce((state, param, index) => {
                    if (!param.type) {
                        return this.output.emitError(param, 'Function parameter does not have a type.');
                    }
                    const paramName = this.transpiler.extractName(param);
                    const paramType = this.extractTypeDef(param.type);
                    return {
                        ...state,
                        [paramName]: { ...paramType, index },
                    };
                }, {}),
                // If node type is not present, we infer the type as Unit
                outputType: node.type ? this.extractTypeDef(node.type) : TypeBuilder.unit(),
                line: LineUtils.getLineAndCharacter(this.sourceFile, node),
            };
        } else if (isAsExpression(node)) {
            // let A = <expression> as <type>
            implicitType = this.checkType(node.expression);
            explicitType = this.extractTypeDef(node.type);
        } else if (isCallExpression(node)) {
            const isNamespace = (expr: Node): boolean => {
                if (isPropertyAccessExpression(expr)) {
                    return isNamespace(expr.expression);
                }
                if (isParenthesizedExpression(expr)) {
                    return false;
                }
                return true;
            };
            if (isNamespace(node.expression)) {
                const namespace = this.transpiler.extractNamespace(node.expression);
                if (namespace[0] in Native.ST_Namespace) {
                    // It is a native call
                    const baseType = this.dereferenceTypeOfNativeMethod(namespace);
                    if (!baseType) {
                        return TypeBuilder.unknown();
                    }
                    const type = this.extractCallReturnType(baseType, node.arguments);
                    if (type) {
                        return type;
                    }
                    this.output.emitError(node, `Could not resolve call expression type.`);
                }

                const expression = this.interpreters.Statement.extractExpression(node);
                if (guards.expression.hasType(expression)) {
                    return expression.type;
                }
            }
        } else if (isBinaryExpression(node)) {
            const operator = this.interpreters.Statement.extractBinaryOperator(node.operatorToken);
            switch (operator) {
                case ST_BinaryToken.Minus:
                    implicitType = {
                        type: FrontendType.TInt,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
            }
        } else if (isParenthesizedExpression(node)) {
            implicitType = this.checkType(node.expression);
        } else if (isEnumMember(node)) {
            implicitType = node.initializer ? this.checkType(node.initializer) : { type: FrontendType.TString };
        } else {
            switch (node.kind) {
                case SyntaxKind.PrefixUnaryExpression:
                case SyntaxKind.NumericLiteral:
                case SyntaxKind.NumberKeyword:
                    implicitType = {
                        type: FrontendType.TIntOrNat,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
                    break;
                case SyntaxKind.TrueKeyword:
                case SyntaxKind.FalseKeyword:
                case SyntaxKind.BooleanKeyword:
                    implicitType = {
                        type: FrontendType.TBool,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
                    break;
                case SyntaxKind.StringLiteral:
                case SyntaxKind.StringKeyword:
                    implicitType = {
                        type: FrontendType.TString,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
                    break;
                case SyntaxKind.NeverKeyword:
                    implicitType = {
                        type: FrontendType.TNever,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
                    break;
                case SyntaxKind.VoidKeyword:
                    implicitType = {
                        type: FrontendType.TUnit,
                        line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                    };
                    break;

                default:
                    this.output.emitError(node, `Unexpected node (${SyntaxKind[node.kind]} with id ${node.kind}).`);
            }
        }

        // Return the explicit type if implicit and explicit types are compatible.
        const checkedTyped = CheckerUtils.checkTypes(implicitType, explicitType);
        if (checkedTyped) {
            return checkedTyped;
        }

        const errorMsg = `Types mismatch, (${printer.type.toString(implicitType)}) !== (${printer.type.toString(
            explicitType,
        )})`;
        return this.output.emitError(node, errorMsg);
    };

    private getTypesFromArray = (nodes: NodeArray<Node>): ST_TypeDefs => {
        return nodes.reduce<ST_TypeDefs>((state, item) => {
            if (isPropertySignature(item)) {
                const typeName = this.transpiler.extractName(item);
                if (item.type) {
                    state[typeName] = this.extractTypeDef(item.type);
                } else {
                    this.output.emitError(item, 'Property signature must specify a type.');
                }
            }

            return state;
        }, {});
    };

    private composeFrontendType = (type: FrontendType, node: TypeReferenceNode): ST_TypeDef => {
        switch (type) {
            case FrontendType.TList:
            case FrontendType.TSet: {
                const typeArguments = node.typeArguments?.map(this.extractTypeDef) || [];
                return {
                    type,
                    innerTypes: typeArguments,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
            case FrontendType.TOption: {
                const typeArguments = node.typeArguments?.map(this.extractTypeDef) || [];
                return {
                    type,
                    innerType: typeArguments[0],
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
            case FrontendType.TTuple: {
                const typeArguments = node.typeArguments?.map(this.extractTypeDef) || [];
                return typeArguments[0];
            }
            case FrontendType.TMap:
            case FrontendType.TBig_map: {
                const typeArguments = node.typeArguments?.map(this.extractTypeDef) || [];
                return {
                    type,
                    keyType: typeArguments[0],
                    valueType: typeArguments[1],
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
            case FrontendType.TVariant:
            case FrontendType.TRecord: {
                let layout: ST_Layout | Layout = [];
                let properties: ST_TypeDefs = {};
                if (node.typeArguments) {
                    if (node.typeArguments.length < 1) {
                        return this.output.emitError(node, 'TRecord and TVariant expects at least one type parameter.');
                    }
                    layout = node.typeArguments[1] ? this.extractLayout(node.typeArguments[1]) : Layout.right_comb;
                    // Get properties (Can be a UnionType in case of variant or TypeLiteral in case of a record)
                    const type = this.extractTypeDef(node.typeArguments[0]);
                    if (!guards.type.hasProperties(type)) {
                        return this.output.emitError(
                            node.typeArguments[0],
                            `${FrontendType.TRecord} and ${FrontendType.TVariant} must have properties.`,
                        );
                    }
                    properties = type.properties;
                }
                return {
                    type,
                    properties,
                    layout: layout,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
            case FrontendType.TContract: {
                const typeArguments = node.typeArguments?.map(this.extractTypeDef) || [];
                return {
                    type,
                    inputType: typeArguments[0],
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
            case FrontendType.TLambda: {
                const typeArguments =
                    node.typeArguments?.map((arg, index) => ({ ...this.extractTypeDef(arg), index })) || [];
                if (typeArguments.length !== 2) {
                    return this.output.emitError(node, `TLambda type requires 2 arguments.`);
                }
                return {
                    type,
                    inputTypes: {
                        input: typeArguments[0],
                    },
                    outputType: typeArguments[1],
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
            }
            case FrontendType.TAddress:
            case FrontendType.TBls12_381_fr:
            case FrontendType.TBls12_381_g1:
            case FrontendType.TBls12_381_g2:
            case FrontendType.TBool:
            case FrontendType.TBytes:
            case FrontendType.TChain_id:
            case FrontendType.TKey:
            case FrontendType.TKey_hash:
            case FrontendType.TInt:
            case FrontendType.TNat:
            case FrontendType.TIntOrNat:
            case FrontendType.TMutez:
            case FrontendType.TString:
            case FrontendType.TSignature:
            case FrontendType.TBool:
            case FrontendType.TNever:
            case FrontendType.TTimestamp:
            case FrontendType.TUnit:
            case FrontendType.TChest:
            case FrontendType.TChest_key:
            case FrontendType.TUnknown:
                return {
                    type,
                    line: LineUtils.getLineAndCharacter(this.sourceFile, node),
                };
        }
        return this.output.emitError(node, `Unexpected type (${type}).`);
    };

    extractCallReturnType = (baseType: ST_TypeDef, args: NodeArray<Node>): Nullable<ST_TypeDef> => {
        switch (baseType.type) {
            case FrontendType.TContract:
                return {
                    type: baseType.type,
                    inputType: {
                        type: FrontendType.TUnknown,
                    },
                };
            case FrontendType.TOption:
                // @TODO : Maybe we can have some validations here or on the caller
                const innerType = args.reduce<ST_TypeDef>((_, c) => this.checkType(c), { type: FrontendType.TUnknown });
                if (innerType) {
                    return {
                        type: baseType.type,
                        innerType,
                    };
                }
                break;
            case FrontendType.TBig_map: {
                return {
                    type: baseType.type,
                    keyType: { type: FrontendType.TUnknown },
                    valueType: { type: FrontendType.TUnknown },
                };
            }
        }
        return baseType;
    };

    /**
     * @description Get type from access expression
     * @param {string[]} namespace type reference
     * @returns {ST_TypeDef} Type Definition
     */
    dereferenceType = (namespace: string[], node: TypeNode): ST_TypeDef => {
        const getNestedType = (type: ST_TypeDef) =>
            namespace
                .slice(1) // Slice the first item (It is the initial state)
                .reduce((p, c) => {
                    if (guards.type.hasProperties(p) && p.properties[c]) {
                        return p.properties[c];
                    }
                    return p;
                }, type);

        if (this.output.declaringMethod) {
            // <== Inside a method declaration
            const method = this.output.currentMethod;
            // Check the method parameters
            if (method.type.inputTypes[namespace[0]]) {
                // The access expression points to a method parameter
                return getNestedType(method.type.inputTypes[namespace[0]]);
            }
            if (method.typeDefs[namespace[0]]) {
                // The access expression points to a type defined inside the method
                return getNestedType(method.typeDefs[namespace[0]]);
            }
        }

        for (const scope of [...this.output.scopes].reverse()) {
            if (scope.typeDefs[namespace[0]]) {
                return getNestedType(scope.typeDefs[namespace[0]]);
            }
        }

        if (this.output.modules[namespace[0]]?.scope.typeDefs[namespace[1]]) {
            // The access expression points to a type exported by a module
            // The path must contain at least 2 items (<module_name>, <typeName>)
            return getNestedType(this.output.modules[namespace[0]].scope.typeDefs[namespace[1]]);
        }
        return this.output.emitError(node, `Cannot resolve path to type definition (${namespace.join('.')})`);
    };

    /**
     * @description Get type from access expression
     * @param {string[]} namespace property reference
     * @returns {ST_TypeDef} Type Definition
     */
    dereferencePropertyType = (namespace: string[], node: Node): ST_TypeDef => {
        const getNestedType = (type: ST_TypeDef, skip = 1) =>
            namespace
                .slice(skip) // Slice (skip) items (It is the initial state)
                .reduce((p, c) => {
                    if (guards.type.hasProperties(p) && p.properties[c]) {
                        return p.properties[c];
                    }
                    return p;
                }, type);

        if (namespace[0] === 'this' && namespace[1] === 'storage') {
            return getNestedType(this.output.scopes[1].properties['storage'].type, 2);
        }

        if (namespace[0] in Native.ST_Namespace) {
            // It points to a native namespace
            const type = this.dereferenceTypeOfNativeValue(namespace);
            if (type) {
                return type;
            }
            return this.output.emitError(node, `Could not dereference type of (${namespace.join('.')})`);
        }

        if (this.output.declaringMethod) {
            const method = this.output.currentMethod;
            // Check the method parameters
            if (method.type.inputTypes[namespace[0]]) {
                // The access expression points to a method parameter
                return getNestedType(method.type.inputTypes[namespace[0]]);
            }
        }

        for (const scope of [...this.output.scopes].reverse()) {
            // Check iterators
            if (scope.iterators[namespace[0]]) {
                return namespace.slice(!!scope.switchCase ? 0 : 1).reduce((prev, cur) => {
                    if (guards.type.hasProperties(prev)) {
                        if (scope.switchCase && prev.properties[scope.switchCase.accessExpr.name]) {
                            return prev.properties[scope.switchCase.accessExpr.name];
                        } else if (
                            (scope.switchCase?.variant === namespace[0] && cur === 'value') /* Variant access */ ||
                            prev.properties[cur]
                        ) {
                            return prev.properties[cur];
                        }
                    }
                    return TypeBuilder.unknown();
                }, scope.iterators[namespace[0]].type);
            }
            if (scope.properties[namespace[0]]) {
                return getNestedType(scope.properties[namespace[0]].type);
            }
            if (scope.functions[namespace[0]]) {
                return scope.functions[namespace[0]].type;
            }
        }

        if (this.output.modules[namespace[0]] && namespace.length > 1) {
            // The access expression points to a type exported by a module
            // The path must contain at least 2 items (<module_name>, <typeName>)
            return getNestedType(this.output.modules[namespace[0]].scope.properties[namespace[1]].type);
        }

        if (namespace[0] in this.output.result.contracts) {
            const contractInfo = this.output.result.contracts[namespace[0]];
            return TypeBuilder.contract(this.output.classes[contractInfo.classRef]?.scope.properties['storage']?.type);
        }

        return TypeBuilder.unknown();
    };

    dereferenceTypeOfNativeValue = (namespace: string[]): ST_TypeDef | void => {
        const type: Nullable<ST_TypeDef> = namespace.reduce(
            (p: any, c) => (p && c in p ? p[c] : null),
            Native.TypeOfNativeValue,
        );
        if (type) {
            return type;
        }
        return TypeBuilder.unknown();
    };

    dereferenceTypeOfNativeMethod = (namespace: string[]): ST_TypeDef | void => {
        const type = namespace.reduce((p: any, c) => (p && c in p ? p[c] : null), Native.ReturnTypeOfNativeMethod);
        if (type) {
            return type;
        }
        return TypeBuilder.unknown();
    };

    extractLayout = (node: Node): ST_Layout | Layout => {
        const normalizeTuple = (elements: NodeArray<TypeNode | NamedTupleMember>): ST_Layout =>
            elements.reduce((p, c) => {
                if (isLiteralTypeNode(c) && isStringLiteral(c.literal)) {
                    return [...p, c.literal.text];
                }
                if (isTupleTypeNode(c)) {
                    return [...p, normalizeTuple(c.elements)];
                }
                return p;
            }, [] as ST_Layout);

        let layout: ST_Layout | Layout | undefined;
        if (isTypeReferenceNode(node)) {
            const namespace = this.transpiler.extractNamespace(node.typeName);
            layout = namespace.reduce((p: any, c: string) => (c in p ? p[c] : null), Native.ValueOfEnum);
        } else if (isTupleTypeNode(node)) {
            layout = normalizeTuple(node.elements);
        } else {
            this.output.emitError(node, 'Layout must be composed by tuples.');
        }
        return layout || [];
    };

    resolveTypeOfAccessExpression = (expr: ST_Expression, node: Node): ST_TypeDef => {
        switch (expr.kind) {
            case ST_ExpressionKind.AttrAccessExpr:
                const attr = expr.attr;
                if (expr.prev) {
                    const type = this.resolveTypeOfAccessExpression(expr.prev, node);
                    switch (type.type) {
                        case FrontendType.TRecord:
                            if (attr in type.properties) {
                                return type.properties[attr];
                            }
                    }
                }
                break;
            case ST_ExpressionKind.ContractBakerAccess:
                return TypeBuilder.address();
            case ST_ExpressionKind.ContractBalanceAccess:
                return TypeBuilder.mutez();
            case ST_ExpressionKind.ContractAddressAccess:
                return TypeBuilder.address();
            case ST_ExpressionKind.ContractStorageAccess:
                return expr.contract.scope.properties['storage'].type;
            case ST_ExpressionKind.OpenVariant:
                return this.resolveTypeOfAccessExpression(expr.source, node);
            case ST_ExpressionKind.Slice:
            case ST_ExpressionKind.PairCAR:
            case ST_ExpressionKind.PairCDR:
            case ST_ExpressionKind.Pack:
            case ST_ExpressionKind.Sha256:
            case ST_ExpressionKind.Sha3:
            case ST_ExpressionKind.Sha512:
            case ST_ExpressionKind.Keccak:
            case ST_ExpressionKind.Blake2b:
            case ST_ExpressionKind.ToInt:
            case ST_ExpressionKind.ToNat:
            case ST_ExpressionKind.Not:
                return this.resolveTypeOfAccessExpression(expr.expression, node);
            case ST_ExpressionKind.MethodPropAccessExpr:
                return expr.property?.type || { type: FrontendType.TUnknown };
            case ST_ExpressionKind.BinaryExpr:
                return TypeBuilder.unknown();
            case ST_ExpressionKind.NativePropAccessExpr:
            case ST_ExpressionKind.MethodParamAccessExpr:
            case ST_ExpressionKind.StorageAccessExpr:
            case ST_ExpressionKind.GetItem:
            case ST_ExpressionKind.Contract:
            case ST_ExpressionKind.AsExpression:
            case ST_ExpressionKind.Ediv:
            case ST_ExpressionKind.ArrayLiteralExpression:
            case ST_ExpressionKind.ObjectLiteralExpression:
            case ST_ExpressionKind.Unpack:
            case ST_ExpressionKind.ViewAccessExpr:
            case ST_ExpressionKind.TypeAnnotation:
            case ST_ExpressionKind.LiteralExpr:
                return expr.type;
            case ST_ExpressionKind.ContractOrigination:
                return TypeBuilder.record({
                    properties: {
                        balance: TypeBuilder.mutez(),
                        storage: expr.contract.scope.properties['storage'].type,
                    },
                });
            case ST_ExpressionKind.Some:
                return TypeBuilder.option(expr.type);
        }

        return this.output.emitError(node, `Cannot resolve expression kind (${expr.kind}).`);
    };

    validateNativeMethodArgs = (methodArgs: (ST_TypeDef & { optional: boolean })[], expr: CallExpression): void => {
        // Validate arguments
        return methodArgs.forEach((argType, i): void => {
            if (!expr.arguments[i]) {
                if (!argType.optional) {
                    return this.output.emitError(expr, `Argument at position ${i} is missing.`);
                }
            }
        });
    };
}
