import type { NodeFlags } from 'typescript';
import type { ST_ExpressionKind } from '../enums/expression';
import type { ST_Modifier } from '../enums/Modifiers';
import type { ST_StatementKind } from '../enums/statement';
import type { FileLineInfo } from './common';
import type { ST_Property_Decorators } from './decorator';
import type { ST_Expression } from './expression';
import type { ST_TypeDef } from './type';

export type ST_Statement =
    | ST_ExpressionStatement
    | ST_VariableStatement
    | ST_IfStatement
    | ST_WhileStatement
    | ST_ForOfStatement
    | ST_SwitchStatement
    | ST_BindStatement;

export interface ST_VariableStatement {
    kind: ST_StatementKind.VariableStatement;
    local?: boolean;
    decorators: ST_Property_Decorators;
    type: ST_TypeDef;
    name: string;
    modifiers: ST_Modifier[];
    flags: NodeFlags;
    expression: ST_Expression;
    line: FileLineInfo;
}

export interface ST_IfStatement {
    kind: ST_StatementKind.IfStatement;
    expression: ST_Expression;
    thenStatement: ST_Statement[];
    elseStatement: ST_Statement[];
    line: FileLineInfo;
}

// + Loops
export interface ST_ForOfStatement {
    kind: ST_StatementKind.ForOfStatement;
    iterator: Extract<ST_Expression, { kind: ST_ExpressionKind.ForIterator }>;
    expression: ST_Expression;
    statements: ST_Statement[];
    line: FileLineInfo;
}

export interface ST_WhileStatement {
    kind: ST_StatementKind.WhileStatement;
    initializer?: ST_VariableStatement;
    incrementor?: ST_Expression;
    condition: ST_Expression;
    statements: ST_Statement[];
    line: FileLineInfo;
}
// - Loops

export interface ST_SwitchStatement {
    kind: ST_StatementKind.SwitchStatement;
    expression: ST_Expression;
    cases: Record<string, ST_Statement[]>;
    line: FileLineInfo;
}

export interface ST_BindStatement {
    kind: ST_StatementKind.Bind;
    name: string;
    statements: ST_Statements;
    line: FileLineInfo;
}

export interface ST_ExpressionStatement {
    kind: ST_StatementKind.Expression | ST_StatementKind.Result;
    expression: ST_Expression;
    line: FileLineInfo;
}

export type ST_Statements = Record<number, ST_Statement>;
