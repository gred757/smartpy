import type { ST_BinaryToken, ST_ExpressionKind } from '../enums/expression';
import type { ST_Namespace } from '../enums/native';
import type { FrontendType } from '../enums/type';
import type { FileLineInfo, Nullable } from './common';
import type { ST_Property_Decorators } from './decorator';
import type { ST_Literal } from './literal';
import type { ST_Class } from './output';
import type { ST_FunctionProperty, ST_VariableValue } from './property';
import type { ST_VariableStatement } from './statement';
import type { ST_TypeDef } from './type';

export type ST_Expression =
    | ((
          | {
                kind: ST_ExpressionKind.InlineFunction;
                function: ST_FunctionProperty;
            }
          | {
                kind: ST_ExpressionKind.BinaryExpr;
                left: ST_Expression;
                operator: ST_BinaryToken;
                right: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.Never;
                variable: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.Verify;
                condition: ST_Expression;
                message?: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.PrivateLambdaAccessExpr;
                name: string;
            }
          | {
                kind: ST_ExpressionKind.PrivateLambdaResult;

                name: string;
            }
          | {
                kind: ST_ExpressionKind.Contains | ST_ExpressionKind.DelItem | ST_ExpressionKind.Push;
                source: ST_Expression;
                subject: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.IsVariant;
                source: ST_Expression;
                subject: string;
            }
          | {
                kind: ST_ExpressionKind.SetItem;
                target: ST_Expression;
                value: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.VariantAccess;
                name: string;
            }
          | {
                kind: ST_ExpressionKind.Transfer;
                param: ST_Expression;
                amount: ST_Expression;
                contract: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.LambdaExpression;
                lambda: ST_FunctionProperty;
            }
          | {
                kind: ST_ExpressionKind.OpenVariant;
                source: ST_Expression;
                subject: string;
                message?: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.OperationsAccessExpr;
            }
          | {
                kind:
                    | ST_ExpressionKind.Blake2b
                    | ST_ExpressionKind.Sha256
                    | ST_ExpressionKind.Sha512
                    | ST_ExpressionKind.Sha3
                    | ST_ExpressionKind.Keccak
                    | ST_ExpressionKind.Pack
                    | ST_ExpressionKind.Size
                    | ST_ExpressionKind.Reverse
                    | ST_ExpressionKind.PairCAR
                    | ST_ExpressionKind.PairCDR
                    | ST_ExpressionKind.Size
                    | ST_ExpressionKind.ToInt
                    | ST_ExpressionKind.ToNat
                    | ST_ExpressionKind.ABS
                    | ST_ExpressionKind.Not
                    | ST_ExpressionKind.Negate;
                expression: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.SelfEntryPoint;
                epName?: string;
            }
          | {
                kind: ST_ExpressionKind.FailWith;
                message: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.SetDelegate;
                baker: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.ClassAccess;
                class: ST_Class;
            }
          | {
                kind: ST_ExpressionKind.Slice;
                expression: ST_Expression;
                offset: ST_Expression;
                length: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.Concat;
                expression: ST_Expression;
            }
          | {
                kind: ST_ExpressionKind.UpdateSet;
                target: ST_Expression;
                expression: ST_Expression;
                add: boolean;
            }
          | {
                kind: ST_ExpressionKind.Variant;
                action: ST_Expression;
                value: ST_Expression;
            }
      ) & {
          line: FileLineInfo;
      })
    | ST_TypedExpression
    | ST_ScenarioExpression;

export type ST_TypedExpression = (
    | {
          kind: ST_ExpressionKind.ViewAccessExpr;
          name: string;
          address: string;
          arguments: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.StaticViewAccessExpr;
          name: string;
          id: string;
          arguments: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.OpenChest;
          chestKey: ST_Expression;
          chest: ST_Expression;
          time: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind:
              | ST_ExpressionKind.GetElements
              | ST_ExpressionKind.GetKeys
              | ST_ExpressionKind.GetValues
              | ST_ExpressionKind.GetEntries;
          expression: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.AddSeconds;
          target: ST_Expression;
          expression: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.Constant;
          hash: string;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.PairingCheck;
          expression: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.CheckSignature;
          type: ST_TypeDef;
          publicKey: ST_Expression;
          signature: ST_Expression;
          content: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.CreateContractResult;
          type: ST_TypeDef;
          id: string;
      }
    | {
          kind: ST_ExpressionKind.CreateContract | ST_ExpressionKind.CreateContractOperation;
          id: string;
          contract: string;
          type: ST_TypeDef;
          storage: ST_Expression;
          amount: ST_Expression;
          baker?: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.ToAddress;
          type: ST_TypeDef;
          contract: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.ImplicitAccount;
          type: ST_TypeDef;
          keyHash: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.HashKey;
          type: ST_TypeDef;
          key: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.Contract;
          type: ST_TypeDef;
          paramType: ST_TypeDef;
          address: ST_Expression;
          epName?: string;
      }
    | {
          kind: ST_ExpressionKind.VotingPower;
          type: ST_TypeDef;
          keyHash: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.Unpack;
          expression: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.AttrAccessExpr;
          attr: string;
          type: ST_TypeDef;
          prev: Nullable<ST_Expression>;
      }
    | {
          kind: ST_ExpressionKind.MethodPropAccessExpr;
          attr: string;
          property?: ST_VariableStatement;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.TypeAnnotation;
          expression: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.Ediv;
          left: ST_Expression;
          right: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.NativePropAccessExpr;
          prop: string;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.GetItem;
          source: ST_Expression;
          subject: ST_Expression;
          defaultValue?: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.GetLocal;
          name: string;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.StorageAccessExpr;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.MethodParamAccessExpr;
          singleParam: boolean;
          type: ST_TypeDef;
          attr: string;
      }
    | {
          kind: ST_ExpressionKind.LambdaCallExpr;
          lambda: ST_Expression;
          arguments: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.LambdaParamAccessExpr;
          singleParam: boolean;
          id: number;
          type: ST_TypeDef;
          attr: string;
      }
    | {
          kind: ST_ExpressionKind.ForIterator;
          name: string;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.Some;
          expression: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.LiteralExpr;
          type: ST_TypeDef;
          literal: ST_Literal;
      }
    | {
          kind: ST_ExpressionKind.VariableDeclaration;
          name: string;
          type: ST_TypeDef;
          value: ST_VariableValue;
          decorators?: ST_Property_Decorators;
      }
    | {
          kind: ST_ExpressionKind.AsExpression;
          type: ST_TypeDef;
          expression: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.ArrayLiteralExpression;
          type: ST_TypeDef;
          elements: ST_Expression[];
      }
    | {
          kind: ST_ExpressionKind.ObjectLiteralExpression;
          type: Extract<ST_TypeDef, { type: FrontendType.TRecord | FrontendType.TVariant }>;
          properties: Record<string, ST_Expression>;
      }
) & {
    line: FileLineInfo;
};

export type ST_ScenarioExpression = (
    | {
          kind: ST_ExpressionKind.ScenarioVariable;
          id: number;
      }
    | {
          kind: ST_ExpressionKind.ScenarioConstantVariable;
          id: number;
      }
    | {
          kind: ST_ExpressionKind.ScenarioPrepareConstantValue;
          expression: ST_Expression;
          hash?: number;
      }
    | {
          kind: ST_ExpressionKind.IsFailing;
          expression: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.CatchException;
          expression: ST_Expression;
          type: ST_TypeDef;
      }
    | {
          kind: ST_ExpressionKind.NativeMethodExpr;
          method: string;
          module: ST_Namespace;
      }
    | {
          kind: ST_ExpressionKind.NativeModuleAccessExpr;
          module: ST_Namespace;
      }
    | {
          kind: ST_ExpressionKind.EntryPointCall;
          epName: string;
          arguments: Record<string, ST_TypedExpression>;
          line: FileLineInfo;
          contract: ST_Class;
      }
    | {
          kind:
              | ST_ExpressionKind.ContractBalanceAccess
              | ST_ExpressionKind.ContractStorageAccess
              | ST_ExpressionKind.ContractBakerAccess
              | ST_ExpressionKind.ContractAddressAccess;
          contract: ST_Class;
      }
    | {
          kind: ST_ExpressionKind.ContractTypedAccess;
          contract: ST_Class;
          epName: string;
      }
    | {
          kind: ST_ExpressionKind.ContractOrigination;
          contract: ST_Class;
      }
    | {
          kind: ST_ExpressionKind.ScenarioHtml;
          content: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.ScenarioShow;
          expression: ST_Expression;
          compile: boolean;
          html: boolean;
          stripStrings: boolean;
      }
    | {
          kind: ST_ExpressionKind.ScenarioMakeSignature;
          privateKey: ST_Expression;
          content: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.ScenarioVerify;
          condition: ST_Expression;
      }
    | {
          kind: ST_ExpressionKind.ScenarioTestAccount;
          seed: string;
      }
    | {
          kind: ST_ExpressionKind.ScenarioTestAccountAccess;
          account: Extract<ST_ScenarioExpression, { kind: ST_ExpressionKind.ScenarioTestAccount }>;
          prop: string;
      }
) & {
    line: FileLineInfo;
};
