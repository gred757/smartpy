import { FrontendType } from '../../../../src/enums/type';
import Transpiler from '../../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../../ExpectAnyLine';

test(`${FrontendType.TBig_map}<${FrontendType.TNat}, ${FrontendType.TNat}>`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type big_map__nat_nat__type = ${FrontendType.TBig_map}<${FrontendType.TNat}, ${FrontendType.TNat}>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        big_map__nat_nat__type: {
            line: ExpectAnyLine,
            type: FrontendType.TBig_map,
            keyType: {
                line: ExpectAnyLine,
                type: FrontendType.TNat,
            },
            valueType: {
                line: ExpectAnyLine,
                type: FrontendType.TNat,
            },
        },
    });
});
