import { FrontendType } from '../../../../src/enums/type';
import Transpiler from '../../../../src/lib/Transpiler';
import { ExpectAnyLine } from '../../../ExpectAnyLine';

test(`${FrontendType.TMap}<${FrontendType.TNat}, ${FrontendType.TNat}>`, async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `​type map__nat_nat__type = ${FrontendType.TMap}<${FrontendType.TNat}, ${FrontendType.TNat}>;`,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();

    expect(transpiler.output.globalTypeDefs).toEqual({
        map__nat_nat__type: {
            line: ExpectAnyLine,
            type: FrontendType.TMap,
            keyType: {
                line: ExpectAnyLine,
                type: FrontendType.TNat,
            },
            valueType: {
                line: ExpectAnyLine,
                type: FrontendType.TNat,
            },
        },
    });
});
