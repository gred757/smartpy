export const ExpectAnyLine = {
    character: expect.any(Number),
    line: expect.any(Number),
    fileName: expect.any(String),
};
