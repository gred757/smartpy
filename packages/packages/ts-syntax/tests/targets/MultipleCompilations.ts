interface StorageInterface {
    text: TString;
}

const initial_storage_1: StorageInterface = {
    text: 'Hello World',
};

@Contract
export class Sample1 {
    constructor(public storage: StorageInterface) {}
}

const initial_storage_2 = 1;

@Contract
export class Sample2 {
    constructor(private storage: TNat = initial_storage_2) {}
}

Dev.compileContract('compile_contract_1', new Sample1(initial_storage_1));
Dev.compileContract(
    'compile_contract_2',
    new Sample1({
        text: 'Hello World',
    }),
);
Dev.compileContract('compile_contract_3', new Sample2(1 as TNat));
Dev.compileContract('compile_contract_4', new Sample2());
