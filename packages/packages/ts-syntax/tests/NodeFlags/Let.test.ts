import Transpiler from '../../src/lib/Transpiler';

test('Let values can be assigned new values', async () => {
    const sourceCode = {
        name: 'code.ts',
        code: `
        @Contract
        class LetValue {
            @EntryPoint
            ep() {
                let let_value = 1;
                let_value = 2;
            }
        }
        `,
    };
    const transpiler = new Transpiler(sourceCode);
    await transpiler.transpile();
});
