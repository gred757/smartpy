import * as fs from 'fs';
import * as path from 'path';

export const getCode = (file: string): string =>
    fs.readFileSync(path.resolve(`${file}.ts`), { encoding: 'utf8', flag: 'r' });
