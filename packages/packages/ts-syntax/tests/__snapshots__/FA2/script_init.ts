/**
 * Type Definitions
 */

// Declare the storage type definition
interface FA2Storage {
    config: {
        admin: TAddress;
        paused: TBool;
    };
    assets: {
        ledger: TBig_map<TTuple<[TAddress, TNat]>, { balance: TNat }>;
        operators: TBig_map<{ operator: TAddress; owner: TAddress; token_id: TNat }, TUnit>;
        token_metadata: TBig_map<TNat, { token_id: TNat; token_info: TMap<TString, TBytes> }>;
        token_total_supply: TBig_map<TNat, TNat>;
    };
    metadata: TBig_map<TString, TBytes>;
}

// transfer entrypoint
type TransferParams = TList<
    TRecord<
        {
            from_: TAddress;
            txs: TList<
                TRecord<
                    {
                        to_: TAddress;
                        token_id: TNat;
                        amount: TNat;
                    },
                    ['to_', ['token_id', 'amount']]
                >
            >;
        },
        ['from_', 'txs']
    >
>;

// update_operators entrypoint
type OperatorParam = TRecord<
    {
        owner: TAddress;
        operator: TAddress;
        token_id: TNat;
    },
    ['owner', ['operator', 'token_id']]
>;
type UpdateOperatorsParams = TList<
    TVariant<
        | {
              kind: 'add_operator';
              value: OperatorParam;
          }
        | {
              kind: 'remove_operator';
              value: OperatorParam;
          },
        ['add_operator', 'remove_operator']
    >
>;

// balance_of entrypoint
type BalanceOfRequest = TRecord<
    {
        owner: TAddress;
        token_id: TNat;
    },
    ['owner', 'token_id']
>;
type BalanceOfResponse = TRecord<
    {
        request: BalanceOfRequest;
        balance: TNat;
    },
    ['request', 'balance']
>;
type BalanceOfParams = TRecord<
    {
        callback: TContract<TList<BalanceOfResponse>>;
        requests: TList<BalanceOfRequest>;
    },
    ['requests', 'callback']
>;

enum ErrorCodes {
    FA2_TOKEN_UNDEFINED,
    FA2_NOT_ADMIN,
    FA2_PAUSED,
}

@Contract
class FA2 {
    // Declare the initial storage
    storage: FA2Storage = {
        config: {
            admin: 'tz1',
            paused: false,
        },
        assets: {
            ledger: Sp.bigMap([]),
            operators: Sp.bigMap([]),
            token_metadata: Sp.bigMap([]),
            token_total_supply: Sp.bigMap([]),
        },
        metadata: Sp.bigMap([]),
    };

    /**
     * @description transfer token ownership
     * @param {TransferParams} transfer - A list that can contain multiple transactions
     */
    @EntryPoint
    transfer(transfer: TransferParams) {
        Sp.verify(!this.storage.config.paused, ErrorCodes.FA2_PAUSED);
        for (const transaction of transfer) {
            for (const tx of transaction.txs) {
                Sp.verify(this.storage.assets.token_metadata.hasKey(tx.token_id), ErrorCodes.FA2_TOKEN_UNDEFINED);
                Sp.verify(
                    Sp.sender === this.storage.config.admin ||
                        transaction.from_ === Sp.sender ||
                        this.storage.assets.operators.hasKey({
                            owner: transaction.from_,
                            operator: Sp.sender,
                            token_id: tx.token_id,
                        } as OperatorParam),
                    'FA2_NOT_OPERATOR',
                );
                if (tx.amount > 0) {
                    const sourceLedger: TTuple<[TAddress, TNat]> = [transaction.from_, tx.token_id];
                    const recipientLedger: TTuple<[TAddress, TNat]> = [tx.to_, tx.token_id];
                    Sp.verify(
                        this.storage.assets.ledger.get(sourceLedger).balance >= tx.amount,
                        'FA2_INSUFFICIENT_BALANCE',
                    );

                    // Decrement balance from source
                    const newBalance: TNat = this.storage.assets.ledger.get(sourceLedger).balance - tx.amount;
                    this.storage.assets.ledger.get(sourceLedger).balance = newBalance as TNat;

                    if (this.storage.assets.ledger.hasKey(recipientLedger)) {
                        this.storage.assets.ledger.get(recipientLedger).balance += tx.amount;
                    } else {
                        this.storage.assets.ledger.set(recipientLedger, { balance: tx.amount });
                    }
                }
            }
        }
    }

    /**
     * @description mint tokens
     * @param {TNat} tokenId - Token identifier
     * @param {TAddress} address - Recipient address
     * @param {TNat} amount - Amount to mint
     * @param {TMap<TString, TBytes>} metadata - Token metadata
     */
    @EntryPoint
    mint(tokenId: TNat, address: TAddress, amount: TNat, metadata: TMap<TString, TBytes>) {
        Sp.verify(Sp.sender == this.storage.config.admin, 'FA2_NOT_ADMIN');
        // We don't check for pauseness because we're the admin.
        const user: TTuple<[TAddress, TNat]> = [address, tokenId];
        if (this.storage.assets.ledger.hasKey(user)) {
            this.storage.assets.ledger.get(user).balance += amount;
        } else {
            this.storage.assets.ledger.set(user, { balance: amount });
        }
        if (!this.storage.assets.token_metadata.hasKey(tokenId)) {
            this.storage.assets.token_metadata.set(tokenId, {
                token_id: tokenId,
                token_info: metadata,
            });
            this.storage.assets.token_total_supply.set(tokenId, amount);
        }
    }

    /**
     * @description update operators
     * @param {UpdateOperatorsParams} params
     */
    @EntryPoint
    update_operators(params: UpdateOperatorsParams) {
        for (const update of params) {
            switch (update.kind) {
                case 'add_operator': {
                    Sp.verify(
                        update.value.owner === Sp.sender || Sp.sender === this.storage.config.admin,
                        'FA2_NOT_ADMIN_OR_OWNER',
                    );
                    this.storage.assets.operators.set(update.value, Sp.unit);
                }
                case 'remove_operator': {
                    Sp.verify(
                        update.value.owner === Sp.sender || Sp.sender === this.storage.config.admin,
                        'FA2_NOT_ADMIN_OR_OWNER',
                    );
                    this.storage.assets.operators.remove(update.value);
                }
            }
        }
    }

    /**
     * @description entrypoint used as view for other contracts to inspect the balance of a given address
     * @param {BalanceOfParams} params
     */
    @EntryPoint
    balance_of(params: BalanceOfParams): void {
        Sp.verify(!this.storage.config.paused, 'FA2_PAUSED');
        const responses: TList<BalanceOfResponse> = [];
        for (const request of params.requests) {
            Sp.verify(this.storage.assets.token_metadata.hasKey(request.token_id), 'FA2_TOKEN_UNDEFINED');
            if (this.storage.assets.ledger.hasKey([request.owner, request.token_id])) {
                responses.push({
                    balance: this.storage.assets.ledger.get([request.owner, request.token_id]).balance,
                    request,
                });
            } else {
                responses.push({
                    balance: 0,
                    request,
                });
            }
        }
        Sp.transfer(responses, 0, params.callback);
    }

    /**
     * @description Pause the contract
     * @param {TBool} paused
     */
    @EntryPoint
    pause(paused: TBool) {
        this.storage.config.paused = paused;
    }

    /**
     * @description Update the administrator address
     * @param {TAddress} address - New admin address
     */
    @EntryPoint
    set_admin(address: TAddress) {
        this.storage.config.admin = address;
    }

    /**
     * @description Update contract metadata
     * @param {TAddress} address - New admin address
     */
    @EntryPoint
    update_metadata(metadata: TBig_map<TString, TBytes>) {
        this.storage.metadata = metadata;
    }
}
