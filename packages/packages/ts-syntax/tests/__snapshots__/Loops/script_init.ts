@Contract
class Loops {
    storage: TUnit = Sp.unit;

    @EntryPoint
    for() {
        for (let i = 0; i < 10; i += 1) {
            i += 1;
        }
    }

    @EntryPoint
    while() {
        let i = 1;
        while (i == 1) {
            i += 1;
        }
    }
}
