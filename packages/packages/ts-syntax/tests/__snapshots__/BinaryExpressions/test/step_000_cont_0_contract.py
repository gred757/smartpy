import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(counter1 = sp.TInt, counter2 = sp.TInt, counter3 = sp.TNat).layout(("counter1", ("counter2", "counter3"))))
    self.init(counter1 = 0,
              counter2 = 0,
              counter3 = 0)

  @sp.entry_point
  def equal(self):
    self.data.counter1 = 1

  @sp.entry_point
  def plus(self):
    self.data.counter1 = 1 + 1
    self.data.counter1 += 1
    self.data.counter1 += self.data.counter2 + 1

  @sp.entry_point
  def minus(self):
    self.data.counter1 = 1 - 1
    self.data.counter1 -= 1
    self.data.counter1 -= self.data.counter2 - 1

  @sp.entry_point
  def mul(self):
    self.data.counter1 = 1 * 1
    self.data.counter1 *= 1
    self.data.counter1 *= self.data.counter2 * 1

  @sp.entry_point
  def div(self):
    self.data.counter3 = 1 // 1