# SmartTS CLI

## Requirements

| Requirement |  version  |
|:-----------:|:---------:|
| NodeJs      | >= v10.x  |

## Install the CLI
```sh
npm i -g @smartpy/smart-ts-cli
```

## Generate a scenario
```sh
# Generate a scenario from ts code.
smart-ts scenario --file code.ts --outDir .
```

## Help
```
smart-ts help scenario
```

<img height="48" href="https://smartpy.io" src="https://smartpy.io/static/img/logo.png">
