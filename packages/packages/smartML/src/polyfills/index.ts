import Bls12 from 'tezos-bls12-381';
import Timelock from '@smartpy/timelock';
import eztz from './eztz';

globalThis.eztz = eztz;
globalThis.smartpyContext = {
    Bls12,
    Timelock
};
