import React from 'react';
import { useDispatch } from 'react-redux';

// Material UI
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Typography from '@mui/material/Typography';
import Chip from '@mui/material/Chip';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ChromeReaderMode from '@mui/icons-material/ChromeReaderMode';
import PlayIcon from '@mui/icons-material/PlayArrow';
import SquareFoot from '@mui/icons-material/SquareFoot';

import actions from '../actions';
import { useNewcomerMode, useNewcomerGuideStep } from '../selectors';
import useTranslation from '../../i18n/hooks/useTranslation';
import useCommonStyles from '../../../hooks/useCommonStyles';
import { IDENewcomerGuideSteps } from '../../common/enums/ide';

interface OwnProps {
    handleClose: () => void;
}

const NewcomerDialog: React.FC<OwnProps> = (props) => {
    const t = useTranslation();
    const classes = useCommonStyles();
    const newcomerMode = useNewcomerMode();
    const newcomerGuideStep = useNewcomerGuideStep();
    const dispatch = useDispatch();

    const { handleClose } = props;

    const toggleNewcomersMode = () => {
        dispatch(actions.updateSettings({ newcomersMode: !newcomerMode }));
    };

    const showHowToGetATemplate = async () => {
        dispatch(actions.updateNewcomerGuideStep(IDENewcomerGuideSteps.TEMPLATES));
    };

    const showHowToRunCode = async () => {
        dispatch(actions.updateNewcomerGuideStep(IDENewcomerGuideSteps.RUN_CODE));
        handleClose();
    };

    const handleLoadTemplate = async (fileName: string) => {
        dispatch(actions.loadTemplate(fileName));
    };

    React.useEffect(() => {
        dispatch(
            actions.toggleNewcomerDialog(Boolean(newcomerMode) && newcomerGuideStep === IDENewcomerGuideSteps.NONE),
        );
    }, [dispatch, newcomerGuideStep, newcomerMode]);

    return (
        <React.Fragment>
            <Typography>{t('ide.newcomer.dialog.This popup presents a few templates for newcomers')}</Typography>
            <Typography variant="h6" className={classes.spacingTopTwo}>
                {t('ide.newcomer.dialog.SmartPy IDE allows you to')}
            </Typography>
            <List>
                <ListItem divider>
                    <ListItemIcon>
                        <SquareFoot color="primary" />
                    </ListItemIcon>
                    <ListItemText primary={t('ide.newcomer.dialog.selectTemplate')} />
                    <ListItemSecondaryAction>
                        <Button variant="contained" onClick={showHowToGetATemplate}>
                            {t('ide.newcomer.dialog.showMeWhere')}
                        </Button>
                    </ListItemSecondaryAction>
                </ListItem>
                <ListItem divider>
                    <ListItemIcon>
                        <PlayIcon color="primary" />
                    </ListItemIcon>
                    <ListItemText primary={t('ide.newcomer.dialog.editAndRun')} />
                    <ListItemSecondaryAction>
                        <Button variant="contained" onClick={showHowToRunCode}>
                            {t('ide.newcomer.dialog.showMeWhere')}
                        </Button>
                    </ListItemSecondaryAction>
                </ListItem>
                <ListItem divider>
                    <ListItemIcon>
                        <ChromeReaderMode color="primary" />
                    </ListItemIcon>
                    <ListItemText primary={t('ide.newcomer.dialog.observeAndDeploy')} />
                </ListItem>
                <ListItem>
                    <ListItemText primary={t('ide.newcomer.dialog.andMuchMore')} />
                </ListItem>
            </List>
            <Typography variant="h6">{t('ide.newcomer.dialog.someTemplates')}</Typography>
            <div>
                <Typography variant="caption">{t('ide.newcomer.dialog.beginnerTemplates')}</Typography>
                <div>
                    <Chip
                        className={classes.spacingOne}
                        clickable
                        variant="outlined"
                        size="small"
                        label={t('ide.newcomer.dialog.calculator')}
                        onClick={() => handleLoadTemplate('calculator.py')}
                    />
                    <Chip
                        className={classes.spacingOne}
                        clickable
                        variant="outlined"
                        size="small"
                        label={t('ide.newcomer.dialog.tictactoe')}
                        onClick={() => handleLoadTemplate('tictactoe.py')}
                    />
                    <Chip
                        className={classes.spacingOne}
                        clickable
                        variant="outlined"
                        size="small"
                        label={t('ide.newcomer.dialog.minikitties')}
                        onClick={() => handleLoadTemplate('minikitties.py')}
                    />
                </div>
            </div>
            <div>
                <Typography variant="caption">{t('ide.newcomer.dialog.advancedTemplates')}</Typography>
                <div>
                    <Chip
                        className={classes.spacingOne}
                        clickable
                        variant="outlined"
                        size="small"
                        label={t('ide.newcomer.dialog.FA1-2')}
                        onClick={() => handleLoadTemplate('FA1.2.py')}
                    />
                    <Chip
                        className={classes.spacingOne}
                        clickable
                        variant="outlined"
                        size="small"
                        label={t('ide.newcomer.dialog.FA2')}
                        onClick={() => handleLoadTemplate('FA2.py')}
                    />
                    <Chip
                        className={classes.spacingOne}
                        clickable
                        variant="outlined"
                        size="small"
                        label={t('ide.newcomer.dialog.oracle')}
                        onClick={() => handleLoadTemplate('oracle.py')}
                    />
                </div>
            </div>
            <div>
                <Typography variant="caption">{t('ide.newcomer.dialog.expertTemplates')}</Typography>
                <div>
                    <Chip
                        className={classes.spacingOne}
                        clickable
                        variant="outlined"
                        size="small"
                        label={t('ide.newcomer.dialog.stateChannels')}
                        onClick={() => handleLoadTemplate('stateChannels.py')}
                    />
                </div>
            </div>
            <FormControlLabel
                className={classes.spacingTopTwo}
                control={<Checkbox checked={newcomerMode} onChange={toggleNewcomersMode} color="primary" />}
                label={t('ide.newcomer.dialog.Stay in Newcomer Mode') as string}
                labelPlacement="end"
            />
        </React.Fragment>
    );
};

export default NewcomerDialog;
