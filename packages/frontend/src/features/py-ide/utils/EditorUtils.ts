import pako from 'pako';

import toast from '../../../services/toast';

/**
 * Evaluate the code in the editor
 *
 * @param {boolean} withTests - When true, the evaluation will run with tests.
 */
export const evalRun = async (withTests: boolean) => {
    try {
        if (typeof window.evalRun == 'function') {
            window.evalRun(withTests);
        } else {
            toast.error('SmartPyIO is not loaded yet...');
        }
    } catch (error) {
        try {
            window.showTraceback(error, String(error));
        } catch (_error) {
            toast.error(String(error));
        }
    }
};

/**
 * Evaluate a specific test.
 *
 * @param {string} testName - Name of the Tests to evaluate.
 */
export const evalTest = async (testName: string) => {
    try {
        window.evalTest(testName);
    } catch (error) {
        window.showTraceback(error, String(error));
    }
};

export const getEmbeddedLink = (contract: string) => {
    const encoded = btoa(pako.deflate(contract, { to: 'string' }))
        .replace(/\+/g, '@')
        .replace(/\//g, '_')
        .replace(/=/g, '-');
    return `${window.location.origin}/ide?code=${encoded}`;
};
