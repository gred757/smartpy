import React from 'react';

// Material UI
import ShareIcon from '@mui/icons-material/ShareOutlined';
import Button from '@mui/material/Button';

import { OriginationContract } from 'SmartPyModels';

// Utils
import useTranslation from '../../i18n/hooks/useTranslation';
import IpfsLinkShare from '../../common/components/IpfsLinkShare';

type OwnProps = {
    contract: OriginationContract;
};

const ShareOrigination: React.FC<OwnProps> = ({ contract }) => {
    const [open, setOpen] = React.useState(false);
    const t = useTranslation();

    const onCloseOpen = () => {
        setOpen((s) => !s);
    };

    return (
        <>
            <Button color="primary" endIcon={<ShareIcon />} onClick={onCloseOpen}>
                {t('origination.shareButton')}
            </Button>
            <IpfsLinkShare
                content={JSON.stringify(contract)}
                baseUrl={`${window.location.origin}${process.env.PUBLIC_URL}/origination`}
                open={open}
                handleClose={onCloseOpen}
                title={t('origination.shareButton')}
            />
        </>
    );
};

export default ShareOrigination;
