import i18n from 'i18next';
import Backend from 'i18next-http-backend';
import { initReactI18next } from 'react-i18next';
import { getBase } from '../../utils/url';

export const SupportedLanguages = ['en', 'fr'];

i18n.use(initReactI18next) // passes i18n down to react-i18next
    .use(Backend)
    .init({
        backend: {
            loadPath: `${getBase()}/static/locales/{{lng}}.json`,
        },
        lng: 'en',
        fallbackLng: 'en',
        // Uncomment when debugging translation issues
        // debug: true,
    });

export default i18n;
