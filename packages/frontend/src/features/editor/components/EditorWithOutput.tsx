import React from 'react';
import SplitPane from 'react-split-pane/lib/SplitPane';
import Pane from 'react-split-pane/lib/Pane';

// Material UI
import { Box, styled } from '@mui/material';

// State Management
import { IDESettings } from 'SmartPyModels';

// Local Components
import OutputPanel from 'src/features/common/components/OutputPanel';

// Local Utils
import debounce from '../../../utils/debounce';
import Logger from 'src/services/logger';

// Debounce helpers
const onDebouncer = debounce(1000);

const PANE_SIZES_STORAGE_KEY = 'PANE_SIZES';

const OutputWrapper = styled('div')<{ useTopBorder?: boolean }>(({ theme, useTopBorder }) => ({
    position: 'relative',
    height: '100%',
    padding: 15,
    overflowY: 'auto',
    ...(useTopBorder
        ? {
              borderTopWidth: 2,
              borderTopStyle: 'solid',
              borderTopColor: theme.palette.primary.light,
          }
        : typeof useTopBorder === 'undefined'
        ? {}
        : {
              borderLeftWidth: 2,
              borderLeftStyle: 'solid',
              borderLeftColor: theme.palette.primary.light,
          }),
}));

interface OwnProps {
    htmlOutput: { __html: string };
    compiling: boolean;
    editor: JSX.Element;
    settings: IDESettings;
    outputPanelRef: React.MutableRefObject<HTMLDivElement>;
    alert?: JSX.Element;
}

const EditorWithOutput: React.FC<OwnProps> = ({ alert, settings, htmlOutput, compiling, editor, outputPanelRef }) => {
    const [sizes, setSizes] = React.useState<{ editorSize?: string; outputSize?: string }>({
        editorSize: undefined,
        outputSize: undefined,
    });

    React.useEffect(() => {
        try {
            const { editorSize, outputSize }: { editorSize?: string; outputSize?: string } = JSON.parse(
                localStorage.getItem(PANE_SIZES_STORAGE_KEY) || '',
            );
            setSizes({
                editorSize,
                outputSize,
            });
        } catch (error: any) {
            Logger.debug(error);
        }
    }, []);

    const saveSplitPositions = React.useCallback((sizes: { editorSize: string; outputSize: string }) => {
        localStorage.setItem(PANE_SIZES_STORAGE_KEY, JSON.stringify(sizes));
        setSizes(sizes);
    }, []);

    if (settings.layout === 'editor-only' || settings.layout === 'output-only') {
        console.error(htmlOutput);
        return (
            <Box
                sx={{
                    width: '100%',
                }}
            >
                {editor}
                <OutputWrapper ref={outputPanelRef}>
                    {alert}
                    <div>
                        <OutputPanel output={htmlOutput} isContractCompiling={compiling} />
                    </div>
                </OutputWrapper>
            </Box>
        );
    }

    return (
        <SplitPane
            split={settings.layout === 'stacked' ? 'horizontal' : 'vertical'}
            onChange={([editorSize, outputSize]: string[]) =>
                onDebouncer(saveSplitPositions, { editorSize, outputSize })
            }
        >
            <Pane minSize={'300px'} initialSize={sizes.editorSize}>
                {editor}
            </Pane>
            <Pane minSize={'200px'} initialSize={sizes.outputSize}>
                <OutputWrapper useTopBorder={settings.layout === 'stacked'} ref={outputPanelRef}>
                    {alert}
                    <OutputPanel output={htmlOutput} isContractCompiling={compiling} />
                </OutputWrapper>
            </Pane>
        </SplitPane>
    );
};

export default EditorWithOutput;
