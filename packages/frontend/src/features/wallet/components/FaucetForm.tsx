import React from 'react';
import { useDropzone } from 'react-dropzone';
import { useDispatch } from 'react-redux';
import WalletStorage from 'smartpy-wallet-storage';
import { FaucetAccount } from 'smartpy-wallet-storage/dist/types/services/faucet/storage';
import type { editor } from 'monaco-editor/esm/vs/editor/editor.api';
import type { OnMount } from '@monaco-editor/react';

// Material UI
import useMediaQuery from '@mui/material/useMediaQuery';
import { Theme, useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Container from '@mui/material/Container';
import ImportFile from '@mui/icons-material/AttachFile';
import CopyPaste from '@mui/icons-material/Description';
import Link from '@mui/material/Link';
import Divider from '@mui/material/Divider';
import StorageIcon from '@mui/icons-material/Storage';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';

import useTranslation from '../../i18n/hooks/useTranslation';
import AccountInfo from './AccountInfo';
import toast from '../../../services/toast';
import TabPanel from '../../common/elements/TabPanel';

import { clearAccountInfo, loadingAccountInfo, updateAccountInfo } from '../actions';
import { getRpcNetwork } from '../../../utils/tezosRpc';
import WalletServices from '../services';
import { useNetworkInfo } from '../selectors/network';
import logger from '../../../services/logger';
import { FaucetParams } from '../services/smartpy/Faucet';
import { AccountSource } from '../constants/sources';
import { RenderIfTrue } from '../../../utils/conditionalRender';
import { objectIsNotEmpty } from '../../../utils/object';
import { getBase } from '../../../utils/url';
import Editor from '../../editor/components/Editor';
import useWalletContext from '../hooks/useWalletContext';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: 5,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%',
        },
        paper: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: 300,
            flexGrow: 1,
            boxShadow: 'inset 0 0 10px #000000',
        },
        fullHeight: {
            height: '100%',
        },
        section: {
            padding: 20,
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        storedFaucet: {
            paddingTop: theme.spacing(2),
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
        },
        input: {
            padding: '10px 12px 8px',
        },
    }),
);

const FAUCET_REQUIRED_FIELDS = ['mnemonic', 'password', 'email'];

const FaucetForm: React.FC = () => {
    const storedAccounts = WalletStorage.faucet.findAll();
    const classes = useStyles();
    const t = useTranslation();
    const theme = useTheme();
    const { addFaucetAccount } = useWalletContext();
    const editorTheme = theme.palette.mode === 'dark' ? 'vs-dark' : 'vs-light';
    const textAreaRef = React.useRef(null as unknown as editor.IStandaloneCodeEditor);
    const downMd = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'));
    const [tab, setTab] = React.useState(objectIsNotEmpty(storedAccounts) ? 0 : 1);
    const { rpc } = useNetworkInfo();
    const dispatch = useDispatch();
    const [accountInfo, setAccountInfo] = React.useState<FaucetAccount>();
    const { getRootProps, getInputProps } = useDropzone({
        accept: '.json, application/json',
        multiple: false,
        getFilesFromEvent: async (event: any) => {
            const files = event.dataTransfer ? event.dataTransfer.files : event.target.files;
            if (files.length > 0) {
                handleFileImporterComplete(files[0]);
            }
            return [];
        },
    });

    const saveAccount = React.useCallback(() => {
        if (accountInfo) {
            addFaucetAccount(accountInfo);
            setAccountInfo(undefined);
        }
    }, [accountInfo, addFaucetAccount]);

    const updateName = (event: React.ChangeEvent<{ value: string }>) => {
        setAccountInfo((state) => {
            if (state) {
                return {
                    ...state,
                    name: event.target.value,
                };
            }
        });
    };

    const loadAccount = async (faucet: FaucetParams) => {
        // Reset Account Information
        dispatch(clearAccountInfo());

        dispatch(loadingAccountInfo(true));
        try {
            const network = await getRpcNetwork(rpc);
            if (network) {
                await WalletServices[AccountSource.SMARTPY_FAUCET].import(rpc, faucet);
                const accountInformation = await WalletServices[AccountSource.SMARTPY_FAUCET].getInformation();
                dispatch(updateAccountInfo(accountInformation));
                const privateKey = await WalletServices[AccountSource.SMARTPY_FAUCET].secretKey();
                const publicKey = await WalletServices[AccountSource.SMARTPY_FAUCET].publicKey();
                if (faucet.pkh && privateKey && publicKey) {
                    setAccountInfo({
                        address: faucet.pkh,
                        name: faucet.pkh,
                        secret: faucet.secret || faucet.activation_code || '',
                        privateKey,
                        publicKey,
                    });
                }
            }
        } catch (e) {
            logger.error(e);
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        } finally {
            dispatch(loadingAccountInfo(false));
        }
    };

    const loadStoredAccount = async (event: SelectChangeEvent<string>) => {
        // Reset Account Information
        dispatch(clearAccountInfo());

        dispatch(loadingAccountInfo(true));
        try {
            await WalletServices[AccountSource.SMARTPY_FAUCET].importSecretKey(rpc, {
                secretKey: storedAccounts[event.target.value].privateKey,
                secret: storedAccounts[event.target.value].secret,
            });
            const accountInformation = await WalletServices[AccountSource.SMARTPY_FAUCET].getInformation();
            dispatch(updateAccountInfo(accountInformation));
        } catch (e) {
            logger.error(e);
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        } finally {
            dispatch(loadingAccountInfo(false));
        }
    };

    const handleTabChange = (event: React.SyntheticEvent<Element, Event>, newValue: number) => {
        setTab(newValue);
    };

    const parseJSONAndImportFaucet = (jsonString: string) => {
        try {
            const json = JSON.parse(jsonString);
            const jsonKeys = Object.keys(json);
            if (FAUCET_REQUIRED_FIELDS.every((f) => jsonKeys.includes(f))) {
                // Faucet is valid
                loadAccount(json);
            } else {
                dispatch(
                    updateAccountInfo({
                        errors: t('wallet.faucetForm.invalid'),
                    }),
                );
            }
        } catch (e) {
            dispatch(
                updateAccountInfo({
                    errors: e.message,
                }),
            );
        }
    };

    function handleFileImporterComplete(file: File) {
        const reader = new FileReader();

        const fail = () => {
            toast.error(t('common.couldNotLoadFile'));
        };

        reader.onabort = fail;
        reader.onerror = fail;
        reader.onload = () => {
            parseJSONAndImportFaucet(reader.result as string);
        };
        reader.readAsText(file);
    }

    const handleEditorDidMount: OnMount = (editor) => {
        textAreaRef.current = editor;
    };

    const onTextInput = () => {
        if (textAreaRef.current) {
            parseJSONAndImportFaucet(textAreaRef.current?.getValue());
        }
    };

    return (
        <div className={classes.root}>
            <Typography variant="overline">{t('wallet.faucetForm.title')}</Typography>
            <Link href="https://teztnets.xyz/" variant="caption" target="_blank">
                {t('wallet.faucetForm.clickToAccessFaucet')}
            </Link>
            <Container fixed maxWidth="sm">
                <Paper className={classes.section}>
                    <AppBar position="static" color="default">
                        <Tabs value={tab} onChange={handleTabChange} aria-label="Faucet Menu" variant="fullWidth">
                            <Tab label={downMd ? undefined : t('wallet.faucetForm.storage')} icon={<StorageIcon />} />
                            <Tab label={downMd ? undefined : t('fileImporter.dragAndDropTab')} icon={<ImportFile />} />
                            <Tab label={downMd ? undefined : t('fileImporter.copyAndPasteTab')} icon={<CopyPaste />} />
                        </Tabs>
                    </AppBar>

                    <TabPanel value={tab} index={0} className={classes.storedFaucet}>
                        <FormControl variant="filled" fullWidth disabled={!objectIsNotEmpty(storedAccounts)}>
                            <InputLabel id="stored-faucet-selector">
                                {t('wallet.faucetForm.selectStoredFaucet')}
                            </InputLabel>
                            <Select
                                labelId="stored-faucet-selector"
                                defaultValue=""
                                displayEmpty
                                onChange={loadStoredAccount}
                            >
                                {Object.keys(storedAccounts).map((acc) => (
                                    <MenuItem value={acc} key={acc} divider>
                                        <Typography variant="caption" noWrap>
                                            {storedAccounts[acc].name}
                                        </Typography>
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                        <Link href={`${getBase()}/wallet`} variant="caption" target="_blank" align="right">
                            {t('wallet.faucetForm.manageFaucet')}
                        </Link>
                    </TabPanel>
                    <TabPanel value={tab} index={1}>
                        <div
                            {...getRootProps({
                                className: classes.paper,
                            })}
                        >
                            <input {...getInputProps()} />
                            <Typography variant="caption">
                                {t(downMd ? 'fileImporter.dragAndDropTab' : 'fileImporter.dragAndDrop')}
                            </Typography>
                        </div>
                    </TabPanel>
                    <TabPanel value={tab} index={2}>
                        <Editor
                            onMount={handleEditorDidMount}
                            language="json"
                            theme={editorTheme}
                            options={{
                                automaticLayout: true,
                                selectOnLineNumbers: true,
                                minimap: {
                                    enabled: false,
                                },
                            }}
                            className={classes.paper}
                            onChange={onTextInput}
                        />
                    </TabPanel>
                    <RenderIfTrue
                        isTrue={
                            !!accountInfo?.address &&
                            !objectIsNotEmpty(WalletStorage.faucet.findAll(accountInfo.address))
                        }
                    >
                        <Divider className={classes.divider} />
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={8}>
                                <TextField
                                    fullWidth
                                    variant="filled"
                                    value={accountInfo?.name || ''}
                                    InputProps={{
                                        classes: { input: classes.input },
                                    }}
                                    onChange={updateName}
                                />
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <Button
                                    fullWidth
                                    onClick={saveAccount}
                                    variant="contained"
                                    className={classes.fullHeight}
                                >
                                    {t('wallet.faucetForm.saveFaucet')}
                                </Button>
                            </Grid>
                        </Grid>
                    </RenderIfTrue>
                </Paper>
            </Container>
            <Divider className={classes.divider} />
            <AccountInfo />
        </div>
    );
};

export default FaucetForm;
