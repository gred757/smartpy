import { RootState } from 'SmartPyTypes';
import { useSelector } from 'react-redux';
import { NetworkInformation } from 'SmartPyModels';

export const useNetworkInfo = () =>
    useSelector<RootState, NetworkInformation>((state: RootState) => state.wallet.networkInfo);
