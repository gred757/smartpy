import TezMonitor from 'tezmonitor';

import { OperationKindType, TezosNodeReader, TezosNodeWriter } from '../../../../services/conseil';

import { AccountSource } from '../../constants/sources';
import WalletError from '../../exceptions/WalletError';
import FaucetErrorCodes from '../../exceptions/FaucetErrorCodes';
import AbstractWallet from '../AbstractWallet';
import LocalSigner from './LocalSigner';

export type FaucetParams = {
    pkh: string;
    secret?: string;
    activation_code?: string;
    email: string;
    password: string;
    mnemonic: string[];
};

class FaucetWallet extends AbstractWallet {
    private faucet?: FaucetParams;

    constructor() {
        super(AccountSource.SMARTPY_FAUCET);
    }

    /**
     * @description Connect to the wallet.
     *
     * @param rpc RPC address.
     * @param params Faucet parameters.
     *
     * @returns A promise that resolves to void;
     */
    public import = async (rpc: string, params: FaucetParams) => {
        this.signer = LocalSigner.fromFaucet(params.email, params.password, params.mnemonic.join(' '));

        if (!this.signer) {
            throw new WalletError(FaucetErrorCodes.NOT_ABLE_TO_IMPORT_ACCOUNT);
        }

        // Some faucet files use `activation_code` instead of `secret`
        this.faucet = { ...params, secret: params.secret || params.activation_code };
        this.rpc = rpc;
        this.pkh = await this.signer.publicKeyHash();
    };

    /**
     * @description Connect to the wallet.
     *
     * @param rpc RPC address.
     * @param params The secret seed and an optional password.
     *
     * @returns A promise that resolves to void;
     */
    public importSecretKey = async (rpc: string, params: { secretKey: string; secret: string }) => {
        this.signer = await LocalSigner.fromSecretKey(params.secretKey);

        if (!this.signer) {
            throw new WalletError(FaucetErrorCodes.NOT_ABLE_TO_IMPORT_ACCOUNT);
        }

        (this.faucet as any) = {
            secret: params.secret,
        };
        this.rpc = rpc;
        this.pkh = await this.signer.publicKeyHash();
    };

    public requiresActivation = async () => {
        if (this.rpc && this.pkh) {
            return await TezosNodeReader.isImplicitAndEmpty(this.rpc, this.pkh);
        }
        return false;
    };

    public activateAccount = async () => {
        if (this.signer && this.rpc && this.pkh && this.faucet?.secret) {
            const blockHeader = await TezosNodeReader.getBlockHead(this.rpc);

            if (blockHeader) {
                const operationContents = {
                    kind: OperationKindType.AccountActivation,
                    pkh: this.pkh,
                    secret: this.faucet.secret,
                };

                const bytes = await TezosNodeWriter.forgeOperations(blockHeader.hash, [operationContents]);
                const { sbytes, prefixSig } = await this.signer.sign(bytes);

                const signedOpBytes = Buffer.from(sbytes, 'hex');
                const signedOpGroup = { bytes: signedOpBytes, signature: prefixSig };

                const opHash = await TezosNodeWriter.injectOperation(this.rpc, signedOpGroup);

                await await TezMonitor.operationConfirmations(this.rpc, JSON.parse(opHash), 1).toPromise();
            }
        }
    };
}

export default FaucetWallet;
