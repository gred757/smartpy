import React from 'react';

import { Alert, Dialog, DialogActions, DialogContent, DialogProps, DialogTitle, Slide } from '@mui/material';
import Button from '@mui/material/Button';
import LockOpenOutlinedIcon from '@mui/icons-material/LockOpenOutlined';

import useTranslation from 'src/features/i18n/hooks/useTranslation';
import useWalletContext from '../hooks/useWalletContext';
import TextField from 'src/features/common/elements/TextField';
import Logger from 'src/services/logger';

const Transition = React.forwardRef(function Transition(
    props: { children: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface OwnProps extends DialogProps {}

const UnlockWalletDialog: React.FC<OwnProps> = (props) => {
    const [password, setPassword] = React.useState('');
    const [error, setError] = React.useState('');
    const { unlock } = useWalletContext();
    const t = useTranslation();

    const handleUnlock = () => {
        try {
            unlock(password);
        } catch (e) {
            setError(e?.message);
            Logger.debug(e);
        }
    };

    const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value);
    };

    return (
        <Dialog fullWidth {...props} TransitionComponent={Transition}>
            <DialogTitle>{t('wallet.labels.unlockAccount')}</DialogTitle>{' '}
            {error ? (
                <Alert severity="error" sx={{ marginBottom: 3 }}>
                    {error}
                </Alert>
            ) : null}
            <DialogContent dividers>
                <TextField
                    fullWidth
                    required
                    label={t('common.password')}
                    type="password"
                    onChange={handleTextChange}
                />
            </DialogContent>
            <DialogActions>
                <Button autoFocus color="primary" onClick={handleUnlock} endIcon={<LockOpenOutlinedIcon />}>
                    {t('common.unlock')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default UnlockWalletDialog;
