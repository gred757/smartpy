import React from 'react';
// Material UI
import { styled, Container as MuiContainer, Grid, Paper, Divider, Typography, PaperProps } from '@mui/material';

import useTranslation from '../../i18n/hooks/useTranslation';
import SmartContractIcon from 'src/features/common/elements/icons/SmartContract';
import RouterButton from 'src/features/navigation/elements/RouterButton';
import FaucetIcon from 'src/features/common/elements/icons/FaucetOutlined';
import TezosWalletIcon from 'src/features/common/elements/icons/TezosWallet';

const BUTTONS = [
    {
        route: '/wallet/smart-contracts',
        label: 'smartContracts',
        icon: SmartContractIcon,
    },
    {
        route: '/wallet/faucet-accounts',
        label: 'faucetAccounts',
        icon: FaucetIcon,
    },
    {
        route: '/wallet/accounts',
        label: 'accounts',
        icon: TezosWalletIcon,
    },
];

const Container = styled(MuiContainer)(() => ({
    display: 'flex',
    flexGrow: 1,
    alignContent: 'center',
    justifyContent: 'center',
    padding: 20,
}));

const Card = styled(Paper)<PaperProps & { component: React.ReactNode; to: string }>(({ theme }) => ({
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: theme.palette.text.primary,
    boxShadow: `5px 5px 0 0 ${theme.palette.text.primary}`,
    height: 200,
    width: 200,
    display: 'flex',
    flexDirection: 'column',
    alignContent: 'center',
    justifyContent: 'center',
    padding: 5,
}));

const WalletView = () => {
    const t = useTranslation();

    return (
        <Container>
            <Grid container justifyContent="center" alignItems="center" spacing={4}>
                {BUTTONS.map(({ route, label, icon: Icon }) => (
                    <Grid key={label} item>
                        <Card elevation={5} component={RouterButton} to={route}>
                            <div style={{ width: 100 }}>
                                <Icon />
                            </div>
                            <Divider flexItem sx={{ margin: 2 }} />
                            <Typography variant="overline" textAlign="center">
                                {t(`wallet.labels.${label}`)}
                            </Typography>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </Container>
    );
};

export default WalletView;
