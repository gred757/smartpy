import React from 'react';

import { Theme } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Divider from '@mui/material/Divider';

import { WalletStatus } from 'SmartPyWalletTypes';
import BeaconFull from '../../common/elements/icons/BeaconFull';
import SmartPyDialog from './SmartPyDialog';
import AccountInfo from '../components/AccountInfo';
import { getBase } from '../../../utils/url';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        button: {
            height: 64,
            borderWidth: 2,
            backgroundColor: theme.palette.background.default,
        },
        buttonImage: {
            height: 42,
        },
        progress: {
            width: 84,
        },
        centralizedContainer: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            padding: 10,
        },
        divider: {
            margin: theme.spacing(1, 0, 1, 0),
        },
        chip: {
            backgroundColor: theme.palette.background.paper,
        },
        card: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'stretch',
            backgroundColor: theme.palette.background.default,
            boxShadow: '0 2px 4px rgba(0,0,0,0.15), 0 4px 2px rgba(0,0,0,0.22)',
            padding: theme.spacing(1),
        },
        avatarSection: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
            padding: 15,
            borderRadius: 4,
            backgroundColor: theme.palette.background.paper,
        },
        infoSection: {
            width: 424,
        },
        alignSelf: {
            alignSelf: 'stretch',
        },
    }),
);

interface OwnProps {
    walletStatus: WalletStatus;
    onTempleClick: () => void;
    onBeaconClick: () => void;
}

const WalletWidget: React.FC<OwnProps> = ({ walletStatus, onTempleClick, onBeaconClick }) => {
    const classes = useStyles();

    return (
        <div>
            <Grid container spacing={3} justifyContent="center">
                <Grid item xs={12} sm={4}>
                    <SmartPyDialog variant="outlined" fullWidth className={classes.button} />
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Button variant="outlined" fullWidth className={classes.button} onClick={onBeaconClick}>
                        <BeaconFull />
                    </Button>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Button
                        variant="outlined"
                        fullWidth
                        className={classes.button}
                        disabled={!walletStatus.temple}
                        onClick={onTempleClick}
                    >
                        <img
                            src={`${getBase()}/static/img/temple-logo.svg`}
                            alt="Temple-Wallet"
                            className={classes.buttonImage}
                        />
                    </Button>
                </Grid>
            </Grid>
            <Divider className={classes.divider} />
            <AccountInfo disableNetworkSelection />
        </div>
    );
};

export default WalletWidget;
