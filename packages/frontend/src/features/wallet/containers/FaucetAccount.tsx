import React from 'react';

import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { styled } from '@mui/styles';
import {
    SelectChangeEvent,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Container as MuiContainer,
    Box,
    Divider,
    TextField,
} from '@mui/material';
import ArrowBackOutlinedIcon from '@mui/icons-material/ArrowBackOutlined';

import RouterFab from 'src/features/navigation/elements/RouterFab';
import { Network } from 'src/constants/networks';
import * as RPC from 'src/constants/rpc';

import { fetchContractOperations, Operation } from 'src/utils/tzkt';
import useWalletContext from '../hooks/useWalletContext';
import { useNetworkInfo } from '../selectors/network';
import AccountView from '../views/Account';
import { updateAccountInfo, updateNetworkInfo } from '../actions';
import WalletService from '../services';
import Logger from 'src/services/logger';
import CircularProgressWithText from 'src/features/loader/components/CircularProgressWithText';
import useTranslation from 'src/features/i18n/hooks/useTranslation';
import { getRpcNetwork } from 'src/utils/tezosRpc';

const Container = styled(MuiContainer)(() => ({
    display: 'flex',
    flexDirection: 'column',
    padding: 20,
}));

const TopDiv = styled('div')(({ theme }) => ({
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
}));

const FaucetAccount = () => {
    const { faucetId } = useParams<{ faucetId: string }>();
    const { faucetAccounts, updateFaucetAccount } = useWalletContext();
    const networkInfo = useNetworkInfo();
    const [operations, setOperations] = React.useState<Operation[]>([]);
    const t = useTranslation();
    const dispatch = useDispatch();

    const network = React.useMemo(() => networkInfo?.network as Network, [networkInfo?.network]);
    const account = React.useMemo(() => faucetAccounts.find(({ id }) => id === faucetId), [faucetAccounts, faucetId]);

    const updateAccount = React.useCallback(
        (_account: { name: string }) => {
            if (account) {
                updateFaucetAccount({
                    ...account,
                    ..._account,
                });
            }
        },
        [account, updateFaucetAccount],
    );

    const fetchInformation = React.useCallback(async () => {
        if (account) {
            try {
                await WalletService.SMARTPY_SECRET_KEY.import(networkInfo.rpc, { secretKey: account.privateKey });
                const accountInfo = await WalletService.SMARTPY_SECRET_KEY.getInformation();
                dispatch(updateAccountInfo(accountInfo));
                fetchContractOperations(account.address, network).then(setOperations);
            } catch (e) {
                Logger.debug(e);
            }
        }
    }, [account, dispatch, network, networkInfo.rpc]);

    React.useEffect(() => {
        fetchInformation();
    }, [fetchInformation]);

    const onRpcChange = async (event: React.ChangeEvent<{ value: string }>) => {
        const rpc = event.target.value;
        dispatch(updateNetworkInfo({ rpc }));
        try {
            const network = RPC.Nodes[rpc] || (await getRpcNetwork(rpc));
            dispatch(updateNetworkInfo({ rpc, network }));
        } catch (e) {
            Logger.debug(e);
        }
    };

    const handleNetworkSelection = (event: SelectChangeEvent<string>) => {
        const _network = event.target.value as Network;
        if (RPC.smartpy[_network]) {
            dispatch(
                updateNetworkInfo({
                    rpc: RPC.smartpy[_network],
                    network: _network,
                }),
            );
        }
    };

    if (!account) {
        return <CircularProgressWithText size={64} margin={40} msg={t('common.loading')} />;
    }

    return (
        <Container>
            <TopDiv>
                <RouterFab color="primary" to="/wallet/faucet-accounts">
                    <ArrowBackOutlinedIcon />
                    {t('wallet.labels.goBackToFaucet')}
                </RouterFab>
                <Box sx={{ display: 'flex' }}>
                    <TextField size="small" value={networkInfo?.rpc} onChange={onRpcChange}></TextField>
                    <Divider sx={{ margin: 1 }} />
                    <FormControl size="small" sx={{ width: 200 }}>
                        <InputLabel id="network">{t('wallet.labels.network')}</InputLabel>
                        <Select
                            name="network"
                            labelId="network"
                            label={t('wallet.labels.network')}
                            value={network || Network.MAINNET}
                            onChange={handleNetworkSelection}
                        >
                            {(Object.values(Network) || []).map((_network) => {
                                return (
                                    <MenuItem disabled={_network === Network.CUSTOM} value={_network} key={_network}>
                                        {_network}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                </Box>
            </TopDiv>
            <AccountView network={network} operations={operations} account={account} updateAccount={updateAccount} />
        </Container>
    );
};

export default FaucetAccount;
