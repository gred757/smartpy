import React from 'react';
import { useDispatch } from 'react-redux';

import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import Light from '@mui/icons-material/Brightness7';
import Dark from '@mui/icons-material/Brightness4';

import selectors from '../../../store/selectors';
import actions from '../../../store/root-action';

const useStyles = makeStyles(() =>
    createStyles({
        darkThemeToggle: {
            marginRight: 20,
        },
    }),
);

const DarkLightSwitch = () => {
    const isDarkMode = selectors.theme.useThemeMode();
    const dispatch = useDispatch();
    const classes = useStyles();

    return (
        <Tooltip
            title="Toggle Light/Dark Theme"
            aria-label="theme"
            placement="left"
            className={classes.darkThemeToggle}
        >
            <IconButton
                aria-label="theme"
                size="small"
                onClick={() => dispatch(actions.theme.setDarkMode(!isDarkMode))}
            >
                {isDarkMode ? <Dark /> : <Light />}
            </IconButton>
        </Tooltip>
    );
};

export default DarkLightSwitch;
