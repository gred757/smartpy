import React from 'react';
import { RenderIfTrue } from '../../../utils/conditionalRender';

interface OwnProps {
    className?: string;
    children?: React.ReactNode;
    index: number;
    value: number;
}
const TabPanel: React.FC<OwnProps> = ({ children, value, index, ...other }) => (
    <RenderIfTrue isTrue={value === index}>
        <div role="tabpanel" id={`tabpanel-${index}`} {...other}>
            {value === index && children}
        </div>
    </RenderIfTrue>
);

export default TabPanel;
