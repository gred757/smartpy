import React from 'react';
import { useTheme } from '@mui/material/styles';

const LedgerIcon = () => {
    const textColor = useTheme().palette.text.primary;
    return (
        <svg width={24} viewBox="0 0 20 20">
            <g>
                <g>
                    <path id="Shape" d="M16.7,0H7.59V12.29h12.3V3.18A3.18,3.18,0,0,0,16.7,0Z" fill={textColor} />
                    <path d="M4.75,0H3.18A3.18,3.18,0,0,0,0,3.18V4.75H4.75Z" fill={textColor} />
                    <polygon id="Rectangle-path" points="0 7.59 4.75 7.59 4.75 12.34 0 12.34 0 7.59" fill={textColor} />
                    <path d="M15.18,19.89h1.57a3.18,3.18,0,0,0,3.18-3.19V15.18H15.18Z" fill={textColor} />
                    <polygon points="7.59 15.18 12.34 15.18 12.34 19.93 7.59 19.93 7.59 15.18" fill={textColor} />
                    <path d="M0,15.18v1.57a3.18,3.18,0,0,0,3.18,3.18H4.75V15.18Z" fill={textColor} />
                </g>
            </g>
        </svg>
    );
};

export default LedgerIcon;
