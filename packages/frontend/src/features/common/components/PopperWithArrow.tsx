import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import Popper, { PopperProps } from '@mui/material/Popper';

const useStyles = (withGlowAnimation: boolean) =>
    makeStyles((theme: Theme) =>
        createStyles({
            popper: {
                position: 'absolute',
                zIndex: 2000,
                marginTop: 6,
                '&[x-placement*="bottom"] $arrow': {
                    top: 0,
                    left: 0,
                    marginTop: '-1em',
                    width: '3em',
                    height: '1em',
                    '&::before': {
                        borderWidth: '0 1em 1em 1em',
                        borderColor: `transparent transparent ${theme.palette.primary.main} transparent`,
                    },
                },
                '&[x-placement*="top"] $arrow': {
                    bottom: 0,
                    left: 0,
                    marginBottom: '-1em',
                    width: '3em',
                    height: '1em',
                    '&::before': {
                        borderWidth: '1em 1em 0 1em',
                        borderColor: `${theme.palette.primary.main} transparent transparent transparent`,
                    },
                },
                '&[x-placement*="right"] $arrow': {
                    left: 0,
                    marginLeft: '-1em',
                    height: '3em',
                    width: '1em',
                    '&::before': {
                        borderWidth: '1em 1em 1em 0',
                        borderColor: `transparent ${theme.palette.primary.main} transparent transparent`,
                    },
                },
                '&[x-placement*="left"] $arrow': {
                    right: 0,
                    marginRight: '-1em',
                    height: '3em',
                    width: '1em',
                    '&::before': {
                        borderWidth: '1em 0 1em 1em',
                        borderColor: `transparent transparent transparent ${theme.palette.primary.main}`,
                    },
                },
                ...(withGlowAnimation ? { animation: '$glow 1s infinite alternate', borderRadius: 3 } : {}),
            },
            arrow: {
                position: 'absolute',
                fontSize: 7,
                width: '3em',
                height: '3em',
                '&::before': {
                    content: '""',
                    margin: 'auto',
                    display: 'block',
                    width: 0,
                    height: 0,
                    borderStyle: 'solid',
                },
            },
            '@keyframes glow': {
                from: {
                    boxShadow: `0 0 2px 0 ${theme.palette.primary.main}`,
                },
                to: {
                    boxShadow: `0 0 2px 2px ${theme.palette.primary.main}`,
                },
            },
        }),
    );

interface OwnProps extends PopperProps {
    withGlowAnimation?: boolean;
}

const PopperWithArrow: React.FC<OwnProps> = ({ children, withGlowAnimation = false, ...props }) => {
    const classes = useStyles(withGlowAnimation)();
    const [arrowRef, setArrowRef] = React.useState(null);

    return (
        <Popper
            {...props}
            placement="bottom"
            disablePortal={false}
            className={classes.popper}
            modifiers={[
                {
                    name: 'flip',
                    enabled: true,
                },
                {
                    name: 'arrow',
                    enabled: true,
                    options: {
                        element: arrowRef,
                    },
                },
            ]}
        >
            <span className={classes.arrow} ref={setArrowRef as any} />
            {children}
        </Popper>
    );
};

export default PopperWithArrow;
