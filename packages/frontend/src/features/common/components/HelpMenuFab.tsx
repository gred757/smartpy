import React from 'react';

// Material UI
import { Theme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';
import Menu from '@mui/material/Menu';
import HelpIcon from '@mui/icons-material/HelpOutlineOutlined';
import Fab from '@mui/material/Fab';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        zoomRoot: {
            position: 'fixed',
            bottom: theme.spacing(2),
            right: theme.spacing(3),
        },
    }),
);

const HelpMenuFab: React.FC = ({ children }) => {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef<HTMLButtonElement>(null);

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
            return;
        }

        setOpen(false);
    };

    return (
        <React.Fragment>
            <div role="presentation" className={classes.zoomRoot} aria-controls="help-menu" onClick={handleToggle}>
                <Fab color="primary" size="small" ref={anchorRef}>
                    <HelpIcon />
                </Fab>
            </div>
            <Menu
                anchorEl={anchorRef.current}
                keepMounted
                open={Boolean(open)}
                onClose={handleClose}
                onClick={handleClose}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
            >
                {children}
            </Menu>
        </React.Fragment>
    );
};

export default HelpMenuFab;
