import React from 'react';

import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import Button from '@mui/material/Button';
import Slide from '@mui/material/Slide';

import useTranslation from '../../i18n/hooks/useTranslation';

const Transition = React.forwardRef(function Transition(
    props: { children: React.ReactElement<any, any> },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="down" ref={ref} {...props} />;
});

interface OwnProps {
    open: boolean;
    onClose: () => void;
    error?: JSX.Element;
}

const ErrorDialog: React.FC<OwnProps> = ({ children, open, onClose, error }) => {
    const t = useTranslation();

    return (
        <Dialog
            maxWidth="md"
            fullWidth
            open={open}
            onClose={onClose}
            aria-labelledby="error-dialog-title"
            TransitionComponent={Transition}
        >
            <DialogTitle id="error-dialog-title">{error || 'Error'}</DialogTitle>
            <DialogContent dividers>{children}</DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="primary">
                    {t('common.close')}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ErrorDialog;
