import React from 'react';
import makeBlockie from 'ethereum-blockies-base64';

import { Theme, Skeleton } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';

const useStyles = (width: number) =>
    makeStyles((theme: Theme) =>
        createStyles({
            blockie: {
                position: 'relative',
                display: 'flex',
                overflow: 'hidden',
                flexShrink: 0,
                alignItems: 'center',
                justifyContent: 'center',
                userSelect: 'none',
                borderRadius: '50%',
                backgroundColor: theme.palette.primary.main,
                padding: 1,
                margin: 5,
                width,
            },
        }),
    );

type Sizes = 'small' | 'medium' | 'large';
enum Size {
    Small = 'small',
    Medium = 'medium',
    Large = 'large',
}
const getWidth = (size: string) => {
    switch (size) {
        case Size.Small:
            return 24;
        case Size.Medium:
            return 32;
        case Size.Large:
            return 64;
        default:
            return 48;
    }
};

interface OwnProps {
    address?: string;
    size: Sizes;
}

const AddressAvatar: React.FC<OwnProps> = ({ address, ...props }) => {
    const size = getWidth(props.size);
    const classes = useStyles(size)();

    const avatar = React.useMemo(() => (address ? makeBlockie(address) : null), [address]);

    return avatar ? (
        <img src={avatar} className={classes.blockie} alt={`avatar-${address}`} />
    ) : (
        <Skeleton variant="circular" width={size} height={size} />
    );
};

export default AddressAvatar;
