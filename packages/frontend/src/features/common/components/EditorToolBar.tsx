import React from 'react';
import { useDispatch } from 'react-redux';

import { IDEContract } from 'SmartPyModels';

// Material UI
import { Theme } from '@mui/material/styles';
import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';
import useMediaQuery from '@mui/material/useMediaQuery';
import Grid from '@mui/material/Grid';
import ButtonGroup from '@mui/material/ButtonGroup';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Tooltip from '@mui/material/Tooltip';
import Chip from '@mui/material/Chip';
import DialogActions from '@mui/material/DialogActions';
import Paper from '@mui/material/Paper';
// Material UI Icons
import DeleteIcon from '@mui/icons-material/DeleteSweep';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import CompilationIcon from '@mui/icons-material/Description';

// Local Components
import ShareMenuItem from './ShareMenuItem';
import ToolsMenuItem from './ToolsMenuItem';
import PopperWithArrow from './PopperWithArrow';

// Local Hooks
import useCommonStyles from '../../../hooks/useCommonStyles';
import useTranslation from '../../i18n/hooks/useTranslation';
import TestIcon from '../../common/elements/icons/Test';
import { IDENewcomerGuideSteps, SYNTAX } from '../enums/ide';
import { ST_Scenario } from '@smartpy/ts-syntax/dist/types/@types/scenario';
import { Divider } from '@mui/material';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            alignItems: 'center',
            height: 50,
            borderWidth: 1,
            backgroundColor: theme.palette.background.paper,
        },
        gridItem: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        noMargin: {
            margin: 0,
        },
    }),
);

interface OwnProps {
    clearOutputs: () => void;
    targets?: ({ kind: string; name: string } | ST_Scenario)[];
    downloadOutputPanel: () => void;
    compileContract: (withTests?: boolean) => void;
    runScenario?: (testName: string) => void;
    updateNewcomerGuideStep?: (step: IDENewcomerGuideSteps) => void;
    newcomerGuideStep?: IDENewcomerGuideSteps;
    setVolatileContract: (code: string) => void;
    selectedContract: IDEContract | undefined;
    baseUrl: string;
    templatesMenu: React.ReactElement;
    settingsMenu: React.ReactElement;
    syntax: SYNTAX;
}
const EditorToolBar: React.FC<OwnProps> = (props) => {
    const classes = { ...useStyles(), ...useCommonStyles() };
    const [open, setOpen] = React.useState(false);
    const refs = {
        testsButton: React.useRef<HTMLButtonElement>(null),
        playButton: React.useRef<HTMLButtonElement>(null),
    };
    const onlyIcon = useMediaQuery((theme: Theme) => theme.breakpoints.down('xl'));
    const t = useTranslation();
    const dispatch = useDispatch();

    const {
        clearOutputs,
        targets,
        downloadOutputPanel,
        compileContract,
        runScenario,
        updateNewcomerGuideStep,
        newcomerGuideStep,
        setVolatileContract,
        selectedContract,
        baseUrl,
        templatesMenu,
        settingsMenu,
        syntax,
    } = props;

    const [compileTargets, testTargets, miscTargets] = React.useMemo(
        () =>
            (targets || []).reduce<
                [
                    ({ kind: string; name: string } | ST_Scenario)[],
                    ({ kind: string; name: string } | ST_Scenario)[],
                    { [kind: string]: ({ kind: string; name: string } | ST_Scenario)[] },
                ]
            >(
                (acc, cur) => {
                    if (cur.kind === 'compilation') {
                        acc[0] = [...acc[0], cur];
                    } else if (cur.kind === 'test') {
                        acc[1] = [...acc[1], cur];
                    } else {
                        acc[2] = {
                            ...acc[2],
                            [cur.kind]: [...(acc[2][cur.kind] || []), cur],
                        };
                    }
                    return acc;
                },
                [[], [], {}],
            ),
        [targets],
    );

    const handleToggle = () => {
        setOpen((prevState) => !prevState);
        handleNewcomerInteraction();
    };

    const handleClose = (event: React.MouseEvent<EventTarget>) => {
        if (refs.testsButton.current && refs.testsButton.current.contains(event.target as HTMLElement)) {
            return;
        }
        setOpen(false);
    };

    const handleNewcomerInteraction = () => {
        if (updateNewcomerGuideStep) {
            dispatch(updateNewcomerGuideStep(IDENewcomerGuideSteps.NONE));
        }
    };
    return (
        <div className={classes.root}>
            <Grid container justifyContent="space-between" alignItems="center">
                <Grid item xs={12} sm={6} className={classes.gridItem}>
                    <ButtonGroup disableElevation>
                        <Tooltip title={t('ide.toolBar.runCode') as string} aria-label="run-code" placement="left">
                            <Button
                                ref={refs.playButton}
                                onClick={() => compileContract(true)}
                                endIcon={onlyIcon ? null : <PlayArrowIcon />}
                                variant="contained"
                            >
                                {onlyIcon ? <PlayArrowIcon /> : 'Run'}
                            </Button>
                        </Tooltip>
                        {targets ? (
                            <Button
                                endIcon={<ArrowDropDownIcon />}
                                variant="outlined"
                                disabled={targets.length === 0}
                                ref={refs.testsButton}
                                aria-controls="tests-menu"
                                aria-haspopup="true"
                                onClick={handleToggle}
                            >
                                Targets
                            </Button>
                        ) : null}
                        <Tooltip
                            title={t('ide.toolBar.clearOutputs') as string}
                            aria-label="clear-outputs"
                            placement="right"
                        >
                            <Button onClick={clearOutputs} variant="outlined">
                                <DeleteIcon />
                            </Button>
                        </Tooltip>
                    </ButtonGroup>
                </Grid>
                <Grid item xs={12} sm={6} className={classes.gridItem} display="flex" justifyContent="center">
                    {templatesMenu}
                    <Divider flexItem orientation="vertical" sx={{ margin: 1 }} />
                    <ToolsMenuItem
                        selectedContract={selectedContract}
                        setVolatileContract={setVolatileContract}
                        onlyIcon={onlyIcon}
                        downloadOutputPanel={downloadOutputPanel}
                        syntax={syntax}
                    />
                    <Divider flexItem orientation="vertical" sx={{ margin: 1 }} />
                    <ShareMenuItem onlyIcon={onlyIcon} code={selectedContract?.code} baseUrl={baseUrl} />
                    <Divider flexItem orientation="vertical" sx={{ margin: 1 }} />
                    {settingsMenu}
                </Grid>
            </Grid>
            {/* Tests menu */}
            <Menu
                anchorEl={refs.testsButton.current}
                open={Boolean(open)}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                {compileTargets.length > 0 ? (
                    <div>
                        <MenuItem disabled>
                            <ListItemText primary={t('ide.output.compilations')} />
                        </MenuItem>
                        {compileTargets.map(({ name }, i) => (
                            <MenuItem key={i} onClick={() => runScenario && runScenario(name)}>
                                <ListItemIcon>
                                    <CompilationIcon />
                                </ListItemIcon>
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </div>
                ) : null}
                {testTargets.length > 0 ? (
                    <div>
                        <MenuItem disabled>
                            <ListItemText primary={t('ide.output.tests')} />
                        </MenuItem>
                        {testTargets.map(({ name }, i) => (
                            <MenuItem key={i} onClick={() => runScenario && runScenario(name)}>
                                <ListItemIcon>
                                    <TestIcon />
                                </ListItemIcon>
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </div>
                ) : null}
                {Object.entries(miscTargets).map(([kind, list]) => (
                    <div key={kind}>
                        <MenuItem disabled>
                            <ListItemText primary={kind} />
                        </MenuItem>
                        {list.map(({ name }, i) => (
                            <MenuItem key={i} onClick={() => runScenario && runScenario(name)}>
                                <ListItemIcon>
                                    <TestIcon />
                                </ListItemIcon>
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </div>
                ))}
            </Menu>
            {/* Play Button Guide Step (Used in newcomer mode) */}
            <PopperWithArrow
                open={newcomerGuideStep === IDENewcomerGuideSteps.RUN_CODE}
                withGlowAnimation
                anchorEl={refs.playButton.current}
            >
                <Paper>
                    <Chip
                        className={classes.spacingTwo}
                        variant="outlined"
                        label={t('ide.newcomer.runCodeStep.popperChip')}
                    />
                    <DialogActions>
                        <Button fullWidth variant="contained" onClick={handleNewcomerInteraction}>
                            {t('common.understood')}
                        </Button>
                    </DialogActions>
                </Paper>
            </PopperWithArrow>
        </div>
    );
};

export default EditorToolBar;
