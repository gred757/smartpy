import { Theme } from '@mui/material/styles';

import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme: Theme) =>
    createStyles({
        spacingTopTwo: { marginTop: theme.spacing(2) },
        spacingOne: {
            margin: theme.spacing(1),
        },
        spacingTwo: {
            margin: theme.spacing(2),
        },
        noShadow: {
            boxShadow: 'none',
        },
    }),
);
