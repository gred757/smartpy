import { Network } from '../constants/networks';
import * as RPC from '../constants/rpc';
import httpRequester from '../services/httpRequester';
import logger from '../services/logger';

/**
 * @description Get the RPC network.
 * @param {string} rpc - RPC address ( e.g. https://mainnet.smartpy.io )
 * @return {string} One of the following networks [MAINNET]
 */
export const getRpcNetwork = async (rpc: string): Promise<Network | undefined> => {
    const {
        network_version: { chain_name },
    } = await httpRequester
        .get(`${rpc}/version`)
        .then(({ data }) => data)
        .catch(async (e) => {
            logger.warn(e);
            return {
                network_version: await httpRequester.get(`${rpc}/network/version`),
            };
        });

    const network = chain_name.split('_')[1];
    if (!Object.keys(Network).includes(network)) {
        logger.error(`Unknown network: ${chain_name}.`);
        return Network.CUSTOM;
    }

    return Network[network as keyof typeof Network];
};

/**
 * @description Find in which network the contract is deployed.
 * @param {string} address Contract Address
 * @return {Promise<Network | void>} Network
 */
export const lookupContractNetwork = async (address: string): Promise<Network | void> => {
    const networks = Object.values(Network);
    for (const network of networks) {
        try {
            if (network in RPC.smartpy) {
                await httpRequester.get(
                    `${RPC.smartpy[network]}${RPC.Endpoint.CONTRACT_BALANCE.replace(':contract_id', address)}`,
                );
                return network;
            }
        } catch (e) {
            logger.debug(e);
        }
    }
    return Network.CUSTOM;
};
