import StringMatchers from './String';
import TimestampMatchers from './Timestamp';

const matchers = {
    StringMatchers,
    TimestampMatchers,
};

export { StringMatchers, TimestampMatchers };
export default matchers;
