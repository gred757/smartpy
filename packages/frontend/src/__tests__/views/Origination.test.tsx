import React, { Suspense } from 'react';
import { act } from 'react-dom/test-utils';

import Origination from '../../features/origination/views/Origination';
import renderWithStore from '../test-helpers/renderWithStore';

jest.mock('../../utils/rand.ts', () => ({
    generateID: (): string => {
        return '123'; // Mock random value for tests
    },
}));

const viewProps = {
    onRpcChange: (_: React.ChangeEvent<{ value: string }>) => null,
    onNetworkSelection: (_: React.ChangeEvent<{ value: string }>) => null,
    originationContent: {
        contractSizes: '',
        storage: '',
        contract: '',
        storageJson: '',
        contractJson: '',
        storageHtml: '',
        contractHtml: '',
        storageJsonHtml: '',
        contractJsonHtml: '',
    },
    network: 'SMARTPY-MAINNET',
    rpc: 'https://mainnet.smartpy.io',
};

describe('Origination Page', () => {
    it('Origination page renders correctly', () => {
        let container;
        act(() => {
            container = renderWithStore(
                <Suspense fallback={'...'}>
                    <Origination {...viewProps} />
                </Suspense>,
            ).container;
        });
        expect(container).toMatchSnapshot();
    });
});
