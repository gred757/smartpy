import React, { Suspense } from 'react';
import { act } from 'react-dom/test-utils';
import { IDELayout } from 'SmartPyModels';

import Editor from '../../features/ml-ide/views/EditorView';
import renderWithStore from '../test-helpers/renderWithStore';

const props = {
    htmlOutput: {
        __html: '',
    },
    settings: {
        layout: 'side-by-side' as IDELayout,
    },
    setTargets: (_: any) => null,
    targets: [] as { kind: string; name: string }[],
    editorRef: React.createRef() as React.RefObject<any>,
    updateContract: (value: string) => null,
    showError: (error: string) => null,
    clearOutputs: () => null,
    contract: '',
    sharedContract: '',
};

describe('ML Editor Page', () => {
    it('Editor renders correctly', async () => {
        let container: Element;
        await act(async () => {
            container = renderWithStore(
                <Suspense fallback={'...'}>
                    <Editor {...props} />
                </Suspense>,
            ).container;

            while (!container.innerHTML.includes('run-code')) {
                // Wait for the content to be available or fail with timeout
                await new Promise((r) => setTimeout(r, 1000));
            }
            expect(container).toMatchSnapshot();
        });
    });
});
