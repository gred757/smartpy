export declare global {
    export interface Window {
        eztz: any;
        sodium: any;
        evalRun: (withTests: boolean) => void;
        evalTest: (testName: string) => void;
        showLine: (line: number, column?: number, endLine?: number, endColumn?: number) => void;
        brython: (options: BrythonOptions) => void;
        setSelectVisibility: any;
        showErrorPre: any;
        showTraceback: (title: string, error: string) => void;
        __BRYTHON__?: any;
        editor: any;
        smartpyContext: SmartpyContext;
        smartmlCtx: any;
        in_browser: boolean;
    }
}

interface SmartpyContext {
    Bls12?: any;
    Timelock: any;
    Keccak256: (s: string) => string;
    gotoOrigination: (
        contractSizes: string,
        storageCode: string,
        contractCode: string,
        storageCodeJson: string,
        contractCodeJson: string,
        storageCodeHtml: string,
        contractCodeHtml: string,
        storageCodeJsonHtml: string,
        contractCodeJsonHtml: string,
    ) => void;
    getContractCode: (name: string) => string | undefined;
    clearOutputs: () => void;
    getEditorValue: () => string;
    addButton: (name: string, kind: string, f: () => void) => void;
    addOutput: (output: string) => void;
    setOutput: (output: string) => void;
    setFirstMessage?: (output: string) => void;
    nextId: () => number;
    showError: (error: string) => void;
}

interface BrythonOptions {
    debug: number;
    pythonpath: string[];
}
