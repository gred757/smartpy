### Technologies included:

- [x] Typescript (4.0.2) Adds types on top of javascript
- [X] ReactJS (16.13.1) as web component framework
- [X] Material UI (4.11.0) as UI framework
- [X] Axios (0.20.0) as HTTP client
- [X] React Redux (7.2.1) as State Container
- [X] Jest (24.9.0) as Test framework

#

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.
