// Copyright 2019-2020 Smart Chain Arena LLC.

$(document).ready(() => {
    const hasEmbeddedModal = document.title.search(/(Internals|Editor Help)/) > -1;
    if (!hasEmbeddedModal) {
        const modal = createModal();
        $('body').append(modal);
    }
});

function createModal() {
    const modal = $('<div/>', {
        id: 'modal',
        tabindex: '-1',
        role: 'dialog',
        'aria-hidden': 'true',
    }).addClass('modal fade');

    const modalContent = `
        <div class="modal-dialog  modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="height:40px">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button id="modal-cancel-button" type="button" class="btn btn-secondary" data-dismiss="modal" style="height:40px">Close</button>
                    <button id="modal-submit-button" type="button" class="btn btn-primary" style="height:40px">Ok</button>
                </div>
            </div>
        </div>`;

    modal.html(modalContent);

    return modal;
}

function showModal({ title, content, cancelLabel, cancelAction, acceptLabel, acceptAction, small, timeout }) {
    $('.modal-title').text(title);

    // Reset Button Labels
    $('#modal-cancel-button').text(cancelLabel || 'Close');
    $('#modal-submit-button').text(acceptLabel || 'Ok');

    if (content) {
        $('.modal-body').html(content);
    }
    if (typeof cancelAction == 'function') {
        $('#modal-cancel-button')
            .unbind('click')
            .click(() => {
                hideModal();
                cancelAction();
            });

        $('.modal-header > .close')
            .unbind('click')
            .click(() => {
                hideModal();
                cancelAction();
            });

        // Also apply cancelAction when the user closes the modal by clicking outside
        $(document)
            .unbind('click')
            .click(function (e) {
                if (!$(e.target).closest('#modal').length) {
                    hideModal();
                    cancelAction();
                }
            });
        // Reset document click event (Only required if the modal is visible and an cancel action exists)
        $('#modal').on('hide.bs.modal', function () {
            setImmediate(() => {
                $('.modal-header > .close').unbind('click');
                $(document).unbind('click');
            });
        });
    }
    if (typeof acceptAction == 'function') {
        $('#modal-submit-button')
            .show()
            .unbind('click')
            .click(() => {
                hideModal();
                acceptAction();
            });
        $('#modal')
            .unbind('keypress')
            .keypress((e) => {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    hideModal();
                    acceptAction();
                }
            });
    } else {
        $('#modal-submit-button').hide();
    }

    $('.modal-dialog')
        .removeClass('modal-lg modal-sm')
        .addClass(small ? 'modal-sm' : 'modal-lg');

    $('#modal').modal();

    timeout && setTimeout(() => hideModal(), timeout);
}

function hideModal() {
    $('#modal-cancel-button').unbind('click');
    $('#modal-submit-button').unbind('click');
    $('#modal').modal('hide');
}

function showShortcuts(platform) {
    showModal({ title: 'Shortcuts', content: showCommands(platform) });
}

function showErrorPre(error) {
    showError('<pre>' + error + '</pre>');
}

function showError(error) {
    showModal({ title: 'Error', content: error });
}

function setDialogAjax(title, name) {
    let textfile;
    if (window.XMLHttpRequest) {
        textfile = new XMLHttpRequest();
    }
    textfile.onreadystatechange = function () {
        if (textfile.readyState == 4 && textfile.status == 200) {
            showModal({ title, content: textfile.response.trim() });
        }
    };
    filename = '' + name;
    textfile.open('GET', filename, true);
    textfile.send();
}

function setPythonAjax(title, name) {
    let textfile;
    if (window.XMLHttpRequest) {
        textfile = new XMLHttpRequest();
    }
    textfile.onreadystatechange = function () {
        if (textfile.readyState == 4 && textfile.status == 200 && editor) {
            editor.setValue(textfile.response.trim());
            editor.selection.moveCursorToPosition({ row: 0, column: 0 });
            editor.setAutoScrollEditorIntoView(true);
            editor.setReadOnly(true);
            showModal({ title });
        }
    };
    filename = '' + name;
    textfile.open('GET', filename, true);
    textfile.send();
}

function setTemplateAjax(name, test) {
    let textfile;
    if (window.XMLHttpRequest) {
        textfile = new XMLHttpRequest();
    }
    textfile.onreadystatechange = function () {
        if (textfile.readyState == 4 && textfile.status == 200 && editor) {
            localStorage.setItem('lastTemplate', name);
            editor.setValue(textfile.response.trim());
            editor.selection.moveCursorToPosition({ row: 0, column: 0 });
            editor.setAutoScrollEditorIntoView(true);
            editor.focus();
            if (test) {
                evalButtonClick(true);
            }
        }
    };
    filename = 'templates/' + name;
    textfile.open('GET', filename, true);
    textfile.send();
    hideModal();
}

function popupJson(title, json) {
    const content = `<pre>${JSON.stringify(json, null, 2)}</pre>`;
    showModal({ title, content });
}

function setSelectVisibility(selectId, ...rest) {
    let i;
    for (i = 0; i < rest.length; i += 2) {
        if (rest[i] == document.getElementById(selectId).value) {
            document.getElementById(rest[i + 1]).style.display = 'block';
        } else {
            document.getElementById(rest[i + 1]).style.display = 'none';
        }
    }
}

function shareLink(script) {
    const encoded =
        script && editor
            ? btoa(pako.deflate(editor.getValue(), { to: 'string' }))
                  .replace(/\+/g, '@')
                  .replace(/\//g, '_')
                  .replace(/=/g, '-')
            : localStorage.getItem('lastTemplate');

    const name = script ? 'code' : 'template';

    const shareLink = `${window.location.origin}/${
        window.location.pathname.slice(1).split('/')[0]
    }/index.html?${name}=${encoded}`;

    copyToClipboard(
        shareLink
            .replace(/http:\/\/localhost:8000\/demo/g, 'https://SmartPy.io/dev')
            .replace(/http:\/\/localhost:8000/g, 'https://SmartPy.io'),
        true,
    );

    $.toast({
        heading: 'Information',
        text: `The ${name} share link was added to the clipboard.`,
        icon: 'info',
        loader: true,
        loaderBg: '#006dcc',
        position: 'bottom-right',
    });
}

/**
 * Copy text to clipboard
 * @param {string} text
 */
const copyToClipboard = (text, isModal) => {
    const el = document.createElement('textarea');
    el.value = text;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    if (isModal) {
        document.getElementById('modal').appendChild(el);
    } else {
        document.body.appendChild(el);
    }

    el.select();
    document.execCommand('copy');

    if (isModal) {
        document.getElementById('modal').removeChild(el);
    } else {
        document.body.removeChild(el);
    }
};

function saveContract(contractValue, download) {
    const save = () => {
        const name = $('input[name="name"]').val();
        if (name == '') {
            $('#save-form .input-error').text(`Name must not be empty`);
        } else {
            if (download) downloadFile(contractValue, name, 'text/python');
            else localStorage.setItem('saved_' + name, contractValue);
            hideModal();
        }
    };

    const modalContent = `
        <form id="save-form">
            <span>Name: </span>
            <input type="text" style="width: 12rem" name="name" >
            <p class="input-error"></p>
        </form>`;

    showModal({
        title: 'Save Script',
        content: modalContent,
        acceptAction: () => save(),
        small: true,
    });
}

function loadContract() {
    const selection = (name, value, height) => `
        <div class="load-contract" >
            <button class="text-button" onclick=\"getEditorFromLocalStorage('${name}');hideModal();\">${name}</button>
            <div class="preview" style="height: ${height}px">
                <pre>${value}</pre>
            </div>
        </div>
    `;

    const saved = Object.keys(localStorage).filter((k) => k.startsWith('saved_'));
    const calculatedHeight =
        window && (window.innerHeight - 100 - saved.length * 40) / saved.length > 100
            ? (window.innerHeight - 100 - saved.length * 40) / saved.length
            : 100;
    const content = saved.length
        ? saved.map((key) => selection(key.substring(6), localStorage.getItem(key), calculatedHeight)).join(' ')
        : "You don't have any saved contracts.";

    showModal({ title: 'Load Contract', content });
}

const callNodeWithData = (url) =>
    new Promise((resolve) => {
        const tezosRpcCall = new XMLHttpRequest();
        tezosRpcCall.timeout = 10_000;
        const result = [url, '', ''];
        tezosRpcCall.onreadystatechange = function () {
            result[0] = 'no answer ' + tezosRpcCall.readyState;
            if (tezosRpcCall.readyState == 4) {
                const res = tezosRpcCall.response;
                if (tezosRpcCall.status == 200) {
                    const parsed = JSON.parse(res);
                    const pp = JSON.stringify(parsed, null, 2);
                    result[0] = `<div><div class='menu'><button onClick='copyMichelson(this);'>Copy</button></div><div class='michelson'><div class='white-space-pre'>${pp}</div></div></div>`;
                    result[1] = parsed;
                    result[2] = res;
                } else {
                    result[0] = `<span style='color:grey;'>Error during RPC call</span>`;
                    result[0] += '<br>Status: ' + tezosRpcCall.status;
                    if (tezosRpcCall.statusText) result[0] += '<br>Text: ' + tezosRpcCall.statusText;
                }
                resolve(result);
            }
        };

        tezosRpcCall.open('GET', url, true);
        tezosRpcCall.send();
    });

const callNode = async (url) => (await callNodeWithData(url))[0];

function dynamicTabs(title, titles, contents) {
    const content = contents
        .map((content, i) => `<div class="tabcontent" style="display: ${i == 0 ? 'block' : 'none'};">${content}</div>`)
        .join('');
    const links = titles
        .map(
            (title, i) =>
                `<button class="tablinks ${i == 0 ? 'active' : ''}" onclick="openTab(event, ${i})">${title}</button>`,
        )
        .join('\n');
    return `<div class="tabs"><div class="tab"><span class="title">${title}</span>${links}</div>${content}</div>`;
}

const explorerUrl = async (node) => {
    let server = 'https://api.tzstats.com';

    const network = await getRpcNetwork(node);
    if (network && API[network] && API[network].tzstats) {
        server = API[network].tzstats;
    }

    return server;
};

const viewNodeStatus = async (node) => {
    const titles = [];
    const contents = [];
    const network = await explorerUrl(node);
    async function add(title, url) {
        const content = await callNode(url);
        titles.push(title);
        contents.push(content);
    }
    await add('Header', node + '/chains/main/blocks/head/header');
    await add('Checkpoint', node + '/chains/main/checkpoint');
    await add('Explorer Tip', `${network}/explorer/tip`);
    await add('Explorer Config', `${network}/explorer/config/head`);
    await add('Constants', node + '/chains/main/blocks/head/context/constants');
    await add('Meta Data', node + '/chains/main/blocks/head/metadata');
    await add('Errors', node + '/chains/main/blocks/head/context/constants/errors');
    //add("BigMaps"       , node + '/chains/main/blocks/head/context/big_maps')
    const content = dynamicTabs('Node', titles, contents);
    showModal({ title: `${node}`, content });
};

function json_no_script(name, val) {
    if (name === 'code') {
        return '...';
    }
    return val;
}

function viewPreSignatureInfo(info) {
    const lines = Math.ceil(info.operationBytes.length / 100);

    const title = 'Pre-Signature Information';
    const hashes = `
    <div id="modal-content">
      <span class="item">Blake 2B Hash:</span> <input value="${info.operationBlake2BHash}" readonly class="large_input"/>
      <br/>
      <span class="item">Block Hash:</span> <input value="${info.blockHash}" readonly class="large_input"/>
    </div>`;
    const titles = ['Hashes', 'JSON Encoding (no script)', 'JSON Full Encoding', 'Bytes Encoding'];
    const contents = [
        hashes,
        `<div><div class='menu'><button onClick='copyMichelson(this);'>Copy</button></div><div class='michelson'><div class='white-space-pre'>${JSON.stringify(
            info.jsonEncoding,
            json_no_script,
            2,
        )}</div></div></div>`,
        `<div><div class='menu'><button onClick='copyMichelson(this);'>Copy</button></div><div class='michelson'><div class='white-space-pre'>${JSON.stringify(
            info.jsonEncoding,
            null,
            2,
        )}</div></div></div>`,
        `<div class='menu'><button onClick='copyMichelson(this);'>Copy</button></div><div class='michelson'><div class='white-space-pre' style="font-size: 9pt; overflow-x: hidden;">${[
            ...Array(lines),
        ]
            .map((_, i) => `\n${info.operationBytes.slice(i * 100, (i + 1) * 100)}`)
            .join('')}</div></div>`,
    ];
    const content = dynamicTabs(title, titles, contents);

    $('.modal-title').text(title);

    // Set default or custom labels
    $('#modal-cancel-button').text(info.cancelLabel || 'Cancel');
    $('#modal-submit-button').text(info.acceptLabel || 'Accept');

    $('.modal-body').html(content);
    if (typeof info.onAccept == 'function') {
        $('#modal-submit-button')
            .show()
            .unbind('click')
            .click(() => {
                hideModal();
                info.onAccept();
            });
        $('#modal')
            .unbind('keypress')
            .keypress((e) => {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    hideModal();
                    info.onAccept();
                }
            });
    }
    if (typeof info.onClose == 'function') {
        $('#modal-cancel-button')
            .show()
            .unbind('click')
            .click(() => {
                hideModal();
                info.onClose();
            });

        // Clicking on top right cross
        $('.modal-header > .close')
            .unbind('click')
            .click(() => {
                hideModal();
                info.onClose();
            });

        // Also apply onClose action when the user closes the modal by clicking outside
        $(document)
            .unbind('click')
            .click(function (e) {
                if (!$(e.target).closest('.modal-dialog').length) {
                    hideModal();
                    info.onClose();
                }
            });
        // Reset document click event (Only required if the modal is visible and an cancel action exists)
        $('#modal').on('hide.bs.modal', function () {
            setImmediate(() => {
                $('.modal-header > .close').unbind('click');
                $(document).unbind('click');
            });
        });
    }

    $('.modal-dialog').removeClass('modal-lg modal-sm').addClass('modal-lg');

    $('#modal').modal();
}

function downloadFile(data, filename, type = 'text/html') {
    const a = document.createElement('a');
    a.style.display = 'none';
    document.body.appendChild(a);
    const d = [data];
    const b = new Blob(d, { type });
    const cb = window.URL.createObjectURL(b);
    a.href = cb;
    a.setAttribute('download', filename);
    a.click();
    window.URL.revokeObjectURL(a.href);
    document.body.removeChild(a);
}

function readTextFile(url) {
    const rawFile = new XMLHttpRequest();
    const result = [null];
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState == 4) {
            if (rawFile.status == 200) {
                result[0] = rawFile.responseText;
            }
        }
    };
    rawFile.open('GET', url, false);
    rawFile.send();
    return result[0];
}

function downloadOutput() {
    let data = outputPanel.innerHTML;
    smartjs = readTextFile('static/js/smart.js');
    themejs = readTextFile('theme.js');
    data = data.replace(/.\/svgs/g, `${window.location.origin}${window.location.pathname}/svgs`);
    head = `<style>${readTextFile('css/typography.css')}</style>`;
    head += `<style>${readTextFile('css/smart.css')}</style>`;
    downloadFile(
        `<html><head><link rel="stylesheet" href="https://SmartPy.io/css/bootstrap.min.css">${head}</head><body><div id="theme"></div><div id="outputPanel">${data}</div><script src="https://code.jquery.com/jquery-3.4.0.min.js" integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script><script>${themejs}</script><script>${smartjs}</script></body></html>`,
        'output_panel.html',
    );
}

window.smartpyContext = {
    ...(window.smartpyContext || {}),
    showError,
    showErrorPre,
};
