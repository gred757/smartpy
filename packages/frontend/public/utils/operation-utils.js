/**
 * Creates a transaction operation for contract invocation.
 */
const constructContractInvocationOperation = (
    keystore,
    counter,
    to,
    amount,
    fee,
    storageLimit,
    gasLimit,
    parameters
) => {
    let transaction = {
        amount: amount.toString(),
        storage_limit: storageLimit.toString(),
        gas_limit: gasLimit.toString(),
        counter: counter.toString(),
        fee: fee.toString(),
        source: keystore.publicKeyHash,
        kind: 'transaction',
    };

    if (['temple', 'beacon'].includes(accountMethod)) {
        transaction.to = to;
        transaction.gasLimit = gasLimit.toString();
        transaction.storageLimit = storageLimit.toString();
        transaction.amount = amount / 1000_000; // temple and beacon expect the value in tez
        transaction['parameter'] = parameters;
    } else {
        transaction.destination = to;
        transaction.gas_limit = gasLimit.toString();
        transaction.storage_limit = storageLimit.toString();
        transaction['parameters'] = parameters;
    }

    return transaction;
};

/**
 * Appends a reveal operation if the user public key was not yet revealed.
 */
const appendRevealOperation = async (server, keystore, accountOperationIndex, operations) => {
    const isKeyRevealed = await conseiljs.TezosNodeReader.isManagerKeyRevealedForAccount(
        server,
        keystore.publicKeyHash,
    );
    const counter = accountOperationIndex + 1;

    if (!isKeyRevealed) {
        const revealOp = {
            kind: 'reveal',
            source: keystore.publicKeyHash,
            fee: '0', // Reveal Fee will be covered by the appended operation
            counter: counter.toString(),
            gas_limit: '10600',
            storage_limit: '0',
            public_key: keystore.publicKey,
        };

        operations.forEach((operation, index) => {
            const c = accountOperationIndex + 2 + index;
            operation.counter = c.toString();
        });

        return [revealOp, ...operations];
    }

    return operations;
};

/**
 * Update Tez Amount Label
 */
const updateTezLabel = (divId) => {
    const tezAmount = $(divId).val();
    $(`${divId}Label`).html(`${tezAmount / 1000000}<img class="nav-icon" src="./svgs/tezos-xtz-logo.svg"/>`);
};

/**
 * Get Base Gas Limit
 *
 * @param {number} baseGasLimit Base gas limit.
 * @param {Boolean} include007Changes Include gas changes from 007 proposal
 */
const calculateGasLimit = (baseGasLimit, include007Changes) => {
    const multiplier = include007Changes ? 1000 : 1;
    return baseGasLimit * multiplier;
};
