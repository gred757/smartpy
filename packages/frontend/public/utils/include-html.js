// Import HTML FILES
function include_html_rec () {
  var substitution = true;
  while(substitution)
  {
    substitution = false;
    Array.from(document.getElementsByTagName("div")).forEach(element => {
      const file = element.getAttribute("include-html");
      if (file) {
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = () => {
          if (xhttp.readyState == 4 && xhttp.status == 200) {
            element.innerHTML = xhttp.responseText;
            Array.from(element.getElementsByTagName("script")).forEach(script => {
              const s = document.createElement('script');
              s.type = script.type || 'text/javascript';
              if(script.hasAttribute('src')) {
                s.src = script.src;
              }
              s.innerHTML = script.innerHTML;
              document.head.appendChild(s);
              document.head.removeChild(s);
              substitution = true;
            });
          }
        }
        xhttp.open("GET", file, false); // It must be synchronous to handle
                                        // "recursive" calls.
        element.removeAttribute("include-html");
        xhttp.send();
      }
    })
  }
}
include_html_rec();
