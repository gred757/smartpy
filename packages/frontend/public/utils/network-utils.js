const Networks = {
    MAINNET: 'Mainnet',
    HANGZHOUNET: 'Hangzhounet',
    ITHACANET: 'Ithacanet',
};

const EXPLORER = {
    MAINNET: {
        tzstats: 'https://tzstats.com',
        tzkt: 'https://tzkt.io',
        bcd: 'https://better-call.dev',
    },
    HANGZHOUNET: {
        tzstats: 'https://hangzhou.tzstats.com/',
        tzkt: 'https://hangzhou2net.tzkt.io',
        bcd: 'https://better-call.dev',
    },
    ITHACANET: {
        tzstats: 'https://ithaca.tzstats.com/',
        tzkt: 'https://ithacanet.tzkt.io',
        bcd: 'https://better-call.dev',
    },
};

const API = {
    MAINNET: {
        tzkt: 'https://api.tzkt.io/v1',
        tzstats: 'https://api.tzstats.com',
    },
    HANGZHOUNET: {
        tzkt: 'https://api.hangzhou2net.tzkt.io/v1',
        tzstats: 'https://api.hangzhou.tzstats.com',
    },
    ITHACANET: {
        tzkt: 'https://api.ithacanet.tzkt.io/v1',
        tzstats: 'https://api.ithaca.tzstats.com',
    },
};

const smartPyNodes = {
    'https://mainnet.smartpy.io': Networks.MAINNET,
    'https://hangzhounet.smartpy.io': Networks.HANGZHOUNET,
    'https://ithacanet.smartpy.io': Networks.ITHACANET,
};

const gigaNodes = {
    'https://mainnet-tezos.giganode.io': Networks.MAINNET,
};

const networkFilterByFeature = {
    faucet: ['mainnet'],
};

/**
 * Filter network by feature
 *
 * @param {string} feature
 * @param {string} node
 *
 * @returns true to skip, false otherwise
 */
const shouldSkipNetwork = (feature, node) => {
    const featureFilter = networkFilterByFeature[feature];
    return featureFilter && featureFilter.some((filter) => node.toLowerCase().includes(filter));
};

/**
 *  GET Request
 */
const requestGET = (url) =>
    new Promise((resolve, reject) => {
        const req = new XMLHttpRequest();
        req.timeout = 2000;
        req.onreadystatechange = () => {
            if (req.readyState === 4) {
                if (req.status === 200) {
                    resolve(JSON.parse(req.response));
                } else {
                    reject(req.responseText);
                }
            }
        };
        req.open('GET', url, true);
        req.send();
    });

/**
 * Get the RPC network.
 * @param {string} rpcAddress - RPC address ( e.g. https://mainnet.smartpy.io )
 * @return {string} One of the following networks [MAINNET]
 */
const getRpcNetwork = async (rpcAddress) => {
    const {
        network_version: { chain_name },
    } = await requestGET(`${rpcAddress}/version`).catch(async (e) => {
        console.warn(e);
        return {
            network_version: await requestGET(`${rpcAddress}/network/version`),
        };
    });

    const network = chain_name.split('_')[1];
    if (!Object.keys(Networks).includes(network)) {
        console.error(`Unknown network: ${network}.`);
    }

    return network?.replace(Networks.EDO2NET, Networks.EDONET);
};
