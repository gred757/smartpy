import smartpy as sp

class OnchainViews(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)

    @sp.onchain_view()
    def view1(self):
        sp.result(self.data.z)

    @sp.onchain_view()
    def view2(self, a):
        sp.result(self.data.x * a)

    @sp.entry_point
    def ep2(self):
        self.data.y = self.data.y * 2

class OnchainViewsWithLazyEntryPoint(OnchainViews):
    @sp.entry_point(lazify = True)
    def ep3(self):
        self.data.y = self.data.y * 2


@sp.add_test(name = "OnchainViews")
def test():
    scenario = sp.test_scenario()

    c1 = OnchainViews(x = 2, y = 1, z = "a")
    c2 = OnchainViewsWithLazyEntryPoint(x = 2, y = 1, z = "a")

    scenario += c1
    scenario += c2

    scenario.compute(c1.view1())
    scenario.compute(c1.view2(1))

    scenario.compute(c2.view1())
    scenario.compute(c2.view2(1))

sp.add_compilation_target("onchain_views_compilation", OnchainViews(x = 2, y = 1, z = "ABC"))
