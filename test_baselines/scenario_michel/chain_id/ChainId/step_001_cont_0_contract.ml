open SmartML

module Contract = struct
  let%entry_point test () =
    data.chainId <- some chain_id

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {chainId = option chain_id}]
      ~storage:[%expr
                 {chainId = None}]
      [test]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())