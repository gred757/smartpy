open SmartML

module Contract = struct
  let%entry_point ep () =
    data.x <- "ghi"

  let%entry_point ep2 params =
    set_type params bounded(["abc", "def", "ghi", "jkl"], t=string);
    data.x <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = bounded(["abc", "def", "ghi", "jkl"], t=string)}]
      ~storage:[%expr
                 {x = "abc"}]
      [ep; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())