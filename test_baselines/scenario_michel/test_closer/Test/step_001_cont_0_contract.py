import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def test_none(self, params):
    sp.verify(params.is_variant('None'))

  @sp.entry_point
  def test_some(self, params):
    sp.verify(params.is_some())

sp.add_compilation_target("test", Contract())