open SmartML

module Contract = struct
  let%entry_point test_none params =
    verify (is_variant "None" params)

  let%entry_point test_some params =
    verify (is_some params)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [test_none; test_some]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())