import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def run(self, params):
    sp.transfer(params.x // 2, sp.tez(0), params.k)

sp.add_compilation_target("test", Contract())