Comment...
 h1: Collatz template - Inter-Contract Calls
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_pre_michelson.michel 20
 -> Unit
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_storage.tz 1
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_storage.json 1
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_storage.py 1
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_types.py 7
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_contract.tz 28
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_contract.json 28
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_contract.py 12
 => test_baselines/scenario_michel/collatz/Collatz/step_001_cont_0_contract.ml 18
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_pre_michelson.michel 18
 -> Unit
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_storage.tz 1
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_storage.json 1
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_sizes.csv 2
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_storage.py 1
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_types.py 7
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_contract.tz 22
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_contract.json 29
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_contract.py 13
 => test_baselines/scenario_michel/collatz/Collatz/step_002_cont_1_contract.ml 19
Creating contract KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_pre_michelson.michel 74
 -> (Pair 0 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_storage.tz 1
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_storage.json 4
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_sizes.csv 2
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_storage.py 1
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_types.py 7
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_contract.tz 93
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_contract.json 94
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_contract.py 23
 => test_baselines/scenario_michel/collatz/Collatz/step_003_cont_2_contract.ml 31
 => test_baselines/scenario_michel/collatz/Collatz/step_004_cont_2_params.py 1
 => test_baselines/scenario_michel/collatz/Collatz/step_004_cont_2_params.tz 1
 => test_baselines/scenario_michel/collatz/Collatz/step_004_cont_2_params.json 1
Executing run(42)...
 -> (Pair 1 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 42)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 42))...
 -> Unit
  + Transfer
     params: 21
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(21)...
 -> (Pair 2 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 21)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 21))...
 -> Unit
  + Transfer
     params: 64
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(64)...
 -> (Pair 3 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 64)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 64))...
 -> Unit
  + Transfer
     params: 32
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(32)...
 -> (Pair 4 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 32)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 32))...
 -> Unit
  + Transfer
     params: 16
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(16)...
 -> (Pair 5 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 16)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 16))...
 -> Unit
  + Transfer
     params: 8
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(8)...
 -> (Pair 6 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 8)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 8))...
 -> Unit
  + Transfer
     params: 4
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(4)...
 -> (Pair 7 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 4)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 4))...
 -> Unit
  + Transfer
     params: 2
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(2)...
 -> (Pair 8 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 2)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 2))...
 -> Unit
  + Transfer
     params: 1
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(1)...
 -> (Pair 8 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
Verifying sp.contract_data(2).counter == 8...
 OK
 => test_baselines/scenario_michel/collatz/Collatz/step_006_cont_2_params.py 1
 => test_baselines/scenario_michel/collatz/Collatz/step_006_cont_2_params.tz 1
 => test_baselines/scenario_michel/collatz/Collatz/step_006_cont_2_params.json 1
Executing reset(sp.record())...
 -> (Pair 0 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
 => test_baselines/scenario_michel/collatz/Collatz/step_007_cont_2_params.py 1
 => test_baselines/scenario_michel/collatz/Collatz/step_007_cont_2_params.tz 1
 => test_baselines/scenario_michel/collatz/Collatz/step_007_cont_2_params.json 1
Executing run(5)...
 -> (Pair 1 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 5)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 5))...
 -> Unit
  + Transfer
     params: 16
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(16)...
 -> (Pair 2 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 16)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 16))...
 -> Unit
  + Transfer
     params: 8
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(8)...
 -> (Pair 3 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 8)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 8))...
 -> Unit
  + Transfer
     params: 4
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(4)...
 -> (Pair 4 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 4)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 4))...
 -> Unit
  + Transfer
     params: 2
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(2)...
 -> (Pair 5 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
  + Transfer
     params: sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 2)
     amount: sp.tez(0)
     to:     sp.contract(sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat).layout(("k", "x")), sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1')).open_some()
Executing (queue) default(sp.record(k = sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some(), x = 2))...
 -> Unit
  + Transfer
     params: 1
     amount: sp.tez(0)
     to:     sp.contract(sp.TNat, sp.address('KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H%run')).open_some()
Executing (queue) run(1)...
 -> (Pair 5 (Pair "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1" "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"))
Verifying sp.contract_data(2).counter == 5...
 OK
