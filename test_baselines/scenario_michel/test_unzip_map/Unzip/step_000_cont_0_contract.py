import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TString, sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b"))), out = sp.TString).layout(("m", "out")))
    self.init(m = {'abc' : sp.record(a = 10, b = 20)},
              out = 'z')

  @sp.entry_point
  def ep(self):
    k = sp.local("k", 'abc')
    with sp.match_record(self.data.m[k.value], "data") as data:
      k.value = 'xyz' + k.value
    self.data.out = k.value

sp.add_compilation_target("test", Contract())