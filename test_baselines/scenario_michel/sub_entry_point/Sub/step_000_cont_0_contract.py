import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z"))))
    self.init(x = 2,
              y = 'aaa',
              z = 0)

  @sp.entry_point
  def f(self):
    compute_sub_entry_point_23 = sp.local("compute_sub_entry_point_23", self.a(5))
    compute_sub_entry_point_23i = sp.local("compute_sub_entry_point_23i", self.a(10))
    self.data.z = compute_sub_entry_point_23.value + compute_sub_entry_point_23i.value

  @sp.entry_point
  def g(self):
    compute_sub_entry_point_27 = sp.local("compute_sub_entry_point_27", self.a(6))
    self.data.z = compute_sub_entry_point_27.value

  @sp.private_lambda()
  def a(_x0):
    sp.send(sp.address('tz0Fakealice'), sp.mul(_x0, sp.tez(1)))
    sp.send(sp.address('tz0Fakebob'), sp.tez(2))
    self.data.x += 1
    sp.result(_x0 * self.data.x)

  @sp.private_lambda()
  def b(_x1):
    sp.send(sp.address('tz0Fakealice'), sp.mul(_x1, sp.tez(1)))
    sp.send(sp.address('tz0Fakebob'), sp.tez(2))
    self.data.x += 1
    sp.result(_x1 * self.data.x)

sp.add_compilation_target("test", Contract())