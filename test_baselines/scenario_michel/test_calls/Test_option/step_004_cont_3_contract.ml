open SmartML

module Contract = struct
  let%entry_point other () =
    ()

  let%entry_point reg_annot () =
    data.x <- amount

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = mutez}]
      ~storage:[%expr
                 {x = tez 0}]
      [other; reg_annot]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())