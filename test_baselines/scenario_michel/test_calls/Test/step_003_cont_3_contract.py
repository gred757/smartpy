import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TMutez).layout("x"))
    self.init(x = sp.tez(0))

  @sp.entry_point
  def other(self):
    pass

  @sp.entry_point
  def reg_annot(self):
    self.data.x = sp.amount

sp.add_compilation_target("test", Contract())