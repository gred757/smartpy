import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(current = sp.TString, played = sp.TIntOrNat, rules = sp.TList(sp.TString), verse = sp.TIntOrNat).layout(("current", ("played", ("rules", "verse")))))
    self.init(current = '',
              played = 0,
              rules = ['Please sing as much as you wish!', 'Happy Holidays from the SmartPy team!'],
              verse = 0)

  @sp.entry_point
  def sing(self, params):
    sp.for i in sp.range(0, params.verses):
      sp.if self.data.verse == 16:
        self.data.played += 1
        self.data.current = ''
        self.data.verse = 0
      sp.if self.data.current != '':
        self.data.current += '
'
      lyrics = sp.local("lyrics", {0 : 'Dashing through the snow', 1 : 'In a one-horse open sleigh', 2 : 'O'er the fields we go', 3 : 'Laughing all the way', 4 : 'Bells on bob tail ring', 5 : 'Making spirits bright', 6 : 'What fun it is to ride and sing', 7 : 'A sleighing song tonight!', 8 : 'Jingle bells, jingle bells,', 9 : 'Jingle all the way.', 10 : 'Oh! what fun it is to ride', 11 : 'In a one-horse open sleigh.', 12 : 'Jingle bells, jingle bells,', 13 : 'Jingle all the way;', 14 : 'Oh! what fun it is to ride', 15 : 'In a one-horse open sleigh.'}, sp.TMap(sp.TIntOrNat, sp.TString))
      self.data.current += lyrics.value[self.data.verse]
      self.data.verse += 1

sp.add_compilation_target("test", Contract())