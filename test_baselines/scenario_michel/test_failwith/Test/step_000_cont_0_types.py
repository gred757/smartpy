import smartpy as sp

tstorage = sp.TRecord(f = sp.TOption(sp.TLambda(sp.TIntOrNat, sp.TIntOrNat))).layout("f")
tparameter = sp.TVariant(ep = sp.TUnit).layout("ep")
tprivates = { }
tviews = { }
