import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(f = sp.TOption(sp.TLambda(sp.TIntOrNat, sp.TIntOrNat))).layout("f"))
    self.init(f = sp.none)

  @sp.entry_point
  def ep(self):
    def f_x0(_x0):
      sp.if _x0 == 0:
        sp.failwith('zero')
      sp.else:
        sp.result(1)
    self.data.f = sp.some(sp.build_lambda(f_x0))

sp.add_compilation_target("test", Contract())