import smartpy as sp

tstorage = sp.TRecord(address = sp.TOption(sp.TInt)).layout("address")
tparameter = sp.TVariant(a = sp.TRecord(address = sp.TOption(sp.TInt)).layout("address"), b = sp.TOption(sp.TInt)).layout(("a", "b"))
tprivates = { }
tviews = { }
