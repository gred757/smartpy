open SmartML

module Contract = struct
  let%entry_point ep params =
    if params = true then
      send (address "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr") (tez 1)
    else
      verify (params = false)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())