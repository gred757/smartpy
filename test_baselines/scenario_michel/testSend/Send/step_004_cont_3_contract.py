import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep1(self):
    sp.send(sp.address('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'), sp.tez(1))

  @sp.entry_point
  def ep2(self, params):
    sp.if params == True:
      sp.send(sp.address('tz1YtuZ4vhzzn7ssCt93Put8U9UJDdvCXci4'), sp.tez(2))

sp.add_compilation_target("test", Contract())