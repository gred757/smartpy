import smartpy as sp

tstorage = sp.TRecord(xy = sp.TPair(sp.TIntOrNat, sp.TIntOrNat)).layout("xy")
tparameter = sp.TVariant(ep1 = sp.TUnit, ep2 = sp.TUnit, ep3 = sp.TUnit, ep4 = sp.TUnit, ep5 = sp.TNat, ep6 = sp.TNat).layout((("ep1", ("ep2", "ep3")), ("ep4", ("ep5", "ep6"))))
tprivates = { }
tviews = { }
