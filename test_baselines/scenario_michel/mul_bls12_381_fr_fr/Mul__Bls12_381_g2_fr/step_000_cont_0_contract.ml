open SmartML

module Contract = struct
  let%entry_point mul params =
    data.fr <- some (mul (fst params) (snd params))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {fr = option bls12_381_fr}]
      ~storage:[%expr
                 {fr = None}]
      [mul]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())