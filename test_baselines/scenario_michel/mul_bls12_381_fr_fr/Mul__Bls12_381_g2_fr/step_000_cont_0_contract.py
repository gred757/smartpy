import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(fr = sp.TOption(sp.TBls12_381_fr)).layout("fr"))
    self.init(fr = sp.none)

  @sp.entry_point
  def mul(self, params):
    self.data.fr = sp.some(sp.mul(sp.fst(params), sp.snd(params)))

sp.add_compilation_target("test", Contract())