open SmartML

module Contract = struct
  let%entry_point play params =
    set_type params.move_nb nat;
    set_type params.move_data (`answer_stalemate (`accept unit + `answer {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}}) + `claim_stalemate unit + `play {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}});
    if data.current.player = 1 then
      verify (sender = data.constants.player1.addr) ~msg:"Game_WrongPlayer"
    else
      verify (sender = data.constants.player2.addr) ~msg:"Game_WrongPlayer";
    verify (is_variant "None" data.current.outcome);
    verify (data.current.move_nb = params.move_nb);
    let%mutable compute_game_tester_40 = ({move_data = params.move_data; move_nb = data.current.move_nb; player = data.current.player; state = data.state} data.apply_) in ();
    match_pair_game_tester_50_fst, match_pair_game_tester_50_snd = match_tuple(compute_game_tester_40, "match_pair_game_tester_50_fst", "match_pair_game_tester_50_snd")
    set_type match_pair_game_tester_50_snd (option bounded(["draw", "player_1_won", "player_2_won"], t=string));
    match match_pair_game_tester_50_snd with
      | `Some Some ->
        data.current.outcome <- some (unbound Some)
      | `None None ->
        data.current.outcome <- None
;
    data.current.move_nb <- data.current.move_nb + 1;
    data.current.player <- 3 - data.current.player;
    data.state <- match_pair_game_tester_50_fst

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {apply_ = lambda {move_data = `answer_stalemate (`accept unit + `answer {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}}) + `claim_stalemate unit + `play {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}}; move_nb = nat; player = int; state = {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; status = `claim_stalemate unit + `force_play unit + `play unit}} (pair {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; status = `claim_stalemate unit + `force_play unit + `play unit} (option bounded(["draw", "player_1_won", "player_2_won"], t=string))); constants = {channel_id = bytes; game_nonce = string; loser = mutez; model_id = bytes; player1 = {addr = address; pk = key}; player2 = {addr = address; pk = key}; winner = mutez}; current = {move_nb = nat; outcome = option string; player = int}; state = {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; status = `claim_stalemate unit + `force_play unit + `play unit}}]
      ~storage:[%expr
                 {apply_ = lambda(lambda {move_data = `answer_stalemate (`accept unit + `answer {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}}) + `claim_stalemate unit + `play {claim_repeat = option (pair nat nat); f = {i = int; j = int}; promotion = option nat; t = {i = int; j = int}}; move_nb = nat; player = int; state = {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; status = `claim_stalemate unit + `force_play unit + `play unit}} (pair {board_state = {castle = map int (map int bool); check = bool; deck = map int (map int int); enPassant = option {i = int; j = int}; fullMove = nat; halfMoveClock = nat; kings = map int {i = int; j = int}; nextPlayer = int; pastMoves = map (pair nat int) bytes}; status = `claim_stalemate unit + `force_play unit + `play unit} (option bounded(["draw", "player_1_won", "player_2_won"], t=string))));
                  constants = {channel_id = bytes "0x01"; game_nonce = ""; loser = tez 0; model_id = bytes "0x"; player1 = {addr = address "tz0Fakeplayer1"; pk = key "edpkFakeplayer1"}; player2 = {addr = address "tz0Fakeplayer2"; pk = key "edpkFakeplayer2"}; winner = tez 0};
                  current = {move_nb = 0; outcome = None; player = 1};
                  state = {board_state = {castle = Map.make [((-1), Map.make [((-1), true); (1, true)]); (1, Map.make [((-1), true); (1, true)])]; check = false; deck = Map.make [(0, Map.make [(0, 2); (1, 3); (2, 4); (3, 5); (4, 6); (5, 4); (6, 3); (7, 2)]); (1, Map.make [(0, 1); (1, 1); (2, 1); (3, 1); (4, 1); (5, 1); (6, 1); (7, 1)]); (2, Map.make [(0, 0); (1, 0); (2, 0); (3, 0); (4, 0); (5, 0); (6, 0); (7, 0)]); (3, Map.make [(0, 0); (1, 0); (2, 0); (3, 0); (4, 0); (5, 0); (6, 0); (7, 0)]); (4, Map.make [(0, 0); (1, 0); (2, 0); (3, 0); (4, 0); (5, 0); (6, 0); (7, 0)]); (5, Map.make [(0, 0); (1, 0); (2, 0); (3, 0); (4, 0); (5, 0); (6, 0); (7, 0)]); (6, Map.make [(0, (-1)); (1, (-1)); (2, (-1)); (3, (-1)); (4, (-1)); (5, (-1)); (6, (-1)); (7, (-1))]); (7, Map.make [(0, (-2)); (1, (-3)); (2, (-4)); (3, (-5)); (4, (-6)); (5, (-4)); (6, (-3)); (7, (-2))])]; enPassant = None; fullMove = 1; halfMoveClock = 0; kings = Map.make [((-1), {i = 7; j = 4}); (1, {i = 0; j = 4})]; nextPlayer = 1; pastMoves = Map.make [((0, (-1)), bytes "0xbaf82e0f3ca2b90e482d6b6846873df9fee4e1baf369bc897bcef3b113a359df")]}; status = play}}]
      [play]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())