open SmartML

module Contract = struct
  let%entry_point entry_point_1 params =
    data.fr <- some (mul (fst (open_some (ediv params (mutez 1)))) (bls12_381_fr "0x01"))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {fr = option bls12_381_fr}]
      ~storage:[%expr
                 {fr = None}]
      [entry_point_1]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())