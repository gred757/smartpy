open SmartML

module Contract = struct
  let%entry_point add params =
    data.g1 <- some ((fst params) + (snd params))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {g1 = option bls12_381_g1}]
      ~storage:[%expr
                 {g1 = None}]
      [add]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())