import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(g1 = sp.TOption(sp.TBls12_381_g1)).layout("g1"))
    self.init(g1 = sp.none)

  @sp.entry_point
  def add(self, params):
    self.data.g1 = sp.some(sp.fst(params) + sp.snd(params))

sp.add_compilation_target("test", Contract())