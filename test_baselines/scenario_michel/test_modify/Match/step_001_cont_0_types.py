import smartpy as sp

tstorage = sp.TRecord(x1 = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))), x2 = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))), x3 = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(((("a", "b"), "c"), "d")), x4 = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout((("a", "b"), ("c", "d"))), y = sp.TTuple(sp.TNat, sp.TInt, sp.TBool, sp.TString), z = sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TRecord(e = sp.TIntOrNat, f = sp.TString).layout(("e", "f"))).layout(("a", ("b", ("c", "d"))))).layout(("x1", ("x2", ("x3", ("x4", ("y", "z"))))))
tparameter = sp.TVariant(ep1 = sp.TUnit, ep2 = sp.TUnit, ep3 = sp.TUnit, ep4 = sp.TUnit, ep5 = sp.TAddress, ep6 = sp.TUnit, ep7 = sp.TUnit).layout((("ep1", ("ep2", "ep3")), (("ep4", "ep5"), ("ep6", "ep7"))))
tprivates = { }
tviews = { }
