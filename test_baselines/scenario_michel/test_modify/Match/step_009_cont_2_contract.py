import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TIntOrNat)
    self.init(42)

  @sp.entry_point
  def ep1(self):
    with sp.modify(self.data, "x") as "x":
      sp.result(x.value + 1)

sp.add_compilation_target("test", Contract())