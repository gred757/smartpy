import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TInt).layout("value"))
    self.init(value = 0)

  @sp.entry_point
  def log(self, params):
    sp.verify(params != 0)
    y = sp.local("y", 0)
    x = sp.local("x", params)
    sp.while x.value < 65536:
      x.value = x.value << 1
      y.value -= 65536
    sp.while x.value >= 131072:
      x.value = x.value >> 1
      y.value += 65536
    b = sp.local("b", 32768)
    sp.while b.value > 0:
      x.value = (x.value * x.value) >> 16
      sp.if x.value > 131072:
        x.value = x.value >> 1
        y.value += sp.to_int(b.value)
      b.value = b.value >> 1
    self.data.value = y.value

sp.add_compilation_target("test", Contract())