open SmartML

module Contract = struct
  let%entry_point log params =
    verify (params <> 0);
    let%mutable y = 0 in ();
    let%mutable x = params in ();
    while x < 65536 do
      x <- x << 1;
      y <- y - 65536
    done;
    while x >= 131072 do
      x <- x >> 1;
      y <- y + 65536
    done;
    let%mutable b = 32768 in ();
    while b > 0 do
      x <- (x * x) >> 16;
      if x > 131072 then
        (
          x <- x >> 1;
          y <- y + (to_int b)
        );
      b <- b >> 1
    done;
    data.value <- y

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = int}]
      ~storage:[%expr
                 {value = 0}]
      [log]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())