Comment...
 h1: Edo - breadth first
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_pre_michelson.michel 92
 -> (Pair "" "")
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_storage.tz 1
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_storage.json 1
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_storage.py 1
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_types.py 7
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_contract.tz 102
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_contract.json 111
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_contract.py 33
 => test_baselines/scenario_michel/check_dfs/bfs/step_002_cont_0_contract.ml 37
Comment...
 h2: no depth first
 => test_baselines/scenario_michel/check_dfs/bfs/step_004_cont_0_params.py 1
 => test_baselines/scenario_michel/check_dfs/bfs/step_004_cont_0_params.tz 1
 => test_baselines/scenario_michel/check_dfs/bfs/step_004_cont_0_params.json 1
Executing check(sp.record())...
 -> (Pair "" "check")
  + Transfer
     params: sp.unit
     amount: sp.tez(0)
     to:     sp.contract(sp.TUnit, sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%a')).open_some()
  + Transfer
     params: sp.unit
     amount: sp.tez(0)
     to:     sp.contract(sp.TUnit, sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%b')).open_some()
Executing (queue) a(sp.unit)...
 -> (Pair "" "check.a")
  + Transfer
     params: sp.unit
     amount: sp.tez(0)
     to:     sp.contract(sp.TUnit, sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1%aa')).open_some()
Executing (queue) b(sp.unit)...
 -> (Pair "BFS" "check.a.b")
Executing (queue) aa(sp.unit)...
 -> (Pair "BFS" "check.a.b.aa")
Verifying sp.contract_data(0).conclusion == 'BFS'...
 OK
