import smartpy as sp

tstorage = sp.TRecord(conclusion = sp.TString, steps = sp.TString).layout(("conclusion", "steps"))
tparameter = sp.TVariant(a = sp.TUnit, aa = sp.TUnit, b = sp.TUnit, check = sp.TUnit).layout((("a", "aa"), ("b", "check")))
tprivates = { }
tviews = { }
