import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)

    @sp.private_lambda()
    def f(self, x):
        with sp.set_result_type(sp.TInt):
            sp.failwith("Aaa" + x)

    @sp.entry_point
    def entry_point_1(self):
        sp.local("aaa", ((self.f(""))))

@sp.add_test(name = "Minimal")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract()
    scenario += c1
