import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def entry_point_1(self):
    aaa = sp.local("aaa", self.f(''))

  @sp.private_lambda()
  def f(_x0):
    with sp.set_result_type(sp.TInt):
      sp.failwith('Aaa' + _x0)

sp.add_compilation_target("test", Contract())