import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(fr = sp.TOption(sp.TInt)).layout("fr"))
    self.init(fr = sp.none)

  @sp.entry_point
  def toInt(self, params):
    self.data.fr = sp.some(sp.to_int(params))

sp.add_compilation_target("test", Contract())