import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(g2 = sp.TOption(sp.TBls12_381_g2)).layout("g2"))
    self.init(g2 = sp.none)

  @sp.entry_point
  def mul(self, params):
    self.data.g2 = sp.some(sp.mul(sp.fst(params), sp.snd(params)))

sp.add_compilation_target("test", Contract())