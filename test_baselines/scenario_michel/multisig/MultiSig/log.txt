Comment...
 h1: Multi Sig
Comment...
 h2: Contract
Comment...
 h3: Simple multisig factories
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_pre_michelson.michel 311
 -> (Pair {} 0)
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_storage.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_storage.json 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_sizes.csv 2
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_storage.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_types.py 7
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_contract.tz 373
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_contract.json 462
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_contract.py 36
 => test_baselines/scenario_michel/multisig/MultiSig/step_003_cont_0_contract.ml 48
Comment...
 h2: First: define a simple multisig
 => test_baselines/scenario_michel/multisig/MultiSig/step_005_cont_0_params.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_005_cont_0_params.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_005_cont_0_params.json 84
Executing build(sp.record(contract = sp.record(groups = sp.list([sp.record(contractWeight = 5, ok = False, participants = sp.list([sp.record(hasVoted = False, id = sp.address('tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J'), weight = 2), sp.record(hasVoted = False, id = sp.address('tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS'), weight = 8), sp.record(hasVoted = False, id = sp.address('tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP'), weight = 1)]), thresholdVoters = 2, thresholdWeight = 5, voters = 0, weight = 0), sp.record(contractWeight = 7, ok = False, participants = sp.list([sp.record(hasVoted = False, id = sp.address('tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J'), weight = 7), sp.record(hasVoted = False, id = sp.address('tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS'), weight = 8)]), thresholdVoters = 1, thresholdWeight = 5, voters = 0, weight = 0), sp.record(contractWeight = 7, ok = False, participants = sp.list([sp.record(hasVoted = False, id = sp.address('tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP'), weight = 10)]), thresholdVoters = 1, thresholdWeight = 10, voters = 0, weight = 0)]), groupsOK = 0, name = 'demo', ok = False, thresholdGroupsOK = 2, thresholdWeight = 10, weight = 0)))...
 -> (Pair {Elt 0 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 2 (Pair 10 0))))))} 1)
Comment...
 h2: Message execution
Comment...
 h3: A first move
 => test_baselines/scenario_michel/multisig/MultiSig/step_008_cont_0_params.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_008_cont_0_params.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_008_cont_0_params.json 1
Executing sign(sp.record(contractId = 0, contractName = 'demo', id = sp.resolve(sp.test_account("Alice").address)))...
 -> (Pair {Elt 0 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 2 (Pair 10 0))))))} 1)
 => test_baselines/scenario_michel/multisig/MultiSig/step_009_cont_0_params.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_009_cont_0_params.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_009_cont_0_params.json 1
Executing sign(sp.record(contractId = 0, contractName = 'demo', id = sp.resolve(sp.test_account("Rob").address)))...
 -> (Pair {Elt 0 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 2 (Pair 10 0))))))} 1)
Comment...
 h2: First: define a simple multi-sig
 => test_baselines/scenario_michel/multisig/MultiSig/step_011_cont_0_params.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_011_cont_0_params.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_011_cont_0_params.json 84
Executing build(sp.record(contract = sp.record(groups = sp.list([sp.record(contractWeight = 5, ok = False, participants = sp.list([sp.record(hasVoted = False, id = sp.address('tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J'), weight = 2), sp.record(hasVoted = False, id = sp.address('tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS'), weight = 8), sp.record(hasVoted = False, id = sp.address('tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP'), weight = 1)]), thresholdVoters = 2, thresholdWeight = 5, voters = 0, weight = 0), sp.record(contractWeight = 7, ok = False, participants = sp.list([sp.record(hasVoted = False, id = sp.address('tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J'), weight = 7), sp.record(hasVoted = False, id = sp.address('tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS'), weight = 8)]), thresholdVoters = 1, thresholdWeight = 5, voters = 0, weight = 0), sp.record(contractWeight = 7, ok = False, participants = sp.list([sp.record(hasVoted = False, id = sp.address('tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP'), weight = 10)]), thresholdVoters = 1, thresholdWeight = 10, voters = 0, weight = 0)]), groupsOK = 0, name = 'demo', ok = False, thresholdGroupsOK = 3, thresholdWeight = 10, weight = 0)))...
 -> (Pair {Elt 0 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 2 (Pair 10 0)))))); Elt 1 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 3 (Pair 10 0))))))} 2)
Comment...
 h2: Message execution
Comment...
 h3: A first move
 => test_baselines/scenario_michel/multisig/MultiSig/step_014_cont_0_params.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_014_cont_0_params.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_014_cont_0_params.json 1
Executing sign(sp.record(contractId = 1, contractName = 'demo', id = sp.resolve(sp.test_account("Alice").address)))...
 -> (Pair {Elt 0 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 2 (Pair 10 0)))))); Elt 1 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 3 (Pair 10 0))))))} 2)
 => test_baselines/scenario_michel/multisig/MultiSig/step_015_cont_0_params.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_015_cont_0_params.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_015_cont_0_params.json 1
Executing sign(sp.record(contractId = 1, contractName = 'demo', id = sp.resolve(sp.test_account("Rob").address)))...
 -> (Pair {Elt 0 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 2 (Pair 10 0)))))); Elt 1 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 3 (Pair 10 0))))))} 2)
Comment...
 h4: We need a third vote
 => test_baselines/scenario_michel/multisig/MultiSig/step_017_cont_0_params.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_017_cont_0_params.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_017_cont_0_params.json 1
Executing sign(sp.record(contractId = 1, contractName = 'demo', id = sp.resolve(sp.test_account("Charlie").address)))...
 -> (Pair {Elt 0 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 2 (Pair 10 0)))))); Elt 1 (Pair {Pair 5 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 2); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8); Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 1)} (Pair 2 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J" 7); Pair False (Pair "tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS" 8)} (Pair 1 (Pair 5 (Pair 0 0))))); Pair 7 (Pair False (Pair {Pair False (Pair "tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP" 10)} (Pair 1 (Pair 10 (Pair 0 0)))))} (Pair 0 (Pair "demo" (Pair False (Pair 3 (Pair 10 0))))))} 2)
Comment...
 h4: Final state
Computing sp.contract_data(0)...
 => sp.record(multisigs = {0 : sp.record(groups = [sp.record(contractWeight = 5, ok = False, participants = [sp.record(hasVoted = False, id = sp.address('tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J'), weight = 2), sp.record(hasVoted = False, id = sp.address('tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS'), weight = 8), sp.record(hasVoted = False, id = sp.address('tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP'), weight = 1)], thresholdVoters = 2, thresholdWeight = 5, voters = 0, weight = 0), sp.record(contractWeight = 7, ok = False, participants = [sp.record(hasVoted = False, id = sp.address('tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J'), weight = 7), sp.record(hasVoted = False, id = sp.address('tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS'), weight = 8)], thresholdVoters = 1, thresholdWeight = 5, voters = 0, weight = 0), sp.record(contractWeight = 7, ok = False, participants = [sp.record(hasVoted = False, id = sp.address('tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP'), weight = 10)], thresholdVoters = 1, thresholdWeight = 10, voters = 0, weight = 0)], groupsOK = 0, name = 'demo', ok = False, thresholdGroupsOK = 2, thresholdWeight = 10, weight = 0), 1 : sp.record(groups = [sp.record(contractWeight = 5, ok = False, participants = [sp.record(hasVoted = False, id = sp.address('tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J'), weight = 2), sp.record(hasVoted = False, id = sp.address('tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS'), weight = 8), sp.record(hasVoted = False, id = sp.address('tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP'), weight = 1)], thresholdVoters = 2, thresholdWeight = 5, voters = 0, weight = 0), sp.record(contractWeight = 7, ok = False, participants = [sp.record(hasVoted = False, id = sp.address('tz1NFevnqBrtcZTZTeKP2YBBjsPs9bih5i3J'), weight = 7), sp.record(hasVoted = False, id = sp.address('tz1ZRjMiF9K9n3S9AcUrTGUzR2okS7dn9KXS'), weight = 8)], thresholdVoters = 1, thresholdWeight = 5, voters = 0, weight = 0), sp.record(contractWeight = 7, ok = False, participants = [sp.record(hasVoted = False, id = sp.address('tz1NLJRAAwYdggijWz9EFtX5Dgs95BLfD6mP'), weight = 10)], thresholdVoters = 1, thresholdWeight = 10, voters = 0, weight = 0)], groupsOK = 0, name = 'demo', ok = False, thresholdGroupsOK = 3, thresholdWeight = 10, weight = 0)}, nbMultisigs = 2)
Comment...
 h3: Multisig factories with payments
Creating contract KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_pre_michelson.michel 341
 -> (Pair {} 0)
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_storage.tz 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_storage.json 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_sizes.csv 2
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_storage.py 1
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_types.py 7
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_contract.tz 418
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_contract.json 514
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_contract.py 37
 => test_baselines/scenario_michel/multisig/MultiSig/step_021_cont_1_contract.ml 51
