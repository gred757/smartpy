import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TIntOrNat, b = sp.TBool).layout(("a", "b")))
    self.init(a = 12,
              b = True)

  @sp.entry_point
  def f(self, params):
    self.data.a = 2 * params

sp.add_compilation_target("test", Contract())