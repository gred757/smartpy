import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TInt).layout("value"))
    self.init(value = 0)

sp.add_compilation_target("test", Contract())