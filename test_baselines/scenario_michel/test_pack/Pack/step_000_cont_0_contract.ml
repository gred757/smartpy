open SmartML

module Contract = struct
  let%entry_point pack_lambda_record () =
    verify ((100 (open_some (unpack (pack (fun _x0 -> result {a = 1; b = (_x0 + 2)})) lambda int {a = int; b = int}))).a = 1);
    verify ((100 (open_some (unpack (pack (fun _x1 -> result {a = (_x1 + 1)})) lambda int {a = int}))).a = 101)

  let%entry_point pack_lambda_variant () =
    verify ((open_variant (100 (open_some (unpack (pack (fun _x2 -> result (set_type_expr (variant a (_x2 + 1)) (`a int + `b int)))) lambda int (`a int + `b int)))) a) = 101);
    verify ((open_variant (100 (open_some (unpack (pack (fun _x3 -> result (variant a (_x3 + 1)))) lambda int (`a int)))) a) = 101)

  let%entry_point pack_lambdas () =
    let%mutable f1 = (set_type_expr (fun _x4 -> let%mutable y = 0 in ();
let%mutable r = "A" in ();
if _x4 = 0 then
  if _x4 = y then
    r <- "B";
result r) lambda int string) in ();
    let%mutable p1 = (pack f1) in ();
    let%mutable f2 = (set_type_expr (open_some (unpack p1 lambda int string)) lambda int string) in ();
    let%mutable p2 = (pack f2) in ();
    verify (p1 = p2);
    verify ((0 f1) = (0 f2));
    verify ((1 f1) = (1 f2));
    verify ((2 f1) = (2 f2))

  let%entry_point pack_lambdas2 () =
    let%mutable f1 = (set_type_expr (fun _x5 -> result (_x5 > 0)) lambda int bool) in ();
    let%mutable p1 = (pack f1) in ();
    let%mutable f2 = (set_type_expr (open_some (unpack p1 lambda int bool)) lambda int bool) in ();
    verify (1 f2)

  let%entry_point run () =
    verify ((pack true) = (bytes "0x05030a"));
    verify ((pack (set_type_expr true bool)) = (pack (set_type_expr true bool)));
    verify ((pack false) = (bytes "0x050303"));
    verify ((pack (set_type_expr false bool)) = (pack (set_type_expr false bool)));
    verify ((pack ()) = (bytes "0x05030b"));
    verify ((pack (set_type_expr () unit)) = (pack (set_type_expr () unit)));
    verify ((pack (tez 0)) = (bytes "0x050000"));
    verify ((pack (set_type_expr (tez 0) mutez)) = (pack (set_type_expr (tez 0) mutez)));
    verify ((pack (mutez 1)) = (bytes "0x050001"));
    verify ((pack (set_type_expr (mutez 1) mutez)) = (pack (set_type_expr (mutez 1) mutez)));
    verify ((pack (mutez 2)) = (bytes "0x050002"));
    verify ((pack (set_type_expr (mutez 2) mutez)) = (pack (set_type_expr (mutez 2) mutez)));
    verify ((pack (mutez 3)) = (bytes "0x050003"));
    verify ((pack (set_type_expr (mutez 3) mutez)) = (pack (set_type_expr (mutez 3) mutez)));
    verify ((pack (mutez 10)) = (bytes "0x05000a"));
    verify ((pack (set_type_expr (mutez 10) mutez)) = (pack (set_type_expr (mutez 10) mutez)));
    verify ((pack (mutez 10000)) = (bytes "0x0500909c01"));
    verify ((pack (set_type_expr (mutez 10000) mutez)) = (pack (set_type_expr (mutez 10000) mutez)));
    verify ((pack (tez 1)) = (bytes "0x050080897a"));
    verify ((pack (set_type_expr (tez 1) mutez)) = (pack (set_type_expr (tez 1) mutez)));
    verify ((pack (tez 1000000)) = (bytes "0x050080c0a8ca9a3a"));
    verify ((pack (set_type_expr (tez 1000000) mutez)) = (pack (set_type_expr (tez 1000000) mutez)));
    verify ((pack 0) = (bytes "0x050000"));
    verify ((pack (set_type_expr 0 nat)) = (pack (set_type_expr 0 nat)));
    verify ((pack 1) = (bytes "0x050001"));
    verify ((pack (set_type_expr 1 nat)) = (pack (set_type_expr 1 nat)));
    verify ((pack 2) = (bytes "0x050002"));
    verify ((pack (set_type_expr 2 nat)) = (pack (set_type_expr 2 nat)));
    verify ((pack 3) = (bytes "0x050003"));
    verify ((pack (set_type_expr 3 nat)) = (pack (set_type_expr 3 nat)));
    verify ((pack 10) = (bytes "0x05000a"));
    verify ((pack (set_type_expr 10 nat)) = (pack (set_type_expr 10 nat)));
    verify ((pack 10000) = (bytes "0x0500909c01"));
    verify ((pack (set_type_expr 10000 nat)) = (pack (set_type_expr 10000 nat)));
    verify ((pack 1000000) = (bytes "0x050080897a"));
    verify ((pack (set_type_expr 1000000 nat)) = (pack (set_type_expr 1000000 nat)));
    verify ((pack 1000000000000) = (bytes "0x050080c0a8ca9a3a"));
    verify ((pack (set_type_expr 1000000000000 nat)) = (pack (set_type_expr 1000000000000 nat)));
    verify ((pack 0) = (bytes "0x050000"));
    verify ((pack (set_type_expr 0 int)) = (pack (set_type_expr 0 int)));
    verify ((pack 1) = (bytes "0x050001"));
    verify ((pack (set_type_expr 1 int)) = (pack (set_type_expr 1 int)));
    verify ((pack 2) = (bytes "0x050002"));
    verify ((pack (set_type_expr 2 int)) = (pack (set_type_expr 2 int)));
    verify ((pack 3) = (bytes "0x050003"));
    verify ((pack (set_type_expr 3 int)) = (pack (set_type_expr 3 int)));
    verify ((pack 10) = (bytes "0x05000a"));
    verify ((pack (set_type_expr 10 int)) = (pack (set_type_expr 10 int)));
    verify ((pack 10000) = (bytes "0x0500909c01"));
    verify ((pack (set_type_expr 10000 int)) = (pack (set_type_expr 10000 int)));
    verify ((pack 1000000) = (bytes "0x050080897a"));
    verify ((pack (set_type_expr 1000000 int)) = (pack (set_type_expr 1000000 int)));
    verify ((pack 1000000000000) = (bytes "0x050080c0a8ca9a3a"));
    verify ((pack (set_type_expr 1000000000000 int)) = (pack (set_type_expr 1000000000000 int)));
    verify ((pack 0) = (bytes "0x050000"));
    verify ((pack (set_type_expr 0 int)) = (pack (set_type_expr 0 int)));
    verify ((pack (-1)) = (bytes "0x050041"));
    verify ((pack (set_type_expr (-1) int)) = (pack (set_type_expr (-1) int)));
    verify ((pack (-2)) = (bytes "0x050042"));
    verify ((pack (set_type_expr (-2) int)) = (pack (set_type_expr (-2) int)))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [pack_lambda_record; pack_lambda_variant; pack_lambdas; pack_lambdas2; run]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())