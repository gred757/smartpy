import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TNat).layout("a"))
    self.init(a = 0)

  @sp.entry_point
  def test_sub_incr(self):
    self.data.a = 0
    sp.verify(self.sub_incr(100) == 101)

  @sp.entry_point
  def test_sub_incr_both(self):
    self.data.a = 0
    sp.verify(self.sub_incr_both(100) == 101)
    sp.verify(self.data.a == 1)

  @sp.entry_point
  def test_sub_incr_store(self):
    self.data.a = self.sub_incr_both(100)

  @sp.entry_point
  def test_with_storage(self):
    self.data.a = 0
    sp.verify(self.incr(100) == 101)
    sp.verify(self.data.a == 1)
    self.data.a = 10
    sp.verify(self.double(100) == 200)
    sp.verify(self.data.a == 20)
    self.data.a = 10
    sp.verify(self.double(self.incr(100)) == 202)
    sp.verify(self.data.a == 22)
    self.data.a = 10
    sp.verify((self.double(100) + self.incr(1000)) == 1201)
    sp.verify(self.data.a == 22)
    self.data.a = 10
    sp.verify((self.incr(1000) + self.double(100)) == 1201)
    sp.verify(self.data.a == 21)

  @sp.private_lambda()
  def double(_x0):
    self.data.a = 2 * self.data.a
    sp.result(2 * _x0)

  @sp.private_lambda()
  def incr(_x1):
    self.data.a += 1
    sp.result(_x1 + 1)

  @sp.private_lambda()
  def sub_double(_x2):
    compute_test_effects_38 = sp.local("compute_test_effects_38", sp.contract(sp.TUnit, sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'), entry_point='sub_double').open_some())
    sp.transfer(sp.unit, sp.tez(0), compute_test_effects_38.value)
    sp.result(2 * _x2)

  @sp.private_lambda()
  def sub_incr(_x3):
    compute_test_effects_32 = sp.local("compute_test_effects_32", sp.contract(sp.TUnit, sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'), entry_point='sub_incr').open_some())
    sp.transfer(sp.unit, sp.tez(0), compute_test_effects_32.value)
    sp.result(_x3 + 1)

  @sp.private_lambda()
  def sub_incr_both(_x4):
    compute_test_effects_44 = sp.local("compute_test_effects_44", sp.contract(sp.TUnit, sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'), entry_point='sub_incr').open_some())
    sp.transfer(sp.unit, sp.tez(0), compute_test_effects_44.value)
    self.data.a += 1
    sp.result(_x4 + 1)

sp.add_compilation_target("test", Contract())