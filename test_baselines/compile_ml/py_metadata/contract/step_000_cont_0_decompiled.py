import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(metadata = sp.TMap(sp.TString, sp.TBytes), x = sp.TInt).layout(("metadata", "x")))

  @sp.entry_point
  def change_metadata(self, params):
    sp.set_type(params, sp.TBytes)
    self.data.metadata = sp.update_map(self.data.metadata, '', sp.some(params))

  @sp.entry_point
  def incr(self, params):
    sp.set_type(params, sp.TUnit)
    self.data.x += 1

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
