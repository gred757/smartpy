#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/test_primitives.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/compile_ml/test_primitives/scenario.json"
else
  exit 1;;