import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(deck = sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TInt)), draw = sp.TBool, nbMoves = sp.TIntOrNat, nextPlayer = sp.TInt, winner = sp.TInt).layout((("deck", "draw"), ("nbMoves", ("nextPlayer", "winner")))))
    self.init(deck = {0 : {0 : 0, 1 : 0, 2 : 0}, 1 : {0 : 0, 1 : 0, 2 : 0}, 2 : {0 : 0, 1 : 0, 2 : 0}},
              draw = False,
              nbMoves = 0,
              nextPlayer = 1,
              winner = 0)

  @sp.entry_point
  def play(self, params):
    sp.verify((self.data.winner == 0) & (~ self.data.draw))
    sp.verify((params.i >= 0) & (params.i < 3))
    sp.verify((params.j >= 0) & (params.j < 3))
    sp.verify(params.move == self.data.nextPlayer)
    sp.verify(self.data.deck[params.i][params.j] == 0)
    self.data.deck[params.i][params.j] = params.move
    self.data.nbMoves += 1
    self.data.nextPlayer = 3 - self.data.nextPlayer
    sp.if ((self.data.deck[params.i][0] != 0) & (self.data.deck[params.i][0] == self.data.deck[params.i][1])) & (self.data.deck[params.i][0] == self.data.deck[params.i][2]):
      self.data.winner = self.data.deck[params.i][0]
    sp.if ((self.data.deck[0][params.j] != 0) & (self.data.deck[0][params.j] == self.data.deck[1][params.j])) & (self.data.deck[0][params.j] == self.data.deck[2][params.j]):
      self.data.winner = self.data.deck[0][params.j]
    sp.if ((self.data.deck[0][0] != 0) & (self.data.deck[0][0] == self.data.deck[1][1])) & (self.data.deck[0][0] == self.data.deck[2][2]):
      self.data.winner = self.data.deck[0][0]
    sp.if ((self.data.deck[0][2] != 0) & (self.data.deck[0][2] == self.data.deck[1][1])) & (self.data.deck[0][2] == self.data.deck[2][0]):
      self.data.winner = self.data.deck[0][2]
    sp.if (self.data.nbMoves == 9) & (self.data.winner == 0):
      self.data.draw = True

sp.add_compilation_target("test", Contract())