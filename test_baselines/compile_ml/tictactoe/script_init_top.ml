#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/tictactoe.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/compile_ml/tictactoe/scenario.json"
else
  exit 1;;