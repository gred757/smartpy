#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/py_onchain_views.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/compile_ml/py_onchain_views/scenario.json"
else
  exit 1;;