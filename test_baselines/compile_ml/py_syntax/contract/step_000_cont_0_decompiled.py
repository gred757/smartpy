import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(aaa = sp.TSet(sp.TNat), abc = sp.TList(sp.TOption(sp.TInt)), abca = sp.TMap(sp.TInt, sp.TOption(sp.TInt)), acb = sp.TString, b = sp.TBool, ddd = sp.TList(sp.TInt), f = sp.TBool, h = sp.TBytes, i = sp.TInt, m = sp.TMap(sp.TInt, sp.TInt), n = sp.TNat, pkh = sp.TKeyHash, s = sp.TString, toto = sp.TString).layout(((("aaa", ("abc", "abca")), (("acb", "b"), ("ddd", "f"))), (("h", ("i", "m")), (("n", "pkh"), ("s", "toto"))))))

  @sp.entry_point
  def comparisons(self, params):
    sp.set_type(params, sp.TUnit)
    sp.verify(self.data.i <= 123, 'WrongCondition: self.data.i <= 123')
    sp.verify((2 + self.data.i) == 12, 'WrongCondition: (2 + self.data.i) == 12')
    sp.verify(((2 + self.data.i) + (- 1234)) != 0, 'WrongCondition: (2 + self.data.i) != 1234')
    sp.verify(((self.data.i + 4) + (- 123)) != 0, 'WrongCondition: (self.data.i + 4) != 123')
    sp.verify((self.data.i - 5) < 123, 'WrongCondition: (self.data.i - 5) < 123')
    sp.verify((7 - self.data.i) < 123, 'WrongCondition: (7 - self.data.i) < 123')
    sp.verify(3 < self.data.i, 'WrongCondition: self.data.i > 3')
    sp.verify((4 * self.data.i) > 3, 'WrongCondition: (4 * self.data.i) > 3')
    sp.verify((self.data.i * 5) > 3, 'WrongCondition: (self.data.i * 5) > 3')
    sp.verify(self.data.i >= 3, 'WrongCondition: self.data.i >= 3')
    sp.verify(3 < self.data.i, 'WrongCondition: self.data.i > 3')
    sp.verify(self.data.i >= 3, 'WrongCondition: self.data.i >= 3')
    sp.verify(3000 > self.data.i, 'WrongCondition: self.data.i < 3000')
    sp.verify(self.data.i <= 3000, 'WrongCondition: self.data.i <= 3000')
    sp.verify(self.data.b, 'WrongCondition: self.data.b & True')
    x178 = sp.bind_block("x178"):
    with x178:
      sp.if 3 < self.data.i:
        sp.verify(4 < self.data.i, 'WrongCondition: self.data.i > 4')
        sp.result((((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
      sp.else:
        sp.result((((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
    x1001 = sp.local("x1001", x178.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x1001)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def iterations(self, params):
    sp.set_type(params, sp.TUnit)
    c182 = sp.local("c182", True)
    y181 = sp.local("y181", 0)
    sp.while c182.value:
      x429 = sp.local("x429", 1 + y181.value)
      c182.value = 5 > x429.value
      y181.value = x429.value
    x471 = sp.local("x471", (((self.data.aaa, (self.data.abc, self.data.abca)), ((self.data.acb, self.data.b), (self.data.ddd, self.data.f))), ((self.data.h, (5, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto)))))
    c224 = sp.local("c224", sp.fst(sp.snd(sp.fst(sp.snd(x471.value)))) <= 42)
    r847 = sp.local("r847", x471.value)
    sp.while c224.value:
      x513 = sp.local("x513", (sp.fst(r847.value), ((sp.fst(sp.fst(sp.snd(r847.value))), (2 + sp.fst(sp.snd(sp.fst(sp.snd(r847.value)))), sp.snd(sp.snd(sp.fst(sp.snd(r847.value)))))), sp.snd(sp.snd(r847.value)))))
      c224.value = sp.fst(sp.snd(sp.fst(sp.snd(x513.value)))) <= 42
      r847.value = x513.value
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., if Le(Compare(Car(Cdr(Car(Cdr(r847)))), 123))
                            then
                              Pair
                                ( Car(r847)
                                , Pair
                                    ( Pair
                                        ( Car(Car(Cdr(r847)))
                                        , Pair
                                            ( Add
                                                ( 12
                                                , Car(Cdr(Car(Cdr(r847))))
                                                )
                                            , Cdr(Cdr(Car(Cdr(r847))))
                                            )
                                        )
                                    , Cdr(Cdr(r847))
                                    )
                                )
                            else
                              Pair
                                ( Car(r847)
                                , Pair
                                    ( Pair
                                        ( Car(Car(Cdr(r847)))
                                        , Pair(5, Cdr(Cdr(Car(Cdr(r847)))))
                                        )
                                    , Cdr(Cdr(r847))
                                    )
                                ))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def localVariable(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(aaa = self.data.aaa, abc = self.data.abc, abca = self.data.abca, acb = self.data.acb, b = self.data.b, ddd = self.data.ddd, f = self.data.f, h = self.data.h, i = self.data.i * 2, m = self.data.m, n = self.data.n, pkh = self.data.pkh, s = self.data.s, toto = self.data.toto)

  @sp.entry_point
  def myMessageName4(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [r846] =
                              iter [ __storage.m
                                   ; Pair
                                       ( Pair
                                           ( Pair
                                               ( __storage.aaa
                                               , Pair
                                                   ( __storage.abc
                                                   , __storage.abca
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.acb
                                                   , __storage.b
                                                   )
                                               , Pair
                                                   ( __storage.ddd
                                                   , __storage.f
                                                   )
                                               )
                                           )
                                       , Pair
                                           ( Pair
                                               ( __storage.h
                                               , Pair
                                                   ( __storage.i
                                                   , __storage.m
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.n
                                                   , __storage.pkh
                                                   )
                                               , Pair
                                                   ( __storage.s
                                                   , __storage.toto
                                                   )
                                               )
                                           )
                                       )
                                   ]
                              step s329; s331 ->
                                [ Pair
                                    ( Car(s331)
                                    , Pair
                                        ( Pair
                                            ( Car(Car(Cdr(s331)))
                                            , Pair
                                                ( Add
                                                    ( Mul
                                                        ( Car(s329)
                                                        , Cdr(s329)
                                                        )
                                                    , Car(Cdr(Car(Cdr(s331))))
                                                    )
                                                , Cdr(Cdr(Car(Cdr(s331))))
                                                )
                                            )
                                        , Cdr(Cdr(s331))
                                        )
                                    )
                                ]
                            r846)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def myMessageName5(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [r845] =
                              iter [ __storage.m
                                   ; Pair
                                       ( Pair
                                           ( Pair
                                               ( __storage.aaa
                                               , Pair
                                                   ( __storage.abc
                                                   , __storage.abca
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.acb
                                                   , __storage.b
                                                   )
                                               , Pair
                                                   ( __storage.ddd
                                                   , __storage.f
                                                   )
                                               )
                                           )
                                       , Pair
                                           ( Pair
                                               ( __storage.h
                                               , Pair
                                                   ( __storage.i
                                                   , __storage.m
                                                   )
                                               )
                                           , Pair
                                               ( Pair
                                                   ( __storage.n
                                                   , __storage.pkh
                                                   )
                                               , Pair
                                                   ( __storage.s
                                                   , __storage.toto
                                                   )
                                               )
                                           )
                                       )
                                   ]
                              step s279; s281 ->
                                [ Pair
                                    ( Car(s281)
                                    , Pair
                                        ( Pair
                                            ( Car(Car(Cdr(s281)))
                                            , Pair
                                                ( Add
                                                    ( Mul(2, Car(s279))
                                                    , Car(Cdr(Car(Cdr(s281))))
                                                    )
                                                , Cdr(Cdr(Car(Cdr(s281))))
                                                )
                                            )
                                        , Cdr(Cdr(s281))
                                        )
                                    )
                                ]
                            r845)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def myMessageName6(self, params):
    sp.set_type(params, sp.TInt)
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [r844] =
          iter [ __storage.m
               ; Pair
                   ( Pair
                       ( Pair
                           ( __storage.aaa
                           , Pair(__storage.abc, __storage.abca)
                           )
                       , Pair
                           ( Pair(__storage.acb, __storage.b)
                           , Pair(__storage.ddd, __storage.f)
                           )
                       )
                   , Pair
                       ( Pair(__storage.h, Pair(__storage.i, __storage.m))
                       , Pair
                           ( Pair(__storage.n, __storage.pkh)
                           , Pair(__storage.s, __storage.toto)
                           )
                       )
                   )
               ]
          step s100; s102 ->
            [ Pair
                ( Car(s102)
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(s102)))
                        , Pair
                            ( Add(Mul(3, Cdr(s100)), Car(Cdr(Car(Cdr(s102)))))
                            , Cdr(Cdr(Car(Cdr(s102))))
                            )
                        )
                    , Cdr(Cdr(s102))
                    )
                )
            ]
        let [s236; s237] =
          if Mem
               ( 12#nat
               , Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r844)))))
               )
          then
            [ l7
            ; Pair
                ( Pair
                    ( Pair
                        ( Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r844)))))
                        , Pair
                            ( Cons(Some_(16), Car(Cdr(Car(Car(r844)))))
                            , Update(0, Some_(Some_(16)), Cdr(Cdr(Car(Car(r844)))))
                            )
                        )
                    , Cdr(Car(r844))
                    )
                , Pair
                    ( Pair
                        ( Car(Car(Cdr(r844)))
                        , Pair
                            ( Car(Cdr(Car(Cdr(r844))))
                            , Update(42, Some_(43), Cdr(Cdr(Car(Cdr(r844)))))
                            )
                        )
                    , Cdr(Cdr(r844))
                    )
                )
            ]
          else
            [ l7
            ; Pair
                ( Pair
                    ( Pair
                        ( Update(12#nat, True, Update(2#nat, False, Car(Car(Car(r844)))))
                        , Pair
                            ( Cons(Some_(16), Car(Cdr(Car(Car(r844)))))
                            , Update(0, Some_(Some_(16)), Cdr(Cdr(Car(Car(r844)))))
                            )
                        )
                    , Cdr(Car(r844))
                    )
                , Cdr(r844)
                )
            ]
        let x991 =
          match IsNat(Neg(s236)) with
          | None _ -> Failwith(75)
          | Some s255 ->
              Pair
                ( Pair
                    ( Pair
                        ( Update(s255, True, Car(Car(Car(s237))))
                        , Cdr(Car(Car(s237)))
                        )
                    , Cdr(Car(s237))
                    )
                , Cdr(s237)
                )
          end
        Pair(Nil<operation>, record_of_tree(..., x991))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def someComputations(self, params):
    sp.set_type(params, sp.TPair(sp.TString, sp.TInt))
    sp.verify(self.data.i <= 123, 'WrongCondition: self.data.i <= 123')
    x69 = sp.local("x69", ((self.data.h, (sp.snd(params) + self.data.i, self.data.m)), ((self.data.n, self.data.pkh), (self.data.s, self.data.toto))))
    x378 = sp.local("x378", (((self.data.aaa, (self.data.abc, self.data.abca)), ((sp.fst(params), self.data.b), (self.data.ddd, self.data.f))), ((sp.fst(sp.fst(x69.value)), (100 - 1, sp.snd(sp.snd(sp.fst(x69.value))))), sp.snd(x69.value))))
    x989 = sp.local("x989", x378.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x989)]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
