open SmartML

module Contract = struct
  let%entry_point add params =
    data.value <- params.x + params.y

  let%entry_point factorial params =
    data.value <- 1;
    List.iter (fun y ->
      data.value <- data.value * y
    ) (range 1 (params + 1) 1)

  let%entry_point log2 params =
    data.value <- 0;
    let%mutable y = params in ();
    while y > 1 do
      data.value <- data.value + 1;
      y <- y / 2
    done

  let%entry_point multiply params =
    data.value <- params.x * params.y

  let%entry_point other params =
    let%mutable compute_test_exception_optimization_47 = (Map.make [(1, 2); (2, 3)]) in ();
    data.value <- Map.get compute_test_exception_optimization_47 params;
    data.value <- data.value + (get ~default_value:%s compute_test_exception_optimization_47 params 12);
    data.value <- data.value + (get ~message:%s compute_test_exception_optimization_47 params "not here")

  let%entry_point square params =
    data.value <- params * params

  let%entry_point squareRoot params =
    verify (params >= 0);
    let%mutable y = params in ();
    while (y * y) > params do
      y <- ((params / y) + y) / 2
    done;
    verify (((y * y) <= params) && (params < ((y + 1) * (y + 1))));
    data.value <- y

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = nat}]
      ~storage:[%expr
                 {value = 0}]
      [add; factorial; log2; multiply; other; square; squareRoot]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())