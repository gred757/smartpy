import smartpy as sp

tstorage = sp.TRecord(value = sp.TNat).layout("value")
tparameter = sp.TVariant(add = sp.TRecord(x = sp.TNat, y = sp.TNat).layout(("x", "y")), factorial = sp.TNat, log2 = sp.TNat, multiply = sp.TRecord(x = sp.TNat, y = sp.TNat).layout(("x", "y")), other = sp.TIntOrNat, square = sp.TNat, squareRoot = sp.TNat).layout((("add", ("factorial", "log2")), (("multiply", "other"), ("square", "squareRoot"))))
tprivates = { }
tviews = { }
