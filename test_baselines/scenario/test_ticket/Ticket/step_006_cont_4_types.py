import smartpy as sp

tstorage = sp.TRecord(m = sp.TMap(sp.TInt, sp.TTicket(sp.TInt)), x = sp.TInt).layout(("m", "x"))
tparameter = sp.TVariant(ep1 = sp.TUnit, ep2 = sp.TContract(sp.TPair(sp.TTicket(sp.TString), sp.TTicket(sp.TString)))).layout(("ep1", "ep2"))
tprivates = { }
tviews = { }
