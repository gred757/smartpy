Comment...
 h1: FA2 Contract Name: FA2-nft-mutez
Table Of Contents

 FA2 Contract Name: FA2-nft-mutez
# Accounts
Comment...
 h2: Accounts
Computing sp.list([sp.test_account("Administrator"), sp.test_account("Alice"), sp.test_account("Robert")])...
 => [sp.record(seed = 'Administrator', address = sp.address('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), public_key = sp.key('edpktzrjdb1tx6dQecQGZL6CwhujWg1D2CXfXWBriqtJSA6kvqMwA2'), public_key_hash = sp.key_hash('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w'), secret_key = sp.secret_key('edskRqFp3Z9AqoKrMNFb9bnWNwEsRzbjqjBhzmFMLF9UqB6VBmw7F8ppTiXaAnHtysmi6xFxoHf6rMUz6Y1ipiDz2EgwZQv3pa')), sp.record(seed = 'Alice', address = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), public_key = sp.key('edpkuvNy6TuQ2z8o9wnoaTtTXkzQk7nhegCHfxBc4ecsd4qG71KYNG'), public_key_hash = sp.key_hash('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'), secret_key = sp.secret_key('edskRijgcXx8gzqkq7SCBbrb6aDZQMmP6dznCQWgU1Jr4qPfJT1yFq5A39ja9G4wahS8uWtBurZy14Hy7GZkQh7WnopJTKtCQG')), sp.record(seed = 'Robert', address = sp.address('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'), public_key = sp.key('edpkvThfdv8Efh1MuqSTUk5EnUFCTjqN6kXDCNXpQ8udN3cKRhNDr2'), public_key_hash = sp.key_hash('tz1Ns3YQJR6piMZ8GrD2iYu94Ybi1HFfNyBP'), secret_key = sp.secret_key('edskRiaffUWqB9zgaEhuX6EmejbLzk2xcpSEXLv3G4cDfcbY75c71ASyGnFHXuaTAVMPt2bJLGGye1gm24oBmAc2k5VDHHo5Ua'))]
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w" (Pair 0 (Pair {} (Pair {Elt "" 0x68747470733a2f2f6578616d706c652e636f6d} (Pair {} (Pair False (Pair {} {})))))))
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_storage.tz 1
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_storage.json 25
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_sizes.csv 2
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_storage.py 1
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_types.py 7
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_metadata.metadata_base.json 179
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_contract.tz 742
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_contract.json 1019
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_contract.py 94
 => test_baselines/scenario/FA2/FA2-nft-mutez/step_004_cont_0_contract.ml 99
