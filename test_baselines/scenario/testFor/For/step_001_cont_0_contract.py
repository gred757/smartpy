import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(ms = sp.TList(sp.TMap(sp.TString, sp.TIntOrNat)), rs = sp.TList(sp.TRecord(a = sp.TIntOrNat, b = sp.TIntOrNat).layout(("a", "b"))), x = sp.TIntOrNat, xxs = sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TNat)), xxxs = sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TIntOrNat)))).layout(("ms", ("rs", ("x", ("xxs", "xxxs"))))))
    self.init(ms = [{'a' : 97, 'b' : 98}, {'a' : 0, 'b' : 1}],
              rs = [sp.record(a = 5, b = 6)],
              x = 0,
              xxs = {0 : {0 : 0}},
              xxxs = {0 : {0 : {0 : 0}}})

  @sp.entry_point
  def ep1(self):
    self.data.x = 0
    s = sp.local("s", sp.record(a = 5, b = 6))
    sp.for r in sp.list([sp.record(a = 2, b = 3), s.value]):
      self.data.x += r.a
    sp.verify(self.data.x == 7)

  @sp.entry_point
  def ep2(self):
    self.data.x = 2
    sp.for i in sp.list([self.data.x, self.data.x, self.data.x]):
      self.data.x += i
    sp.verify(self.data.x == 8)

  @sp.entry_point
  def ep3(self):
    self.data.rs = sp.list([sp.record(a = 2, b = 3), sp.record(a = 2, b = 3), sp.record(a = 2, b = 3)])
    sp.for i in self.data.rs:
      i.a = 0

  @sp.entry_point
  def ep4(self):
    self.data.xxs = sp.set_type_expr({0 : {0 : 2, 1 : 3}, 1 : {0 : 2, 1 : 3}, 2 : {0 : 2, 1 : 3}}, sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TNat)))
    sp.for xs in self.data.xxs.values():
      xs[0] = 0
      sp.send(sp.address('tz1PiDHTNJXhqpkbRUYNZEzmePNd21WcB8yB'), sp.mul(xs[1], sp.mutez(1)))

  @sp.entry_point
  def ep5(self):
    self.data.x = 0
    self.data.xxxs = sp.set_type_expr({0 : {0 : {0 : 1}}, 1 : {0 : {0 : 2}}}, sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TIntOrNat))))
    sp.for xs in self.data.xxxs[self.data.x].values():
      self.data.x = 1
      xs[0] = 0

  @sp.entry_point
  def ep6(self):
    self.data.xxxs = sp.set_type_expr({0 : {0 : {0 : 0}, 1 : {0 : 10}}, 1 : {0 : {0 : 20}, 1 : {0 : 30}}}, sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TMap(sp.TIntOrNat, sp.TIntOrNat))))
    sp.for xs in self.data.xxxs[self.data.xxxs[0][0][0]].values():
      xs[0] = 1

  @sp.entry_point
  def ep7(self):
    self.data.ms = sp.list([{'a' : 97, 'b' : 98}, {'a' : 0, 'b' : 1}])
    sp.for m in self.data.ms:
      del m['a']

sp.add_compilation_target("test", Contract())