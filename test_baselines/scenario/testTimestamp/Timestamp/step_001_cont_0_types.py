import smartpy as sp

tstorage = sp.TRecord(next = sp.TTimestamp, out = sp.TBool).layout(("next", "out"))
tparameter = sp.TVariant(ep = sp.TUnit).layout("ep")
tprivates = { }
tviews = { }
