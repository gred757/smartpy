open SmartML

module Contract = struct
  let%entry_point arith () =
    verify (michelson("ADD")(1, 2) = 3);
    verify (michelson("ADD")(1, -2) = (-1));
    verify (michelson("SUB")(1, -2) = 3);
    verify (michelson("SUB")(1, 2) = (-1))

  let%entry_point concat1 () =
    verify (michelson("CONCAT")(["a"; "b"; "c"]) = "abc")

  let%entry_point concat2 () =
    verify (michelson("CONCAT")("a", "b") = "ab")

  let%entry_point lambdas () =
    verify (michelson("PUSH (lambda int int) {DUP; ADD}; SWAP; EXEC;")(100) = 200);
    verify (michelson("PAIR; PUSH (lambda (pair int int) int) { UNPAIR; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(2, 5) = 25);
    verify (michelson("PAIR 3; PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; SWAP; EXEC;")(2, 5, 7) = 257);
    verify (michelson("PUSH (lambda (pair int int int) int) { UNPAIR 3; PUSH int 10; MUL; ADD; PUSH int 10; MUL; ADD;}; PUSH int 2; APPLY; PUSH int 5; APPLY; PUSH int 7; EXEC;")() = 257)

  let%entry_point overflow_add () =
    let%mutable compute_inlineMichelson_36 = michelson("ADD")(mutez 9223372036854775807, mutez 1) in ()

  let%entry_point overflow_mul () =
    let%mutable compute_inlineMichelson_41 = michelson("MUL")(mutez 4611686018427387904, 2) in ()

  let%entry_point prim0 () =
    verify (michelson("UNIT")() = ());
    verify (michelson("NONE unit")() = None);
    verify (michelson("PUSH int 2; PUSH int 1; PAIR")() = (1, 2));
    verify ((len michelson("NIL unit")()) = 0);
    verify ((len michelson("EMPTY_SET unit")()) = 0);
    verify ((len michelson("EMPTY_MAP unit unit")()) = 0)

  let%entry_point seq () =
    verify (michelson("DIP {SWAP}; ADD; MUL; DUP; MUL;")(15, 16, 17) = 262144)

  let%entry_point test_operations () =
    List.iter (fun op ->
      operations <- op :: operations
    ) [michelson("NONE key_hash; SET_DELEGATE;")()];
    match_pair_inlineMichelson_89_fst, match_pair_inlineMichelson_89_snd = match_tuple(michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")(), "match_pair_inlineMichelson_89_fst", "match_pair_inlineMichelson_89_snd")
    List.iter (fun op ->
      operations <- op :: operations
    ) [fst michelson("PUSH int 123; PUSH mutez 0; NONE key_hash; CREATE_CONTRACT { parameter int; storage int; code { UNPAIR; ADD; NIL operation; PAIR } }; PAIR")()]

  let%entry_point underflow () =
    let%mutable compute_inlineMichelson_46 = michelson("SUB")(tez 0, mutez 1) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {s = string; value = intOrNat}]
      ~storage:[%expr
                 {s = "";
                  value = 0}]
      [arith; concat1; concat2; lambdas; overflow_add; overflow_mul; prim0; seq; test_operations; underflow]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())