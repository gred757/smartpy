open SmartML

module Contract = struct
  let%entry_point negate params =
    set_type params.fr bls12_381_fr;
    data.fr <- some (- params.fr)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {fr = option bls12_381_fr}]
      ~storage:[%expr
                 {fr = None}]
      [negate]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())