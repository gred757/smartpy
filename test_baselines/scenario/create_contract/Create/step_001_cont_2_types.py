import smartpy as sp

tstorage = sp.TRecord(l = sp.TLambda(sp.TInt, sp.TRecord(address = sp.TAddress, operation = sp.TOperation).layout(("operation", "address"))), x = sp.TOption(sp.TAddress)).layout(("l", "x"))
tparameter = sp.TVariant(create1 = sp.TUnit, create2 = sp.TUnit, create3 = sp.TUnit, create4 = sp.TList(sp.TInt), create5 = sp.TUnit, create_op = sp.TUnit).layout((("create1", ("create2", "create3")), ("create4", ("create5", "create_op"))))
tprivates = { }
tviews = { }
