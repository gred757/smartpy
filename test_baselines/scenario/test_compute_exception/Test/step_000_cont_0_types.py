import smartpy as sp

tstorage = sp.TRecord(value = sp.TInt).layout("value")
tparameter = sp.TUnit
tprivates = { }
tviews = { "other": (sp.TInt, sp.TInt), "square_exn": (sp.TInt, sp.TUnknown()) }
