import smartpy as sp

tstorage = sp.TRecord(x = sp.TInt, y = sp.TBool).layout(("x", "y"))
tparameter = sp.TVariant(bounce = sp.TAddress, check_bounce = sp.TUnit, modify_x = sp.TInt, take_ticket = sp.TTicket(sp.TInt), update_bounce = sp.TLambda(sp.TPair(sp.TVariant(bounce = sp.TAddress, modify_x = sp.TInt).layout(("bounce", "modify_x")), sp.TRecord(x = sp.TInt, y = sp.TBool).layout(("x", "y"))), sp.TPair(sp.TList(sp.TOperation), sp.TRecord(x = sp.TInt, y = sp.TBool).layout(("x", "y")))), update_modify_x = sp.TLambda(sp.TPair(sp.TVariant(bounce = sp.TAddress, modify_x = sp.TInt).layout(("bounce", "modify_x")), sp.TRecord(x = sp.TInt, y = sp.TBool).layout(("x", "y"))), sp.TPair(sp.TList(sp.TOperation), sp.TRecord(x = sp.TInt, y = sp.TBool).layout(("x", "y"))))).layout((("bounce", ("check_bounce", "modify_x")), ("take_ticket", ("update_bounce", "update_modify_x"))))
tprivates = { }
tviews = { }
