import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def ep1(self, params):
    sp.transfer(1, sp.tez(0), sp.contract(sp.TNat, params.address1).open_some(message = 'WRONG_INTERFACE_Contract1'))
    sp.transfer(1, sp.tez(0), sp.contract(sp.TNat, params.address2, entry_point='ep').open_some(message = 'WRONG_INTERFACE_Contract2'))

  @sp.entry_point
  def ep2(self, params):
    sp.transfer(1, sp.tez(0), sp.contract(sp.TNat, params.address1).open_some(message = 'WRONG_INTERFACE_Contract1'))
    sp.transfer(1, sp.tez(0), sp.contract(sp.TNat, params.address2, entry_point='ep').open_some(message = 'WRONG_INTERFACE_Contract2'))

sp.add_compilation_target("test", Contract())