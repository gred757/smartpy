open SmartML

module Contract = struct
  let%entry_point add params =
    data.fr <- some ((fst params) + (snd params))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {fr = option bls12_381_fr}]
      ~storage:[%expr
                 {fr = None}]
      [add]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())