open SmartML

module Contract = struct
  let%entry_point negate params =
    set_type params.g1 bls12_381_g1;
    data.g1 <- some (- params.g1)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {g1 = option bls12_381_g1}]
      ~storage:[%expr
                 {g1 = None}]
      [negate]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())