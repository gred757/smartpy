open SmartML

module Contract = struct
  let%entry_point ep1 () =
    set_type data.x2 {a = nat; b = int; c = bool; d = string};
    set_type data.x3 {a = nat; b = int; c = bool; d = string};
    set_type data.x4 {a = nat; b = int; c = bool; d = string};
    with match_record(data.x1, "modify_record_test_modify_20") as modify_record_test_modify_20:
      verify ((abs modify_record_test_modify_20.b) = (modify_record_test_modify_20.a + 1));
    with match_record(data.x2, "modify_record_test_modify_22") as modify_record_test_modify_22:
      verify ((abs modify_record_test_modify_22.b) = (modify_record_test_modify_22.a + 1));
      modify_record_test_modify_22.d <- "xyz";
    with match_record(data.x3, "modify_record_test_modify_25") as modify_record_test_modify_25:
      verify ((abs modify_record_test_modify_25.b) = (modify_record_test_modify_25.a + 1));
      modify_record_test_modify_25.d <- "xyz";
    with match_record(data.x4, "modify_record_test_modify_28") as modify_record_test_modify_28:
      verify ((abs modify_record_test_modify_28.b) = (modify_record_test_modify_28.a + 1));
      modify_record_test_modify_28.d <- "xyz"

  let%entry_point ep2 () =
    with match_record(data.x1, "modify_record_test_modify_34") as modify_record_test_modify_34:
      verify ((abs modify_record_test_modify_34.b) = (modify_record_test_modify_34.a + 1));
      modify_record_test_modify_34.d <- "xyz"

  let%entry_point ep3 () =
    with match_tuple(data.y, "a", "b", "c", "d") as a, b, c, d:
      verify ((abs b) = (a + 1));
      d <- "xyz";
      result (a, b, c, d)

  let%entry_point ep4 () =
    with match_record(data.x1, "modify_record_test_modify_48") as modify_record_test_modify_48:
      ()

  let%entry_point ep5 params =
    with match_record(data.x1, "modify_record_test_modify_60") as modify_record_test_modify_60:
      send params (tez 0);
      modify_record_test_modify_60.d <- "xyz";
    data.x1.a <- data.x1.a + 5

  let%entry_point ep6 () =
    with match_record(data.z, "modify_record_test_modify_67") as modify_record_test_modify_67:
      with match_record(modify_record_test_modify_67.d, "modify_record_test_modify_68") as modify_record_test_modify_68:
        modify_record_test_modify_67.b <- 100;
        modify_record_test_modify_68.e <- 2;
        modify_record_test_modify_68.f <- "y"

  let%entry_point ep7 () =
    verify (data.z.d.e = 2);
    with match_record(data.z.d, "modify_record_test_modify_76") as modify_record_test_modify_76:
      verify (modify_record_test_modify_76.e = 2);
      modify_record_test_modify_76.e <- 3;
      modify_record_test_modify_76.f <- "z";
      verify (modify_record_test_modify_76.e = 3);
      modify_record_test_modify_76.e <- 4;
      verify (modify_record_test_modify_76.e = 4);
    verify (data.z.d.e = 4)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x1 = {a = nat; b = int; c = bool; d = string}; x2 = {a = nat; b = int; c = bool; d = string}; x3 = {a = nat; b = int; c = bool; d = string}; x4 = {a = nat; b = int; c = bool; d = string}; y = [nat; int; bool; string]; z = {a = nat; b = int; c = bool; d = {e = intOrNat; f = string}}}]
      ~storage:[%expr
                 {x1 = {a = 0; b = 1; c = true; d = "abc"};
                  x2 = {a = 0; b = 1; c = true; d = "abc"};
                  x3 = {a = 0; b = 1; c = true; d = "abc"};
                  x4 = {a = 0; b = 1; c = true; d = "abc"};
                  y = (0, 1, true, "abc");
                  z = {a = 0; b = 1; c = true; d = {e = 1; f = "x"}}}]
      [ep1; ep2; ep3; ep4; ep5; ep6; ep7]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())