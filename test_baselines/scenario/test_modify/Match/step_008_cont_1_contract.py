import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TNat, b = sp.TInt, c = sp.TBool, d = sp.TString).layout(("a", ("b", ("c", "d")))))
    self.init(a = 0,
              b = 1,
              c = True,
              d = 'abc')

  @sp.entry_point
  def ep1(self):
    with sp.match_record(self.data, "modify_record_test_modify_91") as modify_record_test_modify_91:
      sp.verify((abs(modify_record_test_modify_91.b)) == (modify_record_test_modify_91.a + 1))
      modify_record_test_modify_91.d = 'xyz'

  @sp.entry_point
  def ep2(self):
    with sp.match_record(self.data, "modify_record_test_modify_97") as modify_record_test_modify_97:
      modify_record_test_modify_97.d = 'abc'

sp.add_compilation_target("test", Contract())