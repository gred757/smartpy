open SmartML

module Contract = struct
  let%entry_point ep1 () =
    with match_record(data, "modify_record_test_modify_91") as modify_record_test_modify_91:
      verify ((abs modify_record_test_modify_91.b) = (modify_record_test_modify_91.a + 1));
      modify_record_test_modify_91.d <- "xyz"

  let%entry_point ep2 () =
    with match_record(data, "modify_record_test_modify_97") as modify_record_test_modify_97:
      modify_record_test_modify_97.d <- "abc"

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {a = nat; b = int; c = bool; d = string}]
      ~storage:[%expr
                 {a = 0;
                  b = 1;
                  c = true;
                  d = "abc"}]
      [ep1; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())