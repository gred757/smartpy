open SmartML

module Contract = struct
  let%entry_point aggregated_endorsement params =
    set_type params (list {proposalId = nat; signatures = list {signature = signature; signerAddress = address}});
    List.iter (fun endorsement ->
      List.iter (fun signature ->
        verify (contains signature.signerAddress data.signers) ~msg:"MULTISIG_SignerUnknown";
        verify (check_signature (Map.get data.signers signature.signerAddress).publicKey signature.signature (pack {contractAddress = self_address; proposalId = endorsement.proposalId})) ~msg:"MULTISIG_Badsig";
        let%mutable compute_admin_multisig_302 = ({proposalId = endorsement.proposalId; signerAddress = signature.signerAddress} self.registerEndorsement) in ()
      ) endorsement.signatures;
      let%mutable compute_admin_multisig_308 = (Map.get data.proposals endorsement.proposalId) in ();
      if (len compute_admin_multisig_308.endorsements) >= data.quorum then
        let%mutable compute_admin_multisig_310 = ({actions = compute_admin_multisig_308.actions; proposalId = endorsement.proposalId} self.onApproved) in ()
    ) params

  let%entry_point aggregated_proposal params =
    set_type params {actions = `external (list {actions = bytes; target = address}) + `internal (list (`changeMetadata (pair string (option bytes)) + `changeQuorum nat + `changeSigners (`added (list {address = address; publicKey = key}) + `removed (set address)))); proposalId = nat; signatures = list {signature = signature; signerAddress = address}};
    verify (contains sender data.signers) ~msg:"MULTISIG_SignerUnknown";
    data.lastProposalId <- data.lastProposalId + 1;
    verify (data.lastProposalId = params.proposalId) ~msg:"MULTISIG_InvalidProposalId";
    let%mutable compute_admin_multisig_232 = {actions = params.actions; endorsements = (Set.make [sender]); initiator = sender; startedAt = now} in ();
    if (Map.get data.signers sender).lastProposalId <> None then
      Set.remove data.activeProposals (open_some (Map.get data.signers sender).lastProposalId);
    (Map.get data.signers sender).lastProposalId <- some params.proposalId;
    Set.add data.activeProposals params.proposalId;
    Map.set data.proposals params.proposalId compute_admin_multisig_232;
    let%mutable compute_admin_multisig_251 = (pack {actions = params.actions; contractAddress = self_address; proposalId = params.proposalId}) in ();
    List.iter (fun signature ->
      verify (contains signature.signerAddress data.signers) ~msg:"MULTISIG_SignerUnknown";
      verify (check_signature (Map.get data.signers signature.signerAddress).publicKey signature.signature compute_admin_multisig_251) ~msg:"MULTISIG_Badsig";
      Set.add compute_admin_multisig_232.endorsements signature.signerAddress
    ) params.signatures;
    if (len compute_admin_multisig_232.endorsements) >= data.quorum then
      let%mutable compute_admin_multisig_273 = ({actions = compute_admin_multisig_232.actions; proposalId = params.proposalId} self.onApproved) in ()

  let%entry_point cancel_proposal params =
    verify (contains sender data.signers) ~msg:"MULTISIG_SignerUnknown";
    verify ((Map.get data.proposals params).initiator = sender) ~msg:"MULTISIG_NotInitiator";
    Set.remove data.activeProposals params

  let%entry_point endorsement params =
    verify (contains sender data.signers) ~msg:"MULTISIG_SignerUnknown";
    List.iter (fun pId ->
      let%mutable compute_admin_multisig_204 = ({proposalId = pId; signerAddress = sender} self.registerEndorsement) in ();
      if (len (Map.get data.proposals pId).endorsements) >= data.quorum then
        let%mutable compute_admin_multisig_214 = ({actions = (Map.get data.proposals pId).actions; proposalId = pId} self.onApproved) in ()
    ) params

  let%entry_point proposal params =
    verify (contains sender data.signers) ~msg:"MULTISIG_SignerUnknown";
    if (Map.get data.signers sender).lastProposalId <> None then
      Set.remove data.activeProposals (open_some (Map.get data.signers sender).lastProposalId);
    data.lastProposalId <- data.lastProposalId + 1;
    Set.add data.activeProposals data.lastProposalId;
    Map.set data.proposals data.lastProposalId {actions = params; endorsements = (Set.make [sender]); initiator = sender; startedAt = now};
    (Map.get data.signers sender).lastProposalId <- some data.lastProposalId;
    if data.quorum < 2 then
      let%mutable compute_admin_multisig_187 = ({actions = params; proposalId = data.lastProposalId} self.onApproved) in ()

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {activeProposals = set nat; lastProposalId = nat; metadata = big_map string bytes; proposals = big_map nat {actions = `external (list {actions = bytes; target = address}) + `internal (list (`changeMetadata (pair string (option bytes)) + `changeQuorum nat + `changeSigners (`added (list {address = address; publicKey = key}) + `removed (set address)))); endorsements = set address; initiator = address; startedAt = timestamp}; quorum = nat; signers = map address {lastProposalId = option nat; publicKey = key}}]
      ~storage:[%expr
                 {activeProposals = Set.make([]);
                  lastProposalId = 0;
                  metadata = Map.make [("", bytes "0x697066733a2f2f")];
                  proposals = Map.make [];
                  quorum = 3;
                  signers = Map.make [(address "tz1MNTgxNCJinsG86jZ6AKvAUgR3PFnAznyT", {lastProposalId = None; publicKey = key "edpkusqVU9kM4TLmtngbqxvLQrppRB5F2Z3v5c2pwsMvxNEttFiSnd"}); (address "tz1gJvJtsFuC5i6PvXt8coR8EppsYNiMH12n", {lastProposalId = None; publicKey = key "edpkujeVfAaG7goBNVtUgnVP2NBCRjA6DR6pVNUTCoHN4c3fZUXMWU"}); (address "tz1giQU1Zg8XhcXizVs9GKQDezSMjB1S2urR", {lastProposalId = None; publicKey = key "edpkvBCwcC6XYQMkAkuvayNw9n6UPTXYZhPchTstRJkkXve5ajYzfe"})]}]
      [aggregated_endorsement; aggregated_proposal; cancel_proposal; endorsement; proposal]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())