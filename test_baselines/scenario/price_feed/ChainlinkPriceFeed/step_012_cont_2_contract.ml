open SmartML

module Contract = struct
  let%entry_point administrate params =
    verify (sender = data.admin) ~msg:"Proxy_NotAdmin";
    set_type params (list (`changeActive bool + `changeAdmin address + `changeAggregator address));
    List.iter (fun action ->
      match action with
        | `changeActive changeActive ->
          data.active <- changeActive
        | `changeAdmin changeAdmin ->
          data.admin <- changeAdmin
        | `changeAggregator changeAggregator ->
          data.aggregator <- some changeAggregator

    ) params

  let%entry_point decimals params =
    transfer params (tez 0) (open_some (contract (pair unit address) (open_some data.aggregator ~message:"Proxy_AggregatorNotConfigured") , entry_point='decimals') ~message:"Proxy_InvalidParametersInDecimalsView")

  let%entry_point description params =
    transfer params (tez 0) (open_some (contract (pair unit address) (open_some data.aggregator ~message:"Proxy_AggregatorNotConfigured") , entry_point='description') ~message:"Proxy_InvalidParametersInDescriptionView")

  let%entry_point latestRoundData params =
    transfer params (tez 0) (open_some (contract (pair unit address) (open_some data.aggregator ~message:"Proxy_AggregatorNotConfigured") , entry_point='latestRoundData') ~message:"Proxy_InvalidParametersInLatestRoundDataView")

  let%entry_point version params =
    transfer params (tez 0) (open_some (contract (pair unit address) (open_some data.aggregator ~message:"Proxy_AggregatorNotConfigured") , entry_point='version') ~message:"Proxy_InvalidParametersInVersionView")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {active = bool; admin = address; aggregator = option address}]
      ~storage:[%expr
                 {active = true;
                  admin = address "tz1evBmfWZoPDN38avoRGbjJaLBJUP8AZz6a";
                  aggregator = Some(address "KT1CfuSjCcunNQ5qCCro2Kc74uivnor9d8ba%latestRoundData")}]
      [administrate; decimals; description; latestRoundData; version]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())