import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TNat).layout("a"))
    self.init(a = 1234567)

  @sp.entry_point
  def test_and(self, params):
    self.data.a = self.data.a & params

  @sp.entry_point
  def test_exclusive_or(self, params):
    self.data.a = self.data.a ^ params

  @sp.entry_point
  def test_or(self, params):
    self.data.a = self.data.a | params

  @sp.entry_point
  def test_shift_left(self, params):
    self.data.a = self.data.a << params

  @sp.entry_point
  def test_shift_right(self, params):
    self.data.a = self.data.a >> params

sp.add_compilation_target("test", Contract())