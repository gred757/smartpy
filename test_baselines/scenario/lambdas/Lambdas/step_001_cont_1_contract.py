import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(abcd = sp.TInt, f = sp.TLambda(sp.TIntOrNat, sp.TIntOrNat), fff = sp.TOption(sp.TLambda(sp.TNat, sp.TNat)), ggg = sp.TOption(sp.TIntOrNat), value = sp.TNat).layout(("abcd", ("f", ("fff", ("ggg", "value"))))))
    self.init(abcd = 0,
              f = sp.build_lambda(lambda _x0: _x0 + 1),
              fff = sp.none,
              ggg = sp.some(42),
              value = 0)

  @sp.entry_point
  def abs_test(self, params):
    self.data.abcd = self.abs(params)

  @sp.entry_point
  def comp_test(self):
    self.data.abcd = self.comp(sp.record(f = sp.build_lambda(lambda _x7: _x7 + 3), x = 2))

  @sp.entry_point
  def f(self):
    toto = sp.local("toto", sp.build_lambda(lambda _x8: sp.fst(_x8) + sp.snd(_x8)))
    titi = sp.local("titi", toto.value.apply(5))
    self.data.value = titi.value(8)

  @sp.entry_point
  def flambda(self):
    self.data.value = self.flam(self.flam(15)) + self.square_root(12345)

  @sp.entry_point
  def h(self):
    def f_x9(_x9):
      sp.verify(_x9 >= 0)
      y = sp.local("y", _x9)
      sp.while (y.value * y.value) > _x9:
        y.value = ((_x9 // y.value) + y.value) // 2
      sp.verify(((y.value * y.value) <= _x9) & (_x9 < ((y.value + 1) * (y.value + 1))))
      sp.result(y.value)
    self.data.fff = sp.some(sp.build_lambda(f_x9))

  @sp.entry_point
  def hh(self, params):
    self.data.value = self.data.fff.open_some()(params)

  @sp.entry_point
  def i(self):
    def f_x10(_x10):
      sp.verify(_x10 >= 0)
    ch1 = sp.local("ch1", sp.build_lambda(f_x10))
    def f_x11(_x11):
      sp.verify(_x11 >= 0)
      sp.result(_x11 - 2)
    ch2 = sp.local("ch2", sp.build_lambda(f_x11))
    def f_x12(_x12):
      sp.verify(_x12 >= 0)
      sp.result(True)
    ch3 = sp.local("ch3", sp.build_lambda(f_x12))
    def f_x13(_x13):
      def f_x14(_x14):
        sp.verify(_x14 >= 0)
        sp.result(False)
      ch3b = sp.local("ch3b", sp.build_lambda(f_x14))
      sp.verify(_x13 >= 0)
      sp.result(3 * _x13)
    ch4 = sp.local("ch4", sp.build_lambda(f_x13))
    self.data.value = ch4.value(12)
    compute_lambdas_96 = sp.local("compute_lambdas_96", self.not_pure(sp.unit))
    sp.verify(compute_lambdas_96.value == self.data.value)

  @sp.entry_point
  def operation_creation(self):
    def f_x15(_x15):
      __operations__ = sp.local("__operations__", sp.list([]), sp.TList(sp.TOperation))
      create_contract_lambdas_101 = sp.local("create_contract_lambdas_101", create contract ...)
      sp.operations().push(create_contract_lambdas_101.value.operation)
      create_contract_lambdas_102 = sp.local("create_contract_lambdas_102", create contract ...)
      sp.operations().push(create_contract_lambdas_102.value.operation)
      sp.result(sp.operations())
    f = sp.local("f", sp.build_lambda(f_x15))
    sp.for op in f.value(12345001):
      sp.operations().push(op)
    sp.for op in f.value(12345002):
      sp.operations().push(op)

  @sp.entry_point
  def operation_creation_result(self):
    def f_x16(_x16):
      __operations__ = sp.local("__operations__", sp.list([]), sp.TList(sp.TOperation))
      create_contract_lambdas_110 = sp.local("create_contract_lambdas_110", create contract ...)
      sp.operations().push(create_contract_lambdas_110.value.operation)
      __s1 = sp.local("__s1", 4)
      sp.result((sp.operations(), __s1.value))
    f = sp.local("f", sp.build_lambda(f_x16))
    x = sp.local("x", f.value(12345001))
    y = sp.local("y", f.value(12345002))
    sp.for op in sp.fst(x.value):
      sp.operations().push(op)
    sp.for op in sp.fst(y.value):
      sp.operations().push(op)
    sum = sp.local("sum", sp.snd(x.value) + sp.snd(y.value))

  @sp.private_lambda()
  def abs(_x1):
    sp.if _x1 > 0:
      sp.result(_x1)
    sp.else:
      sp.result(- _x1)

  @sp.private_lambda()
  def comp(_x2):
    sp.result(_x2.f(_x2.x))

  @sp.private_lambda()
  def flam(_x3):
    sp.result(322 * _x3)

  @sp.private_lambda()
  def not_pure(_x4):
    sp.result(self.data.value)

  @sp.private_lambda()
  def oh_no(_x5):
    with sp.set_result_type(sp.TInt):
      sp.if _x5 > 0:
        sp.failwith('too big')
      sp.else:
        sp.failwith('too small')

  @sp.private_lambda()
  def square_root(_x6):
    sp.verify(_x6 >= 0)
    y = sp.local("y", _x6)
    sp.while (y.value * y.value) > _x6:
      y.value = ((_x6 // y.value) + y.value) // 2
    sp.verify(((y.value * y.value) <= _x6) & (_x6 < ((y.value + 1) * (y.value + 1))))
    sp.result(y.value)

sp.add_compilation_target("test", Contract())