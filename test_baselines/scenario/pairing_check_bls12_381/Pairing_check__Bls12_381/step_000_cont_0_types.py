import smartpy as sp

tstorage = sp.TRecord(check = sp.TOption(sp.TBool)).layout("check")
tparameter = sp.TVariant(pairing_check = sp.TList(sp.TPair(sp.TBls12_381_g1, sp.TBls12_381_g2))).layout("pairing_check")
tprivates = { }
tviews = { }
