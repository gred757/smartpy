import smartpy as sp

tstorage = sp.TRecord(counter = sp.TInt).layout("counter")
tparameter = sp.TVariant(compute = sp.TInt, reset = sp.TUnit, run = sp.TInt).layout(("compute", ("reset", "run")))
tprivates = { }
tviews = { }
