import smartpy as sp

tstorage = sp.TRecord(admin = sp.TAddress, escrow = sp.TAddress, job_id = sp.TBytes, next_request_id = sp.TNat, oracle = sp.TAddress, receiver = sp.TOption(sp.TAddress), token = sp.TAddress).layout(("admin", ("escrow", ("job_id", ("next_request_id", ("oracle", ("receiver", "token")))))))
tparameter = sp.TVariant(cancel_value = sp.TRecord(force = sp.TBool).layout("force"), request_value = sp.TRecord(amount = sp.TNat, cancel_timeout_minutes = sp.TInt, fulfill_timeout_minutes = sp.TInt, parameters = sp.TOption(sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))))).layout(("amount", ("cancel_timeout_minutes", ("fulfill_timeout_minutes", "parameters")))), set_receiver = sp.TAddress, setup = sp.TRecord(admin = sp.TAddress, escrow = sp.TAddress, job_id = sp.TBytes, oracle = sp.TAddress, token = sp.TAddress).layout(("admin", ("escrow", ("job_id", ("oracle", "token")))))).layout((("cancel_value", "request_value"), ("set_receiver", "setup")))
tprivates = { }
tviews = { }
