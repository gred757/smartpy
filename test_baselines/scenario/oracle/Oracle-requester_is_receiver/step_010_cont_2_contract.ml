open SmartML

module Contract = struct
  let%entry_point cancel_request params =
    let%mutable compute_oracle_304 = {client = sender; client_request_id = params.client_request_id} in ();
    verify (contains compute_oracle_304 data.locked) ~msg:"EscrowRequestIdUnknownForClient";
    verify (now >= (Map.get data.locked compute_oracle_304).cancel_timeout) ~msg:"EscrowCantCancelBeforeTimeout";
    transfer [{from_ = self_address; txs = [{to_ = sender; token_id = data.token_id; amount = (Map.get data.locked compute_oracle_304).amount}]}] (tez 0) (open_some (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.token , entry_point='transfer'));
    Map.delete data.locked compute_oracle_304;
    if not params.force then
      transfer compute_oracle_304 (tez 0) (open_some (contract {client = address; client_request_id = nat} params.oracle , entry_point='cancel_request') ~message:"EscrowOracleNotFound")

  let%entry_point force_fulfill_request params =
    set_type params {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}};
    request_oracle_324, result_oracle_324 = match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = match_tuple(read_ticket_raw request_oracle_324, "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = match_tuple(read_ticket_raw result_oracle_324, "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    let%mutable compute_oracle_330 = {client = ticket_oracle_325_ticketer; client_request_id = ticket_oracle_325_content.client_request_id} in ();
    verify (contains compute_oracle_330 data.locked) ~msg:"EscrowRequestUnknown";
    verify (ticket_oracle_325_content.fulfill_timeout >= now) ~msg:"EscrowCantFulfillAfterTimeout";
    verify (ticket_oracle_325_content.tag = "OracleRequest") ~msg:"TicketExpectedTag:OracleRequest";
    verify (ticket_oracle_326_content.tag = "OracleResult") ~msg:"TicketExpectedTag:OracleResult";
    verify (ticket_oracle_325_ticketer = ticket_oracle_326_content.client) ~msg:"TicketClientNotMatch";
    verify (ticket_oracle_326_ticketer = ticket_oracle_325_content.oracle) ~msg:"TicketOracleNotMatch";
    verify (ticket_oracle_325_content.client_request_id = ticket_oracle_326_content.client_request_id) ~msg:"TicketClientRequestIdNotMatch";
    verify (sender = ticket_oracle_326_ticketer) ~msg:"EscrowSenderAndTicketerNotMatch";
    transfer [{from_ = self_address; txs = [{to_ = ticket_oracle_325_content.oracle; token_id = data.token_id; amount = (Map.get data.locked compute_oracle_330).amount}]}] (tez 0) (open_some (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.token , entry_point='transfer'));
    Map.delete data.locked compute_oracle_330

  let%entry_point fulfill_request params =
    set_type params {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}};
    request_oracle_324, result_oracle_324 = match_record(params, "request", "result")
    ticket_oracle_325_data, ticket_oracle_325_copy = match_tuple(read_ticket_raw request_oracle_324, "ticket_oracle_325_data", "ticket_oracle_325_copy")
    ticket_oracle_325_ticketer, ticket_oracle_325_content, ticket_oracle_325_amount = match_tuple(ticket_oracle_325_data, "ticket_oracle_325_ticketer", "ticket_oracle_325_content", "ticket_oracle_325_amount")
    ticket_oracle_326_data, ticket_oracle_326_copy = match_tuple(read_ticket_raw result_oracle_324, "ticket_oracle_326_data", "ticket_oracle_326_copy")
    ticket_oracle_326_ticketer, ticket_oracle_326_content, ticket_oracle_326_amount = match_tuple(ticket_oracle_326_data, "ticket_oracle_326_ticketer", "ticket_oracle_326_content", "ticket_oracle_326_amount")
    let%mutable compute_oracle_330i = {client = ticket_oracle_325_ticketer; client_request_id = ticket_oracle_325_content.client_request_id} in ();
    verify (contains compute_oracle_330i data.locked) ~msg:"EscrowRequestUnknown";
    verify (ticket_oracle_325_content.fulfill_timeout >= now) ~msg:"EscrowCantFulfillAfterTimeout";
    verify (ticket_oracle_325_content.tag = "OracleRequest") ~msg:"TicketExpectedTag:OracleRequest";
    verify (ticket_oracle_326_content.tag = "OracleResult") ~msg:"TicketExpectedTag:OracleResult";
    verify (ticket_oracle_325_ticketer = ticket_oracle_326_content.client) ~msg:"TicketClientNotMatch";
    verify (ticket_oracle_326_ticketer = ticket_oracle_325_content.oracle) ~msg:"TicketOracleNotMatch";
    verify (ticket_oracle_325_content.client_request_id = ticket_oracle_326_content.client_request_id) ~msg:"TicketClientRequestIdNotMatch";
    verify (sender = ticket_oracle_326_ticketer) ~msg:"EscrowSenderAndTicketerNotMatch";
    transfer [{from_ = self_address; txs = [{to_ = ticket_oracle_325_content.oracle; token_id = data.token_id; amount = (Map.get data.locked compute_oracle_330i).amount}]}] (tez 0) (open_some (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.token , entry_point='transfer'));
    Map.delete data.locked compute_oracle_330i;
    transfer {request = ticket_oracle_325_copy; result = ticket_oracle_326_copy} (tez 0) (open_some (contract {request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}; result = ticket {client = address; client_request_id = nat; result = `bytes bytes + `int int + `string string; tag = string}} ticket_oracle_325_content.target ) ~message:"EscrowTargetNotFound")

  let%entry_point send_request params =
    set_type params {amount = nat; request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}};
    amount_oracle_273, request_oracle_273 = match_record(params, "amount", "request")
    ticket_oracle_276_data, ticket_oracle_276_copy = match_tuple(read_ticket_raw request_oracle_273, "ticket_oracle_276_data", "ticket_oracle_276_copy")
    ticket_oracle_276_ticketer, ticket_oracle_276_content, ticket_oracle_276_amount = match_tuple(ticket_oracle_276_data, "ticket_oracle_276_ticketer", "ticket_oracle_276_content", "ticket_oracle_276_amount")
    verify (sender = ticket_oracle_276_ticketer) ~msg:"EscrowSenderAndTicketerNotMatch";
    transfer [{from_ = sender; txs = [{to_ = self_address; token_id = data.token_id; amount = amount_oracle_273}]}] (tez 0) (open_some (contract (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}}) data.token , entry_point='transfer'));
    verify (ticket_oracle_276_content.tag = "OracleRequest") ~msg:"TicketExpectedTag:OracleRequest";
    let%mutable compute_oracle_292 = {client = ticket_oracle_276_ticketer; client_request_id = ticket_oracle_276_content.client_request_id} in ();
    verify (not (contains compute_oracle_292 data.locked)) ~msg:"EscrowRequestIdAlreadyKnownForClient";
    Map.set data.locked compute_oracle_292 {amount = amount_oracle_273; cancel_timeout = ticket_oracle_276_content.cancel_timeout};
    transfer {amount = amount_oracle_273; request = ticket_oracle_276_copy} (tez 0) (open_some (contract {amount = nat; request = ticket {cancel_timeout = timestamp; client_request_id = nat; fulfill_timeout = timestamp; job_id = bytes; oracle = address; parameters = option (`bytes bytes + `int int + `string string); tag = string; target = address}} ticket_oracle_276_content.oracle , entry_point='create_request') ~message:"EscrowOracleNotFound")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {locked = big_map {client = address; client_request_id = nat} {amount = nat; cancel_timeout = timestamp}; token = address; token_id = nat}]
      ~storage:[%expr
                 {locked = Map.make [];
                  token = address "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1";
                  token_id = 0}]
      [cancel_request; force_fulfill_request; fulfill_request; send_request]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())