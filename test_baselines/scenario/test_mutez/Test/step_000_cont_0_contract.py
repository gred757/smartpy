import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TUnit)
    self.init()

  @sp.entry_point
  def test(self, params):
    sp.verify(sp.fst(sp.ediv(sp.amount, sp.mutez(1)).open_some(message = sp.unit)) == params)
    sp.verify(sp.mul(params, sp.mutez(1)) == sp.amount)

  @sp.entry_point
  def test_diff(self, params):
    compute_test_mutez_11 = sp.local("compute_test_mutez_11", params.x - params.y)

sp.add_compilation_target("test", Contract())