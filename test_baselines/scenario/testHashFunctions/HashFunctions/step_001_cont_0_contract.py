import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(b2b = sp.TBytes, keccak = sp.TBytes, s256 = sp.TBytes, s512 = sp.TBytes, sha3 = sp.TBytes, tz1 = sp.TKeyHash, v = sp.TBytes).layout(("b2b", ("keccak", ("s256", ("s512", ("sha3", ("tz1", "v"))))))))
    self.init(b2b = sp.bytes('0x'),
              keccak = sp.bytes('0x'),
              s256 = sp.bytes('0x'),
              s512 = sp.bytes('0x'),
              sha3 = sp.bytes('0x'),
              tz1 = sp.key_hash('tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr'),
              v = sp.bytes('0x'))

  @sp.entry_point
  def new_key(self, params):
    self.data.tz1 = sp.hash_key(params)

  @sp.entry_point
  def new_value(self, params):
    self.data.v = params
    self.data.b2b = sp.blake2b(params)
    self.data.s256 = sp.sha256(params)
    self.data.s512 = sp.sha512(params)
    self.data.sha3 = sp.sha3(params)
    self.data.keccak = sp.keccak(params)

sp.add_compilation_target("test", Contract())