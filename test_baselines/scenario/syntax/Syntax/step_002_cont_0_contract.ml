open SmartML

module Contract = struct
  let%entry_point comparisons () =
    verify (data.i <= 123);
    verify ((2 + data.i) = 12);
    verify ((2 + data.i) <> 1234);
    verify ((data.i + 4) <> 123);
    verify ((data.i - 5) < 123);
    verify ((7 - data.i) < 123);
    verify (data.i > 3);
    verify ((4 * data.i) > 3);
    verify ((data.i * 5) > 3);
    verify (data.i >= 3);
    verify (data.i > 3);
    verify (data.i >= 3);
    verify (data.i < 3000);
    verify (data.i <= 3000);
    verify (data.b && true);
    if data.i > 3 then
      verify (data.i > 4)

  let%entry_point iterations () =
    let%mutable x = data.i in ();
    x <- 0;
    x <- 1;
    x <- 2;
    x <- 3;
    x <- 4;
    List.iter (fun i ->
      x <- i
    ) (range 0 5 1);
    data.i <- to_int data.n;
    data.i <- 5;
    while data.i <= 42 do
      data.i <- data.i + 2
    done;
    if data.i <= 123 then
      (
        x <- 12;
        data.i <- data.i + x
      )
    else
      (
        x <- 5;
        data.i <- x
      )

  let%entry_point localVariable () =
    let%mutable x = data.i in ();
    x <- x * 2;
    data.i <- 10;
    data.i <- x

  let%entry_point myMessageName4 () =
    List.iter (fun x ->
      data.i <- data.i + (x.key * x.value)
    ) (Map.items data.m)

  let%entry_point myMessageName5 () =
    List.iter (fun x ->
      data.i <- data.i + (2 * x)
    ) (Map.keys data.m)

  let%entry_point myMessageName6 params =
    List.iter (fun x ->
      data.i <- data.i + (3 * x)
    ) (Map.values data.m);
    Set.remove data.aaa 2;
    Set.add data.aaa 12;
    data.abc <- (some 16) :: data.abc;
    Map.set data.abca 0 (some 16);
    if contains 12 data.aaa then
      Map.set data.m 42 43;
    Set.add data.aaa (open_some (is_nat (- params)))

  let%entry_point someComputations params =
    verify (data.i <= 123);
    data.i <- data.i + params.y;
    data.acb <- params.x;
    data.i <- 100;
    data.i <- data.i - 1

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {aaa = set nat; abc = list (option intOrNat); abca = map intOrNat (option intOrNat); acb = string; b = bool; ddd = list intOrNat; f = bool; h = bytes; i = int; m = map int int; n = nat; pkh = key_hash; s = string; toto = string}]
      ~storage:[%expr
                 {aaa = Set.make([1; 2; 3]);
                  abc = [Some(123); None];
                  abca = Map.make [(0, Some(123)); (1, None)];
                  acb = "toto";
                  b = true;
                  ddd = [0; 1; 2; 3; 4; 5; 6; 7; 8; 9];
                  f = false;
                  h = bytes "0x0000ab112233aa";
                  i = 7;
                  m = Map.make [];
                  n = 123;
                  pkh = key_hash "tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk";
                  s = "abc";
                  toto = "ABC"}]
      [comparisons; iterations; localVariable; myMessageName4; myMessageName5; myMessageName6; someComputations]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())