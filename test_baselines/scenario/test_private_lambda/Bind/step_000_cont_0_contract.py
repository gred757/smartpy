import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y")))
    self.init(x = 12,
              y = 0)

  @sp.private_lambda()
  def f(_x0):
    sp.result(_x0 * 2)

  @sp.private_lambda()
  def g(_x1):
    sp.result(_x1 * 2)

sp.add_compilation_target("test", Contract())