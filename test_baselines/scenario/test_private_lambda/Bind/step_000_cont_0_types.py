import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y"))
tparameter = sp.TUnit
tprivates = { "f": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat), "g": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat) }
tviews = { }
