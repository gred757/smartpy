import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(x = sp.TOption(sp.TIntOrNat)).layout("x"))
    self.init(x = sp.none)

  @sp.entry_point
  def ep(self, params):
    self.data.x = sp.some(params)

sp.add_compilation_target("test", Contract())