import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admin = sp.TAddress, collateral = sp.TMutez, duration = sp.TInt, ledger = sp.TMap(sp.TAddress, sp.TRecord(amount = sp.TMutez, due = sp.TTimestamp).layout(("amount", "due"))), rate = sp.TNat).layout(("admin", ("collateral", ("duration", ("ledger", "rate"))))))
    self.init(admin = sp.address('tz1SfRoaCkrBkXqTzhz67QYVPJAU9Y2g48kq'),
              collateral = sp.tez(0),
              duration = 365,
              ledger = {},
              rate = 700)

  @sp.entry_point
  def collateralize(self, params):
    sp.verify(sp.sender == self.data.admin)
    self.data.collateral += params

  @sp.entry_point
  def delegate(self, params):
    sp.verify(sp.sender == self.data.admin)
    sp.verify(sp.amount == sp.tez(0))
    sp.set_delegate(params)

  @sp.entry_point
  def deposit(self, params):
    sp.verify(self.data.rate >= params.rate)
    sp.verify(self.data.duration <= params.duration)
    sp.verify(~ (self.data.ledger.contains(sp.sender)))
    self.data.collateral -= sp.split_tokens(sp.amount, self.data.rate, 10000)
    sp.verify(self.data.collateral >= sp.tez(0))
    self.data.ledger[sp.sender] = sp.record(amount = sp.amount + sp.split_tokens(sp.amount, self.data.rate, 10000), due = sp.add_seconds(sp.now, (self.data.duration * 24) * 3600))

  @sp.entry_point
  def setOffer(self, params):
    sp.verify(sp.sender == self.data.admin)
    self.data.rate = params.rate
    self.data.duration = params.duration

  @sp.entry_point
  def uncollateralize(self, params):
    sp.verify(sp.sender == self.data.admin)
    self.data.collateral -= params
    sp.verify(self.data.collateral >= sp.tez(0))

  @sp.entry_point
  def withdraw(self):
    sp.verify(sp.now >= self.data.ledger[sp.sender].due)
    sp.send(sp.sender, self.data.ledger[sp.sender].amount)
    del self.data.ledger[sp.sender]

sp.add_compilation_target("test", Contract())