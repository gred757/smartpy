import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TAddress).layout("a"))
    self.init(a = sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'))

  @sp.entry_point
  def ep3(self, params):
    sp.set_type(params, sp.TBool)
    sp.set_type(sp.self_entry_point('ep4'), sp.TContract(sp.TTimestamp))
    sp.transfer(sp.self_entry_point('ep4'), sp.tez(0), sp.contract(sp.TContract(sp.TTimestamp), self.data.a, entry_point='ep2').open_some())

  @sp.entry_point
  def ep4(self, params):
    sp.set_type(params, sp.TTimestamp)

sp.add_compilation_target("test", Contract())