open SmartML

module Contract = struct
  let%entry_point play params =
    set_type params.move_nb nat;
    set_type params.move_data {i = int; j = int};
    if data.current.player = 1 then
      verify (sender = data.constants.player1.addr) ~msg:"Game_WrongPlayer"
    else
      verify (sender = data.constants.player2.addr) ~msg:"Game_WrongPlayer";
    verify (is_variant "None" data.current.outcome);
    verify (data.current.move_nb = params.move_nb);
    let%mutable compute_game_tester_40i = ({move_data = params.move_data; move_nb = data.current.move_nb; player = data.current.player; state = data.state} data.apply_) in ();
    match_pair_game_tester_50_fst, match_pair_game_tester_50_snd = match_tuple(compute_game_tester_40i, "match_pair_game_tester_50_fst", "match_pair_game_tester_50_snd")
    set_type match_pair_game_tester_50_snd (option bounded(["draw", "player_1_won", "player_2_won"], t=string));
    match match_pair_game_tester_50_snd with
      | `Some Some ->
        data.current.outcome <- some (unbound Some)
      | `None None ->
        data.current.outcome <- None
;
    data.current.move_nb <- data.current.move_nb + 1;
    data.current.player <- 3 - data.current.player;
    data.state <- match_pair_game_tester_50_fst

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {apply_ = lambda {move_data = {i = int; j = int}; move_nb = nat; player = int; state = map int (map int int)} (pair (map int (map int int)) (option bounded(["draw", "player_1_won", "player_2_won"], t=string))); constants = {channel_id = bytes; game_nonce = string; loser = mutez; model_id = bytes; player1 = {addr = address; pk = key}; player2 = {addr = address; pk = key}; winner = mutez}; current = {move_nb = nat; outcome = option string; player = int}; state = map int (map int int)}]
      ~storage:[%expr
                 {apply_ = lambda(lambda {move_data = {i = int; j = int}; move_nb = nat; player = int; state = map int (map int int)} (pair (map int (map int int)) (option bounded(["draw", "player_1_won", "player_2_won"], t=string))));
                  constants = {channel_id = bytes "0x01"; game_nonce = ""; loser = tez 0; model_id = bytes "0x"; player1 = {addr = address "tz0Fakeplayer1"; pk = key "edpkFakeplayer1"}; player2 = {addr = address "tz0Fakeplayer2"; pk = key "edpkFakeplayer2"}; winner = tez 0};
                  current = {move_nb = 0; outcome = None; player = 1};
                  state = Map.make [(0, Map.make [(0, 0); (1, 0); (2, 0)]); (1, Map.make [(0, 0); (1, 0); (2, 0)]); (2, Map.make [(0, 0); (1, 0); (2, 0)])]}]
      [play]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())