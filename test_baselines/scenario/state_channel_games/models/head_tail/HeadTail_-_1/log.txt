Comment...
 h1: HeadTail
Comment...
 h2: Contract
Creating contract KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1
 -> (Pair { DUP; GET 6; NONE string; PUSH int 1; DUP 4; GET 5; COMPARE; EQ; IF { SWAP; DUP; DUG 2; CAR; IF_NONE { SWAP; DUP 3; CAR; IF_LEFT {} { PUSH int 16; FAILWITH }; SOME; UPDATE 1; SWAP } { DROP 2; SWAP; DUP; DUG 2; CAR; IF_LEFT { PUSH int 18; FAILWITH } {}; SWAP; DUP; DUG 2; CAR; IF_NONE { PUSH int 19; FAILWITH } {}; SWAP; DUP; DUG 2; PACK; BLAKE2B; COMPARE; EQ; IF {} { PUSH string "Invalid Secret"; FAILWITH }; SOME; UPDATE 3; PUSH nat 0; PUSH nat 2; DUP 3; GET 4; IF_NONE { PUSH int 21; FAILWITH } {}; DUP 4; GET 3; IF_NONE { PUSH int 21; FAILWITH } {}; ADD; EDIV; IF_NONE { PUSH int 22; FAILWITH } { CDR }; COMPARE; EQ; IF { PUSH (option string) (Some "player_1_won") } { PUSH (option string) (Some "player_2_won") } } } {}; PUSH int 2; DUP 4; GET 5; COMPARE; EQ; IF { SWAP; DIG 2; CAR; IF_LEFT { PUSH int 18; FAILWITH } {}; SOME; UPDATE 4; SWAP } { DIG 2; DROP }; SWAP; PAIR } (Pair (Pair 0x01 (Pair "" (Pair 0 (Pair 0x (Pair (Pair "tz0Fakeplayer1" "edpkFakeplayer1") (Pair (Pair "tz0Fakeplayer2" "edpkFakeplayer2") 0)))))) (Pair (Pair 0 (Pair None 1)) (Pair None (Pair None None)))))
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_storage.tz 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_storage.json 149
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_sizes.csv 2
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_storage.py 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_types.py 7
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_contract.tz 165
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_contract.json 309
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_contract.py 34
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_002_cont_0_contract.ml 41
Comment...
 h2: A sequence of interactions with a winner
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_004_cont_0_params.py 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_004_cont_0_params.tz 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_004_cont_0_params.json 1
Executing play(sp.record(move_data = variant('hash', sp.resolve(sp.blake2b(sp.pack(1234)))), move_nb = 0))...
 -> (Pair { DUP; GET 6; NONE string; PUSH int 1; DUP 4; GET 5; COMPARE; EQ; IF { SWAP; DUP; DUG 2; CAR; IF_NONE { SWAP; DUP 3; CAR; IF_LEFT {} { PUSH int 16; FAILWITH }; SOME; UPDATE 1; SWAP } { DROP 2; SWAP; DUP; DUG 2; CAR; IF_LEFT { PUSH int 18; FAILWITH } {}; SWAP; DUP; DUG 2; CAR; IF_NONE { PUSH int 19; FAILWITH } {}; SWAP; DUP; DUG 2; PACK; BLAKE2B; COMPARE; EQ; IF {} { PUSH string "Invalid Secret"; FAILWITH }; SOME; UPDATE 3; PUSH nat 0; PUSH nat 2; DUP 3; GET 4; IF_NONE { PUSH int 21; FAILWITH } {}; DUP 4; GET 3; IF_NONE { PUSH int 21; FAILWITH } {}; ADD; EDIV; IF_NONE { PUSH int 22; FAILWITH } { CDR }; COMPARE; EQ; IF { PUSH (option string) (Some "player_1_won") } { PUSH (option string) (Some "player_2_won") } } } {}; PUSH int 2; DUP 4; GET 5; COMPARE; EQ; IF { SWAP; DIG 2; CAR; IF_LEFT { PUSH int 18; FAILWITH } {}; SOME; UPDATE 4; SWAP } { DIG 2; DROP }; SWAP; PAIR } (Pair (Pair 0x01 (Pair "" (Pair 0 (Pair 0x (Pair (Pair "tz0Fakeplayer1" "edpkFakeplayer1") (Pair (Pair "tz0Fakeplayer2" "edpkFakeplayer2") 0)))))) (Pair (Pair 1 (Pair None 2)) (Pair (Some 0x64fabcca0db8d1beeaed6aa9a387fbed25054c08783d649f886a87873d47b00f) (Pair None None)))))
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_005_cont_0_params.py 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_005_cont_0_params.tz 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_005_cont_0_params.json 1
Executing play(sp.record(move_data = variant('revealed', 4568), move_nb = 1))...
 -> (Pair { DUP; GET 6; NONE string; PUSH int 1; DUP 4; GET 5; COMPARE; EQ; IF { SWAP; DUP; DUG 2; CAR; IF_NONE { SWAP; DUP 3; CAR; IF_LEFT {} { PUSH int 16; FAILWITH }; SOME; UPDATE 1; SWAP } { DROP 2; SWAP; DUP; DUG 2; CAR; IF_LEFT { PUSH int 18; FAILWITH } {}; SWAP; DUP; DUG 2; CAR; IF_NONE { PUSH int 19; FAILWITH } {}; SWAP; DUP; DUG 2; PACK; BLAKE2B; COMPARE; EQ; IF {} { PUSH string "Invalid Secret"; FAILWITH }; SOME; UPDATE 3; PUSH nat 0; PUSH nat 2; DUP 3; GET 4; IF_NONE { PUSH int 21; FAILWITH } {}; DUP 4; GET 3; IF_NONE { PUSH int 21; FAILWITH } {}; ADD; EDIV; IF_NONE { PUSH int 22; FAILWITH } { CDR }; COMPARE; EQ; IF { PUSH (option string) (Some "player_1_won") } { PUSH (option string) (Some "player_2_won") } } } {}; PUSH int 2; DUP 4; GET 5; COMPARE; EQ; IF { SWAP; DIG 2; CAR; IF_LEFT { PUSH int 18; FAILWITH } {}; SOME; UPDATE 4; SWAP } { DIG 2; DROP }; SWAP; PAIR } (Pair (Pair 0x01 (Pair "" (Pair 0 (Pair 0x (Pair (Pair "tz0Fakeplayer1" "edpkFakeplayer1") (Pair (Pair "tz0Fakeplayer2" "edpkFakeplayer2") 0)))))) (Pair (Pair 2 (Pair None 1)) (Pair (Some 0x64fabcca0db8d1beeaed6aa9a387fbed25054c08783d649f886a87873d47b00f) (Pair None (Some 4568))))))
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_006_cont_0_params.py 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_006_cont_0_params.tz 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_006_cont_0_params.json 1
Executing play(sp.record(move_data = variant('revealed', 42), move_nb = 2))...
 -> --- Expected failure in transaction --- Wrong condition: (sp.blake2b(sp.pack(compute_head_tail_18.value)) == state.value.hash1.open_some() : sp.TBool) (python/templates/state_channel_games/models/head_tail.py, line 19)
Message: 'Invalid Secret'
 (python/templates/state_channel_games/models/head_tail.py, line 19)
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_007_cont_0_params.py 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_007_cont_0_params.tz 1
 => test_baselines/scenario/state_channel_games/models/head_tail/HeadTail_-_1/step_007_cont_0_params.json 1
Executing play(sp.record(move_data = variant('revealed', 1234), move_nb = 2))...
 -> (Pair { DUP; GET 6; NONE string; PUSH int 1; DUP 4; GET 5; COMPARE; EQ; IF { SWAP; DUP; DUG 2; CAR; IF_NONE { SWAP; DUP 3; CAR; IF_LEFT {} { PUSH int 16; FAILWITH }; SOME; UPDATE 1; SWAP } { DROP 2; SWAP; DUP; DUG 2; CAR; IF_LEFT { PUSH int 18; FAILWITH } {}; SWAP; DUP; DUG 2; CAR; IF_NONE { PUSH int 19; FAILWITH } {}; SWAP; DUP; DUG 2; PACK; BLAKE2B; COMPARE; EQ; IF {} { PUSH string "Invalid Secret"; FAILWITH }; SOME; UPDATE 3; PUSH nat 0; PUSH nat 2; DUP 3; GET 4; IF_NONE { PUSH int 21; FAILWITH } {}; DUP 4; GET 3; IF_NONE { PUSH int 21; FAILWITH } {}; ADD; EDIV; IF_NONE { PUSH int 22; FAILWITH } { CDR }; COMPARE; EQ; IF { PUSH (option string) (Some "player_1_won") } { PUSH (option string) (Some "player_2_won") } } } {}; PUSH int 2; DUP 4; GET 5; COMPARE; EQ; IF { SWAP; DIG 2; CAR; IF_LEFT { PUSH int 18; FAILWITH } {}; SOME; UPDATE 4; SWAP } { DIG 2; DROP }; SWAP; PAIR } (Pair (Pair 0x01 (Pair "" (Pair 0 (Pair 0x (Pair (Pair "tz0Fakeplayer1" "edpkFakeplayer1") (Pair (Pair "tz0Fakeplayer2" "edpkFakeplayer2") 0)))))) (Pair (Pair 3 (Pair (Some "player_1_won") 2)) (Pair (Some 0x64fabcca0db8d1beeaed6aa9a387fbed25054c08783d649f886a87873d47b00f) (Pair (Some 1234) (Some 4568))))))
Verifying sp.contract_data(0).current.outcome == sp.some('player_1_won')...
 OK
