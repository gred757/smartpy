import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(counter = sp.TIntOrNat, steps = sp.TList(sp.TInt)).layout(("counter", "steps")))
    self.init(counter = 0,
              steps = [])

  @sp.entry_point
  def compute(self, params):
    self.data.counter = 0
    self.data.steps = sp.list([])
    sp.transfer(params, sp.tez(0), sp.self_entry_point('run'))

  @sp.entry_point
  def run(self, params):
    self.data.steps.push(params)
    sp.if params > 1:
      sp.transfer(params - 2, sp.tez(0), sp.self_entry_point('run'))
      sp.transfer(params - 1, sp.tez(0), sp.self_entry_point('run'))
    sp.else:
      self.data.counter += 1

sp.add_compilation_target("test", Contract())