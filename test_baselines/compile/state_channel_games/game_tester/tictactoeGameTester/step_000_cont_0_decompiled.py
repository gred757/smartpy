import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(apply_ = sp.TLambda(sp.TPair(sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TNat), sp.TPair(sp.TInt, sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)))), sp.TPair(sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)), sp.TOption(sp.TString))), constants = sp.TPair(sp.TPair(sp.TBytes, sp.TPair(sp.TString, sp.TMutez)), sp.TPair(sp.TPair(sp.TBytes, sp.TPair(sp.TAddress, sp.TKey)), sp.TPair(sp.TPair(sp.TAddress, sp.TKey), sp.TMutez))), current = sp.TPair(sp.TNat, sp.TPair(sp.TOption(sp.TString), sp.TInt)), state = sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt))).layout((("apply_", "constants"), ("current", "state"))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TInt, sp.TInt), sp.TNat))
    x48 = sp.bind_block("x48"):
    with x48:
      sp.if sp.snd(sp.snd(self.data.current)) == 1:
        sp.verify(sp.sender == sp.fst(sp.snd(sp.fst(sp.snd(self.data.constants)))), 'Game_WrongPlayer')
        sp.result((params, ((self.data.apply_, self.data.constants), (self.data.current, self.data.state))))
      sp.else:
        sp.verify(sp.sender == sp.fst(sp.fst(sp.snd(sp.snd(self.data.constants)))), 'Game_WrongPlayer')
        sp.result((params, ((self.data.apply_, self.data.constants), (self.data.current, self.data.state))))
    s44 = sp.local("s44", sp.fst(x48.value))
    s45 = sp.local("s45", sp.snd(x48.value))
    with sp.fst(sp.snd(sp.fst(sp.snd(s45.value)))).match_cases() as arg:
      with arg.match('None') as _l49:
        sp.verify(sp.fst(sp.fst(sp.snd(s45.value))) == sp.snd(s44.value), 'WrongCondition: self.data.current.move_nb == params.move_nb')
        x111 = sp.local("x111", sp.fst(sp.fst(s45.value))(((sp.fst(s44.value), sp.fst(sp.fst(sp.snd(s45.value)))), (sp.snd(sp.snd(sp.fst(sp.snd(s45.value)))), sp.snd(sp.snd(s45.value))))))
        with sp.snd(x111.value).match_cases() as arg:
          with arg.match('None') as _l78:
    def ferror(error):
      sp.failwith('[Error: to_cmd: Unpair(2, s45)]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))
          with arg.match('Some') as s123:
    def ferror(error):
      sp.failwith('[Error: to_cmd: Unpair(2, s45)]')
            sp.failwith(sp.build_lambda(ferror)(sp.unit))

      with arg.match('Some') as _r50:
        sp.failwith('WrongCondition: self.data.current.outcome.is_variant('None')')


sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
