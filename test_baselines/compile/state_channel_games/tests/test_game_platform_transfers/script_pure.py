import smartpy as sp

gp         = sp.io.import_template("state_channel_games/game_platform.py")
lgr        = sp.io.import_template("state_channel_games/ledger.py")
Transfer   = sp.io.import_template("state_channel_games/models/transfer.py").Transfer
model_wrap = sp.io.import_template("state_channel_games/model_wrap.py")
types      = sp.io.import_template("state_channel_games/types.py").Types()
FA2        = sp.io.import_template("FA2.py")


def transfer_settlements(
        p1_to_p2 = {}, p2_to_p1 = {},
        p1_to_p1 = {}, p2_to_p2 = {},
        p0_to_p1 = {}, p0_to_p2 = {}
    ):
    transfers = []
    if len(p1_to_p2) > 0:
        transfers.append(gp.transfer(1, 2, p1_to_p2))
    if len(p2_to_p1) > 0:
        transfers.append(gp.transfer(2, 1, p2_to_p1))
    if len(p1_to_p1) > 0:
        transfers.append(gp.transfer(1, 1, p1_to_p1))
    if len(p2_to_p2) > 0:
        transfers.append(gp.transfer(2, 2, p2_to_p2))
    if len(p0_to_p1) > 0:
        transfers.append(gp.transfer(0, 1, p0_to_p1))
    if len(p0_to_p2) > 0:
        transfers.append(gp.transfer(0, 2, p0_to_p2))
    settlements = sp.map({
        sp.variant("player_double_played", 1)     : transfers,
        sp.variant("player_double_played", 2)     : transfers,
        sp.variant("player_inactive",      1)     : transfers,
        sp.variant("player_inactive",      2)     : transfers,
        sp.variant("game_finished", "transferred"): transfers,
        sp.variant("game_aborted", sp.unit)       : []
    })
    bonds = {1: p1_to_p2, 2: p2_to_p1}
    sp.set_type_expr(bonds, gp.types.t_game_bonds)
    sp.set_type_expr(settlements, gp.types.t_constants)
    return settlements, bonds

def make_signatures(p1, p2, x):
    sig1 = sp.make_signature(p1.secret_key, x)
    sig2 = sp.make_signature(p2.secret_key, x)
    return sp.map({p1.public_key: sig1, p2.public_key: sig2})

if "templates" not in __name__:
    admin = sp.test_account('admin')
    player1 = sp.test_account('player1')
    player2 = sp.test_account('player2')
    players = {player1.address: player1.public_key, player2.address: player2.public_key}

    transfer = Transfer()
    play_delay = 3600 * 24

    # Ledger
    ledger_admin = sp.test_account('ledger_admin')
    ledger_config = FA2.FA2_config(single_asset = False)
    ledger_metadata = sp.utils.metadata_of_url("https://example.com")
    wXTZ_metadata = FA2.FA2.make_metadata(
        name     = "Wrapped XTZ",
        symbol   = "WXTZ",
        decimals = 6,
    )
    wXTZ_permissions = lgr.build_token_permissions({
            "type"            : "NATIVE",
            "mint_cost"       : sp.mutez(1),
            "mint_permissions": sp.variant("allow_everyone", sp.unit)
    })

    free_metadata = FA2.FA2.make_metadata(
        name     = "Free tokens",
        symbol   = "FREE",
        decimals = 6,
    )
    free_permissions = lgr.build_token_permissions({
        "type"            : "NATIVE",
        "mint_cost"       : sp.mutez(0),
        "mint_permissions": sp.variant("allow_everyone", sp.unit)
    })

    reputation_metadata = FA2.FA2.make_metadata(
        name     = "Reputation",
        symbol   = "R",
        decimals = 0,
    )

    def new_game_sigs(constants, params = sp.unit):
        new_game = gp.action_new_game(constants, sp.pack(params))
        return make_signatures(player1, player2, new_game)

    def play_transfer(game_id):
        new_current = sp.record(move_nb = 1, player = 2, outcome = sp.some(sp.variant("final", sp.variant("game_finished", "transferred"))))
        signatures = make_signatures(player1, player2, gp.action_play(game_id, new_current, sp.pack(sp.unit), sp.pack(sp.unit)))
        return sp.record(game_id = game_id, new_current = new_current, new_state = sp.pack(sp.unit), move_data = sp.pack(sp.unit), signatures = signatures)

    @sp.add_test(name="Simple transfers")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        ###########
        #  Test 1 #
        ###########
        sc.h2("Simple transfer")
        game_num = 1
        sc.h3("Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {0: 10}")
        ledger.mint(token_id = 0, amount = 10, address = player1.address).run(sender = player1, amount = sp.mutez(10))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {0: 10}).run(sender = player1)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(0))
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(0))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        sc.p("P2 received 10 tokens 0")

        ############
        #  Test 2  #
        ############
        sc.h2("Transfer back")
        game_num += 1
        sc.h3("P1 and P2 inverted. Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P2 Instanciate new game : Transfer")
        constants = sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player2.address, 2:player1.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements)
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player2)

        sc.h3("(Onchain) play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player2)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(0))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        c1.game_settle(game_id).run(sender = player1)
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(0))
        sc.p("P2 (player 1 of previous game) received 10 tokens 0")

        ###########
        #  Test 3 #
        ###########
        sc.h2("Multiple transfers")
        game_num += 1
        sc.h3("Admin setup free tokens")
        ledger.set_token_info(token_id = 1, token_info = free_metadata).run(sender = ledger_admin)
        ledger.set_token_info(token_id = 2, token_info = free_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 1, token_permissions = free_permissions).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 2, token_permissions = free_permissions).run(sender = ledger_admin)

        sc.h3("Player 1 transfers 10 token_1 to Player 2 and 10 token_2 ")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:10, 2:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {1: 10, 2: 10}")
        ledger.mint(token_id = 1, amount = 10, address = player1.address).run(sender = player1)
        ledger.mint(token_id = 2, amount = 10, address = player1.address).run(sender = player1)
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {1: 10, 2 : 10}).run(sender = player1)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[1] == 10)
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[2] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(1))
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(2))
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(1))
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(2))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[1] == 10)
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[2] == 10)
        sc.p("P1 received 10 token_1 and 10 token_2")

        ###########
        #  Test 4 #
        ###########
        sc.h2("Self transfer")
        game_num += 1
        sc.h3("Player 1 transfers 5 tokens to himself")
        settlements, bonds = transfer_settlements(p1_to_p1 = {0:5})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.p("P1 tokens didn't changed")

        ###########
        #  Test 5 #
        ###########
        sc.h2("Transfer in both direction")
        game_num += 1
        sc.h3("Player 1 transfers 5 tokens 0 to player 2, 2 tokens to himself, Player 2 transfer 4 tokens 0 to player 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:5}, p1_to_p1 = {0:2}, p2_to_p1 = {0: 4})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("Player 2 push {0: 10}")
        ledger.mint(token_id = 0, amount = 10, address = player2.address).run(sender = player1, amount = sp.mutez(10))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player2.address, bonds = {0: 10}).run(sender = player2)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Player 2 settle the game")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        c1.game_settle(game_id).run(sender = player2)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 9)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[0] == 11)
        sc.p("P1 lost 1 token, P2 won 1 token")

        ###########
        #  Test 6 #
        ###########
        sc.h2("Simple withdraw")
        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1)
        sc.p("Player 1 channel bonds")
        sc.show(c1.data.channels[channel_id].players[player1.address])
        sc.h3("Player 2 request leave empty challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([])).run(sender = player2)
        sc.h3("Player 1 finalize withdraw request")
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 9)
        c1.withdraw_finalize(channel_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 4)
        sc.verify(lgr.fa2_balance(player1.address, ledger, token_id = 0) == 5)

    @sp.add_test(name="Withdraw challenges - set_outcome")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Withdraw challenges")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 push {0: 10}")
        ledger.mint(token_id = 0, amount = 10, address = player1.address).run(sender = player1, amount = sp.mutez(10))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {0: 10}).run(sender = player1)

        sc.h2("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:5})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        ###########
        #  Test 1 #
        ###########
        sc.h2("Challenged withdraw")
        sc.h3("Player 1 request withdraw 8 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 8})).run(sender = player1)
        sc.h3("Player 2 challenge by pushing a game_id")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id])).run(sender = player2)
        sc.h3("Player 1 can't finalize withdraw")
        c1.withdraw_finalize(channel_id).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")
        sc.h3("Player 1 proposes an abort outcome for running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id, gp.abort, sp.timestamp(3600 * 24))
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.game_set_outcome(game_id = game_id, outcome = gp.abort, timeout = sp.timestamp(3600 * 24), signatures = outcome_sigs).run(sender = player1)
        c1.game_settle(game_id).run(sender = player1)
        sc.h3("Player 1 resolve challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id])).run(sender = player1)
        sc.h3("Player 1 finalize withdraw request")
        c1.withdraw_finalize(channel_id).run(sender = player1)

        ###########
        #  Test 2 #
        ###########
        sc.h2("Multi challenged withdraw")
        sc.h3("Admin setup free tokens")
        ledger.set_token_info(token_id = 1, token_info = free_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 1, token_permissions = free_permissions).run(sender = ledger_admin)
        sc.p("3 running transfers: 1 with low money, 2 with high money.<br/>\
              Player 1 resolves game 1 but it's not sufficient to withdraw and then resolve game 2, that become sufficient to withdraw")
        sc.h2("Player 1 push {1: 100}")
        ledger.mint(token_id = 1, amount = 100, address = player1.address).run(sender = player1)
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {1: 100}).run(sender = player1)
        sc.h2("Instanciate new transfers games")
        sc.h3("Game 1: 5 tokens 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:5})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_1 = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h3("Game 2: 40 tokens 1")
        settlements, bonds = transfer_settlements(p1_to_p2 = {1:40})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_2", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_2 = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.h3("Game 3: 40 tokens 1")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game_3", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id_3 = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 request withdraw 50 tokens 1")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({1: 50})).run(sender = player1)
        sc.h3("Player 2 challenge by pushing 3 game ids")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id_1, game_id_2, game_id_3])).run(sender = player2)
        sc.h3("Player 1 can't finalize withdraw")
        c1.withdraw_finalize(channel_id).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")

        sc.h3("Player 1 proposes an abort outcome for game_id_1 running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id_1, gp.abort, sp.timestamp(3600 * 24))
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.game_set_outcome(game_id = game_id_1, outcome = gp.abort, timeout = sp.timestamp(3600 * 24), signatures = outcome_sigs).run(sender = player1)
        c1.game_settle(game_id_1).run(sender = player1)
        sc.h3("Player 1 tries to resolve challenge but still doesn't have resolved enough challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id_1])).run(sender = player1)
        c1.withdraw_finalize(channel_id).run(sender = player1, valid = False, exception = "Platform_TokenChallenged")

        sc.h3("Player 1 proposes an abort outcome for game_id_2 running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id_2, gp.abort, sp.timestamp(3600 * 24))
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.game_set_outcome(game_id = game_id_2, outcome = gp.abort, timeout = sp.timestamp(3600 * 24), signatures = outcome_sigs).run(sender = player1)
        c1.game_settle(game_id_2).run(sender = player1)
        sc.h3("Player 1 resolves challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id_2])).run(sender = player1)
        sc.h3("Player 1 finalizes withdraw request")
        c1.withdraw_finalize(channel_id).run(sender = player1)

    @sp.add_test(name="Withdraw timeout and not enough tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Expected withdraw errors")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 push {0: 10}")
        ledger.mint(token_id = 0, amount = 10, address = player1.address).run(sender = player1, amount = sp.mutez(10))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {0: 10}).run(sender = player1)

        sc.h2("Not enough tokens")
        sc.h3("Player 1 request withdraw 50 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 50})).run(sender = player1)
        sc.h3("Player 1 fails to withdraw 50 tokens")
        c1.withdraw_finalize(channel_id).run(sender = player1, now = sp.timestamp(3600 * 24 + 1), valid = False, exception = "Platform_NotEnoughTokens")

        sc.h3("Player 1 can't open another request")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1, now = sp.timestamp(3600 * 24 + 1), valid = False, exception = "Platform_AnotherWithdrawIsRunning")
        sc.h3("Player 1 cancels its request")
        c1.withdraw_cancel(channel_id).run(sender = player1, now = sp.timestamp(3600 * 24 + 1))

        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1, now = sp.timestamp(3600 * 24 + 1))
        sc.h3("Player 1 can't finalize before timeout")
        c1.withdraw_finalize(channel_id).run(sender = player1, valid = False, exception = "Platform_ChallengeDelayNotOver")
        sc.h3("Player 1 finalize after timeout")
        c1.withdraw_finalize(channel_id).run(sender = player1, now = sp.timestamp(3600 * 24 * 2 + 2))
        sc.verify(gp.player_bonds(c1, channel_id, player1.address)[0] == 5)
        sc.verify(lgr.fa2_balance(player1.address, ledger, token_id = 0) == 5)

    @sp.add_test(name="Settled game Challenge")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Settled game Challenge")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Player 1 and 2 push {0: 10}")
        ledger.mint(token_id = 0, amount = 10, address = player1.address).run(sender = player1, amount = sp.mutez(10))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {0: 10}).run(sender = player1)

        sc.h2("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:100})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 proposes an abort outcome for game_id running transfer and Player 2 accepts")
        proposed_outcome = gp.action_new_outcome(game_id, gp.abort, sp.timestamp(3600 * 24))
        outcome_sigs = make_signatures(player1, player2, proposed_outcome)
        c1.game_set_outcome(game_id = game_id, outcome = gp.abort, timeout = sp.timestamp(3600 * 24), signatures = outcome_sigs).run(sender = player1)
        c1.game_settle(game_id).run(sender = player1)

        sc.h2("Player 2 tries to challenge with a settled game")
        sc.h3("Player 1 request withdraw 5 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({0: 5})).run(sender = player1)
        sc.h3("Player 2 fails to push settled game id as a challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([game_id])).run(
            sender = player2, valid = False, exception = "Platform_GameSettled")

    @sp.add_test(name="Expected errors")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Expected transfer errors")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h3("P1 Instanciate new game : Transfer")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 plays")
        c1.game_play(play_transfer(game_id)).run(sender = player1)

        sc.h2("(fail) Settle: Don't own token")
        error_message = sp.pair("Platform_NotEnoughToken:", sp.record(sender = player1.address, token = 0, amount = 10))
        sp.set_type_expr(error_message, sp.TPair(sp.TString, sp.TRecord(sender = sp.TAddress, token = sp.TNat, amount = sp.TNat)))
        c1.game_settle(game_id).run(sender = player1, valid = False, exception = error_message)

        sc.h2("Push 5 tokens 0")
        ledger.mint(token_id = 0, amount = 5, address = player1.address).run(sender = player1, amount = sp.mutez(5))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {0: 5}).run(sender = player1)

        sc.h2("fail) Settle: No enough tokens")
        error_message = sp.pair("Platform_NotEnoughToken:", sp.record(sender = player1.address, token = 0, amount = 10))
        sp.set_type_expr(error_message, sp.TPair(sp.TString, sp.TRecord(sender = sp.TAddress, token = sp.TNat, amount = sp.TNat, bond = gp.types.t_token_map)))
        c1.game_settle(game_id).run(sender = player1, valid = False, exception = error_message)

        sc.h2("Not enought tez for bonds")
        ledger.mint(token_id = 0, amount = 5, address = player1.address).run(sender = player1, amount = sp.mutez(0),
            valid = False, exception = sp.pair("FA2_WRONG_AMOUNT_expected", sp.utils.nat_to_mutez(5))
        )

        sc.h2("Too much tez for bonds")
        ledger.mint(token_id = 0, amount = 5, address = player1.address).run(sender = player1, amount = sp.mutez(20),
            valid = False, exception = sp.pair("FA2_WRONG_AMOUNT_expected", sp.utils.nat_to_mutez(5))
        )

        sc.h2("Trying to mint not configured token")
        ledger.mint(token_id = 404, amount = 5, address = player1.address).run(sender = player1, amount = sp.mutez(20),
            valid = False, exception = sp.pair("Ledger_token_unpermitted", 404)
        )

        sc.h2("Trying to push bonds for not configured token")
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {404: 5}).run(
            sender = player1, amount = sp.mutez(0), valid = False, exception = sp.pair("Ledger_token_unpermitted", 404)
        )

    @sp.add_test(name="Settlements verification")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Settlements verification")
        sc.h2("Contract")
        sc.h3("GamePlatform")
        ledger_address = sp.address("KT1_LEDGER_ADDRESS")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger_address)
        sc += c1

        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600*24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h3("Missing game_finished: transferd in settlements")
        settlements = sp.map({
            sp.variant("player_double_played", 1)     : [],
            sp.variant("player_double_played", 2)     : [],
            sp.variant("player_inactive",      1)     : [],
            sp.variant("player_inactive",      2)     : [],
            sp.variant("game_aborted", sp.unit)       : []
        })
        bonds = {}
        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(
            constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)
        ).run(sender = player1, valid = False, exception = sp.pair("Platform_MissingSettlement", sp.variant("game_finished", "transferred")))

        sc.h3("Missing game_aborted in settlements")
        settlements = sp.map({
            sp.variant("player_double_played", 1)     : [],
            sp.variant("player_double_played", 2)     : [],
            sp.variant("player_inactive",      1)     : [],
            sp.variant("player_inactive",      2)     : [],
            sp.variant("game_finished", "transferred"): [],
        })

        settlements = sc.compute(settlements)
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                game_nonce = "game1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(
            constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)
        ).run(sender = player1, valid = False, exception = sp.pair("Platform_MissingSettlement", sp.variant("game_aborted", sp.unit)))

    @sp.add_test(name="Platform transfers tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("New token: Reputation")
        reputation_permissions = lgr.build_token_permissions({
            "type"            : "NATIVE",
            "mint_cost"       : sp.mutez(0),
            "mint_permissions": sp.variant("allow_only", sp.set([c1.address]))
        })
        ledger.set_token_info(token_id = 42, token_info = reputation_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 42, token_permissions = reputation_permissions).run(sender = ledger_admin)

        sc.h2("Simple transfer")
        game_num = 1
        sc.h3("Platform will mint 10 tokens 0 to Player 2")
        settlements, bonds = transfer_settlements(p0_to_p2 = {42:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Admin allows reputation transfer in game permissions")
        c1.admin_game_permissions(game_id = game_id, key = "allowed_mint", permission = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(10)})))).run(sender = admin)

        sc.h3("Play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(42))
        c1.game_settle(game_id).run(sender = player1)
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[42] == 10)
        sc.p("P2 received 10 tokens 0")

        sc.h2("Admin allow transfer model to mint Reputation token")
        c1.admin_model_permissions(model_id = model_id, key = "allowed_mint", permission = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(15)})))).run(sender = admin)

        game_num += 1
        sc.h2("Simple transfer that mint Reputation token")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                         game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[42] == 10)
        sc.verify(sp.unpack(c1.data.model_permissions[model_id]["allowed_mint"], sp.TMap(sp.TNat, sp.TNat)).open_some()[42] == 15)
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[42] == 20)
        sc.verify(sp.unpack(c1.data.model_permissions[model_id]["allowed_mint"], sp.TMap(sp.TNat, sp.TNat)).open_some()[42] == 5)
        sc.p("P2 received 10 tokens 0")

        game_num += 1
        sc.h2("Simple transfer that mint Reputation token")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                                         game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game cannot be completed because the model is not allowed to mint more")
        c1.game_settle(game_id).run(sender = player1, valid = False)

        sc.h3("Admin allows enough reputation transfer in game permissions")
        c1.admin_game_permissions(game_id = game_id, key = "allowed_mint", permission = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(6)})))).run(sender = admin)
        sc.h3("Settle game")
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[42] == 20)
        sc.verify(sp.unpack(c1.data.model_permissions[model_id]["allowed_mint"], sp.TMap(sp.TNat, sp.TNat)).open_some()[42] == 5)
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(gp.player_bonds(c1, channel_id, player2.address)[42] == 30)
        sc.verify(sp.unpack(c1.data.model_permissions[model_id]["allowed_mint"], sp.TMap(sp.TNat, sp.TNat)).open_some()[42] == 1)
        sc.p("P2 received 10 tokens 42")

    @sp.add_test(name="Rewards")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Rewards")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("Ledger: Reputation configuration")
        reputation_permissions = lgr.build_token_permissions({
            "type"            : "NATIVE",
            "mint_cost"       : sp.mutez(0),
            "mint_permissions": sp.variant("allow_only", sp.set([ledger_admin.address]))
        })
        ledger.set_token_info(token_id = 42, token_info = reputation_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 42, token_permissions = reputation_permissions).run(sender = ledger_admin)

        sc.h2("Ledger: Mint 1 million reputation tokens for platform")
        ledger.mint(token_id = 42, amount = 1_000_000, address = c1.address).run(sender = ledger_admin)

        sc.h2("New model: Transfer (gives 1000 reputation token to player1)")
        model_rewards = [sp.record(token_id = 42, amount = 1000, to_ = player1.address)]
        model = sc.compute(model_wrap.model_wrap(transfer, rewards = model_rewards))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Simple transfer")
        game_num = 1
        sc.h3("Transfer of nothing")
        settlements, bonds = transfer_settlements()
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)
        sc.verify(lgr.fa2_balance(player1.address, ledger, token_id = 42) == 1000)

    @sp.add_test(name="Platform tokens transfers errors")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        sc.h2("Simple transfer")
        sc.h3("Platform will mint 10 tokens 42 to Player 2")
        settlements, bonds = transfer_settlements(p0_to_p2 = {42:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game1", play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        sc.show(game_id)
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("(Onchain) play transfer")
        c1.game_play(play_transfer(game_id)).run(sender = player1)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Game transfer allowance not configured")
        c1.game_settle(game_id).run(
            sender = player1, valid = False,
            exception =  sp.pair('Platform_NotAllowedMinting', sp.record(amount = 10, game_id = game_id, token = 42))
        )

        sc.h3("Game transfer allowance not configured")
        c1.game_settle(game_id).run(
            sender = player1, valid = False,
            exception = sp.pair("Platform_NotAllowedMinting", sp.record(game_id = game_id, token = 42, amount = 10))
        )

        sc.h3("(Onchain) admin allows reputation transfer in metadata")
        c1.admin_game_permissions(game_id = game_id, key = "allowed_mint", permission = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(5)})))).run(sender = admin)

        sc.h3("Game transfer allowance is not enough")
        c1.game_settle(game_id).run(
            sender = player1, valid = False,
            exception = sp.pair("Platform_NotAllowedMinting", sp.record(game_id = game_id, token = 42, amount = 10))
        )

        sc.h3("(Onchain) admin set allowed reputation transfer to 10")
        c1.admin_game_permissions(game_id = game_id, key = "allowed_mint", permission = sp.some(sp.pack(sp.map(l = {sp.nat(42): sp.nat(10)})))).run(sender = admin)

        sc.h2("New token: Reputation")
        reputation_permissions = lgr.build_token_permissions({
            "type"            : "NATIVE",
            "mint_cost"       : sp.mutez(0),
            "mint_permissions": sp.variant("allow_only", sp.set([c1.address]))
        })
        ledger.set_token_info(token_id = 42, token_info = reputation_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 42, token_permissions = reputation_permissions).run(sender = ledger_admin)

        sc.h3("Valid transfer")
        c1.game_settle(game_id).run(sender = player1)


    class DummyFA2(FA2.FA2, FA2.FA2_mint):
        @sp.private_lambda(with_storage="read-write", with_operations=True, wrap_call=True)
        def _transfer(self, params):
            super().transfer(params)

        @sp.entry_point
        def transfer(self, params):
            self._transfer(params)

        @sp.entry_point
        def transfer_and_call(self, batchs):
            sp.set_type(batchs, lgr.t_transfer_and_call)

            # Transfer
            with sp.for_('batchs', batchs) as batch:
                with sp.for_('transaction', batch.txs) as tx:
                    self._transfer([
                        sp.record(
                            from_=batch.from_,
                            txs=[
                                sp.record(
                                    to_=tx.to_,
                                    token_id=tx.token_id,
                                    amount=tx.amount
                                )
                            ]
                        )
                    ])

                    # Callback
                    callback = sp.contract(types.t_callback, tx.callback).open_some("FA2_WRONG_CALLBACK_INTERFACE")
                    args = sp.record(
                        amount   = tx.amount,
                        data     = tx.data,
                        sender   = batch.from_,
                        receiver = tx.to_,
                        token_id = tx.token_id,
                    )
                    sp.transfer(args, sp.tez(0), callback)


    @sp.add_test(name="FA2 tokens")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        FA2_admin = sp.test_account("FA2_admin")
        sc.h1("Transfers")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1
        sc.h3("FA2")
        dummyToken = DummyFA2(
            FA2.FA2_config(single_asset = False),
            admin = FA2_admin.address,
            metadata = sp.utils.metadata_of_url("https://example.com")
        )
        sc += dummyToken
        sc.h2("FA2: Initial minting")
        dummy_md = FA2.FA2.make_metadata(
            name     = "Dummy FA2",
            decimals = 0,
            symbol   = "DFA2" )
        dummyToken.mint(
            address  = FA2_admin.address,
            token_id = 0,
            amount   = 100_000_000_000,
            metadata = dummy_md
        ).run(sender = FA2_admin)
        sc.h2("FA2_admin transfer 100 tokens to player1")
        dummyToken.transfer([dummyToken.batch_transfer.item(
            from_ = FA2_admin.address,
            txs = [ sp.record(to_ = player1.address, amount = 100, token_id = 0)])
        ]).run(sender = FA2_admin)
        sc.h2("GamePlatform admin registers FA2 token 0 on platform as token 1")
        wDummy_metadata = FA2.FA2.make_metadata(
            name     = "Wrapped Dummy",
            symbol   = "WD",
            decimals = 6,
        )
        wDummy_permissions = lgr.build_token_permissions({
                "type"            : "FA2",
                "fa_token"        : sp.pair(dummyToken.address, 0),
                "mint_cost"       : sp.mutez(1),
                "mint_permissions": sp.variant("allow_everyone", sp.unit)
        })
        ledger.set_token_info(token_id = 1, token_info = wDummy_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 1, token_permissions = wDummy_permissions).run(sender = ledger_admin)

        sc.h2("Admin set ledger as platform operator for Dummy token")
        c1.admin_update_operators(fa2 = dummyToken.address, operators = [
            sp.variant("add_operator", dummyToken.operator_param.make(
                owner = c1.address,
                operator = ledger.address,
                token_id = 0)),
        ]).run(sender = admin)

        sc.h2("Player1 set ledger as operator")
        dummyToken.update_operators([
                sp.variant("add_operator", dummyToken.operator_param.make(
                    owner = player1.address,
                    operator = ledger.address,
                    token_id = 0)),
        ]).run(sender = player1)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams).run(sender = player1)

        sc.h2("Player1 push dummy FA2 tokens as bonds")
        sc.verify(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance == 100)
        sc.verify(~dummyToken.data.ledger.contains(dummyToken.ledger_key.make(c1.address, 0)))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {1: 100}).run(sender = player1)
        sc.verify(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance == 0)
        sc.verify(dummyToken.data.ledger[dummyToken.ledger_key.make(c1.address, 0)].balance == 100)

        sc.h2("Withdraw")
        sc.h3("Player 1 request withdraw 50 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({1: 50})).run(sender = player1)
        sc.h3("Player 2 request leave empty challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([])).run(sender = player2)
        sc.h3("Player 1 finalize withdraw request")
        sc.verify_equal(gp.player_bonds(c1, channel_id, player1.address)[1], 100)
        c1.withdraw_finalize(channel_id).run(sender = player1)
        sc.verify_equal(gp.player_bonds(c1, channel_id, player1.address)[1], 50)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance, 50)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(c1.address, 0)].balance, 50)

        ###################################
        # Test 2 : with transfer_and_call #
        ###################################
        sc.h2("Player1 remove ledger as operator")
        dummyToken.update_operators([
                sp.variant("remove_operator", dummyToken.operator_param.make(
                    owner = player1.address,
                    operator = ledger.address,
                    token_id = 0)),
        ]).run(sender = player1)

        sc.h2("Player1 push dummy FA2 tokens as bonds via transfer and call")
        ledger_on_received_bonds = sp.to_address(sp.contract(types.t_callback, ledger.address, entry_point = "on_received_bonds").open_some())
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(c1.address, 0)].balance, 50)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance, 50)
        dummyToken.transfer_and_call([
            sp.record(
                from_ = player1.address,
                txs = [sp.record(
                    to_      = c1.address,
                    callback = ledger_on_received_bonds,
                    data     = sp.pack(sp.record(channel_id = channel_id, player_addr = player1.address)),
                    token_id = 0,
                    amount   = 42
                )]
            )
        ]).run(sender = player1)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(c1.address, 0)].balance, 92)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance, 8)

        sc.h2("Withdraw")
        sc.h3("Player 1 request withdraw 42 tokens")
        c1.withdraw_request(channel_id = channel_id, tokens = sp.map({1: 42})).run(sender = player1)
        sc.h3("Player 2 request leave empty challenge")
        c1.withdraw_challenge(channel_id = channel_id, withdrawer = player1.address, game_ids = sp.set([])).run(sender = player2)
        sc.h3("Player 1 finalize withdraw request")
        sc.verify_equal(gp.player_bonds(c1, channel_id, player1.address)[1], 92)
        c1.withdraw_finalize(channel_id).run(sender = player1)
        sc.verify_equal(gp.player_bonds(c1, channel_id, player1.address)[1], 50)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(player1.address, 0)].balance, 50)
        sc.verify_equal(dummyToken.data.ledger[dummyToken.ledger_key.make(c1.address, 0)].balance, 50)

        sc.h2("Expected error on wrong receiver")
        sc.p("This case is never supposed to happen if the ledger is well configured.<br>\
                It should build the callback according to the receiver. \
                But the platform is still responsible for failing if that happens. \
            ")
        c1.on_received_bonds(
            sp.record(
                amount   = 42,
                data = sp.pack(sp.record(channel_id = channel_id, player_addr = player1.address)),
                receiver = player1.address,
                sender = player1.address,
                token_id = 0
            )
        ).run(sender = ledger.address, valid = False, exception = "Platform_NotReceiver")
        # Notice that the sender is a contract of this transction is a contract address,
        # it means we bypass some of the contract logic
        # to test what would happen in the platform if the ledger logic were wrong.

    @sp.add_test(name="transfers - one signature play with outcome")
    def test():
        sc = sp.test_scenario()
        sc.table_of_contents()
        sc.h1("Transfers")
        sc.h2("Contracts")
        sc.h3("Ledger")
        ledger = lgr.Ledger(ledger_config, admin = ledger_admin.address, metadata = ledger_metadata)
        sc += ledger
        sc.h3("GamePlatform")
        c1 = gp.GamePlatform(sp.set([admin.address]), ledger.address)
        sc += c1

        sc.h2("Ledger: XTZ configuration")
        ledger.set_token_info(token_id = 0, token_info = wXTZ_metadata).run(sender = ledger_admin)
        ledger.update_token_permissions(token_id = 0, token_permissions = wXTZ_permissions).run(sender = ledger_admin)

        sc.h2("New model: Transfer")
        model = sc.compute(model_wrap.model_wrap(transfer))
        model_id = sc.compute(model_wrap.model_id(model))
        c1.admin_new_model(model).run(sender = admin)

        sc.h2("New channel")
        channelParams = sp.record(players = players, nonce = "Channel 1", withdraw_delay = 3600 * 24)
        channel_id = sp.blake2b(sp.pack((c1.address, channelParams.players, channelParams.nonce)))
        c1.new_channel(channelParams)

        ###########
        #  Test 1 #
        ###########
        sc.h2("One player signature")
        game_num = 1
        sc.h3("Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {0: 10}")
        ledger.mint(token_id = 0, amount = 10, address = player1.address).run(sender = player1, amount = sp.mutez(10))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {0: 10}).run(sender = player1)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 send transfer with its signature")
        new_current = sp.record(move_nb = 1, player = 2, outcome = sp.some(sp.variant("final", sp.variant("game_finished", "transferred"))))
        player_1_signature = sp.make_signature(player1.secret_key, gp.action_play(game_id, new_current, sp.pack(sp.unit), sp.pack(sp.unit)))
        c1.game_play(game_id = game_id, new_current = new_current, new_state = sp.pack(sp.unit), move_data = sp.pack(sp.unit), signatures = sp.map({player1.public_key: player_1_signature})).run(sender = player1)
        sc.verify(c1.data.games[game_id].current.outcome == sp.some(sp.variant("pending", sp.variant("game_finished", "transferred"))))

        sc.h3("Player 2 countersign")
        player_2_signature = sp.make_signature(player2.secret_key, gp.action_play(game_id, new_current, sp.pack(sp.unit), sp.pack(sp.unit)))
        c1.game_play(game_id = game_id, new_current = new_current, new_state = sp.pack(sp.unit), move_data = sp.pack(sp.unit), signatures = sp.map({player2.public_key: player_2_signature})).run(sender = player2)
        sc.verify(gp.finished_outcome(c1, game_id) == "transferred")

        sc.h3("Settle game")
        sc.verify( gp.player_bonds(c1, channel_id, player1.address)[0] == 10)
        sc.verify(~gp.player_bonds(c1, channel_id, player2.address).contains(0))
        c1.game_settle(game_id).run(sender = player1)
        sc.verify(~gp.player_bonds(c1, channel_id, player1.address).contains(0))
        sc.verify( gp.player_bonds(c1, channel_id, player2.address)[0] == 10)
        sc.p("P2 received 10 tokens 0")

        ###########
        #  Test 2 #
        ###########
        sc.h2("Counter signature starved")
        game_num += 1
        sc.h3("Player 1 will transfer 10 to Player 2")
        settlements, bonds = transfer_settlements(p1_to_p2 = {0:10})
        settlements = sc.compute(settlements)
        sc.show(settlements)
        sc.h3("Player 1 push {0: 10}")
        ledger.mint(token_id = 0, amount = 10, address = player1.address).run(sender = player1, amount = sp.mutez(10))
        ledger.push_bonds(platform = c1.address, channel_id = channel_id, player_addr = player1.address, bonds = {0: 10}).run(sender = player1)

        sc.h3("P1 Instanciate new game : Transfer")
        constants = sc.compute(sp.record(model_id = model_id, channel_id = channel_id, players_addr = {1:player1.address, 2:player2.address},
                              game_nonce = "game"+str(game_num), play_delay = play_delay, bonds = bonds, settlements = settlements))
        game_id = sc.compute(gp.compute_game_id(constants))
        c1.new_game(constants = constants, initial_config = sp.pack(sp.unit), signatures = new_game_sigs(constants)).run(sender = player1)

        sc.h3("Player 1 send transfer with its signature")
        new_current = sp.record(move_nb = 1, player = 2, outcome = sp.some(sp.variant("final", sp.variant("game_finished", "transferred"))))
        player_1_signature = sp.make_signature(player1.secret_key, gp.action_play(game_id, new_current, sp.pack(sp.unit), sp.pack(sp.unit)))
        c1.game_play(game_id = game_id, new_current = new_current, new_state = sp.pack(sp.unit), move_data = sp.pack(sp.unit), signatures = sp.map({player1.public_key: player_1_signature})).run(sender = player1)
        sc.show(c1.data.games[game_id].current.outcome)
        sc.verify(c1.data.games[game_id].current.outcome == sp.some(sp.variant("pending", sp.variant("game_finished", "transferred"))))

        sc.h3("Player 1 starved when pending outcome")
        c1.dispute_starving(game_id = game_id, flag = True).run(sender = player1)
        c1.dispute_starved(game_id = game_id, player_id = 2).run(sender = player1, now = sp.timestamp(3600 * 24 + 1))

        sc.verify(gp.outcome(c1, game_id) == sp.variant("player_inactive", 2))
