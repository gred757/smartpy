import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(admins = sp.TSet(sp.TAddress), channels = sp.TBigMap(sp.TBytes, sp.TRecord(closed = sp.TBool, nonce = sp.TString, players = sp.TMap(sp.TAddress, sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), pk = sp.TKey, withdraw = sp.TOption(sp.TRecord(challenge = sp.TSet(sp.TBytes), challenge_tokens = sp.TMap(sp.TNat, sp.TInt), timeout = sp.TTimestamp, tokens = sp.TMap(sp.TNat, sp.TNat)).layout((("challenge", "challenge_tokens"), ("timeout", "tokens")))), withdraw_id = sp.TNat).layout(("bonds", ("pk", ("withdraw", "withdraw_id"))))), withdraw_delay = sp.TInt).layout(("closed", ("nonce", ("players", "withdraw_delay"))))), games = sp.TBigMap(sp.TBytes, sp.TRecord(addr_players = sp.TMap(sp.TAddress, sp.TInt), constants = sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), current = sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(final = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), pending = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive"))))).layout(("final", "pending"))), player = sp.TInt).layout(("move_nb", ("outcome", "player"))), initial_config = sp.TBytes, permissions = sp.TMap(sp.TString, sp.TBytes), settled = sp.TBool, state = sp.TOption(sp.TBytes), timeouts = sp.TMap(sp.TInt, sp.TTimestamp)).layout(("addr_players", ("constants", ("current", ("initial_config", ("permissions", ("settled", ("state", "timeouts"))))))))), ledger = sp.TAddress, metadata = sp.TBigMap(sp.TString, sp.TBytes), model_metadata = sp.TBigMap(sp.TBytes, sp.TMap(sp.TString, sp.TBytes)), model_permissions = sp.TBigMap(sp.TBytes, sp.TMap(sp.TString, sp.TBytes)), models = sp.TBigMap(sp.TBytes, sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = sp.TBytes, move_nb = sp.TNat, player = sp.TInt, state = sp.TBytes).layout(("move_data", ("move_nb", ("player", "state")))), sp.TPair(sp.TBytes, sp.TOption(sp.TString))), init = sp.TLambda(sp.TBytes, sp.TBytes), outcomes = sp.TList(sp.TString)).layout(("apply_", ("init", "outcomes"))))).layout(("admins", ("channels", ("games", ("ledger", ("metadata", ("model_metadata", ("model_permissions", "models")))))))))
    self.init(admins = sp.set([sp.address('tz1_GAMEPLATFORM_ADMIN')]),
              channels = {},
              games = {},
              ledger = sp.address('KT1_LEDGER'),
              metadata = {'' : sp.bytes('0x68747470733a2f2f636c6f7564666c6172652d697066732e636f6d2f697066732f516d5a364e5433535134596d53394646745a6a41764d51366844624d6868487654473144364671426e6d38635846')},
              model_metadata = {},
              model_permissions = {},
              models = {})

  @sp.entry_point
  def admin_game_permissions(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.verify(self.data.games.contains(params.game_id), 'Platform_GameNotFound')
    sp.set_type(params.permission, sp.TOption(sp.TBytes))
    sp.set_type(params.key, sp.TString)
    sp.if params.permission.is_some():
      self.data.games[params.game_id].permissions[params.key] = params.permission.open_some()
    sp.else:
      del self.data.games[params.game_id].permissions[params.key]

  @sp.entry_point
  def admin_global_metadata(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.set_type(params.metadata, sp.TOption(sp.TBytes))
    sp.set_type(params.key, sp.TString)
    sp.if params.metadata.is_some():
      self.data.metadata[params.key] = params.metadata.open_some()
    sp.else:
      del self.data.metadata[params.key]

  @sp.entry_point
  def admin_model_metadata(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.verify(self.data.models.contains(params.model_id), 'Platform_ModelNotFound')
    sp.set_type(params.metadata, sp.TOption(sp.TBytes))
    sp.set_type(params.key, sp.TString)
    sp.if params.metadata.is_some():
      self.data.model_metadata[params.model_id][params.key] = params.metadata.open_some()
    sp.else:
      del self.data.model_metadata[params.model_id][params.key]

  @sp.entry_point
  def admin_model_permissions(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.verify(self.data.models.contains(params.model_id), 'Platform_ModelNotFound')
    sp.set_type(params.permission, sp.TOption(sp.TBytes))
    sp.set_type(params.key, sp.TString)
    sp.if params.permission.is_some():
      self.data.model_permissions[params.model_id][params.key] = params.permission.open_some()
    sp.else:
      del self.data.model_permissions[params.model_id][params.key]

  @sp.entry_point
  def admin_new_model(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    compute_game_platform_962 = sp.local("compute_game_platform_962", sp.blake2b(params.model))
    sp.if ~ (self.data.models.contains(compute_game_platform_962.value)):
      compute_game_platform_964 = sp.local("compute_game_platform_964", sp.unpack(params.model, sp.TRecord(apply_ = sp.TLambda(sp.TRecord(move_data = sp.TBytes, move_nb = sp.TNat, player = sp.TInt, state = sp.TBytes).layout(("move_data", ("move_nb", ("player", "state")))), sp.TPair(sp.TBytes, sp.TOption(sp.TString))), init = sp.TLambda(sp.TBytes, sp.TBytes), outcomes = sp.TList(sp.TString)).layout(("apply_", ("init", "outcomes")))).open_some(message = 'Platform_ModelUnpackFailure'))
      self.data.model_metadata[compute_game_platform_962.value] = params.metadata
      self.data.model_permissions[compute_game_platform_962.value] = params.permissions
      self.data.models[compute_game_platform_962.value] = sp.record(apply_ = compute_game_platform_964.value.apply_, init = compute_game_platform_964.value.init, outcomes = compute_game_platform_964.value.outcomes)
    sp.if sp.len(params.rewards) > 0:
      sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = params.rewards)]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.ledger, entry_point='transfer').open_some(message = 'Platform_LedgerNotFound'))

  @sp.entry_point
  def admin_setup(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.if params.ledger.is_some():
      self.data.ledger = params.ledger.open_some()
    sp.for admin in params.add_admins:
      self.data.admins.add(admin)
    sp.for admin in params.remove_admins.elements():
      sp.verify(~ (admin == sp.sender), 'Platform_CannotRemoveSelf')
      self.data.admins.remove(admin)

  @sp.entry_point
  def admin_update_operators(self, params):
    sp.verify(self.data.admins.contains(sp.sender), 'Platform_NotAdmin')
    sp.transfer(params.operators, sp.tez(0), sp.contract(sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))), params.fa2, entry_point='update_operators').open_some(message = 'Plateform_FA2Unreacheable'))

  @sp.entry_point
  def dispute_double_signed(self, params):
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    with game.value.current.outcome.match('Some') as Some:
      sp.verify(Some.is_variant('pending'), 'Platform_GameNotRunning')
    sp.set_type(game.value.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))))
    player = sp.local("player", self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[params.player_id]])
    sp.verify(sp.check_signature(player.value.pk, params.statement1.sig, sp.pack(('Play', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.statement1.current, sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(final = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), pending = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive"))))).layout(("final", "pending"))), player = sp.TInt).layout(("move_nb", ("outcome", "player")))), sp.set_type_expr(params.statement1.state, sp.TBytes), sp.set_type_expr(params.statement1.move_data, sp.TBytes)))), 'Platform_badSig')
    sp.verify(sp.check_signature(player.value.pk, params.statement2.sig, sp.pack(('Play', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.statement2.current, sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(final = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), pending = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive"))))).layout(("final", "pending"))), player = sp.TInt).layout(("move_nb", ("outcome", "player")))), sp.set_type_expr(params.statement2.state, sp.TBytes), sp.set_type_expr(params.statement2.move_data, sp.TBytes)))), 'Platform_badSig')
    sp.verify(params.statement1.current.move_nb == params.statement2.current.move_nb, 'Platform_MoveNBMismatch')
    sp.verify(params.statement1.sig != params.statement2.sig, 'Platform_NotDifferentSignatures')
    game.value.current.outcome = sp.some(variant('final', variant('player_double_played', params.player_id)))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def dispute_starved(self, params):
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    with game.value.current.outcome.match('Some') as Some:
      sp.verify(Some.is_variant('pending'), 'Platform_GameNotRunning')
    sp.verify(game.value.current.player == params.player_id, 'Platform_NotPlayersTurn')
    sp.verify(game.value.timeouts.contains(params.player_id), 'Platform_NoTimeoutSetup')
    sp.verify(sp.now > game.value.timeouts[params.player_id], 'Platform_NotTimedOut')
    game.value.current.outcome = sp.some(variant('final', variant('player_inactive', params.player_id)))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def dispute_starving(self, params):
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    with game.value.current.outcome.match('Some') as Some:
      sp.verify(Some.is_variant('pending'), 'Platform_GameNotRunning')
    sp.if params.flag:
      game.value.timeouts[3 - game.value.addr_players[sp.sender]] = sp.add_seconds(sp.now, game.value.constants.play_delay)
    sp.else:
      del game.value.timeouts[3 - game.value.addr_players[sp.sender]]
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def game_play(self, params):
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    with game.value.current.outcome.match('Some') as Some:
      sp.verify(Some.is_variant('pending'), 'Platform_GameNotRunning')
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    signers = sp.local("signers", sp.list([]))
    sp.if sp.len(params.signatures) == 1:
      pending = sp.local("pending", False)
      sp.set_type(game.value.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))))
      player = sp.local("player", self.data.channels[game.value.constants.channel_id].players[game.value.constants.players_addr[game.value.current.player]])
      signers.value = sp.list([player.value])
      with game.value.current.outcome.match_cases() as arg:
        with arg.match('None') as None:
          model = sp.local("model", self.data.models.get(game.value.constants.model_id, message = ('Platform_ModelNotFound: ', game.value.constants.model_id)))
          sp.if ~ game.value.state.is_some():
            game.value.state = sp.some(model.value.init(game.value.initial_config))
          compute_game_platform_347 = sp.local("compute_game_platform_347", model.value.apply_(sp.record(move_data = params.move_data, move_nb = game.value.current.move_nb, player = game.value.current.player, state = game.value.state.open_some())))
          game.value.current = sp.record(move_nb = game.value.current.move_nb + 1, outcome = sp.eif(sp.snd(compute_game_platform_347.value).is_some(), sp.some(variant('final', variant('game_finished', sp.snd(compute_game_platform_347.value).open_some()))), sp.none), player = 3 - game.value.current.player)
          game.value.state = sp.some(sp.fst(compute_game_platform_347.value))
          sp.if game.value.current.outcome.is_some():
            pending.value = True
        with arg.match('Some') as Some:
          game.value.current.outcome = sp.some(variant('final', Some.open_variant('pending')))

      sp.verify(game.value.current == params.new_current, 'Platform_CurrentMismatch')
      sp.verify(game.value.state.open_some() == params.new_state, 'Platform_StateMismatch')
      sp.if pending.value:
        game.value.current.outcome = sp.some(variant('pending', game.value.current.outcome.open_some().open_variant('final')))
    sp.else:
      equal_move_nb = sp.local("equal_move_nb", False)
      sp.if game.value.current.outcome.is_some():
        sp.if game.value.current.outcome.open_some().is_variant('pending'):
          sp.if params.new_current.move_nb == game.value.current.move_nb:
            equal_move_nb.value = True
      sp.verify((params.new_current.move_nb > game.value.current.move_nb) | equal_move_nb.value, 'Platform_OutdatedMoveNB')
      channel = sp.local("channel", self.data.channels.get(game.value.constants.channel_id, message = ('Platform_ChannelNotFound: ', game.value.constants.channel_id)))
      sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
      signers.value = channel.value.players.values()
      game.value.current = params.new_current
      game.value.state = sp.some(params.new_state)
    compute_game_platform_441 = sp.local("compute_game_platform_441", sp.pack(('Play', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.new_current, sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(final = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), pending = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive"))))).layout(("final", "pending"))), player = sp.TInt).layout(("move_nb", ("outcome", "player")))), sp.set_type_expr(params.new_state, sp.TBytes), sp.set_type_expr(params.move_data, sp.TBytes))))
    sp.for player in signers.value:
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_441.value), ('Platform_badSig', player.pk))
    sp.if game.value.timeouts.contains(game.value.current.player):
      game.value.timeouts[game.value.current.player] = sp.add_seconds(sp.now, game.value.constants.play_delay)
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def game_set_outcome(self, params):
    sp.set_type(params.outcome, sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString).layout(("game_aborted", "game_finished")))
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    sp.verify(sp.now <= params.timeout, 'Platform_OutcomeTimedout')
    game = sp.local("game", self.data.games.get(params.game_id, message = ('Platform_GameNotFound: ', params.game_id)))
    with game.value.current.outcome.match('Some') as Some:
      sp.verify(Some.is_variant('pending'), 'Platform_GameNotRunning')
    compute_game_platform_618 = sp.local("compute_game_platform_618", sp.pack(('New Outcome', sp.set_type_expr(params.game_id, sp.TBytes), sp.set_type_expr(params.outcome, sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString).layout(("game_aborted", "game_finished"))), params.timeout)))
    channel = sp.local("channel", self.data.channels.get(game.value.constants.channel_id, message = ('Platform_ChannelNotFound: ', game.value.constants.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    players = sp.local("players", channel.value.players)
    sp.for player in players.value.values():
      sp.verify(params.signatures.contains(player.pk), 'Platform_MissingSig')
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], compute_game_platform_618.value), 'Platform_badSig')
    sp.if params.outcome.is_variant('game_aborted'):
      game.value.current.outcome = sp.some(variant('final', variant('game_aborted', sp.unit)))
    sp.else:
      game.value.current.outcome = sp.some(variant('final', variant('game_finished', params.outcome.open_variant('game_finished'))))
    self.data.games[params.game_id] = game.value

  @sp.entry_point
  def game_settle(self, params):
    game = sp.local("game", self.data.games.get(params, message = ('Platform_GameNotFound: ', params)))
    compute_game_platform_658 = sp.local("compute_game_platform_658", game.value.current.outcome.open_some(message = 'Plateform_GameIsntOver'))
    model_permissions = sp.local("model_permissions", sp.eif(self.data.model_permissions.contains(game.value.constants.model_id), sp.some(self.data.model_permissions[game.value.constants.model_id]), sp.none))
    channel = sp.local("channel", self.data.channels.get(game.value.constants.channel_id, message = ('Platform_ChannelNotFound: ', game.value.constants.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(~ game.value.settled, 'Plateform_GameSettled')
    allowed_mint_model = sp.local("allowed_mint_model", {})
    allowed_mint_game = sp.local("allowed_mint_game", {})
    sp.if model_permissions.value.is_some():
      sp.if model_permissions.value.open_some().contains('allowed_mint'):
        allowed_mint_model.value = sp.unpack(model_permissions.value.open_some()['allowed_mint'], sp.TMap(sp.TNat, sp.TNat)).open_some()
    sp.if game.value.permissions.contains('allowed_mint'):
      allowed_mint_game.value = sp.unpack(game.value.permissions['allowed_mint'], sp.TMap(sp.TNat, sp.TNat)).open_some()
    sp.for transfer in game.value.constants.settlements.get(compute_game_platform_658.value.open_variant('final', message = 'Plateform_OutcomeNotFinalized'), message = ('Platform_UnknownOutcome', compute_game_platform_658.value)):
      sp.if transfer.sender == 0:
        compute_game_platform_502 = sp.local("compute_game_platform_502", game.value.constants.players_addr[transfer.receiver])
        receiver = sp.local("receiver", channel.value.players[compute_game_platform_502.value])
        sp.for tokens in transfer.bonds.items():
          delta = sp.local("delta", allowed_mint_game.value.get(tokens.key, default_value = 0) - tokens.value)
          allowed_mint_game.value[tokens.key] = sp.eif(delta.value > 0, sp.as_nat(delta.value), 0)
          sp.if delta.value < 0:
            allowed_mint_model.value[tokens.key] = sp.as_nat(sp.to_int(allowed_mint_model.value.get(tokens.key, default_value = 0)) + delta.value, message = ('Platform_NotAllowedMinting', sp.record(amount = tokens.value, game_id = params, token = tokens.key)))
          receiver.value.bonds[tokens.key] = receiver.value.bonds.get(tokens.key, default_value = 0) + tokens.value
        channel.value.players[compute_game_platform_502.value] = receiver.value
      sp.else:
        compute_game_platform_529 = sp.local("compute_game_platform_529", game.value.constants.players_addr[transfer.sender])
        compute_game_platform_530 = sp.local("compute_game_platform_530", game.value.constants.players_addr[transfer.receiver])
        sp.if ~ (compute_game_platform_529.value == compute_game_platform_530.value):
          sender = sp.local("sender", channel.value.players[compute_game_platform_529.value])
          receiver = sp.local("receiver", channel.value.players[compute_game_platform_530.value])
          sp.for bond in transfer.bonds.items():
            sp.if ~ (bond.value == 0):
              sp.verify(sender.value.bonds.contains(bond.key), ('Platform_NotEnoughToken:', sp.record(amount = bond.value, sender = compute_game_platform_529.value, token = bond.key)))
              sender.value.bonds[bond.key] = sp.as_nat(sender.value.bonds[bond.key] - bond.value, message = ('Platform_NotEnoughToken:', sp.record(amount = bond.value, sender = compute_game_platform_529.value, token = bond.key)))
              sp.if sender.value.bonds[bond.key] == 0:
                del sender.value.bonds[bond.key]
              receiver.value.bonds[bond.key] = receiver.value.bonds.get(bond.key, default_value = 0) + bond.value
          channel.value.players[compute_game_platform_529.value] = sender.value
          channel.value.players[compute_game_platform_530.value] = receiver.value
    self.data.channels[game.value.constants.channel_id].players = channel.value.players
    game.value.settled = True
    self.data.games[params] = game.value
    sp.if sp.len(allowed_mint_model.value) > 0:
      self.data.model_permissions[game.value.constants.model_id]['allowed_mint'] = sp.pack(allowed_mint_model.value)
    sp.else:
      sp.if model_permissions.value.is_some():
        del self.data.model_permissions[game.value.constants.model_id]['allowed_mint']

  @sp.entry_point
  def new_channel(self, params):
    compute_game_platform_577 = sp.local("compute_game_platform_577", sp.blake2b(sp.pack((sp.self_address, params.players, params.nonce))))
    sp.verify(~ (self.data.channels.contains(compute_game_platform_577.value)), 'Platform_ChannelAlreadyExists')
    sp.verify(sp.len(params.players) == 2, 'Platform_Only2PlayersAllowed')
    players_map = sp.local("players_map", {})
    sp.for player in params.players.items():
      players_map.value[player.key] = sp.record(bonds = {}, pk = player.value, withdraw = sp.none, withdraw_id = 0)
    self.data.channels[compute_game_platform_577.value] = sp.record(closed = False, nonce = params.nonce, players = players_map.value, withdraw_delay = params.withdraw_delay)

  @sp.entry_point
  def new_game(self, params):
    sp.set_type(params.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))))
    sp.set_type(params.initial_config, sp.TBytes)
    sp.set_type(params.signatures, sp.TMap(sp.TKey, sp.TSignature))
    sp.verify(~ (self.data.games.contains(sp.blake2b(sp.pack((params.constants.channel_id, (params.constants.model_id, params.constants.game_nonce)))))), 'Platform_GameAlreadyExists')
    sp.verify(sp.len(params.constants.players_addr) == 2, 'Platform_Only2PlayersAllowed')
    channel = sp.local("channel", self.data.channels.get(params.constants.channel_id, message = ('Platform_ChannelNotFound: ', params.constants.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    addr_players = sp.local("addr_players", {})
    sp.for addr_player in params.constants.players_addr.items():
      addr_players.value[addr_player.value] = addr_player.key
      sp.verify(channel.value.players.contains(addr_player.value), 'Platform_GamePlayerNotInChannel')
    sp.for player in channel.value.players.values():
      sp.verify(params.signatures.contains(player.pk), 'Platform_MissingSig')
      sp.verify(sp.check_signature(player.pk, params.signatures[player.pk], sp.pack(('New Game', sp.set_type_expr(params.constants, sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements")))))))), sp.set_type_expr(params.initial_config, sp.TBytes)))), ('Platform_BadSig', sp.record(player = player.pk, signature = params.signatures[player.pk])))
    model = sp.local("model", self.data.models.get(params.constants.model_id, message = ('Platform_ModelNotFound: ', params.constants.model_id)))
    sp.for outcome in model.value.outcomes:
      sp.verify(params.constants.settlements.contains(variant('game_finished', outcome)), ('Platform_MissingSettlement', variant('game_finished', outcome)))
    sp.verify(params.constants.settlements.contains(variant('player_double_played', 1)), ('Platform_MissingSettlement', variant('player_double_played', 1)))
    sp.verify(params.constants.settlements.contains(variant('player_double_played', 2)), ('Platform_MissingSettlement', variant('player_double_played', 2)))
    sp.verify(params.constants.settlements.contains(variant('player_inactive', 1)), ('Platform_MissingSettlement', variant('player_inactive', 1)))
    sp.verify(params.constants.settlements.contains(variant('player_inactive', 2)), ('Platform_MissingSettlement', variant('player_inactive', 2)))
    sp.verify(params.constants.settlements.contains(variant('game_aborted', sp.unit)), ('Platform_MissingSettlement', variant('game_aborted', sp.unit)))
    self.data.games[sp.blake2b(sp.pack((params.constants.channel_id, (params.constants.model_id, params.constants.game_nonce))))] = sp.set_type_expr(sp.record(addr_players = addr_players.value, constants = params.constants, current = sp.record(move_nb = 0, outcome = sp.none, player = 1), initial_config = params.initial_config, permissions = {}, settled = False, state = sp.none, timeouts = {}), sp.TRecord(addr_players = sp.TMap(sp.TAddress, sp.TInt), constants = sp.TRecord(bonds = sp.TMap(sp.TInt, sp.TMap(sp.TNat, sp.TNat)), channel_id = sp.TBytes, game_nonce = sp.TString, model_id = sp.TBytes, play_delay = sp.TInt, players_addr = sp.TMap(sp.TInt, sp.TAddress), settlements = sp.TMap(sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), sp.TList(sp.TRecord(bonds = sp.TMap(sp.TNat, sp.TNat), receiver = sp.TInt, sender = sp.TInt).layout(("bonds", ("receiver", "sender")))))).layout(("bonds", ("channel_id", ("game_nonce", ("model_id", ("play_delay", ("players_addr", "settlements"))))))), current = sp.TRecord(move_nb = sp.TNat, outcome = sp.TOption(sp.TVariant(final = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive")))), pending = sp.TVariant(game_aborted = sp.TUnit, game_finished = sp.TString, player_double_played = sp.TInt, player_inactive = sp.TInt).layout(("game_aborted", ("game_finished", ("player_double_played", "player_inactive"))))).layout(("final", "pending"))), player = sp.TInt).layout(("move_nb", ("outcome", "player"))), initial_config = sp.TBytes, permissions = sp.TMap(sp.TString, sp.TBytes), settled = sp.TBool, state = sp.TOption(sp.TBytes), timeouts = sp.TMap(sp.TInt, sp.TTimestamp)).layout(("addr_players", ("constants", ("current", ("initial_config", ("permissions", ("settled", ("state", "timeouts")))))))))

  @sp.entry_point
  def on_received_bonds(self, params):
    sp.set_type(params, sp.TRecord(amount = sp.TNat, data = sp.TBytes, receiver = sp.TAddress, sender = sp.TAddress, token_id = sp.TNat).layout(("amount", ("data", ("receiver", ("sender", "token_id"))))))
    sp.verify(self.data.ledger == sp.sender, 'Platform_NotLedger')
    sp.verify(sp.self_address == params.receiver, 'Platform_NotReceiver')
    compute_game_platform_807 = sp.local("compute_game_platform_807", sp.unpack(params.data, sp.TRecord(channel_id = sp.TBytes, player_addr = sp.TAddress).layout(("channel_id", "player_addr"))).open_some(message = 'Platform_UnpackErr'))
    channel = sp.local("channel", self.data.channels.get(compute_game_platform_807.value.channel_id, message = ('Platform_ChannelNotFound: ', compute_game_platform_807.value.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    player = sp.local("player", channel.value.players.get(compute_game_platform_807.value.player_addr, message = 'Platform_NotChannelPlayer'))
    player.value.bonds[params.token_id] = player.value.bonds.get(params.token_id, default_value = 0) + params.amount
    self.data.channels[compute_game_platform_807.value.channel_id].players[compute_game_platform_807.value.player_addr] = player.value

  @sp.entry_point
  def withdraw_cancel(self, params):
    channel = sp.local("channel", self.data.channels.get(params, message = ('Platform_ChannelNotFound: ', params)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    player = sp.local("player", channel.value.players[sp.sender])
    sp.verify(player.value.withdraw.is_some(), 'Platform_NoWithdrawOpened')
    sp.verify(sp.now >= player.value.withdraw.open_some().timeout, 'Platform_ChallengeDelayNotOver')
    channel.value.players[sp.sender].withdraw = sp.none
    self.data.channels[params] = channel.value

  @sp.entry_point
  def withdraw_challenge(self, params):
    sp.set_type(params.game_ids, sp.TSet(sp.TBytes))
    channel = sp.local("channel", self.data.channels.get(params.channel_id, message = ('Platform_ChannelNotFound: ', params.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    sp.verify(channel.value.players.contains(params.withdrawer), 'Platform_WithdrawerNotInChannel')
    sp.verify(channel.value.players[params.withdrawer].withdraw.is_some(), 'Platform_NoWithdrawRequestOpened')
    withdraw = sp.local("withdraw", channel.value.players[params.withdrawer].withdraw.open_some())
    sp.for game_id in params.game_ids.elements():
      game = sp.local("game", self.data.games.get(game_id, message = ('Platform_GameNotFound: ', game_id)))
      sp.verify(game.value.constants.channel_id == params.channel_id, 'Platform_ChannelIdMismatch')
      sp.if game.value.settled:
        sp.verify(withdraw.value.challenge.contains(game_id), 'Platform_GameSettled')
        withdraw.value.challenge.remove(game_id)
        sp.set_type(withdraw.value.challenge_tokens, sp.TMap(sp.TNat, sp.TInt))
        sp.set_type(game.value.constants.bonds[game.value.addr_players[params.withdrawer]], sp.TMap(sp.TNat, sp.TNat))
        sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
          sp.if withdraw.value.challenge_tokens.contains(token.key):
            compute_game_platform_1074 = sp.local("compute_game_platform_1074", withdraw.value.challenge_tokens[token.key] - sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]))
            sp.if compute_game_platform_1074.value == 0:
              del withdraw.value.challenge_tokens[token.key]
            sp.else:
              withdraw.value.challenge_tokens[token.key] = compute_game_platform_1074.value
          sp.else:
            withdraw.value.challenge_tokens[token.key] = 0 - sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
      sp.else:
        sp.if ~ (withdraw.value.challenge.contains(game_id)):
          withdraw.value.challenge.add(game_id)
          sp.set_type(withdraw.value.challenge_tokens, sp.TMap(sp.TNat, sp.TInt))
          sp.set_type(game.value.constants.bonds[game.value.addr_players[params.withdrawer]], sp.TMap(sp.TNat, sp.TNat))
          sp.for token in game.value.constants.bonds[game.value.addr_players[params.withdrawer]].items():
            sp.if withdraw.value.challenge_tokens.contains(token.key):
              withdraw.value.challenge_tokens[token.key] += sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
            sp.else:
              sp.if ~ (sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key]) == 0):
                withdraw.value.challenge_tokens[token.key] = sp.to_int(game.value.constants.bonds[game.value.addr_players[params.withdrawer]][token.key])
    sp.if params.withdrawer != sp.sender:
      withdraw.value.timeout = sp.now
    channel.value.players[params.withdrawer].withdraw = sp.some(withdraw.value)
    self.data.channels[params.channel_id] = channel.value

  @sp.entry_point
  def withdraw_finalize(self, params):
    channel = sp.local("channel", self.data.channels.get(params, message = ('Platform_ChannelNotFound: ', params)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    sp.verify(channel.value.players.contains(sp.sender), 'Platform_SenderNotInChannel')
    player = sp.local("player", channel.value.players[sp.sender])
    sp.verify(player.value.withdraw.is_some(), 'Platform_NoWithdrawOpened')
    withdraw = sp.local("withdraw", player.value.withdraw.open_some())
    sp.verify(sp.now >= withdraw.value.timeout, 'Platform_ChallengeDelayNotOver')
    txs = sp.local("txs", sp.list([]))
    sp.for token in withdraw.value.challenge_tokens.items():
      sp.if (token.value > 0) & (player.value.bonds.contains(token.key)):
        sp.verify((player.value.bonds[token.key] - withdraw.value.tokens[token.key]) > token.value, 'Platform_TokenChallenged')
    sp.for token in withdraw.value.tokens.items():
      sp.if token.value > 0:
        sp.verify(player.value.bonds.contains(token.key), 'Platform_NotEnoughTokens')
        player.value.bonds[token.key] = sp.as_nat(player.value.bonds[token.key] - token.value, message = 'Platform_NotEnoughTokens')
        txs.value.push(sp.record(to_ = sp.sender, token_id = token.key, amount = token.value))
    sp.transfer(sp.list([sp.record(from_ = sp.self_address, txs = txs.value)]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.ledger, entry_point='transfer').open_some(message = 'Platform_LedgerNotFound'))
    player.value.withdraw = sp.none
    channel.value.players[sp.sender] = player.value
    self.data.channels[params] = channel.value

  @sp.entry_point
  def withdraw_request(self, params):
    channel = sp.local("channel", self.data.channels.get(params.channel_id, message = ('Platform_ChannelNotFound: ', params.channel_id)))
    sp.verify(~ channel.value.closed, 'Platform_ChannelClosed')
    player = sp.local("player", channel.value.players.get(sp.sender, message = 'Platform_SenderNotInChannel'))
    sp.verify(player.value.withdraw.is_variant('None'), 'Platform_AnotherWithdrawIsRunning')
    player.value.withdraw_id += 1
    player.value.withdraw = sp.some(sp.record(challenge = sp.set([]), challenge_tokens = {}, timeout = sp.add_seconds(sp.now, channel.value.withdraw_delay), tokens = params.tokens))
    channel.value.players[sp.sender] = player.value
    self.data.channels[params.channel_id] = channel.value

sp.add_compilation_target("test", Contract())