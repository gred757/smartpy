import smartpy as sp


class Neg__BLS12_381_g1(sp.Contract):
    def __init__(self, **a):
        self.init_type(sp.TRecord(g1 = sp.TOption(sp.TBls12_381_g1)))
        self.init(g1 = sp.none)

    """
    NEG: Negate a curve point or field element.

    :: bls12_381_g1 : 'S -> bls12_381_g1 : 'S
    """
    @sp.entry_point
    def negate(self, params):
        sp.set_type(params.g1, sp.TBls12_381_g1);
        self.data.g1 = sp.some(- params.g1)

@sp.add_test(name = "Neg__BLS12_381_g1")
def test():
    c1 = Neg__BLS12_381_g1();

    scenario = sp.test_scenario()
    scenario += c1

    c1.negate(g1 = sp.bls12_381_g1("0x17f1d3a73197d7942695638c4fa9ac0fc3688c4f9774b905a14e3a3f171bac586c55e83ff97a1aeffb3af00adb22c6bb08b3f481e3aaa0f1a09e30ed741d8ae4fcf5e095d5d00af600db18cb2c04b3edd03cc744a2888ae40caa232946c5e7e1"));
