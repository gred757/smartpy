import smartpy as sp

tstorage = sp.TRecord(x = sp.TNat, y = sp.TString, z = sp.TNat).layout(("x", ("y", "z")))
tparameter = sp.TVariant(f = sp.TUnit, g = sp.TUnit).layout(("f", "g"))
tprivates = { "a": sp.TLambda(sp.TNat, sp.TNat, with_storage="read-write", with_operations=True), "b": sp.TLambda(sp.TNat, sp.TNat, with_storage="read-write", with_operations=True) }
tviews = { }
