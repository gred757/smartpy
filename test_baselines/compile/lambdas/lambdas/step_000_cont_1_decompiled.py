import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(abcd = sp.TInt, f = sp.TLambda(sp.TInt, sp.TInt), fff = sp.TOption(sp.TLambda(sp.TNat, sp.TNat)), ggg = sp.TOption(sp.TInt), value = sp.TNat).layout((("abcd", "f"), ("fff", ("ggg", "value")))))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TVariant(Left = sp.TInt, Right = sp.TUnit).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TNat).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right"))).layout(("Left", "Right"))).layout(("Left", "Right")))
    def ferror(error):
      sp.failwith('[Error: to_cmd: (fun (x12 : { lambda(int, int) ; int }) : int ->
Exec(Cdr(x12), Car(x12)))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
