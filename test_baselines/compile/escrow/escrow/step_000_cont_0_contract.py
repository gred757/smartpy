import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(balanceCounterparty = sp.TMutez, balanceOwner = sp.TMutez, counterparty = sp.TAddress, epoch = sp.TTimestamp, fromCounterparty = sp.TMutez, fromOwner = sp.TMutez, hashedSecret = sp.TBytes, owner = sp.TAddress).layout(((("balanceCounterparty", "balanceOwner"), ("counterparty", "epoch")), (("fromCounterparty", "fromOwner"), ("hashedSecret", "owner")))))
    self.init(balanceCounterparty = sp.tez(0),
              balanceOwner = sp.tez(0),
              counterparty = sp.address('tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi'),
              epoch = sp.timestamp(123),
              fromCounterparty = sp.tez(4),
              fromOwner = sp.tez(50),
              hashedSecret = sp.bytes('0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e'),
              owner = sp.address('tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C'))

  @sp.entry_point
  def addBalanceCounterparty(self):
    sp.verify(self.data.balanceCounterparty == sp.tez(0))
    sp.verify(sp.amount == self.data.fromCounterparty)
    self.data.balanceCounterparty = self.data.fromCounterparty

  @sp.entry_point
  def addBalanceOwner(self):
    sp.verify(self.data.balanceOwner == sp.tez(0))
    sp.verify(sp.amount == self.data.fromOwner)
    self.data.balanceOwner = self.data.fromOwner

  @sp.entry_point
  def claimCounterparty(self, params):
    sp.verify(sp.now < self.data.epoch)
    sp.verify(self.data.hashedSecret == sp.blake2b(params.secret))
    sp.verify(sp.sender == self.data.counterparty)
    sp.send(self.data.counterparty, self.data.balanceOwner + self.data.balanceCounterparty)
    self.data.balanceOwner = sp.tez(0)
    self.data.balanceCounterparty = sp.tez(0)

  @sp.entry_point
  def claimOwner(self):
    sp.verify(self.data.epoch < sp.now)
    sp.verify(sp.sender == self.data.owner)
    sp.send(self.data.owner, self.data.balanceOwner + self.data.balanceCounterparty)
    self.data.balanceOwner = sp.tez(0)
    self.data.balanceCounterparty = sp.tez(0)

sp.add_compilation_target("test", Contract())