import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(b0 = sp.TOption(sp.TBytes), l0 = sp.TNat, l1 = sp.TNat, nat_of_string = sp.TInt, s0 = sp.TOption(sp.TString), split = sp.TList(sp.TString), string_of_nat = sp.TString).layout((("b0", ("l0", "l1")), (("nat_of_string", "s0"), ("split", "string_of_nat")))))

  @sp.entry_point
  def concatenating(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TBytes, sp.TBytes), sp.TPair(sp.TList(sp.TString), sp.TList(sp.TBytes))))
    def ferror(error):
      sp.failwith('[Error: prim1: Concat1]')
    def ferror(error):
      sp.failwith('[Error: prim1: Concat1]')
    self.data = sp.record(b0 = sp.some(sp.build_lambda(ferror)(sp.unit)), l0 = self.data.l0, l1 = self.data.l1, nat_of_string = self.data.nat_of_string, s0 = sp.some(sp.build_lambda(ferror)(sp.unit)), split = self.data.split, string_of_nat = self.data.string_of_nat)

  @sp.entry_point
  def concatenating2(self, params):
    sp.set_type(params, sp.TPair(sp.TPair(sp.TBytes, sp.TBytes), sp.TPair(sp.TString, sp.TString)))
    def ferror(error):
      sp.failwith('[Error: Decompiler TODO prim2: Concat2]')
    def ferror(error):
      sp.failwith('[Error: Decompiler TODO prim2: Concat2]')
    self.data = sp.record(b0 = sp.some(sp.build_lambda(ferror)(sp.unit)), l0 = self.data.l0, l1 = self.data.l1, nat_of_string = self.data.nat_of_string, s0 = sp.some(sp.build_lambda(ferror)(sp.unit)), split = self.data.split, string_of_nat = self.data.string_of_nat)

  @sp.entry_point
  def slicing(self, params):
    sp.set_type(params, sp.TPair(sp.TBytes, sp.TString))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let x459 =
                              Pair
                                ( Pair
                                    ( Slice(1#nat, 2#nat, Car(r409))
                                    , Pair(Size(Cdr(r409)), __storage.l1)
                                    )
                                , Pair
                                    ( Pair
                                        ( __storage.nat_of_string
                                        , Slice(2#nat, 5#nat, Cdr(r409))
                                        )
                                    , Pair
                                        ( __storage.split
                                        , __storage.string_of_nat
                                        )
                                    )
                                )
                            let [s491; s492] =
                              match Cdr(Car(Cdr(x459))) with
                              | None _ -> [ r409; x459 ]
                              | Some s465 ->
                                  [ r409
                                  ; Pair
                                      ( Pair
                                          ( Slice(1#nat, 2#nat, Car(r409))
                                          , Pair
                                              ( Add
                                                  ( Size(s465)
                                                  , Size(Cdr(r409))
                                                  )
                                              , __storage.l1
                                              )
                                          )
                                      , Pair
                                          ( Pair
                                              ( __storage.nat_of_string
                                              , Slice(2#nat, 5#nat, Cdr(r409))
                                              )
                                          , Pair
                                              ( __storage.split
                                              , __storage.string_of_nat
                                              )
                                          )
                                      )
                                  ]
                              end
                            Pair
                              ( Pair
                                  ( Car(Car(s492))
                                  , Pair(Car(Cdr(Car(s492))), Size(Car(s491)))
                                  )
                              , Cdr(s492)
                              ))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def test_nat_of_string(self, params):
    sp.set_type(params, sp.TString)
    def ferror(error):
      sp.failwith('[Error: prim1: Size]')
    x314 = sp.local("x314", sp.build_lambda(ferror)(sp.unit))
    c67 = sp.local("c67", x314.value > 0)
    y65 = sp.local("y65", 0)
    r715 = sp.local("r715", 0)
    sp.while c67.value:
      with (sp.slice(params, y65.value, 1)).match_cases() as arg:
        with arg.match('None') as _l83:
          sp.failwith(34)
        with arg.match('Some') as s337:
    def ferror(error):
      sp.failwith('[Error: to_cmd: ["0": 0; "1": 1; "2": 2; "3": 3; "4": 4; "5": 5; "6": 6; "7": 7; "8": 8; "9": 9]#map(string,int)]')
          sp.failwith(sp.build_lambda(ferror)(sp.unit))

    self.data = sp.record(b0 = self.data.b0, l0 = self.data.l0, l1 = self.data.l1, nat_of_string = r715.value, s0 = self.data.s0, split = self.data.split, string_of_nat = self.data.string_of_nat)

  @sp.entry_point
  def test_split(self, params):
    sp.set_type(params, sp.TString)
    def ferror(error):
      sp.failwith('[Error: prim1: Size]')
    x112 = sp.local("x112", sp.build_lambda(ferror)(sp.unit))
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [_; _; r226; r227; r228; r229] =
          loop [ Gt(Compare(x112, 0#nat))
               ; 0#nat
               ; x112
               ; Nil<string>
               ; 0#nat
               ; l7
               ; Pair
                   ( Pair(__storage.b0, Pair(__storage.l0, __storage.l1))
                   , Pair
                       ( Pair(__storage.nat_of_string, __storage.s0)
                       , Pair(__storage.split, __storage.string_of_nat)
                       )
                   )
               ]
          step s116; s117; s118; s119; s120; s121 ->
            match Slice(s116, 1#nat, s120) with
            | None _ -> Failwith(12)
            | Some s137 ->
                let [s208; s209; s210; s211; s212; s213] =
                  if Eq(Compare(s137, ","))
                  then
                    match IsNat(Sub(s116, s119)) with
                    | None _ -> Failwith(13)
                    | Some s171 ->
                        match Slice(s119, s171, s120) with
                        | None _ -> Failwith(13)
                        | Some s188 ->
                            [ s116
                            ; s117
                            ; Cons(s188, s118)
                            ; Add(1#nat, s116)
                            ; s120
                            ; s121
                            ]
                        end
                    end
                  else
                    [ s116; s117; s118; s119; s120; s121 ]
                let x217 = Add(1#nat, s208)
                [ Gt(Compare(s209, x217))
                ; x217
                ; s209
                ; s210
                ; s211
                ; s212
                ; s213
                ]
            end
        let [s286; s287] =
          if Gt(Compare(Size(r228), 0#nat))
          then
            match IsNat(Sub(Size(r228), r227)) with
            | None _ -> Failwith(16)
            | Some s268 ->
                match Slice(r227, s268, r228) with
                | None _ -> Failwith(16)
                | Some s280 -> [ Cons(s280, r226); r229 ]
                end
            end
          else
            [ r226; r229 ]
        Pair
          ( Nil<operation>
          , record_of_tree(..., let [r637] =
                                  iter [ s286; Nil<string> ]
                                  step s291; s292 ->
                                    [ Cons(s291, s292) ]
                                let [x1034; x1035] = Unpair(2, s287)
                                Pair
                                  ( x1034
                                  , let [x1038; x1039] = Unpair(2, x1035)
                                    Pair
                                      ( x1038
                                      , let [x1042; x1043] = Unpair(2, x1039)
                                        let _ = x1042
                                        Pair(r637, x1043)
                                      )
                                  ))
          )]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def test_string_of_nat(self, params):
    sp.set_type(params, sp.TNat)
    def ferror(error):
      sp.failwith('[Error: to_cmd: let [s21; s22; _; s24] =
          if Eq(Compare(r8, 0#nat))
          then
            [ Cons("0", Nil<string>)
            ; r8
            ; r8
            ; Pair
                ( Pair(__storage.b0, Pair(__storage.l0, __storage.l1))
                , Pair
                    ( Pair(__storage.nat_of_string, __storage.s0)
                    , Pair(__storage.split, __storage.string_of_nat)
                    )
                )
            ]
          else
            [ Nil<string>
            ; r8
            ; r8
            ; Pair
                ( Pair(__storage.b0, Pair(__storage.l0, __storage.l1))
                , Pair
                    ( Pair(__storage.nat_of_string, __storage.s0)
                    , Pair(__storage.split, __storage.string_of_nat)
                    )
                )
            ]
        let [r705; _] =
          loop [ Gt(Compare(s22, 0#nat)); s21; s22 ]
          step s33; s34 ->
            match Ediv(s34, 10#nat) with
            | None _ -> Failwith(26)
            | Some s45 ->
                match Get
                        ( Cdr(s45)
                        , [0#nat: "0"; 1#nat: "1"; 2#nat: "2"; 3#nat: "3"; 4#nat: "4"; 5#nat: "5"; 6#nat: "6"; 7#nat: "7"; 8#nat: "8"; 9#nat: "9"]#map(nat,string)
                        ) with
                | None _ -> Failwith(26)
                | Some s55 ->
                    match Ediv(s34, 10#nat) with
                    | None _ -> Failwith(27)
                    | Some s68 ->
                        [ Gt(Compare(Car(s68), 0#nat))
                        ; Cons(s55, s33)
                        ; Car(s68)
                        ]
                    end
                end
            end
        Pair
          ( Nil<operation>
          , record_of_tree(..., let [x1032; x1033] = Unpair(2, s24)
                                Pair
                                  ( x1032
                                  , let [x1036; x1037] = Unpair(2, x1033)
                                    Pair
                                      ( x1036
                                      , let [x1040; x1041] = Unpair(2, x1037)
                                        Pair
                                          ( x1040
                                          , let _ = x1041
                                            Concat1(r705)
                                          )
                                      )
                                  ))
          )]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
