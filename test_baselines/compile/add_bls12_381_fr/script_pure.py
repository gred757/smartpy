import smartpy as sp

class Add__Bls12_381_fr(sp.Contract):
    def __init__(self):
        self.init(fr = sp.none)

    """
    ADD: Add two curve points or field elements.

    :: bls12_381_fr : bls12_381_fr : 'S -> bls12_381_fr : 'S
    """
    @sp.entry_point
    def add(self, pair):
        self.data.fr = sp.some(sp.fst(pair) + sp.snd(pair));

@sp.add_test(name = "Add__BLS12_381_fr")
def test():
    c1 = Add__Bls12_381_fr();

    scenario = sp.test_scenario()
    scenario += c1

    c1.add(
        sp.pair(
            sp.bls12_381_fr("0x2ef123703093cbbbd124e15f2054fa5781ed0b8d092ec3c6e5d76b4ca918a221"),
            sp.bls12_381_fr("0xd30edc8fce6c34442d371da0e24fc3fb83ea957cfea9766c62a531dda98e4b52")
        )
    );
