import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TList(sp.TPair(sp.TAddress, sp.TString)))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TString)
    self.data.push((sp.sender, params))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
