import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TMap(sp.TInt, sp.TMap(sp.TInt, sp.TInt)))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right")), Right = sp.TVariant(Left = sp.TUnit, Right = sp.TUnit).layout(("Left", "Right"))).layout(("Left", "Right")))
    def ferror(error):
      sp.failwith('[Error: to_cmd: [0: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 1: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 2: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 3: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 4: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 5: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 6: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 7: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 8: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int); 9: [0: 0; 1: 0; 2: 0; 3: 0; 4: 0; 5: 0; 6: 0; 7: 0; 8: 0; 9: 0]#map(int,int)]#map(int,map(int, int))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
