open SmartML

module Contract = struct
  let%entry_point setCurrentValue params =
    verify (check_signature data.bossPublicKey params.userSignature (pack {c = data.counter; n = params.newValue; o = data.currentValue}));
    data.currentValue <- params.newValue;
    data.counter <- data.counter + 1

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {bossPublicKey = key; counter = intOrNat; currentValue = string}]
      ~storage:[%expr
                 {bossPublicKey = key "edpku5ZuqYibUQrAhove2B1bZDYCQyDRTGGa1ZwtZYiJ6nvJh4GXcE";
                  counter = 0;
                  currentValue = "Hello World"}]
      [setCurrentValue]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())