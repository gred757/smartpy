import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(m = sp.TMap(sp.TInt, sp.TString), x = sp.TString, y = sp.TOption(sp.TString), z = sp.TOption(sp.TString)).layout((("m", "x"), ("y", "z"))))

  @sp.entry_point
  def test_get_and_update(self, params):
    sp.set_type(params, sp.TUnit)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let [x140; _] =
                              Get_and_update(1, Some_("one"), __storage.m)
                            Pair
                              ( Pair(__storage.m, __storage.x)
                              , Pair(__storage.y, x140)
                              ))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def test_map_get(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('None') as _l2:
        sp.failwith(12)
      with arg.match('Some') as s120:
        x17 = sp.local("x17", ((self.data.m, s120), (self.data.y, self.data.z)))
        x186 = sp.local("x186", x17.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x186)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


  @sp.entry_point
  def test_map_get2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('None') as _l20:
        sp.failwith(17)
      with arg.match('Some') as s98:
        x35 = sp.local("x35", ((self.data.m, s98), (self.data.y, self.data.z)))
        x184 = sp.local("x184", x35.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x184)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


  @sp.entry_point
  def test_map_get_default_values(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., let x75 = __storage.m
                            let [s81; s82; s83] =
                              match Get(12, r68) with
                              | None _ ->
                                  [ "abc"
                                  ; x75
                                  ; Pair(__storage.y, __storage.z)
                                  ]
                              | Some s79 ->
                                  [ s79
                                  ; x75
                                  ; Pair(__storage.y, __storage.z)
                                  ]
                              end
                            Pair(Pair(s82, s81), s83))]')
    sp.failwith(sp.build_lambda(ferror)(sp.unit))

  @sp.entry_point
  def test_map_get_missing_value(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('None') as _l40:
        sp.failwith('missing 12')
      with arg.match('Some') as s54:
        x55 = sp.local("x55", ((self.data.m, s54), (self.data.y, self.data.z)))
        x180 = sp.local("x180", x55.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x180)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


  @sp.entry_point
  def test_map_get_missing_value2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    with sp.some(params[12]).match_cases() as arg:
      with arg.match('None') as _l58:
        sp.failwith(1234)
      with arg.match('Some') as s35:
        x73 = sp.local("x73", ((self.data.m, s35), (self.data.y, self.data.z)))
        x178 = sp.local("x178", x73.value)
    def ferror(error):
      sp.failwith('[Error: to_cmd: record_of_tree(..., x178)]')
        sp.failwith(sp.build_lambda(ferror)(sp.unit))


  @sp.entry_point
  def test_map_get_opt(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data = sp.record(m = self.data.m, x = self.data.x, y = sp.some(params[12]), z = self.data.z)

  @sp.entry_point
  def test_update_map(self, params):
    sp.set_type(params, sp.TUnit)
    self.data = sp.record(m = sp.update_map(self.data.m, 1, sp.some('one')), x = self.data.x, y = self.data.y, z = self.data.z)

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
