import smartpy as sp

tstorage = sp.TRecord(games = sp.TMap(sp.TIntOrNat, sp.TRecord(bound = sp.TInt, claimed = sp.TBool, deck = sp.TMap(sp.TInt, sp.TInt), lastWins = sp.TBool, nextPlayer = sp.TInt, size = sp.TInt, winner = sp.TInt).layout((("bound", ("claimed", "deck")), (("lastWins", "nextPlayer"), ("size", "winner"))))), nbGames = sp.TIntOrNat).layout(("games", "nbGames"))
tparameter = sp.TVariant(build = sp.TRecord(bound = sp.TInt, size = sp.TInt).layout(("bound", "size")), claim = sp.TRecord(gameId = sp.TIntOrNat).layout("gameId"), remove = sp.TRecord(cell = sp.TInt, gameId = sp.TIntOrNat, k = sp.TInt).layout(("cell", ("gameId", "k")))).layout(("build", ("claim", "remove")))
tprivates = { }
tviews = { }
