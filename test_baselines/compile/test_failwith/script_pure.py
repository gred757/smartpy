import smartpy as sp

class C1(sp.Contract):
    def __init__(self):
        self.init(f = sp.none)

    @sp.entry_point
    def ep(self):
        def f(x):
            with sp.if_(x == 0):
                sp.failwith("zero")
            with sp.else_():
                sp.result(1)
        self.data.f = sp.some(f)

class C2(sp.Contract):
    @sp.entry_point
    def ep(self, params):
        with sp.if_(params == 0):
            sp.failwith("zero")
        with sp.else_():
            sp.failwith("non-zero")

@sp.add_test(name = "Test")
def test():
    scenario = sp.test_scenario()
    scenario += C1()
    scenario += C2()
