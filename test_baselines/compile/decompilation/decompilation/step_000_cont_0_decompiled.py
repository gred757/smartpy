import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(Left = sp.TInt, Right = sp.TInt).layout(("Left", "Right")))

  @sp.entry_point
  def ep0(self, params):
    sp.set_type(params, sp.TInt)
    sp.verify(self.data.Left <= 123, 'WrongCondition: self.data.Left <= 123')
    self.data.Left = params + self.data.Left

sp.add_compilation_target("test", Contract())

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
