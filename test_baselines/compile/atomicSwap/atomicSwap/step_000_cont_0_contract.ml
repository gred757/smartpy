open SmartML

module Contract = struct
  let%entry_point allSigned () =
    verify (data.notional <> (tez 0));
    verify (data.owner = sender);
    send data.counterparty data.notional;
    data.notional <- tez 0

  let%entry_point cancelSwap () =
    verify (data.notional <> (tez 0));
    verify (data.owner = sender);
    verify (data.epoch < now);
    send data.owner data.notional;
    data.notional <- tez 0

  let%entry_point knownSecret params =
    verify (data.notional <> (tez 0));
    verify (data.counterparty = sender);
    verify (data.hashedSecret = (blake2b params.secret));
    send data.counterparty data.notional;
    data.notional <- tez 0

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {counterparty = address; epoch = timestamp; hashedSecret = bytes; notional = mutez; owner = address}]
      ~storage:[%expr
                 {counterparty = address "tz1c3GoMtLJgd3pyodoG7BpHdXgbKYVfnK3N";
                  epoch = timestamp 50;
                  hashedSecret = bytes "0x307839304336453830343039324344304138324531443132413043324538324238384544";
                  notional = mutez 12;
                  owner = address "tz1c3GoMtLJgd3pyodoG7BpHdXgbKYVfnK3N"}]
      [allSigned; cancelSwap; knownSecret]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())