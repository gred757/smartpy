open SmartML

module Contract = struct
  let%entry_point createWithoutOrigination params =
    set_type params string;
    let%mutable op = create contract ... in ();
    data.value <- some op.address

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = option address}]
      ~storage:[%expr
                 {value = None}]
      [createWithoutOrigination]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())