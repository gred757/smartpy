/*  This contract is an example of a 'double Snap hook Admin system'

    To update the admin, the current admin must add another admin
    Then the new admin must remove the old one

    This security pattern prevent the admin to specify a non-working account
    as the new administrator.

*/
interface TStorage {
    admins: TSet<TAddress>;
}

@Contract
export class AdminContract {
    storage: TStorage = {
        admins: ['tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm'],
    };

    @EntryPoint
    add(admins: TSet<TAddress>): void {
        Sp.verify(this.storage.admins.contains(Sp.sender));
        this.storage.admins = admins;
        for (const admin of admins.elements()) {
            this.storage.admins.add(admin);
        }
    }

    @EntryPoint
    remove(admins: TSet<TAddress>): void {
        Sp.verify(this.storage.admins.contains(Sp.sender));
        for (const admin of admins.elements()) {
            Sp.verify(admin == Sp.selfAddress);
            this.storage.admins.remove(admin);
        }
    }

    @EntryPoint
    changeAdmins(remove: TOption<TSet<TAddress>>, add: TOption<TSet<TAddress>>): void {
        Sp.verify(this.storage.admins.contains(Sp.sender));
        if (remove.isSome()) {
            for (const admin of remove.openSome().elements()) {
                Sp.verify(admin == Sp.toAddress(Sp.self));
                this.storage.admins.remove(admin);
            }
        }
        if (add.isSome()) {
            for (const admin of add.openSome().elements()) {
                this.storage.admins.add(admin);
            }
        }
    }
}

Dev.test({ name: 'Double Snap hook Admin Contract' }, () => {
    const c1 = Scenario.originate(new AdminContract());
    Scenario.transfer(c1.add(['tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU']), {
        sender: 'tz1aTgF2c3vyrk2Mko1yzkJQGAnqUeDapxxm',
    });
});

Dev.compileContract('compile_contract', new AdminContract());
