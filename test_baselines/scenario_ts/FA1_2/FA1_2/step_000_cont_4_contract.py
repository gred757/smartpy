import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(balances = sp.TBigMap(sp.TAddress, sp.TRecord(approvals = sp.TMap(sp.TAddress, sp.TNat), balance = sp.TNat).layout(("approvals", "balance"))), config = sp.TRecord(admin = sp.TAddress, paused = sp.TBool).layout(("admin", "paused")), metadata = sp.TBigMap(sp.TString, sp.TBytes), token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info"))), totalSupply = sp.TNat).layout(("balances", ("config", ("metadata", ("token_metadata", "totalSupply"))))))
    self.init(balances = {},
              config = sp.record(admin = sp.address('tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU'), paused = False),
              metadata = {},
              token_metadata = {},
              totalSupply = 0)

  @sp.entry_point
  def approve(self, params):
    sp.set_type(params, sp.TRecord(spender = sp.TAddress, value = sp.TNat).layout(("spender", "value")))
    sp.verify(~ self.data.config.paused, 'FA1.2_Paused')
    sp.if ~ (self.data.balances.contains(sp.sender)):
      value = sp.local("value", sp.record(approvals = {}, balance = 0))
      self.data.balances[sp.sender] = value.value
    sp.verify((self.data.balances[sp.sender].approvals.get(params.spender, default_value = 0) == 0) | (params.value == 0), 'FA1.2_UnsafeAllowanceChange')
    self.data.balances[sp.sender].approvals[params.spender] = params.value

  @sp.entry_point
  def burn(self, params):
    sp.set_type(params, sp.TRecord(address = sp.TAddress, value = sp.TNat).layout(("address", "value")))
    sp.verify(sp.sender == self.data.config.admin, 'FA1.2_NotAdmin')
    sp.verify(self.data.balances[params.address].balance >= params.value, 'FA1.2_InsufficientBalance')
    self.data.balances[params.address].balance = sp.as_nat(self.data.balances[params.address].balance - params.value)
    self.data.totalSupply = sp.as_nat(self.data.totalSupply - params.value)

  @sp.entry_point
  def mint(self, params):
    sp.set_type(params, sp.TRecord(address = sp.TAddress, value = sp.TNat).layout(("address", "value")))
    sp.verify(sp.sender == self.data.config.admin, 'FA1.2_NotAdmin')
    sp.if ~ (self.data.balances.contains(params.address)):
      value = sp.local("value", sp.record(approvals = {}, balance = 0))
      self.data.balances[params.address] = value.value
    self.data.balances[params.address].balance += params.value
    self.data.totalSupply += params.value

  @sp.entry_point
  def setAdministrator(self, params):
    sp.set_type(params, sp.TAddress)
    sp.verify(sp.sender == self.data.config.admin, 'FA1.2_NotAdmin')
    self.data.config.admin = params

  @sp.entry_point
  def setPause(self, params):
    sp.set_type(params, sp.TBool)
    sp.verify(sp.sender == self.data.config.admin, 'FA1.2_NotAdmin')
    self.data.config.paused = params

  @sp.entry_point
  def transfer(self, params):
    sp.set_type(params, sp.TRecord(from = sp.TAddress, to = sp.TAddress, value = sp.TNat).layout(("from", ("to", "value"))))
    sp.verify(~ self.data.config.paused, 'FA1.2_Paused')
    sp.verify((params.from == sp.sender) | (self.data.balances[params.from].approvals[sp.sender] >= params.value), 'FA1.2_NotAllowed')
    sp.if ~ (self.data.balances.contains(params.from)):
      value = sp.local("value", sp.record(approvals = {}, balance = 0))
      self.data.balances[params.from] = value.value
    sp.if ~ (self.data.balances.contains(params.to)):
      value = sp.local("value", sp.record(approvals = {}, balance = 0))
      self.data.balances[params.to] = value.value
    sp.verify(self.data.balances[params.from].balance >= params.value, 'FA1.2_InsufficientBalance')
    self.data.balances[params.from].balance = sp.as_nat(self.data.balances[params.from].balance - params.value)
    self.data.balances[params.to].balance += params.value
    sp.if params.from != sp.sender:
      approval = sp.local("approval", sp.as_nat(self.data.balances[params.from].approvals[sp.sender] - params.value))
      self.data.balances[params.from].approvals[sp.sender] = approval.value

  @sp.entry_point
  def updateMetadata(self, params):
    sp.set_type(params, sp.TBigMap(sp.TString, sp.TBytes))
    sp.verify(sp.sender == self.data.config.admin, 'FA1.2_NotAdmin')
    self.data.metadata = params

  @sp.entry_point
  def getAdministrator(self, params):
    sp.set_type(params, sp.TPair(sp.TUnit, sp.TContract(sp.TAddress)))
    sp.transfer(self.data.config.admin, sp.tez(0), sp.snd(params))

  @sp.entry_point
  def getAllowance(self, params):
    sp.set_type(params, sp.TPair(sp.TRecord(owner = sp.TAddress, spender = sp.TAddress).layout(("owner", "spender")), sp.TContract(sp.TNat)))
    allowance = sp.local("allowance", 0)
    sp.if self.data.balances.contains(sp.fst(params).owner):
      allowance.value = self.data.balances[sp.fst(params).owner].approvals.get(sp.fst(params).spender, default_value = 0)
    sp.transfer(allowance.value, sp.tez(0), sp.snd(params))

  @sp.entry_point
  def getBalance(self, params):
    sp.set_type(params, sp.TPair(sp.TAddress, sp.TContract(sp.TNat)))
    balance = sp.local("balance", 0)
    sp.if self.data.balances.contains(sp.fst(params)):
      balance.value = self.data.balances[sp.fst(params)].balance
    sp.transfer(balance.value, sp.tez(0), sp.snd(params))

  @sp.entry_point
  def getTotalSupply(self, params):
    sp.set_type(params, sp.TPair(sp.TUnit, sp.TContract(sp.TNat)))
    sp.transfer(self.data.totalSupply, sp.tez(0), sp.snd(params))

sp.add_compilation_target("test", Contract())