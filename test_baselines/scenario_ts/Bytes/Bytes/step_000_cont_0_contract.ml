open SmartML

module Contract = struct
  let%entry_point concat () =
    let%mutable b1 = (bytes "0x0dae11") in ();
    let%mutable b2 = (bytes "0x0001") in ();
    data.bytes <- some (concat [(bytes "0x0000") + b1; b2])

  let%entry_point size params =
    set_type params bytes;
    data.size <- some (len params)

  let%entry_point slice () =
    data.bytes <- some (open_some (slice (open_some data.bytes ~message:"Error: Empty value") 2 4) ~message:"Error: offset/length invalid")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {bytes = option bytes; size = option nat}]
      ~storage:[%expr
                 {bytes = None;
                  size = None}]
      [concat; size; slice]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())