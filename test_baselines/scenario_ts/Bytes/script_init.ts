interface TStorage {
    bytes: TOption<TBytes>;
    size: TOption<TNat>;
}

@Contract
export class Bytes {
    constructor(public storage: TStorage) {}

    @EntryPoint
    concat(): void {
        const b1: TBytes = '0x0dae11';
        const b2: TBytes = '0x001';
        this.storage.bytes = Sp.some(Sp.concat([('0x000' as TBytes).concat(b1), b2]));
    }

    @EntryPoint
    size(bytes: TBytes): void {
        this.storage.size = Sp.some(bytes.size());
    }

    @EntryPoint
    slice(): void {
        this.storage.bytes = Sp.some(
            this.storage.bytes.openSome('Error: Empty value').slice(2, 4).openSome('Error: offset/length invalid'),
        );
    }
}

Dev.test({ name: 'Bytes' }, () => {
    const c1 = Scenario.originate(new Bytes({ bytes: Sp.none, size: Sp.none }));
    Scenario.transfer(c1.concat());
    Scenario.transfer(c1.size('0x00abc'));
});

Dev.compileContract('bytes_compilation', new Bytes({ bytes: Sp.none, size: Sp.none }));
