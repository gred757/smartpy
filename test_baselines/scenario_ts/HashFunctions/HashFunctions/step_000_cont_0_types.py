import smartpy as sp

tstorage = sp.TOption(sp.TRecord(blake2b = sp.TBytes, keccak = sp.TBytes, sha256 = sp.TBytes, sha3 = sp.TBytes, sha512 = sp.TBytes).layout(("blake2b", ("keccak", ("sha256", ("sha3", "sha512"))))))
tparameter = sp.TVariant(ep = sp.TString).layout("ep")
tprivates = { }
tviews = { }
