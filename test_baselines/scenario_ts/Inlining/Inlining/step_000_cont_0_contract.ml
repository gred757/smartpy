open SmartML

module Contract = struct
  let%entry_point ep1 () =
    let%mutable i = 0 in ();
    while i < 10 do
      i <- i + (i + 1);
      let%mutable ii = 0 in ();
      while ii < 10 do
        data.value <- data.value + ((i + ii) + 1);
        ii <- ii + 1
      done;
      i <- i + 1
    done

  let%entry_point ep2 () =
    if (data.value + 1) > 2 then
      data.value <- 0

  let%entry_point ep3 () =
    data.value <- fst (open_some (ediv (mutez 10) (mutez 1)) ~message:"Failed to divide mutez")

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = nat}]
      ~storage:[%expr
                 {value = 1}]
      [ep1; ep2; ep3]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())