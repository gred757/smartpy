open SmartML

module Contract = struct
  let%entry_point transfer params =
    set_type params (list {from_ = address; txs = list {amount = nat; to_ = address; token_id = nat}});
    verify (not data.config.paused) ~msg:"FA2_PAUSED";
    List.iter (fun transaction ->
      List.iter (fun tx ->
        verify (contains tx.token_id data.assets.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
        verify ((transaction.from_ = sender) || (contains {owner = transaction.from_; operator = sender; token_id = tx.token_id} data.assets.operators)) ~msg:"FA2_NOT_OPERATOR";
        if tx.amount > 0 then
          (
            let%mutable sourceLedger = (transaction.from_, tx.token_id) in ();
            let%mutable recipientLedger = (tx.to_, tx.token_id) in ();
            verify ((Map.get data.assets.ledger sourceLedger).balance >= tx.amount) ~msg:"FA2_INSUFFICIENT_BALANCE";
            let%mutable newBalance = ((Map.get data.assets.ledger sourceLedger).balance - tx.amount) in ();
            (Map.get data.assets.ledger sourceLedger).balance <- open_some (is_nat newBalance);
            if contains recipientLedger data.assets.ledger then
              (Map.get data.assets.ledger recipientLedger).balance <- (Map.get data.assets.ledger recipientLedger).balance + tx.amount
            else
              Map.set data.assets.ledger recipientLedger {balance = tx.amount}
          )
      ) transaction.txs
    ) params

  let%entry_point mint params =
    set_type params.tokenId nat;
    set_type params.address address;
    set_type params.amount nat;
    set_type params.metadata (map string bytes);
    verify (sender = data.config.admin) ~msg:"FA2_NOT_ADMIN";
    let%mutable user = (params.address, params.tokenId) in ();
    if contains user data.assets.ledger then
      (Map.get data.assets.ledger user).balance <- (Map.get data.assets.ledger user).balance + params.amount
    else
      Map.set data.assets.ledger user {balance = params.amount};
    if not (contains params.tokenId data.assets.token_metadata) then
      Map.set data.assets.token_metadata params.tokenId {token_id = params.tokenId; token_info = params.metadata};
    Map.set data.assets.token_total_supply params.tokenId ((get ~default_value:%s data.assets.token_total_supply params.tokenId 0) + params.amount)

  let%entry_point update_operators params =
    set_type params (list (`add_operator {operator = address; owner = address; token_id = nat} + `remove_operator {operator = address; owner = address; token_id = nat}));
    verify (not data.config.paused) ~msg:"FA2_PAUSED";
    List.iter (fun update ->
      match update with
        | `add_operator add_operator ->
          verify (add_operator.owner = sender) ~msg:"FA2_NOT_ADMIN_OR_OWNER";
          Map.set data.assets.operators add_operator ()
        | `remove_operator remove_operator ->
          verify (remove_operator.owner = sender) ~msg:"FA2_NOT_ADMIN_OR_OWNER";
          Map.delete data.assets.operators remove_operator

    ) params

  let%entry_point balance_of params =
    set_type params {callback = contract (list {balance = nat; request = {owner = address; token_id = nat}}); requests = list {owner = address; token_id = nat}};
    verify (not data.config.paused) ~msg:"FA2_PAUSED";
    let%mutable responses = [] in ();
    List.iter (fun request ->
      verify (contains request.token_id data.assets.token_metadata) ~msg:"FA2_TOKEN_UNDEFINED";
      if contains (request.owner, request.token_id) data.assets.ledger then
        responses <- {request = request; balance = (Map.get data.assets.ledger (request.owner, request.token_id)).balance} :: responses
      else
        responses <- {request = request; balance = 0} :: responses
    ) params.requests;
    transfer responses (tez 0) params.callback

  let%entry_point pause params =
    set_type params bool;
    data.config.paused <- params

  let%entry_point set_admin params =
    set_type params address;
    data.config.admin <- params

  let%entry_point update_metadata params =
    set_type params (map string bytes);
    List.iter (fun entry ->
      Map.set data.metadata entry.key entry.value
    ) (Map.items params)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {assets = {ledger = big_map (pair address nat) {balance = nat}; operators = big_map {operator = address; owner = address; token_id = nat} unit; token_metadata = big_map nat {token_id = nat; token_info = map string bytes}; token_total_supply = big_map nat nat}; config = {admin = address; paused = bool}; metadata = big_map string bytes}]
      ~storage:[%expr
                 {assets = {ledger = Map.make []; operators = Map.make []; token_metadata = Map.make []; token_total_supply = Map.make []};
                  config = {admin = address "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w"; paused = false};
                  metadata = Map.make [("name", bytes "0x54686520546f6b656e205a65726f")]}]
      [transfer; mint; update_operators; balance_of; pause; set_admin; update_metadata]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())