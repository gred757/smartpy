open SmartML

module Contract = struct

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ int]
      ~storage:[%expr 1]
      []
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())