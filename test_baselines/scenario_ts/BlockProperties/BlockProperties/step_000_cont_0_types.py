import smartpy as sp

tstorage = sp.TRecord(amount = sp.TOption(sp.TMutez), balance = sp.TOption(sp.TMutez), chainId = sp.TOption(sp.TChainId), level = sp.TOption(sp.TNat), now = sp.TOption(sp.TTimestamp), sender = sp.TOption(sp.TAddress), source = sp.TOption(sp.TAddress), totalVotingPower = sp.TOption(sp.TNat), votingPower = sp.TOption(sp.TNat)).layout(("amount", ("balance", ("chainId", ("level", ("now", ("sender", ("source", ("totalVotingPower", "votingPower")))))))))
tparameter = sp.TVariant(ep = sp.TUnit, epFail = sp.TUnit).layout(("ep", "epFail"))
tprivates = { }
tviews = { }
