type TStorage = {
    amount: TOption<TMutez>;
    balance: TOption<TMutez>;
    sender: TOption<TAddress>;
    source: TOption<TAddress>;
    chainId: TOption<TChain_id>;
    now: TOption<TTimestamp>;
    level: TOption<TNat>;
    totalVotingPower: TOption<TNat>;
    votingPower: TOption<TNat>;
};

@Contract
export class BlockProperties {
    storage: TStorage = {
        amount: Sp.none,
        balance: Sp.none,
        sender: Sp.none,
        source: Sp.none,
        chainId: Sp.none,
        now: Sp.none,
        level: Sp.none,
        totalVotingPower: Sp.none,
        votingPower: Sp.none,
    };

    @EntryPoint
    ep(): void {
        this.storage.amount = Sp.some(Sp.amount);
        this.storage.balance = Sp.some(Sp.balance);
        this.storage.sender = Sp.some(Sp.sender);
        this.storage.source = Sp.some(Sp.source);
        this.storage.chainId = Sp.some(Sp.chainId);
        this.storage.now = Sp.some(Sp.now);
        this.storage.level = Sp.some(Sp.level);
        this.storage.totalVotingPower = Sp.some(Sp.totalVotingPower);
        this.storage.votingPower = Sp.some(Sp.votingPower('tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w' as TKey_hash));
    }

    @EntryPoint
    epFail(): void {
        Sp.failWith('FAIL ON PURPOSE');
    }
}

Dev.test({ name: 'BlockProperties' }, () => {
    const c1 = Scenario.originate(new BlockProperties());
    Scenario.transfer(c1.ep(), {
        sender: 'tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w',
        source: 'KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1',
        level: 100,
        now: 1000009,
        amount: 999,
        chainId: '0xa1',
        votingPowers: [['tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w', 10]],
    });

    Scenario.transfer(c1.epFail(), {
        valid: false,
        exception: 'FAIL ON PURPOSE',
    });
});

Dev.compileContract('Hash Functions', new BlockProperties());
