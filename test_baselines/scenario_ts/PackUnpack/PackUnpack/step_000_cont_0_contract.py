import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(packed = sp.TOption(sp.TBytes), unpacked = sp.TOption(sp.TRecord(value = sp.TNat).layout("value"))).layout(("packed", "unpacked")))
    self.init(packed = sp.none,
              unpacked = sp.none)

  @sp.entry_point
  def pack(self, params):
    sp.set_type(params, sp.TRecord(value = sp.TNat).layout("value"))
    self.data.packed = sp.some(sp.pack(params))

  @sp.entry_point
  def unpack(self):
    sp.if self.data.packed.is_some():
      self.data.unpacked = sp.unpack(self.data.packed.open_some(message = 'this.storage.packed is None'), sp.TRecord(value = sp.TNat).layout("value"))

sp.add_compilation_target("test", Contract())