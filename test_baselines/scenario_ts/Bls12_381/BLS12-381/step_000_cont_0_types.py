import smartpy as sp

tstorage = sp.TRecord(checkResult = sp.TOption(sp.TBool), fr = sp.TBls12_381_fr, g1 = sp.TBls12_381_g1, g2 = sp.TBls12_381_g2, mulResult = sp.TOption(sp.TBls12_381_fr)).layout(("checkResult", ("fr", ("g1", ("g2", "mulResult")))))
tparameter = sp.TVariant(add = sp.TRecord(fr = sp.TBls12_381_fr, g1 = sp.TBls12_381_g1, g2 = sp.TBls12_381_g2).layout(("fr", ("g1", "g2"))), mul = sp.TPair(sp.TBls12_381_fr, sp.TBls12_381_fr), negate = sp.TUnit, pairing_check = sp.TList(sp.TPair(sp.TBls12_381_g1, sp.TBls12_381_g2)), toInt = sp.TUnit).layout((("add", "mul"), ("negate", ("pairing_check", "toInt"))))
tprivates = { }
tviews = { }
