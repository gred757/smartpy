open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params nat;
    data <- (params constant("expru54tk2k4E81xQy63P6x3RijnTz51s2m7BV7pr3fDQH8YDqiYvR", t = lambda nat nat)) + constant("exprtX5EDHZJaBbSmnq14wSwcadxvXJDk5Du7eUpbmMgQFPeGsaV2M", t = nat)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ nat]
      ~storage:[%expr 0]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())