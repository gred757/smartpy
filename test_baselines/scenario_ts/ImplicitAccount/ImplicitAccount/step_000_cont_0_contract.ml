open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params key_hash;
    verify ((address "tz1hdQscorfqMzFqYxnrApuS5i6QSTuoAp3w") = (to_address (implicit_account params)))

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())