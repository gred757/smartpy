import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(storedValue = sp.TNat).layout("storedValue"))
    self.init(storedValue = 1)

  @sp.entry_point
  def replace(self, params):
    sp.set_type(params, sp.TRecord(value = sp.TNat).layout("value"))
    self.data.storedValue = params.value

  @sp.entry_point
  def double(self):
    self.data.storedValue *= 2

  @sp.entry_point
  def divide(self, params):
    sp.set_type(params, sp.TRecord(divisor = sp.TNat).layout("divisor"))
    self.data.storedValue //= params.divisor

sp.add_compilation_target("test", Contract())