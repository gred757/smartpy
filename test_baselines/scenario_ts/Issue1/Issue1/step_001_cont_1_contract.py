import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TAddress)
    self.init(sp.address('KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1'))

  @sp.entry_point
  def unpack(self):
    self.data = sp.unpack(sp.pack(sp.address('tz1')), sp.TAddress).open_some(message = 'Could not extract unpacked data.')

sp.add_compilation_target("test", Contract())