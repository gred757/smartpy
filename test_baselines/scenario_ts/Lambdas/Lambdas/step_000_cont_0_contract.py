import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(decrement = sp.TLambda(sp.TNat, sp.TInt), increment = sp.TLambda(sp.TNat, sp.TNat), storedValue = sp.TNat, transformer = sp.TLambda(sp.TNat, sp.TNat)).layout(("decrement", ("increment", ("storedValue", "transformer")))))
    self.init(decrement = sp.build_lambda(lambda value: value - 1),
              increment = sp.build_lambda(lambda value: value + 1),
              storedValue = 1,
              transformer = lambda(sp.TLambda(sp.TNat, sp.TNat)))

  @sp.entry_point
  def store(self, params):
    sp.set_type(params.value, sp.TNat)
    sp.set_type(params.transformer2, sp.TLambda(sp.TNat, sp.TNat))
    def fvalue(value):
      sp.if value > 5:
        sp.result(2)
      sp.else:
        sp.result(value)
    transformer3 = sp.local("transformer3", sp.build_lambda(fvalue))
    self.data.storedValue = self.data.increment(params.value)
    self.data.storedValue = self.data.transformer(self.data.storedValue)
    def fvalue(value):
      sp.if value > 5:
        sp.result(1)
      sp.else:
        sp.result(value)
    self.data.storedValue = sp.build_lambda(fvalue)(self.data.storedValue)
    self.data.storedValue = params.transformer2(self.data.storedValue)
    self.data.storedValue = transformer3.value(self.data.storedValue)
    var_0 = sp.local("var_0", self.transformer4(self.data.storedValue))
    self.data.storedValue = var_0.value
    var_1 = sp.local("var_1", self.transformer5(self.data.storedValue))
    self.data.storedValue = var_1.value
    self.data.storedValue = sp.as_nat(self.data.decrement(self.data.storedValue))
    var_2 = sp.local("var_2", self.callback(sp.unit))
    sp.verify(var_2.value == sp.unit)

  @sp.entry_point
  def default(self):
    self.data.storedValue = 10

  @sp.private_lambda()
  def transformer4(value):
    sp.result(value + 5)

  @sp.private_lambda()
  def transformer5(value):
    sp.result(value + self.data.storedValue)

  @sp.private_lambda()
  def callback(_x5):
    contract = sp.local("contract", sp.contract(sp.TUnit, sp.self_address, entry_point='default').open_some())
    sp.transfer(sp.unit, sp.tez(0), contract.value)

sp.add_compilation_target("test", Contract())