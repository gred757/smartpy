open SmartML

module Contract = struct
  let%entry_point store params =
    set_type params.value nat;
    set_type params.transformer2 lambda nat nat;
    let%mutable transformer3 = (fun value -> if value > 5 then
  result 2
else
  result value) in ();
    data.storedValue <- params.value data.increment;
    data.storedValue <- data.storedValue data.transformer;
    data.storedValue <- data.storedValue (fun value -> if value > 5 then
  result 1
else
  result value);
    data.storedValue <- data.storedValue params.transformer2;
    data.storedValue <- data.storedValue transformer3;
    let%mutable var_0 = (data.storedValue self.transformer4) in ();
    data.storedValue <- var_0;
    let%mutable var_1 = (data.storedValue self.transformer5) in ();
    data.storedValue <- var_1;
    data.storedValue <- open_some (is_nat (data.storedValue data.decrement));
    let%mutable var_2 = (() self.callback) in ();
    verify (var_2 = ())

  let%entry_point default () =
    data.storedValue <- 10

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {decrement = lambda nat int; increment = lambda nat nat; storedValue = nat; transformer = lambda nat nat}]
      ~storage:[%expr
                 {decrement = lambda(lambda nat int);
                  increment = lambda(lambda nat nat);
                  storedValue = 1;
                  transformer = lambda(lambda nat nat)}]
      [store; default]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())