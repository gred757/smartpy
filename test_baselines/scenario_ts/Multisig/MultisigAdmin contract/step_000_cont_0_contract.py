import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(activeProposals = sp.TSet(sp.TNat), lastProposalId = sp.TNat, proposals = sp.TBigMap(sp.TNat, sp.TRecord(actions = sp.TVariant(external = sp.TList(sp.TRecord(actions = sp.TBytes, target = sp.TAddress).layout(("actions", "target"))), internal = sp.TList(sp.TVariant(changeQuorum = sp.TNat, changeSigners = sp.TVariant(added = sp.TList(sp.TRecord(address = sp.TAddress, publicKey = sp.TKey).layout(("address", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout(("changeQuorum", "changeSigners")))).layout(("external", "internal")), endorsements = sp.TSet(sp.TAddress), initiator = sp.TAddress, startedAt = sp.TTimestamp).layout(("actions", ("endorsements", ("initiator", "startedAt"))))), quorum = sp.TNat, signers = sp.TMap(sp.TAddress, sp.TRecord(lastProposalId = sp.TOption(sp.TNat), publicKey = sp.TKey).layout(("lastProposalId", "publicKey")))).layout(("activeProposals", ("lastProposalId", ("proposals", ("quorum", "signers"))))))
    self.init(activeProposals = sp.set([]),
              lastProposalId = 0,
              proposals = {},
              quorum = 3,
              signers = {sp.address('tz1L3Pgin3YSeqZJb6UF9Fis9LBjp3L1YusM') : sp.record(lastProposalId = sp.none, publicKey = sp.key('edpkub2iNvM3KnaccBaHujEz93Naj3ySBvAoEFLELjaLXk21otyTyT')), sp.address('tz1M43YV5vVt7HALGJeWgQ1GRQfiGExA1Jbf') : sp.record(lastProposalId = sp.none, publicKey = sp.key('edpkvF1HmWAdtpqPAc27pNRpNaYoF3vp9cgWXbhmC45RHN1JWh1f4z'))})

  @sp.entry_point
  def proposal(self, params):
    sp.set_type(params, sp.TVariant(external = sp.TList(sp.TRecord(actions = sp.TBytes, target = sp.TAddress).layout(("actions", "target"))), internal = sp.TList(sp.TVariant(changeQuorum = sp.TNat, changeSigners = sp.TVariant(added = sp.TList(sp.TRecord(address = sp.TAddress, publicKey = sp.TKey).layout(("address", "publicKey"))), removed = sp.TSet(sp.TAddress)).layout(("added", "removed"))).layout(("changeQuorum", "changeSigners")))).layout(("external", "internal")))
    sp.verify(self.data.signers.contains(sp.sender), 'Not_Signer')
    signerLastProposalId = sp.local("signerLastProposalId", self.data.signers[sp.sender].lastProposalId)
    sp.if signerLastProposalId.value != sp.none:
      self.data.activeProposals.remove(signerLastProposalId.value.open_some())
    self.data.lastProposalId += 1
    proposalId = sp.local("proposalId", self.data.lastProposalId)
    self.data.activeProposals.add(proposalId.value)
    self.data.proposals[proposalId.value] = sp.record(actions = params, endorsements = sp.set([sp.sender]), initiator = sp.sender, startedAt = sp.now)
    self.data.signers[sp.sender].lastProposalId = sp.some(proposalId.value)
    sp.if self.data.quorum < 2:
      var_0 = sp.local("var_0", self.onApproved(params))

  @sp.private_lambda()
  def onApproved(actions):
    with actions.match_cases() as arg:
      with arg.match('internal') as internal:
        sp.for action in internal:
          with action.match_cases() as arg:
            with arg.match('changeQuorum') as changeQuorum:
              self.data.quorum = changeQuorum
            with arg.match('changeSigners') as changeSigners:
              changeSignersAction = sp.local("changeSignersAction", changeSigners)
              with changeSignersAction.value.match_cases() as arg:
                with arg.match('removed') as removed:
                  removeSet = sp.local("removeSet", removed)
                  sp.for address in removeSet.value.elements():
                    del self.data.signers[address]
                with arg.match('added') as added:
                  addList = sp.local("addList", added)
                  sp.for signer in addList.value:
                    self.data.signers[signer.address] = sp.record(lastProposalId = sp.none, publicKey = signer.publicKey)


          sp.verify(self.data.quorum <= sp.len(self.data.signers), 'MoreQuorumThanSigners')
        self.data.activeProposals = sp.set([])
      with arg.match('external') as external:
        sp.for action in external:
          target = sp.local("target", sp.contract(sp.TBytes, action.target).open_some(message = 'Invalid Interface'))
          sp.transfer(action.actions, sp.tez(0), target.value)


sp.add_compilation_target("test", Contract())