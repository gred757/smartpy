open SmartML

module Contract = struct

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {tokens = big_map nat {balance = nat}}]
      ~storage:[%expr
                 {tokens = Map.make [(0, {balance = 11})]}]
      []
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())