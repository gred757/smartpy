import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(tokens = sp.TBigMap(sp.TNat, sp.TRecord(balance = sp.TNat).layout("balance"))).layout("tokens"))
    self.init(tokens = {0 : sp.record(balance = 11)})

sp.add_compilation_target("test", Contract())