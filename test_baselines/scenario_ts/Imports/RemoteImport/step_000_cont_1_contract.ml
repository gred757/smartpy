open SmartML

module Contract = struct
  let%entry_point ep params =
    set_type params nat;
    data.value <- params

  let%entry_point setValue params =
    set_type params nat;
    data.value <- params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {value = nat}]
      ~storage:[%expr
                 {value = 1}]
      [ep; setValue]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())