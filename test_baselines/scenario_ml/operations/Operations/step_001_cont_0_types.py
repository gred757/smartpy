import smartpy as sp

tstorage = sp.TRecord(x = sp.TIntOrNat, y = sp.TIntOrNat).layout(("x", "y"))
tparameter = sp.TVariant(test_commands = sp.TRecord(amount = sp.TMutez, delegate = sp.TOption(sp.TKeyHash), destination = sp.TContract(sp.TIntOrNat)).layout(("amount", ("delegate", "destination"))), test_create_contract = sp.TUnit, test_set_delegate = sp.TOption(sp.TKeyHash), test_transfer = sp.TRecord(amount = sp.TMutez, destination = sp.TContract(sp.TIntOrNat)).layout(("amount", "destination"))).layout((("test_commands", "test_create_contract"), ("test_set_delegate", "test_transfer")))
tprivates = { }
tviews = { }
