open SmartML

module Contract = struct
  let%entry_point collateralize params =
    verify (sender = data.admin);
    data.collateral <- data.collateral + params

  let%entry_point delegate params =
    verify (sender = data.admin);
    verify (amount = (tez 0));
    set_delegate params

  let%entry_point deposit params =
    verify (data.rate >= params.rate);
    verify (data.duration <= params.duration);
    verify (not (contains sender data.ledger));
    data.collateral <- data.collateral - (split_tokens amount data.rate 10000);
    verify (data.collateral >= (tez 0));
    Map.set data.ledger sender {amount = (amount + (split_tokens amount data.rate 10000)); due = (add_seconds now ((data.duration * 24) * 3600))}

  let%entry_point setOffer params =
    verify (sender = data.admin);
    data.rate <- params.rate;
    data.duration <- params.duration

  let%entry_point uncollateralize params =
    verify (sender = data.admin);
    data.collateral <- data.collateral - params;
    verify (data.collateral >= (tez 0))

  let%entry_point withdraw () =
    verify (now >= (Map.get data.ledger sender).due);
    send sender (Map.get data.ledger sender).amount;
    Map.delete data.ledger sender

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {admin = address; collateral = mutez; duration = int; ledger = map address {amount = mutez; due = timestamp}; rate = nat}]
      ~storage:[%expr
                 {admin = address "tz1UyQDepgtUBnWjyzzonqeDwaiWoQzRKSP5";
                  collateral = tez 0;
                  duration = 365;
                  ledger = Map.make [];
                  rate = 700}]
      [collateralize; delegate; deposit; setOffer; uncollateralize; withdraw]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())