open SmartML

module Contract = struct
  let%entry_point vote params =
    set_type params.vote string;
    data.votes <- {sender = sender; vote = params.vote} :: data.votes

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {votes = list {sender = address; vote = string}}]
      ~storage:[%expr
                 {votes = []}]
      [vote]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())