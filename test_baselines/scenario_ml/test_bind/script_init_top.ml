#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/test_bind.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/scenario_ml/test_bind/scenario.json"
else
  exit 1;;