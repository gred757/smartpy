import smartpy as sp

tstorage = sp.TRecord(abcd = sp.TInt, value = sp.TIntOrNat).layout(("abcd", "value"))
tparameter = sp.TVariant(abs_test = sp.TInt, f = sp.TUnit).layout(("abs_test", "f"))
tprivates = { "flam": sp.TLambda(sp.TIntOrNat, sp.TIntOrNat), "square_root": sp.TLambda(sp.TNat, sp.TNat), "abs": sp.TLambda(sp.TInt, sp.TInt) }
tviews = { }
