#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/py_check_dfs.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/scenario_ml/py_check_dfs/scenario.json"
else
  exit 1;;