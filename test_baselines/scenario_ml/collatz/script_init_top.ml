#directory "+compiler-libs";;
let r = Toploop.run_script Format.std_formatter "ml_templates/collatz.ml" [||] in
if r then
  SmartML.Target.dump "test_baselines/scenario_ml/collatz/scenario.json"
else
  exit 1;;