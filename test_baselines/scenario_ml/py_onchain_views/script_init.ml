open SmartML

module Contract = struct
  let%entry_point ep2 () =
    data.y <- data.y * 2

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {x = intOrNat; y = intOrNat; z = string}]
      ~storage:[%expr
                 {x = 2;
                  y = 1;
                  z = "ABC"}]
      [ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())