open SmartML

module Contract = struct
  let%entry_point ep () =
    let%mutable x = {a = 1; b = 2} in ();
    x.a <- 15

  let%entry_point ep2 params =
    set_type params (pair int nat)

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ unit]
      ~storage:[%expr ()]
      [ep; ep2]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())