open SmartML

module Contract = struct
  let%entry_point new_key params =
    data.tz1 <- hash_key params

  let%entry_point new_value params =
    data.v <- params;
    data.b2b <- blake2b params;
    data.s256 <- sha256 params;
    data.s512 <- sha512 params;
    data.sha3 <- sha3 params;
    data.keccak <- keccak params

  let init () =
    Basics.build_contract
      ~tstorage_explicit:[%typ {b2b = bytes; keccak = bytes; s256 = bytes; s512 = bytes; sha3 = bytes; tz1 = key_hash; v = bytes}]
      ~storage:[%expr
                 {b2b = bytes "0x";
                  keccak = bytes "0x";
                  s256 = bytes "0x";
                  s512 = bytes "0x";
                  sha3 = bytes "0x";
                  tz1 = key_hash "tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr";
                  v = bytes "0x"}]
      [new_key; new_value]
end

let () =
  Target.register_compilation
    ~name:"contract"
    ~is_default:true
    (Contract.init ())