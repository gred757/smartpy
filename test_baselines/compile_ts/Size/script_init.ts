interface TStorage {
    string: TString;
    bytes: TBytes;
    map: TMap<TString, TBytes>;
    set: TSet<TString>;
    list: TList<TNat>;
}

@Contract
export class Size {
    storage: TStorage = {
        string: 'A STRING',
        bytes: '0x012345',
        map: [['String', '0x00']] as TMap<TString, TBytes>,
        set: ['String', 'String2'],
        list: [1, 2],
    };

    @EntryPoint
    checkSizes(): void {
        Sp.verify(this.storage.string.size() === 8, 'String size is not 8.');
        Sp.verify(this.storage.bytes.size() === 3, 'Bytes size is not 3.');
        Sp.verify(this.storage.map.size() === 1, 'Map size is not 1.');
        Sp.verify(this.storage.set.size() === 2, 'Set size is not 2.');
        Sp.verify(this.storage.list.size() === 2, 'List size is not 2.');
    }
}

Dev.test({ name: 'Size' }, () => {
    const c1 = Scenario.originate(new Size());
    Scenario.transfer(c1.checkSizes());
});

Dev.compileContract('compile_contract', new Size());
