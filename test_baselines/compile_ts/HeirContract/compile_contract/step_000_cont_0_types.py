import smartpy as sp

tstorage = sp.TRecord(heir = sp.TAddress, owner = sp.TAddress, timeout = sp.TOption(sp.TTimestamp)).layout(("heir", ("owner", "timeout")))
tparameter = sp.TVariant(alive = sp.TUnit, heirWithdraw = sp.TRecord(amount = sp.TMutez, to_ = sp.TAddress).layout(("amount", "to_")), withdraw = sp.TRecord(amount = sp.TMutez, to_ = sp.TAddress).layout(("amount", "to_"))).layout(("alive", ("heirWithdraw", "withdraw")))
tprivates = { }
tviews = { }
