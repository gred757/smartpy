import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(storedValue = sp.TNat).layout("storedValue"))
    self.init(storedValue = 10)

  @sp.entry_point
  def check(self):
    sp.verify(self.data.storedValue == 1)

sp.add_compilation_target("test", Contract())