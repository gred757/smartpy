import smartpy as sp

tstorage = sp.TRecord(currentDelegate = sp.TOption(sp.TKeyHash), nominatedDelegates = sp.TSet(sp.TKeyHash)).layout(("currentDelegate", "nominatedDelegates"))
tparameter = sp.TVariant(testDelegate = sp.TKeyHash).layout("testDelegate")
tprivates = { }
tviews = { }
