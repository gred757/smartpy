import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TOption(sp.TAddress)).layout("value"))
    self.init(value = sp.none)

  @sp.entry_point
  def create(self, params):
    sp.set_type(params, sp.TString)
    var_0 = sp.local("var_0", create contract ...)
    sp.operations().push(var_0.value.operation)
    var_1 = sp.local("var_1", create contract ...)
    sp.operations().push(var_1.value.operation)
    self.data.value = sp.some(var_1.value.address)

sp.add_compilation_target("test", Contract())