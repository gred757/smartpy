import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TNat).layout("value"))
    self.init(value = 1)

  @sp.entry_point
  def ep(self, params):
    sp.set_type(params, sp.TTimestamp)
    self.data.value = abs(sp.timestamp(110) - sp.timestamp(0))
    self.data.value = abs(params - sp.timestamp(0))

sp.add_compilation_target("test", Contract())