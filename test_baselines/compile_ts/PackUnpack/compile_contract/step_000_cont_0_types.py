import smartpy as sp

tstorage = sp.TRecord(packed = sp.TOption(sp.TBytes), unpacked = sp.TOption(sp.TRecord(value = sp.TNat).layout("value"))).layout(("packed", "unpacked"))
tparameter = sp.TVariant(pack = sp.TRecord(value = sp.TNat).layout("value"), unpack = sp.TUnit).layout(("pack", "unpack"))
tprivates = { }
tviews = { }
