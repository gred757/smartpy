import smartpy as sp

tstorage = sp.TRecord(counter1 = sp.TInt, counter2 = sp.TInt, counter3 = sp.TNat).layout(("counter1", ("counter2", "counter3")))
tparameter = sp.TVariant(div = sp.TUnit, equal = sp.TUnit, minus = sp.TUnit, mul = sp.TUnit, plus = sp.TUnit).layout((("div", "equal"), ("minus", ("mul", "plus"))))
tprivates = { }
tviews = { }
