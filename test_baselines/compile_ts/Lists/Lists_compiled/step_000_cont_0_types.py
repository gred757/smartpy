import smartpy as sp

tstorage = sp.TRecord(list = sp.TList(sp.TString)).layout("list")
tparameter = sp.TVariant(concat = sp.TList(sp.TString), push = sp.TString, reverse = sp.TUnit).layout(("concat", ("push", "reverse")))
tprivates = { }
tviews = { }
