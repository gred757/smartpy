import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(list = sp.TList(sp.TString)).layout("list"))
    self.init(list = [])

  @sp.entry_point
  def push(self, params):
    sp.set_type(params, sp.TString)
    self.data.list.push(params)

  @sp.entry_point
  def concat(self, params):
    sp.set_type(params, sp.TList(sp.TString))
    self.data.list.push(sp.concat(params))

  @sp.entry_point
  def reverse(self):
    sp.if sp.len(self.data.list) == 4:
      self.data.list = self.data.list.rev()

sp.add_compilation_target("test", Contract())