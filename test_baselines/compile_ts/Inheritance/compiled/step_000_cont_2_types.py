import smartpy as sp

tstorage = sp.TRecord(paused = sp.TBool, value = sp.TOption(sp.TNat)).layout(("paused", "value"))
tparameter = sp.TVariant(ep1 = sp.TUnit, ep2 = sp.TNat).layout(("ep1", "ep2"))
tprivates = { }
tviews = { }
