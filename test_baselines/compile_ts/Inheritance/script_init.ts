interface StorageSpec {
    paused: TBool;
    value: TOption<TNat>;
}

class BaseStorage {
    constructor(
        public storage: StorageSpec = {
            paused: false,
            value: Sp.none,
        },
    ) {}
}

class B extends BaseStorage {
    @EntryPoint
    ep1() {
        Sp.verify(!this.storage.paused);
    }
}

@Contract
export class A extends B {
    @EntryPoint
    ep2(value: TNat): void {
        this.storage.value = Sp.some(value);
    }
}

Dev.test({ name: 'A' }, () => {
    const c1 = Scenario.originate(new A());
    Scenario.transfer(c1.ep2(1));
});

Dev.compileContract(
    'compiled',
    new A({
        paused: true,
        value: Sp.none,
    }),
);
