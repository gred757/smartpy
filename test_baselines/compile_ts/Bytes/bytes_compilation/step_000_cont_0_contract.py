import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(bytes = sp.TOption(sp.TBytes), size = sp.TOption(sp.TNat)).layout(("bytes", "size")))
    self.init(bytes = sp.none,
              size = sp.none)

  @sp.entry_point
  def concat(self):
    b1 = sp.local("b1", sp.bytes('0x0dae11'))
    b2 = sp.local("b2", sp.bytes('0x0001'))
    self.data.bytes = sp.some(sp.concat(sp.list([sp.bytes('0x0000') + b1.value, b2.value])))

  @sp.entry_point
  def size(self, params):
    sp.set_type(params, sp.TBytes)
    self.data.size = sp.some(sp.len(params))

  @sp.entry_point
  def slice(self):
    self.data.bytes = sp.some(sp.slice(self.data.bytes.open_some(message = 'Error: Empty value'), 2, 4).open_some(message = 'Error: offset/length invalid'))

sp.add_compilation_target("test", Contract())