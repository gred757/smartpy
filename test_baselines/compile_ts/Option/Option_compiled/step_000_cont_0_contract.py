import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(value = sp.TOption(sp.TNat)).layout("value"))
    self.init(value = sp.none)

  @sp.entry_point
  def set(self, params):
    sp.set_type(params, sp.TNat)
    sp.if ~ self.data.value.is_some():
      self.data.value = sp.some(params)

  @sp.entry_point
  def openAndAdd(self, params):
    sp.set_type(params, sp.TNat)
    value = sp.local("value", self.data.value.open_some(message = 'Error: is None'))
    sp.if value.value == 10:
      self.data.value = sp.some(value.value + params)

sp.add_compilation_target("test", Contract())