import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(publicKey = sp.TKey).layout("publicKey"))
    self.init(publicKey = sp.key('edpkufVmvzkm4oFQ7WcF5NJbq9BFB2mWRsm4Dyh2spMDuDxWSQWHuT'))

  @sp.entry_point
  def checkSignature(self, params):
    sp.set_type(params, sp.TSignature)
    sp.verify(sp.check_signature(self.data.publicKey, params, sp.bytes('0x00')), 'Signature doesn't match.')

sp.add_compilation_target("test", Contract())