import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(assets = sp.TRecord(ledger = sp.TBigMap(sp.TPair(sp.TAddress, sp.TNat), sp.TRecord(balance = sp.TNat).layout("balance")), operators = sp.TBigMap(sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), sp.TUnit), token_metadata = sp.TBigMap(sp.TNat, sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes)).layout(("token_id", "token_info"))), token_total_supply = sp.TBigMap(sp.TNat, sp.TNat)).layout((("ledger", "operators"), ("token_metadata", "token_total_supply"))), config = sp.TRecord(admin = sp.TAddress, paused = sp.TBool).layout(("admin", "paused")), metadata = sp.TBigMap(sp.TString, sp.TBytes)).layout(("assets", ("config", "metadata"))))
    self.init(assets = sp.record(ledger = {}, operators = {}, token_metadata = {}, token_total_supply = {}),
              config = sp.record(admin = sp.address('<admin address>'), paused = False),
              metadata = {'' : sp.bytes('0x697066733a2f2f516d616941556a3146464e4759547538724c426a633365654e3963534b7761463845474d424e446d687a504e4664')})

  @sp.entry_point
  def transfer(self, params):
    sp.set_type(params, sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))))
    sp.verify(~ self.data.config.paused, 'FA2_PAUSED')
    sp.for transaction in params:
      sp.for tx in transaction.txs:
        sp.verify(self.data.assets.token_metadata.contains(tx.token_id), 'FA2_TOKEN_UNDEFINED')
        sp.verify((transaction.from_ == sp.sender) | (self.data.assets.operators.contains(sp.record(owner = transaction.from_, operator = sp.sender, token_id = tx.token_id))), 'FA2_NOT_OPERATOR')
        sp.if tx.amount > 0:
          sourceLedger = sp.local("sourceLedger", (transaction.from_, tx.token_id))
          recipientLedger = sp.local("recipientLedger", (tx.to_, tx.token_id))
          sp.verify(self.data.assets.ledger[sourceLedger.value].balance >= tx.amount, 'FA2_INSUFFICIENT_BALANCE')
          newBalance = sp.local("newBalance", self.data.assets.ledger[sourceLedger.value].balance - tx.amount)
          self.data.assets.ledger[sourceLedger.value].balance = sp.as_nat(newBalance.value)
          sp.if self.data.assets.ledger.contains(recipientLedger.value):
            self.data.assets.ledger[recipientLedger.value].balance += tx.amount
          sp.else:
            self.data.assets.ledger[recipientLedger.value] = sp.record(balance = tx.amount)

  @sp.entry_point
  def mint(self, params):
    sp.set_type(params.tokenId, sp.TNat)
    sp.set_type(params.address, sp.TAddress)
    sp.set_type(params.amount, sp.TNat)
    sp.set_type(params.metadata, sp.TMap(sp.TString, sp.TBytes))
    sp.verify(sp.sender == self.data.config.admin, 'FA2_NOT_ADMIN')
    user = sp.local("user", (params.address, params.tokenId))
    sp.if self.data.assets.ledger.contains(user.value):
      self.data.assets.ledger[user.value].balance += params.amount
    sp.else:
      self.data.assets.ledger[user.value] = sp.record(balance = params.amount)
    sp.if ~ (self.data.assets.token_metadata.contains(params.tokenId)):
      self.data.assets.token_metadata[params.tokenId] = sp.record(token_id = params.tokenId, token_info = params.metadata)
    self.data.assets.token_total_supply[params.tokenId] = self.data.assets.token_total_supply.get(params.tokenId, default_value = 0) + params.amount

  @sp.entry_point
  def update_operators(self, params):
    sp.set_type(params, sp.TList(sp.TVariant(add_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id"))), remove_operator = sp.TRecord(operator = sp.TAddress, owner = sp.TAddress, token_id = sp.TNat).layout(("owner", ("operator", "token_id")))).layout(("add_operator", "remove_operator"))))
    sp.verify(~ self.data.config.paused, 'FA2_PAUSED')
    sp.for update in params:
      with update.match_cases() as arg:
        with arg.match('add_operator') as add_operator:
          sp.verify(add_operator.owner == sp.sender, 'FA2_NOT_ADMIN_OR_OWNER')
          self.data.assets.operators[add_operator] = sp.unit
        with arg.match('remove_operator') as remove_operator:
          sp.verify(remove_operator.owner == sp.sender, 'FA2_NOT_ADMIN_OR_OWNER')
          del self.data.assets.operators[remove_operator]


  @sp.entry_point
  def balance_of(self, params):
    sp.set_type(params, sp.TRecord(callback = sp.TContract(sp.TList(sp.TRecord(balance = sp.TNat, request = sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id"))).layout(("request", "balance")))), requests = sp.TList(sp.TRecord(owner = sp.TAddress, token_id = sp.TNat).layout(("owner", "token_id")))).layout(("requests", "callback")))
    sp.verify(~ self.data.config.paused, 'FA2_PAUSED')
    responses = sp.local("responses", sp.list([]))
    sp.for request in params.requests:
      sp.verify(self.data.assets.token_metadata.contains(request.token_id), 'FA2_TOKEN_UNDEFINED')
      sp.if self.data.assets.ledger.contains((request.owner, request.token_id)):
        responses.value.push(sp.record(request = request, balance = self.data.assets.ledger[(request.owner, request.token_id)].balance))
      sp.else:
        responses.value.push(sp.record(request = request, balance = 0))
    sp.transfer(responses.value, sp.tez(0), params.callback)

  @sp.entry_point
  def pause(self, params):
    sp.set_type(params, sp.TBool)
    self.data.config.paused = params

  @sp.entry_point
  def set_admin(self, params):
    sp.set_type(params, sp.TAddress)
    self.data.config.admin = params

  @sp.entry_point
  def update_metadata(self, params):
    sp.set_type(params, sp.TMap(sp.TString, sp.TBytes))
    sp.for entry in params.items():
      self.data.metadata[entry.key] = entry.value

sp.add_compilation_target("test", Contract())