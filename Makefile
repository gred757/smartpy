# Copyright 2019-2020 Smart Chain Arena LLC.

LANG := C
SOURCES := $(wildcard ./smartML/**/*.ml) $(wildcard ./smartML/**/*.mli)

# Recursive wildcard method
rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

build: .phony
	@$(MAKE) -s _build/local-tezos-binaries.ok
	@$(MAKE) -s _build/zcash.ok
	@$(MAKE) -s _build/smartML.ok

all: .phony
	@$(MAKE) -s build
	@$(MAKE) -s manual
	@$(MAKE) -s test-common
	@git status --short
	@$(MAKE) test-mockup-only-warn

full: .phony test manual doc

clean: .phony
	rm -rf _build
	rm -rf env/naked/switches
	rm -rf ext/tezos/tezos-master/_opam

test: .phony test-common test-mockup
	git status --short

test-quick: .phony test-common test-mockup-only-warn
	git status --short

test-common: .phony \
  test-scenario-ml \
  test-scenario-ts \
  test-scenario \
  test-scenario-michel \
  test-compile \
  test-compile-ml \
  test-compile-ts

touch_ok=@mkdir -p $(@D); touch $@

########################################################################
# SmartML

DUNE_TARGETS := \
  smartML/web_js/smartmljs.js \
  smartML/smartpyc/smartpyc_js.bc.js \
  smartML/smartpyc/smartpyc_unix.exe \
  smartML/ppx_smartml/driver.exe

_build/smartML.ok: $(SOURCES) $(wildcard ./smartML/**/dune) $(wildcard ./smartML/**/.) $(wildcard ./smartML/**/*.opam)
	dune build $(DUNE_TARGETS)
	cp _build/default/smartML/web_js/smartmljs.js _build/default/smartML/web_js/smartmljs.bc.js
	smartML/smartpyc/stitch_smartpyc_js
	dune build @install
	dune install 2> /dev/null
	ocamlfind ocamlmktop -o _build/smarttop.exe -package num,utils_pure,smartML -linkpkg
	ocamlformat --check $(SOURCES)
	$(touch_ok)

########################################################################
# Documentation

doc: _build/doc.ok .phony

_build/doc.ok:
	dune build @doc
	@echo -e 'SmartML doc:\n  open _build/default/_doc/_html/smartml/index.html'

manual: _build/manual.ok .phony

_build/manual.ok: doc/*.md doc/*.css packages/doc/docs/**/*.md packages/doc/docs/**/*.mdx scripts/build_completion.py _build/packages/deps.ok
	@mkdir -p packages/frontend/build
	@echo "asciidoctor ..."
	@asciidoctor doc/reference.md      -a toc=left -a linkcss -a stylesheet=reference.css --destination-dir packages/frontend/public -d book
	@asciidoctor doc/releases.md       -a toc=left -a linkcss --destination-dir packages/frontend/public
	@python3 scripts/build_completion.py
	@./wrapper --silent _build/packages/frontend/build.log.txt npm run prettier:write 'build/completers.ts' --prefix packages/frontend
	@cp packages/frontend/build/completers.ts packages/frontend/src/features/editor/language/completers/python.ts
	$(touch_ok)

########################################################################
# Versioning

patch-version:
	./scripts/bump-version.sh patch

minor-version:
	./scripts/bump-version.sh minor

major-version:
	./scripts/bump-version.sh major

with-rev-version: export WITH_REV := true
with-rev-version:
	./scripts/bump-version.sh

custom-version:
	./scripts/bump-version.sh $(VERSION)

########################################################################
# Frontend and its tests

packages-deps: .phony _build/packages/deps.ok

_build/packages/deps.ok: packages/package.json $(wildcard packages/**/**/package.json)
	./wrapper --silent _build/packages/deps.log.txt npm install --prefix packages
	./wrapper --silent _build/packages/deps.log.txt npm run bootstrap --prefix packages
	@mkdir -p packages/doc/build
	$(touch_ok)

packages-build: _build/packages/build.ok

_build/packages/build.ok: .phony _build/packages/deps.ok packages/package.json _build/smartML.ok $(wildcard packages/**/**/*.js) $(wildcard packages/**/**/*.ts*) _build/manual.ok
	$(MAKE) -s update-ts-cli
	./wrapper --silent _build/packages/check.log.txt npm run prettier:check --prefix packages/frontend
	./wrapper --silent _build/packages/build.log.txt npm run build --prefix packages
	$(touch_ok)

packages-test: .phony _build/packages/test.ok

_build/packages/test.ok: _build/packages/deps.ok packages/package.json _build/smartML.ok $(wildcard packages/**/**/*.js) $(wildcard packages/**/**/*.ts*)
	npm run test --prefix packages
	$(touch_ok)

frontend-start: .phony _build/packages/deps.ok
	npm run start-web --prefix packages

doc-start: .phony _build/packages/deps.ok
	npm run start-doc --prefix packages

packages-ci-test: .phony _build/packages/deps.ok _build/packages/ts_cli.ok
	./wrapper --silent _build/packages/ci-test.log.txt npm run ci-test --prefix packages

packages-fmt: .phony
	npm run prettier:fix --prefix packages

update-ts-cli: .phony _build/packages/ts_cli.ok

_build/packages/ts_cli.ok: _build/packages/deps.ok $(call rwildcard,packages/packages/ts-syntax/src,*.ts) $(call rwildcard,packages/packages/ts-cli/src,*.ts) _build/manual.ok
	npm run build --prefix packages/packages/ts-syntax
	npm install --prefix packages/packages/ts-cli
	npm run build --prefix packages/packages/ts-cli
	$(touch_ok)

update-ts-snapshots: .phony update-ts-cli
	CI=true npm run test --prefix packages/packages/ts-syntax -- --no-watch --all -u

########################################################################
# SmartML tests

NOT_TEMPLATES := python/templates/state_channel_games/game_platform.py python/templates/state_channel_games/model_wrap.py python/templates/state_channel_games/types.py

TEMPLATES := $(filter-out $(NOT_TEMPLATES), $(wildcard python/templates/*.py python/templates/*/*.py python/templates/*/*/*.py))
TEMPLATES_OK := $(TEMPLATES:python/templates/%.py=%.ok)
TEMPLATES_ML := $(wildcard ml_templates/*.ml)
TEMPLATES_ML_OK := $(TEMPLATES_ML:ml_templates/%.ml=%.ok)
TEMPLATES_TS := $(wildcard ts_templates/*.ts)
TEMPLATES_TS_OK := $(TEMPLATES_TS:ts_templates/%.ts=%.ok)

FLAG_HTML=$(if $(filter $(addprefix python/templates/, welcome.py stateChannels.py),$<), --html,)

FLAG_NATIVE=$(if $(filter $(addprefix python/templates/, FA1.2.py	\
  FA2.py bakingSwap.py bls12381.py testHashFunctions.py bls12_381.py	\
  int_bls12_381_fr.py neg_bls12_381_g1.py neg_bls12_381_g2.py		\
  neg_bls12_381_fr.py add_bls12_381_g1.py add_bls12_381_g2.py		\
  add_bls12_381_fr.py mul_bls12_381_g1_fr.py mul_bls12_381_g2_fr.py	\
  mul_bls12_381_fr_fr.py mul_bls12_381_int_fr.py			\
  pairing_check_bls12_381.py bls12_381_conv.py voting_power.py		\
  test_pack.py sapling2.py stateChannels.py testCheckSignature.py	\
  test_eval_order.py price_feed_multisign_admin.py			\
  state_channel_games/tests/test_game_platform_tictactoe.py		\
  state_channel_games/tests/test_game_platform_transfers.py		\
  state_channel_games/tests/test_game_platform_adversarial.py		\
  state_channel_games/tests/test_game_platform_admin.py			\
  state_channel_games/tests/test_game_platform_chess.py  \
  state_channel_games/scenarios/scenario_alice.py			\
  state_channel_games/scenarios/scenario_bob.py timelock.py admin_multisig.py),$<),,	\
  --native)

FLAG_DECOMPILE=$(if $(filter $(addprefix python/templates/, FA1.2.py	\
  FA2.py atomicSwap.py bakingSwap.py collatz.py testCheckSignature.py	\
  testFor.py testTimestamp.py testVariant.py fibonacci.py		\
  check_dfs.py price_feed.py oracle.py inlineMichelson.py		\
  constants.py onchain_views.py timelock.py test_eval_order.py		\
  test_onchain_views.py admin_multisig.py ), $<), --no-decompile, --decompile)

FLAG_DECOMPILE_ML=$(if $(filter $(addprefix ml_templates/, py_FA2.ml	\
  py_atomicSwap.ml py_bakingSwap.ml py_check_dfs.ml py_collatz.ml	\
  py_fibonacci.ml), $<), --no-decompile, --decompile)

FLAGS_MICHEL=$(if $(filter $(addprefix python/templates/, constants.py onchain_views.py test_eval_order.py test_onchain_views.py admin_multisig.py), $<), , --simplify-via-michel --dump-michel)

FLAG_PROTOCOL=$(if $(filter $(addprefix python/templates/, test_baker_hash.py), $<), --protocol baking_accounts, --protocol hangzhou)

FLAG_LAYOUT=$(if $(filter $(addprefix python/templates/, ), $<),, --default_record_layout comb)

FLAGS_EXTRA ?=

FLAGS_SCENARIO=$(FLAG_HTML) $(FLAG_NATIVE) $(FLAG_LAYOUT) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_SCENARIO_ML=$(FLAG_HTML) $(FLAG_NATIVE) $(FLAG_LAYOUT) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_SCENARIO_TS=$(FLAG_HTML) $(FLAG_LAYOUT) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_SCENARIO_MICHEL=$(FLAGS_SCENARIO) $(FLAGS_MICHEL) $(FLAGS_EXTRA) --accept_empty

FLAGS_MOCKUP=$(FLAG_PROTOCOL) $(FLAGS_EXTRA) --accept_empty

FLAGS_COMPILE=$(FLAG_PROTOCOL) $(FLAG_DECOMPILE) $(FLAG_NATIVE) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_COMPILE_ML=$(FLAG_PROTOCOL) $(FLAG_DECOMPILE_ML) $(FLAG_NATIVE) $(FLAGS_EXTRA) --accept_empty --languages "SmartPy,SmartML"

FLAGS_COMPILE_TS=$(FLAG_PROTOCOL) $(FLAG_NATIVE) $(FLAGS_EXTRA) --accept_empty

TEST_SCENARIO := $(addprefix _build/test/scenario/,            \
  $(filter-out __init__.ok, $(TEMPLATES_OK)))

TEST_SCENARIO_ML := $(addprefix _build/test/scenario_ml/, $(TEMPLATES_ML_OK))

TEST_SCENARIO_TS := $(addprefix _build/test/scenario_ts/, $(TEMPLATES_TS_OK))

_build/test/scenario/%.ok: python/templates/%.py _build/smartML.ok python/smartpy.py
	@./wrapper _build/test/scenario/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario/$* --purge $(FLAGS_SCENARIO)
	$(touch_ok)

test-scenario: $(TEST_SCENARIO) .phony
	@scripts/remove_unused.sh test_baselines/scenario $(TEST_SCENARIO)


TEST_SCENARIO_MICHEL := $(addprefix _build/test/scenario_michel/,	\
  $(filter-out __init__.ok \
     state_channel_games/tests/test_game_platform_transfers.ok \
     state_channel_games/tests/test_game_platform_chess.ok \
     state_channel_games/tests/test_game_platform_tictactoe.ok \
     state_channel_games/tests/test_game_platform_adversarial.ok \
     state_channel_games/tests/test_game_platform_admin.ok \
     state_channel_games/scenarios/scenario_alice.ok \
     state_channel_games/scenarios/scenario_bob.ok \
   , $(TEMPLATES_OK)))

_build/test/scenario_michel/%.ok: python/templates/%.py _build/smartML.ok python/smartpy.py
	@./wrapper _build/test/scenario_michel/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario_michel/$* --purge $(FLAGS_SCENARIO_MICHEL)
	$(touch_ok)

test-scenario-michel: $(TEST_SCENARIO_MICHEL) .phony
	@scripts/remove_unused.sh test_baselines/scenario_michel $(TEST_SCENARIO_MICHEL)


TEST_MOCKUP := $(addprefix test_baselines/mockup/, $(filter-out	\
  __init__.tsv test_expression_compilation.tsv test_michelson_error.tsv chess.tsv state_channel_games/models/chess.tsv, $(TEMPLATES_OK:%.ok=%.tsv)))

test_baselines/mockup/%.tsv: python/templates/%.py _build/local-tezos-binaries.ok _build/zcash.ok _build/smartML.ok
	@rm -rf _build/test/mockup/$*/ test_baselines/mockup/$*.tsv
	@./wrapper --silent _build/test/mockup/$*.log.txt smartpy-cli/SmartPy.sh test $< _build/test/mockup/$* --mockup --purge $(FLAGS_MOCKUP)
	@mkdir -p test_baselines/mockup/$(dir $*)
	@ls _build/test/mockup/$*/mockup/*/result.tsv | xargs cat > test_baselines/mockup/$*.tsv

test-mockup: $(TEST_MOCKUP) .phony

test-mockup-only-warn:
	@for i in $(TEST_MOCKUP); do \
	  test -f $$i || (echo $$i does not exist; false) ; \
	done

test-mockup-only-new:
	$(MAKE) $(filter-out $(wildcard test_baselines/mockup/*.tsv), $(TEST_MOCKUP))


TEST_COMPILE := $(addprefix _build/test/compile/, $(filter-out	\
  __init__.ok, $(TEMPLATES_OK)))

TEST_COMPILE_ML := $(addprefix _build/test/compile_ml/, $(TEMPLATES_ML_OK))

TEST_COMPILE_TS := $(addprefix _build/test/compile_ts/, $(TEMPLATES_TS_OK))

_build/test/compile/%.ok: python/templates/%.py _build/smartML.ok python/smartpy.py
	@./wrapper _build/test/compile/$*.log.txt smartpy-cli/SmartPy.sh compile $< test_baselines/compile/$* --purge $(FLAGS_COMPILE)
	$(touch_ok)

_build/test/compile_ml/%.ok: ml_templates/%.ml _build/smartML.ok
	@./wrapper _build/test/compile_ml/$*.log.txt smartpy-cli/SmartPy.sh compile $< test_baselines/compile_ml/$* --purge $(FLAGS_COMPILE_ML)
	$(touch_ok)

_build/test/compile_ts/%.ok: ts_templates/%.ts _build/smartML.ok _build/packages/ts_cli.ok
	@./wrapper _build/test/compile_ts/$*.log.txt smartpy-cli/SmartPy.sh compile $< test_baselines/compile_ts/$* --purge $(FLAGS_COMPILE_TS)
	$(touch_ok)

test-compile: $(TEST_COMPILE) .phony
	@scripts/remove_unused.sh test_baselines/compile $(TEST_COMPILE)

test-compile-ml: $(TEST_COMPILE_ML) .phony
	@scripts/remove_unused.sh test_baselines/compile_ml $(TEST_COMPILE_ML)

test-compile-ts: $(TEST_COMPILE_TS) .phony
	@scripts/remove_unused.sh test_baselines/compile_ts $(TEST_COMPILE_TS)

_build/test/scenario_ml/%.ok: ml_templates/%.ml _build/smartML.ok
	@./wrapper _build/test/scenario_ml/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario_ml/$* --purge $(FLAGS_SCENARIO_ML)
	$(touch_ok)

_build/test/scenario_ts/%.ok: ts_templates/%.ts _build/smartML.ok _build/packages/ts_cli.ok
	@./wrapper _build/test/scenario_ts/$*.log.txt smartpy-cli/SmartPy.sh test $< test_baselines/scenario_ts/$* --purge $(FLAGS_SCENARIO_TS)
	$(touch_ok)

test-scenario-ml: $(TEST_SCENARIO_ML) .phony
	@scripts/remove_unused.sh test_baselines/scenario_ml $(TEST_SCENARIO_ML)

test-scenario-ts: $(TEST_SCENARIO_TS) .phony
	@scripts/remove_unused.sh test_baselines/scenario_ts $(TEST_SCENARIO_TS)

test_baselines/scenario/%: .phony
	@$(MAKE) -s _build/test/scenario/$*.ok

test_baselines/scenario_michel/%: .phony
	@$(MAKE) -s _build/test/scenario_michel/$*.ok

test_baselines/scenario_ml/%: .phony
	@$(MAKE) -s _build/test/scenario_ml/$*.ok

test_baselines/scenario_ts/%: .phony
	@$(MAKE) -s _build/test/scenario_ts/$*.ok

test_baselines/compile/%: .phony
	@$(MAKE) -s _build/test/compile/$*.ok

test_baselines/compile_ml/%: .phony
	@$(MAKE) -s _build/test/compile_ml/$*.ok

test_baselines/compile_ts/%: .phony
	@$(MAKE) -s _build/test/compile_ts/$*.ok

########################################################################
# Local Tezos build

LOCAL_TEZOS_TARGETS=\
  src/bin_sandbox/main.exe \
  src/bin_client/main_client.exe \
  src/bin_client/main_admin.exe \
  src/bin_node/main.exe

_build/local-tezos-binaries.ok: scripts/fetch_tezos_binaries
	scripts/fetch_tezos_binaries
	$(touch_ok)

_build/zcash.ok:
	mkdir -p ~/.zcash-params
	cd ~/.zcash-params; \
	for f in sapling-output.params sapling-spend.params sprout-groth16.params; do \
	  [ -e $$f ] || curl -OL https://download.z.cash/downloads/$$f; \
	done
	@mkdir -p $(@D); touch $@


########################################################################
# Miscellaneous phony targets

fmt_check: .phony
	ocamlformat --check $(SOURCES)

fmt_fix: .phony
	ocamlformat --inplace $(SOURCES)

www: .phony
	(sleep 1; open http://localhost:$(PORT)) &
	cd packages/packages/build && python3 -m http.server $(PORT)

open-index: .phony
	open _build/default/_doc/_html/smartml/index.html

open-manual: .phony manual
	open packages/frontend/public/reference.html


########################################################################
# Inter-template dependencies

ml_templates/py_%.ml: _build/test/compile/%.ok
	@touch $@

ml_templates/ml_%.ml: _build/test/compile_ml/%.ok
	@touch $@

python/templates/py_%.py: _build/test/compile/%.ok
	@touch $@

python/templates/ml_%.py: _build/test/compile_ml/%.ok
	@touch $@

python/templates/chess.py: python/templates/chess_logic.py
	@touch $@


python/templates/state_channel_games/models/chess.py: \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py \
 python/templates/chess_logic.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_adversarial.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_transfers.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_tictactoe.py: \
$(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_chess.py: \
$(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py \
 python/templates/chess_logic.py
	@touch $@

python/templates/state_channel_games/tests/test_game_platform_admin.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_platform.py
	@touch $@


python/templates/state_channel_games/tests/test_game_tester.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_tester.py
	@touch $@

python/templates/state_channel_games/scenarios/scenario_alice.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_tester.py
	@touch $@

python/templates/state_channel_games/scenarios/scenario_bob.py: \
 $(wildcard python/templates/state_channel_games/models/*.py) \
 python/templates/state_channel_games/model_wrap.py \
 python/templates/state_channel_games/types.py \
 python/templates/state_channel_games/game_tester.py
	@touch $@

#########################################################################
# Bundling

_build/cli.ok: _build/smartML.ok _build/packages/ts_cli.ok
	rm -rf _build/cli/
	mkdir -p _build/cli/
	tar czf _build/cli/smartpy-cli.tar.gz \
    --dereference \
    --owner=0 \
    --group=0 \
    -C ./smartpy-cli \
    --files-from scripts/smartpy-cli.files
	tar czf _build/cli/smartML.tar.gz \
    --dereference \
    --owner=0 \
    --group=0 \
    --files-from scripts/smartML.files
	$(touch_ok)

_build/cli-test.ok: _build/cli.ok
	./smartpy-cli/install.sh --from file://$(shell pwd)/_build/cli/ --prefix _build/test/cli-install/ --yes --with-smartml
	@rm -rf _build/test/cli-run/
	_build/test/cli-install/SmartPy.sh test python/templates/welcome.py _build/test/cli-run/welcome_py
	_build/test/cli-install/SmartPy.sh test ml_templates/welcome.ml _build/test/cli-run/welcome_ml
	$(touch_ok)

########################################################################
# Makefile and environment plumbing

.phony:

.PHONY: .phony


ifeq (,$(wildcard env/current))
  $(error Please run './env/nix/init' or './env/naked/init' before running make)
endif

ifndef SMARTPY_ENV
  ifeq ($(shell readlink env/current),nix/)
    $(warning Initializing a new nix shell for each command. Run 'make' under './envsh' or directly as './with_env make' for better performance.)
  endif
  SHELL=./envsh
endif

MAKEFLAGS += --warn-undefined-variables --no-builtin-rules --output-sync

# Disable further implicit rules, resulting in a slight speed-up:
.SUFFIXES:
